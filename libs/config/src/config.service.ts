import {Injectable, Logger} from '@nestjs/common';
import {get, isUndefined} from 'lodash';
import {environment} from '@app/environment';

// tslint:disable-next-line
const packageJson = require('../../../package.json');

@Injectable()
export class ConfigService {
  private readonly logger = new Logger(ConfigService.name);
  private readonly config = environment;
  private static configStatic = environment;

  constructor() {
    this.logger.log('Is Production: ' + environment.production);
  }

  get<T = any>(propertyPath: string): T | undefined;

  get<T = any>(propertyPath: string, defaultValue: T): T;

  get<T = any>(propertyPath: string, defaultValue?: T): T | undefined {
    const processValue = get(this.config, propertyPath);
    return isUndefined(processValue) ? defaultValue : processValue;
  }

  static get<T = any>(propertyPath: string, defaultValue?: T): T | undefined {
    const processValue = get(this.configStatic, propertyPath);
    return isUndefined(processValue) ? defaultValue : processValue;
  }

  getVersion(): string {
    if (!process.env.APP_VERSION) {
      if (packageJson && packageJson.version) {
        process.env.APP_VERSION = packageJson.version;
      }
    }
    return process.env.APP_VERSION;
  }

  isProd(): boolean {
    return this.config.production;
  }

  getAllowWhitelist(): string[] {
    return this.config.ALLOW_WHITE_LIST ? this.config.ALLOW_WHITE_LIST : [];
  }


}
