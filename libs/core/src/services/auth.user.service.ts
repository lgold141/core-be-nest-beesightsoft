import {Injectable, Inject} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {InsertResult} from 'typeorm';
import {CrudService} from '../crud';
import {UserEntity} from '../entities';
import {AbstractBaseRepository} from '../repositories';
import {DB_CONNECTION_DEFAULT} from '@app/database';

@Injectable()
export class AuthUserService extends CrudService<UserEntity> {
  constructor(@InjectRepository(UserEntity, DB_CONNECTION_DEFAULT) private readonly userRepository: AbstractBaseRepository<UserEntity>) {
    super(userRepository);
  }

  async createOne(user: UserEntity): Promise<InsertResult> {
    return await this.userRepository.insert(user);
  }

  async changePassword(id: string, hashedPassword: string): Promise<UserEntity> {
    const user = await this.findOne(id);
    user.password = hashedPassword;
    user.updatePasswordAt = new Date();
    return await this.userRepository.save(user);
  }


  async findByEmail(email: string) {
    return await this.userRepository.findOne({email});
  }

  async findByUsername(username: string) {
    return await this.userRepository.findOne({username});
  }

  async findOneByPropertyValue(options) {
    return await this.userRepository.findOne(options);
  }
}
