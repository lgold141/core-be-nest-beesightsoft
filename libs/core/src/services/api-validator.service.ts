import {validate} from "class-validator";
import {AbstractDto} from '@nest-core/core';
import {flatten, get} from 'lodash';
import {IApiErrorInterface, IApiValidator} from '../interfaces';
import {ApiErrorCodeService} from '@app/api-error-code';

export class ApiValidatorService implements IApiValidator {
  constructor() {
    console.log(['ApiValidatorService']);
  }

  async validateWith(request: AbstractDto): Promise<IApiErrorInterface[] | any> {
    const errors = await validate(request);
    // console.error(['ApiValidatorService.validateWith', request, errors]);
    const errorsCodeMapping = request.getErrorCodeMapping();
    const data = errors.map(error => {
      return Object.keys(error.constraints).map((type: string) => {
        return error.property + '.' + type;
      });
    }).map(types => {
      return types.map(type => {
        const errorCodeMapping = get(errorsCodeMapping, type);
        if (errorCodeMapping) {
          const [code, options] = errorCodeMapping;
          return ApiErrorCodeService.translate(code, options);
        } else {
          throw Error(`Missing error code mapping for ${JSON.stringify(type)}`);
        }
      });
    });
    return flatten(data);
  }
}