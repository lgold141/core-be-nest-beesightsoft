import {HttpException, HttpStatus} from '@nestjs/common';

export class EntityWriteException extends HttpException {
  constructor(objectOrError?: string | object | any,
              description = 'Entity write exception') {
    super(
      HttpException.createBody(
        objectOrError,
        description,
        HttpStatus.BAD_REQUEST,
      ),
      HttpStatus.BAD_REQUEST,
    );
  }
}
