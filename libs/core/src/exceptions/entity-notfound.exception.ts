import {HttpException, HttpStatus} from '@nestjs/common';

export class EntityNotfoundException extends HttpException {
  constructor(objectOrError?: string | object | any,
              description = 'Entity notfound exception') {
    super(
      HttpException.createBody(
        objectOrError,
        description,
        HttpStatus.NOT_FOUND,
      ),
      HttpStatus.NOT_FOUND,
    );
  }
}
