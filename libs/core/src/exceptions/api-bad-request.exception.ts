import {HttpException, HttpStatus} from '@nestjs/common';

export class ApiBadRequestException extends HttpException {
  constructor(objectOrError?: string | object | any,
              description = 'Api Bad Request') {
    super(
      HttpException.createBody(
        objectOrError,
        description,
        HttpStatus.BAD_REQUEST,
      ),
      HttpStatus.BAD_REQUEST,
    );
  }
}
