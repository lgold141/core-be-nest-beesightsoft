import {each} from 'lodash';
import {ruleToCodeValidation, SchemaCodeEnum} from '@app/api-error-code';
import {IApiErrorCodeMappingInterface, IDtoInterface} from '../interfaces';

export abstract class AbstractDto implements IDtoInterface {
  schemaCode?: SchemaCodeEnum;

  getErrorCodeMapping(): IApiErrorCodeMappingInterface | null {
    return ruleToCodeValidation[this.schemaCode] ? ruleToCodeValidation[this.schemaCode] : ruleToCodeValidation[SchemaCodeEnum.common];
  };
}
