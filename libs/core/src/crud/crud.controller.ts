import {Body, Delete, Get, HttpStatus, Param, Post, Put} from '@nestjs/common';
import {ApiOperation, ApiResponse} from '@nestjs/swagger';
import {DeleteResult, FindConditions, FindOneOptions} from 'typeorm';
import {UtilsService} from '@nest-core/shared';
import {IApiErrorInterface, ICrudControllerInterface, ICrudServiceInterface, IPaginationInterface} from '../interfaces';
import {AbstractEntity} from '../entities';
import {AbstractDto} from '../dto';
import {CRUD_OPTIONS_DTO_METADATA} from '../constants';
import {PaginationParams} from './pagination-params';
import {AbstractApiController} from '../controllers'
import {
  ApiBadRequestResponseExample,
  ApiForbidenResponseExample,
  ApiNotfoundResponseExample,
  ApiUnauthorizedResponseExample,
  R,
  SerializeHelper,
  SwaggerHelper
} from '../swagger';


@ApiResponse({
  status: HttpStatus.BAD_REQUEST,
  description: 'Invalid request params',
  type: ApiBadRequestResponseExample
})
@ApiResponse({
  status: HttpStatus.UNAUTHORIZED, description: 'Caller is not authenticated',
  type: ApiUnauthorizedResponseExample
})
@ApiResponse({
  status: HttpStatus.FORBIDDEN, description: 'Forbidden',
  type: ApiForbidenResponseExample
})
@ApiResponse({
  status: HttpStatus.NOT_FOUND,
  description: 'Resource not found',
  type: ApiNotfoundResponseExample
})

export abstract class CrudController<T extends AbstractEntity> extends AbstractApiController implements ICrudControllerInterface<T> {
  private target: any;
  private crudOptionsDto: any;

  protected constructor(private readonly crudService: ICrudServiceInterface<T>) {
    super();
    this.target = this.constructor;
    this.syncMetaData();
  }

  @ApiOperation({summary: 'Find all'})
  @Post('findAll')
  async findAll(@Body() filter: PaginationParams<T>): Promise<void> {
    let {items, total} = await this.crudService.findAll(filter);
    this.respondWithSuccess<IPaginationInterface<T | any>>({
      items: this.transformData(items),
      total
    } as IPaginationInterface<T>);
  }

  async findOne(id: string | number | FindOneOptions<T> | FindConditions<T>): Promise<void> {
    try {
      const data: T = await this.crudService.findOneOrFail(id);
      if (!data) {
        this.respondWithErrorResourceNotfound();
      }
      this.respondWithSuccess<T | any>(this.transformData(data));
    } catch (ex) {
      this.respondWithErrorResourceNotfound();
    }
  }

  @Get(':id')
  @ApiOperation({summary: 'Find one record by id'})
  async findById(@Param('id') id: string): Promise<void> {
    try {
      const data: T = await this.crudService.findOneOrFail(id);
      this.respondWithSuccess<T | any>(this.transformData(data));
    } catch (ex) {
      this.respondWithErrorResourceNotfound();
    }
  }

  @ApiOperation({summary: 'Create new record'})
  @Post()
  async create(@Body() entity: AbstractDto, ...options: any[]): Promise<void> {
    const errors: IApiErrorInterface[] = await this.validateWith(entity);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: T = await this.crudService.create(UtilsService.classToPlain(entity) as any);
      this.respondWithSuccess<T | any>(this.transformData(data));
    }
  }

  @ApiOperation({summary: 'Update an existing record'})
  @Put(':id')
  async update(@Param('id') id: string, @Body() entity: AbstractDto): Promise<void> {
    Object.assign(entity, {id});
    const errors: IApiErrorInterface[] = await this.validateWith(entity);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      // FIXME: https://github.com/typeorm/typeorm/issues/1544
      await this.crudService.update(id, UtilsService.classToPlain(entity) as any);
      const data: T = await this.crudService.findOneOrFail(id);
      this.respondWithSuccess<T | any>(this.transformData(data));
    }
  }

  @ApiOperation({summary: 'Delete a record'})
  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    const data: DeleteResult = await this.crudService.delete(id);
    this.respondWithSuccess<DeleteResult>(data)
  }

  private syncMetaData(): void {
    if (Reflect.hasMetadata(CRUD_OPTIONS_DTO_METADATA, this.target)) {
      this[CRUD_OPTIONS_DTO_METADATA] = Reflect.getMetadata(CRUD_OPTIONS_DTO_METADATA, this.target);
      this.crudOptionsDto = Reflect.getMetadata(CRUD_OPTIONS_DTO_METADATA, this.target);
    }
    this.bindingSwaggerResponse();
  }

  private transformData<T>(data: T | T[]): T | T[] {
    const {transformDto} = this.crudOptionsDto;
    return transformDto ? UtilsService.plainToClass(transformDto, data) : data;
  }

  private bindingSwaggerResponse() {
    const {transformDto, createDto, updateDto, searchDto, resourceName} = this.crudOptionsDto;
    if (transformDto) {
      const GetOneDto = {
        [HttpStatus.OK]: {
          description: 'OK',
          type: SerializeHelper.createGetOneDto(transformDto, resourceName)
        },
      };
      const GetManyDto = {
        [HttpStatus.OK]: {
          description: 'OK',
          type: SerializeHelper.createGetManyDto(transformDto, resourceName)
        },
      };
      SwaggerHelper.setResponseOk(GetOneDto, this['findById']);
      SwaggerHelper.setResponseOk(GetOneDto, this['create']);
      SwaggerHelper.setResponseOk(GetOneDto, this['update']);
      SwaggerHelper.setResponseOk(GetManyDto, this['findAll']);
    }
    if (createDto) {
      R.setRouteArgsTypes([createDto], this, 'create');
    }
    if (updateDto) {
      R.setRouteArgsTypes(['', updateDto], this, 'update');
    }
    if (searchDto) {
      R.setRouteArgsTypes([searchDto], this, 'findAll');
    }
  }
}
