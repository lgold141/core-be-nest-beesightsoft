import {Transform, Expose} from 'class-transformer';
import {IsOptional, Max, Min} from 'class-validator';
import {PaginationParamsInterface} from '../interfaces';
import {ApiProperty, OmitType} from '@nestjs/swagger';

export abstract class PaginationParams<T> implements PaginationParamsInterface<T> {
  /**
   * Pagination limit
   */
  @ApiProperty({example: 10})
  @IsOptional()
  @Transform((val: string) => parseInt(val, 10) || 0)
  @Expose()
  take?: number;

  /**
   * Pagination offset
   */
  @ApiProperty({example: 0})
  @IsOptional()
  @Transform((val: string) => parseInt(val, 10) || 0)
  @Expose()
  skip?: number;
}
