import {
  DeepPartial,
  DeleteResult,
  FindConditions,
  FindManyOptions,
  FindOneOptions,
  SaveOptions,
  UpdateResult
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { ICrudServiceInterface, IPaginationInterface } from '../interfaces';
import { AbstractEntity } from '../entities';
import { EntityNotfoundException, EntityWriteException } from '../exceptions';
import { AbstractBaseRepository } from '../repositories';


export abstract class CrudService<T extends AbstractEntity> implements ICrudServiceInterface<T> {
  protected constructor(protected readonly repository: AbstractBaseRepository<T>) {
  }

  public async count(filter?: FindManyOptions<T>): Promise<number> {
    return await this.repository.count(filter);
  }

  public async findAll(filter?: FindManyOptions<T>): Promise<IPaginationInterface<T>> {
    const total = await this.repository.count(filter);
    const items = await this.repository.find(filter);
    return { items, total };
  }

  public async findOneOrFail(id: string | number | FindOneOptions<T> | FindConditions<T>,
    options?: FindOneOptions<T>): Promise<T> {
    try {
      return await this.repository.findOneOrFail(
        id as any,
        options
      );
    } catch (err) {
      throw new EntityNotfoundException(err);
    }
  }

  public async findOne(id: string | number | FindOneOptions<T> | FindConditions<T>,
    options?: FindOneOptions<T>): Promise<T> {
    try {
      return await this.repository.findOne(id as any, options);
    } catch (err) {
      throw new EntityNotfoundException(err);
    }
  }


  public async create(entity: DeepPartial<T>, options?: SaveOptions): Promise<T> {
    const obj: T = this.repository.create(entity);
    try {
      // https://github.com/Microsoft/TypeScript/issues/21592
      return await this.repository.save(obj as any, options);
    } catch (err /*: WriteError*/) {
      throw new EntityWriteException(err);
    }
  }

  public async update(id: string | number | FindConditions<T>,
    partialEntity: QueryDeepPartialEntity<T>): Promise<UpdateResult> {
    try {
      return await this.repository.update(id, partialEntity);
    } catch (err /*: WriteError*/) {
      throw new EntityWriteException(err);
    }
  }

  public async delete(criteria: string | number | FindConditions<T>): Promise<DeleteResult> {
    try {
      return this.repository.delete(criteria);
    } catch (err) {
      throw new EntityNotfoundException(err);
    }
  }
}
