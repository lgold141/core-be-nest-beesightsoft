import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  UnauthorizedException,
  InternalServerErrorException
} from '@nestjs/common';
import {Request, Response} from 'express';
import {IApiResponseInterface} from '../interfaces';
import {ApiBadRequestException, EntityNotfoundException, EntityWriteException} from '../exceptions';
import {ApiErrorCodeService, CodeEnum} from '@app/api-error-code';
import {EntityNotFoundError} from 'typeorm/error/EntityNotFoundError';
import {QueryFailedError} from 'typeorm/error/QueryFailedError';
import {JsonWebTokenError} from 'jsonwebtoken';

@Catch(HttpException, EntityNotFoundError, QueryFailedError, JsonWebTokenError)
export class AllApiExceptionsFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const errorResponse: any =
      exception instanceof HttpException ?
        exception.getResponse()
        : 'INTERNAL_SERVER_ERROR';

    console.error([
      'AllExceptionsFilter',
      {
        exception,
        // ctx,
        // response,
        // request,
        status,
        errorResponse
      }
    ]);

    if (exception instanceof ApiBadRequestException) {
      response
      .status(HttpStatus.BAD_REQUEST)
      .json(errorResponse);
    } else if (exception instanceof JsonWebTokenError) {
      response
      .status(HttpStatus.BAD_REQUEST)
      .json({
          data: null,
          errors: [{
            code: CodeEnum.BAD_REQUEST,
            message: exception.message
          }]
        } as IApiResponseInterface<null>
      );
    } else if (exception instanceof EntityWriteException) {
      response
      .status(HttpStatus.BAD_REQUEST)
      .json({
          data: null,
          errors: [{
            code: CodeEnum.BAD_REQUEST,
            message: exception.message
          }]
        } as IApiResponseInterface<null>
      );
    } else if (exception instanceof QueryFailedError) {
      response
      .status(HttpStatus.BAD_REQUEST)
      .json({
          data: null,
          errors: [{
            code: CodeEnum.QUERY_FAILED_ERROR,
            message: exception.message
          }]
        } as IApiResponseInterface<null>
      );
    }
    else if (exception instanceof EntityNotfoundException || exception instanceof EntityNotFoundError) {
      response
      .status(HttpStatus.NOT_FOUND)
      .json({
          data: null,
          errors: [ApiErrorCodeService.translate(CodeEnum.NOT_FOUND)]
        } as IApiResponseInterface<null>
      );
    } else if (exception instanceof UnauthorizedException) {
      response
      .status(HttpStatus.UNAUTHORIZED)
      .json({
          data: null,
          errors: [ApiErrorCodeService.translate(CodeEnum.UNAUTHORIZED)]
        } as IApiResponseInterface<null>
      );
    }
    else if (exception instanceof InternalServerErrorException) {
      response
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({
          data: null,
          errors: [ApiErrorCodeService.translate(CodeEnum.INTERNAL_SERVER)]
        } as IApiResponseInterface<null>
      );
    } else {
      response
      .status(HttpStatus.BAD_REQUEST)
      .json({
          data: null,
          errors: [{
            code: CodeEnum.BAD_REQUEST,
            message: typeof errorResponse === 'string' ? errorResponse : errorResponse.message
          }]
        } as IApiResponseInterface<null>
      );
    }
  }
}
