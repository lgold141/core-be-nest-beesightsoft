import {FindManyOptions} from 'typeorm';

/**
 * Describes generic pagination params
 */
export interface PaginationParamsInterface<T> extends FindManyOptions<T> {
}
