export interface ITryRequestInterface {
	success: boolean;
	record?: any;
	error?: any;
}
