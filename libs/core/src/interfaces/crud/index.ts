export * from './crud-controller.interface';
export * from './crud-service.interface';
export * from './pagination.interface';
export * from './try-request.interface';
export * from './pagination-params.interface';

