import {DeepPartial, DeleteResult, FindConditions, FindManyOptions, FindOneOptions, SaveOptions} from 'typeorm';
import {QueryDeepPartialEntity} from 'typeorm/query-builder/QueryPartialEntity';
import {IPaginationInterface} from './pagination.interface';
import {AbstractDto} from '../../dto';
import {IApiResponseInterface} from '../../interfaces';
import {ApiBadRequestException} from '../../exceptions';
import {Response} from 'express';

export interface ICrudControllerInterface<T> {
  findAll(filter: FindManyOptions<T>): Promise<void>;

  findOne(id: string | number | FindOneOptions<T> | FindConditions<T>, options?: FindOneOptions<T>):  Promise<void>;

  create(entity: AbstractDto | DeepPartial<T>, options?: SaveOptions): Promise<void>;

  update(id: string | number | FindConditions<T>, entity: AbstractDto | QueryDeepPartialEntity<T>):  Promise<void>;

  delete(criteria: string | number | FindConditions<T>): Promise<void>;
}
