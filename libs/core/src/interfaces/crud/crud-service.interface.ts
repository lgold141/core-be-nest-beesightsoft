import {DeepPartial, DeleteResult, FindConditions, FindManyOptions, FindOneOptions, UpdateResult} from 'typeorm';
import {QueryDeepPartialEntity} from 'typeorm/query-builder/QueryPartialEntity';
import {IPaginationInterface} from './pagination.interface';
import {AbstractDto} from '../../dto';
import {Observable} from 'rxjs';

export interface ICrudServiceInterface<T> {
  findAll(filter?: FindManyOptions<T>): Promise<IPaginationInterface<T>>

  findOneOrFail(id: string | number | FindOneOptions<T> | FindConditions<T>, options?: FindOneOptions<T>): Promise<T>;

  findOne(id: string | number | FindOneOptions<T> | FindConditions<T>, options?: FindOneOptions<T>): Promise<T>;

  create(entity: AbstractDto | DeepPartial<T>, ...options: any[]): Promise<T>;

  update(id: any, entity: QueryDeepPartialEntity<T> | AbstractDto, ...options: any[]): Promise<UpdateResult | T>;

  delete(id: any, ...options: any[]): Promise<DeleteResult>;

}
