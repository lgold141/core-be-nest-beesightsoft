import {IDtoInterface} from './dto.interface';

export interface ICrudOptionsDtoInterface {
  resourceName: string,
  createDto: IDtoInterface,
  updateDto: IDtoInterface,
  transformDto: IDtoInterface,
  searchDto: IDtoInterface
}