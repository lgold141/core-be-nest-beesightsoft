import {AbstractDto} from '../../dto';
import {IApiErrorInterface} from './api-error.interface';

export interface IApiValidator {
  validateWith(request: AbstractDto): Promise<IApiErrorInterface[]>
}
