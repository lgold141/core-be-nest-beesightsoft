import {CodeEnum} from '@app/api-error-code';

export interface IApiServiceResponseInterface<T> {
  code?: CodeEnum | null,
  options?: any | null,
  data?: T | null
}