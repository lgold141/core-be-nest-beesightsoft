export interface IApiErrorInterface {
  code: number,
  message: string
}