import {IApiResponseInterface} from './api-response.interface';
import {IApiErrorInterface} from './api-error.interface';
import {ApiBadRequestException} from '../../exceptions';
import {CodeEnum} from '@app/api-error-code';
import {IApiServiceResponseInterface} from './api-service-response.interface';
import {Response} from 'express';

export interface IApiControllerInterface {

  respondWithSuccess<T>(data: T): void,

  respondWithErrors(errors: IApiErrorInterface[]): void,

  respondWithErrorCodeNumber(code: CodeEnum): void ,

  respondWithErrorCodeNumber(code: CodeEnum, options: any): void ,

  respondWithErrorsOrSuccess<T>(apiServiceResponse: IApiServiceResponseInterface<T>): void
}
