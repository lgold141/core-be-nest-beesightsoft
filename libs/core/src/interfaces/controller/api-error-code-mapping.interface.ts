type ErrorMapperType = [number, any];

export interface IApiErrorCodeMappingInterface {
  [key: string]: {
    [key: string]: ErrorMapperType
  }
}