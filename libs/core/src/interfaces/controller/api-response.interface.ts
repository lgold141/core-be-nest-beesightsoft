import {IApiErrorInterface} from './api-error.interface';

export interface  IApiResponseInterface<T> {
  data: T,
  errors: IApiErrorInterface[]
}


