import {CRUD_OPTIONS_DTO_METADATA} from '../constants';
import {ICrudOptionsDtoInterface} from '../interfaces';

export function CrudOptionsDtoDecorator(crudOptionsDto: ICrudOptionsDtoInterface): ClassDecorator {
  return (target: Function) => {
    Reflect.defineMetadata(CRUD_OPTIONS_DTO_METADATA, crudOptionsDto, target);
  };
}
