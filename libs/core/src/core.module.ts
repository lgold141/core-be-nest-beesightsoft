import {Global, MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {CoreBootstrapModule} from './core.bootstrap.module';
import {RequestContextMiddleware} from './context';

@Global()
@Module({
  imports : [
    CoreBootstrapModule,
  ]
})
export class CoreModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestContextMiddleware).forRoutes('*');
  }

}
