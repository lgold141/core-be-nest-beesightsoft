import {HttpException, HttpStatus} from '@nestjs/common';
import * as cls from 'cls-hooked';
import {UserEntity} from '../entities';
import {Request, Response} from 'express';

export class RequestContext {
  readonly id: number;
  request: Request;
  response: Response;

  constructor(request: Request, response: Response) {
    this.id = Math.random();
    this.request = request;
    this.response = response;
    // console.log(['RequestContext', {
    //   id: this.id
    // }]);
  }

  static currentRequestContext(): RequestContext {
    const session = cls.getNamespace(RequestContext.name);
    if (session && session.active) {
      return session.get(RequestContext.name);
    }

    return null;
  }

  static currentRequest(): Request {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      return requestContext.request;
    }

    return null;
  }

  static currentResponse(): Response {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      return requestContext.response;
    }

    return null;
  }

  static currentUser(throwError?: boolean): UserEntity {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      // tslint:disable-next-line
      const user: any = requestContext.request['user'];
      if (user) {
        return user;
      }
    }

    if (throwError) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    return null;
  }

  static currentToken(throwError?: boolean): any {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      // tslint:disable-next-line
      return requestContext.request['token'];
    }

    if (throwError) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
    return null;
  }
}
