import {Global, Module} from '@nestjs/common';
import {ImageEntity, ProfileEntity, UserEntity} from './entities';
import {IsEmailUniqueConstraint, IsUsernameUniqueConstraint} from './validator';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DatabaseModule, DB_CONNECTION_DEFAULT} from '@app/database';
// export function isClass(obj) {
//   return !!obj.prototype && !!obj.prototype.constructor.name;
// }
//
// function requireAllClasses(rc) {
//   return rc
//   .keys()
//   .flatMap(key => Object.values(rc(key)))
//   .filter(isClass);
// }
// const requireContext = (require as any).context('../..', true, /\.entity.ts/);
// const entities = requireAllClasses(requireContext);
// const migrationsRequireContext = (require as any).context('../../../migrations/', true, /\.ts/);
// const migrations = requireAllClasses(migrationsRequireContext);
// TODO: above `require.context` is not compatible with `Jest`, manually add all entities as workaround
// const entities = [UserEntity, ImageEntity, ProfileEntity];
// @Module({
//   imports: [
//     ConfigModule,
//     // EmailModule.forRoot(),
//     TypeOrmModule.forRootAsync({
//       imports: [ConfigModule],
//       useFactory: (config: ConfigService): TypeOrmModuleOptions => ({
//         ...config.get('database'),
//         entities,
//         // subscribers,
//         // migrations,
//       }),
//       inject: [ConfigService],
//     }),
//   ],
//   controllers: [],
//   providers: [
//     // Enable for debugging in Dev env.
//     // {
//     //   provide: APP_INTERCEPTOR,
//     //   useClass: LoggingInterceptor,
//     // },
//     // {
//     //   provide: APP_INTERCEPTOR,
//     //   useClass: TransformInterceptor,
//     // },
//   ],
// })
import {AuthUserService} from './services';

const entities = [
  ImageEntity, ProfileEntity, UserEntity
];

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature(
      entities,
      DB_CONNECTION_DEFAULT),
  ],
  providers: [
    AuthUserService,
    IsEmailUniqueConstraint,
    IsUsernameUniqueConstraint
  ],
  exports: [
    AuthUserService,
    IsEmailUniqueConstraint,
    IsUsernameUniqueConstraint
  ]
})
export class CoreBootstrapModule {
}
