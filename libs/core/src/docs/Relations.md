#Relations
## What are relations
Relations helps you to work with related entities easily. There are several types of relations:

* one-to-one using @OneToOne
* many-to-one using @ManyToOne
* one-to-many using @OneToMany
* many-to-many using @ManyToMany
## Relation options
There are several options you can specify for relations:

* eager: boolean - If set to true, the relation will always be loaded with the main entity when using find* methods or QueryBuilder on this entity
* cascade: boolean | ("insert" | "update")[] - If set to true, the related object will be inserted and updated in the database. You can also specify an array of cascade options.
* onDelete: "RESTRICT"|"CASCADE"|"SET NULL" - specifies how foreign key should behave when referenced object is deleted
* primary: boolean - Indicates whether this relation's column will be a primary column or not.
* nullable: boolean - Indicates whether this relation's column is nullable or not. By default it is nullable.

## Cascades
Cascades example:
* 1 Category can has many Questions
* 1 Question can belong to many Categories
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";


@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToMany(type => Question, question => question.categories)
    questions: Question[];

}

import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
@Entity()
export class Question {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToMany(type => Category, category => category.questions, {
        cascade: true
    })
    @JoinTable()
    categories: Category[];

}
```
## Cascade Options
For example:
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
@Entity(Post)
export class Post {

    @PrimaryGeneratedColumn()
    id: number;

    // Full cascades on categories.
    @ManyToMany(type => PostCategory, {
        cascade: true
    })
    @JoinTable()
    categories: PostCategory[];

    // Cascade insert here means if there is a new PostDetails instance set
    // on this relation, it will be inserted automatically to the db when you save this Post entity
    @ManyToMany(type => PostDetails, details => details.posts, {
        cascade: ["insert"]
    })
    @JoinTable()
    details: PostDetails[];

    // Cascade update here means if there are changes to an existing PostImage, it
    // will be updated automatically to the db when you save this Post entity
    @ManyToMany(type => PostImage, image => image.posts, {
        cascade: ["update"]
    })
    @JoinTable()
    images: PostImage[];

    // Cascade insert & update here means if there are new PostInformation instances
    // or an update to an existing one, they will be automatically inserted or updated
    // when you save this Post entity
    @ManyToMany(type => PostInformation, information => information.posts, {
        cascade: ["insert", "update"]
    })
    @JoinTable()
    informations: PostInformation[];
}
```

## @JoinColumn options
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne , JoinColumn} from "typeorm";
@Entity(Post)
export class Post {

    //@JoinColumn, it automatically creates a column in the database named propertyName + referencedColumnName
    // This code will automatically create a categoryId column in the database.
    @ManyToOne(type => Category)
    @JoinColumn() // this decorator is optional for @ManyToOne, but required for @OneToOne
    category: Category;
    
    
   //specify a custom join column name
   @ManyToOne(type => Category)
   @JoinColumn({ name: "cat_id" })
   category: Category;
   
    //specify a reference join column name
   @ManyToOne(type => Category)
   @JoinColumn({ referencedColumnName: "name" })
   category: Category;
   
   //join multiple columns
   @ManyToOne(type => Category)
   @JoinColumn([
       { name: "category_id", referencedColumnName: "id" },
       { name: "locale_id", referencedColumnName: "locale_id" }
   ])
   category: Category;
```

## @JoinTable options
```typescript
import {Entity, ManyToMany, JoinTable} from "typeorm";
@Entity(Question)
export class Question {
    @ManyToMany(type => Category)
    @JoinTable({
        name: "question_categories", // table name for the junction table of this relation
        joinColumn: {
            name: "question",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "category",
            referencedColumnName: "id"
        }
    })
    categories: Category[];
}
```

#One-to-one relations

* Uni-directional are relations with a relation decorator only on one side.
```typescript
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";

@Entity()
export class Profile {

    @PrimaryGeneratedColumn()
    id: number;
}


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => Profile)
    @JoinColumn()
    profile: Profile;

}
This example will produce following tables:

+-------------+--------------+----------------------------+
|                        profile                          |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| gender      | varchar(255) |                            |
| photo       | varchar(255) |                            |
+-------------+--------------+----------------------------+

+-------------+--------------+----------------------------+
|                          user                           |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| name        | varchar(255) |                            |
| profileId   | int(11)      | FOREIGN KEY                |
+-------------+--------------+----------------------------+
```
* Bi-directional are relations with decorators on both sides of a relation.
```typescript
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";

@Entity()
export class Profile {

    @PrimaryGeneratedColumn()
    id: number;
    
    @OneToOne(type => User, user => user.profile) // specify inverse side as a second parameter
    user: User;
}


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => Profile, profile => profile.user) // specify inverse side as a second parameter
    @JoinColumn()
    profile: Profile;

}
```

# Many-to-one / one-to-many relations
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";


@Entity()
export class Photo {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    url: string;

    @ManyToOne(type => User, user => user.photos)
    user: User;

}


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => Photo, photo => photo.user)
    photos: Photo[];
}

This example will produce following tables:

+-------------+--------------+----------------------------+
|                         photo                           |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| url         | varchar(255) |                            |
| userId      | int(11)      | FOREIGN KEY                |
+-------------+--------------+----------------------------+

+-------------+--------------+----------------------------+
|                          user                           |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| name        | varchar(255) |                            |
```

# Many-to-many relations
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";

@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

}


@Entity()
export class Question {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    text: string;

    @ManyToMany(type => Category)
    @JoinTable()
    categories: Category[];

}

This example will produce following tables:

+-------------+--------------+----------------------------+
|                        category                         |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| name        | varchar(255) |                            |
+-------------+--------------+----------------------------+

+-------------+--------------+----------------------------+
|                        question                         |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| title       | varchar(255) |                            |
+-------------+--------------+----------------------------+

+-------------+--------------+----------------------------+
|              question_categories_category               |
+-------------+--------------+----------------------------+
| questionId  | int(11)      | PRIMARY KEY FOREIGN KEY    |
| categoryId  | int(11)      | PRIMARY KEY FOREIGN KEY    |
+-------------+--------------+----------------------------+
```

# Eager relations
Eager relations are loaded automatically each time you load entities from the database
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";


@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(type => Question, question => question.categories)
    questions: Question[];

}
@Entity()
export class Question {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    text: string;

    @ManyToMany(type => Category, category => category.questions, {
        eager: true
    })
    @JoinTable()
    categories: Category[];

}


```

Example how to load objects inside relations:
```typescript
const question = await connection.getRepository(Question).findOne(1);
const categories = question.categories;
```
# Lazy relations
```typescript
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";

@Entity()
export class Question {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    text: string;

    @ManyToMany(type => Category, category => category.questions)
    @JoinTable()
    categories: Promise<Category[]>;

}
```
Example how to load objects inside lazy relations:
```typescript
const question = await connection.getRepository(Question).findOne(1);
const categories = await question.categories;
```