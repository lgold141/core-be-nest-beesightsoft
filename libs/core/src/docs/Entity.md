#Entity
https://typeorm.io/#/entities
## Entity Primary columns


```typescript
import {Entity, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class User {

    //@PrimaryColumn() :  it will create id with int as type which you must manually assign before save.
    //@PrimaryGeneratedColumn() : creates a primary column which value will be automatically generated with an auto-increment value. It will create int column with auto-increment/serial/sequence (depend on the database)
    //@PrimaryGeneratedColumn("uuid") creates a primary column which value will be automatically generated with uuid.
    
    @PrimaryColumn()
    @PrimaryGeneratedColumn()
    @PrimaryGeneratedColumn("uuid")
    id: number;
}
```
## Entity Special columns

```typescript
import {Entity, CreateDateColumn, UpdateDateColumn, VersionColumn} from "typeorm";

@Entity()
export class User {

    //@CreateDateColumn is a special column that is automatically set to the entity's insertion date
    //@UpdateDateColumn is a special column that is automatically set to the entity's update time each time you call save of entity manager or repository. 
    //@VersionColumn is a special column that is automatically set to the version of the entity (incremental number)
    //  each time you call save of entity manager or repository.
    
    @CreateDateColumn({ type: 'timestamptz' })
    createdAt?: Date;
    
    @UpdateDateColumn({ type: 'timestamptz' })
    updatedAt?: Date;
   
    @VersionColumn()
    version?: number;
}
```
## Column types
```typescript
export enum UserRole {
    ADMIN = "admin",
    EDITOR = "editor",
    GHOST = "ghost"
}


import {Entity, Column} from "typeorm";

@Entity()
export class User {
  @Column({ type: "int", width: 200 })
  
      @Column({
          type: "enum",
          enum: UserRole,
          default: UserRole.GHOST
      })
      role: UserRole
}

```
## set column type
```typescript
export enum UserRole {
    ADMIN = "admin",
    EDITOR = "editor",
    GHOST = "ghost"
}
import {Entity, Column} from "typeorm";
@Entity()
export class User {
    @Column({
        type: "set",
        enum: UserRole,
        default: [UserRole.GHOST, UserRole.EDITOR]
    })
    roles: UserRole[]

}
```
## simple-array column type
```typescript
import {Entity, Column} from "typeorm";
@Entity()
export class User {
    @Column("simple-array")
    names: string[];

}
const user = new User();
user.names = [
    "Alexander",
    "Alex",
    "Sasha",
    "Shurik"
];
```

## simple-json column type
```typescript
import {Entity, Column} from "typeorm";
@Entity()
export class User {
    @Column("simple-json")
    profile: { name: string, nickname: string };

}
const user = new User();
user.profile = { name: "John", nickname: "Malkovich" };
```
## Column options
```typescript
import {Entity, Column} from "typeorm";
@Entity()
export class User {
    @Column({
        type: "varchar",
        length: 150,
        unique: true,
        // ...
    })
    name: string;
}
```

## Entity inheritance
```typescript
import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";
export abstract class Content {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

}
@Entity()
export class Photo extends Content {

    @Column()
    size: string;

}

@Entity()
export class Question extends Content {

    @Column()
    answersCount: number;

}

@Entity()
export class Post extends Content {

    @Column()
    viewCount: number;

}
```

## Adjacency list
```typescript
import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany} from "typeorm";

@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @OneToMany(type => Category, category => category.children)
    parent: Category;

    @ManyToOne(type => Category, category => category.parent)
    children: Category;
}
```
## Closure table
```typescript
import {Entity, Tree, Column, PrimaryGeneratedColumn, TreeChildren, TreeParent, TreeLevelColumn} from "typeorm";

@Entity()
@Tree("closure-table")
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @TreeChildren()
    children: Category[];

    @TreeParent()
    parent: Category;

    @TreeLevelColumn()
    level: number;
}
```
