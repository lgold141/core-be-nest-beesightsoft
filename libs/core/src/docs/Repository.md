# Repository
https://typeorm.io/#/repository-api/repository-api
```typescript
import {getRepository} from "typeorm";
import {User} from "./entity/User";

const repository = getRepository(User); // you can also get it via getConnection().getRepository() or getManager().getRepository()
```
#Create
* create - Creates a new instance of User. Optionally accepts an object literal with user properties which will be written into newly created user object
* do not save into database yet
```typescript
const user = repository.create(); // same as const user = new User();
const user = repository.create({
    id: 1,
    firstName: "Timber",
    lastName: "Saw"
}); // same as con
```

* save - Saves a given entity or array of entities. 
If the entity already exist in the database, it is updated. 
If the entity does not exist in the database, it is inserted. 
It saves all given entities in a single transaction (in the case of entity, manager is not transactional). 
Also supports partial updating since all undefined properties are skipped. Returns the saved entity/entities.
```typescript
await repository.save(user);
await repository.save([
    category1,
    category2,
    category3
]);
```

#Read

* count - Counts entities that match given options. Useful for pagination.
```typescript
 const count = await repository.count({ firstName: "Timber" });
```
* findByIds - Finds multiple entities by id.
```typescript
const users = await repository.findByIds([1, 2, 3]);
```
* find - Finds entities that match given options.
```typescript
const timbers = await repository.find({ firstName: "Timber" });
```

*findAndCount - Finds entities that match given find options. Also counts all entities that match given conditions, but ignores pagination settings (skip and take options).
```typescript
const [timbers, timbersCount] = await repository.findAndCount({ firstName: "Timber" });
```
 
* findOne - Finds first entity that matches some id or find options.
```typescript
const user = await repository.findOne(1);
const timber = await repository.findOne({ firstName: "Timber" });
```
* findOneOrFail - Finds the first entity that matches the some id or find options. Rejects the returned promise if nothing matches.
```typescript
const user = await repository.findOneOrFail(1);
const timber = await repository.findOneOrFail({ firstName: "Timber" });
```
* query - Executes a raw SQL query.
```typescript
const rawData = await repository.query(`SELECT * FROM USERS`);
```

#Update
update - Partially updates entity by a given update options or entity id.
```typescript
await repository.update({ firstName: "Timber" }, { firstName: "Rizzrak" });
// executes UPDATE user SET firstName = Rizzrak WHERE firstName = Timber

await repository.update(1, { firstName: "Rizzrak" });
// executes UPDATE user SET firstName = Rizzrak WHERE id = 1
```

#Delete
 - Deletes entities by entity id, ids or given conditions:
```typescript
await repository.delete(1);
await repository.delete([1, 2, 3]);
await repository.delete({ firstName: "Timber" });
```


