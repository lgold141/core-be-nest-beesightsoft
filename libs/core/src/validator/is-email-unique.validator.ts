import {Inject, Injectable, forwardRef} from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface
} from 'class-validator';
import {AuthUserService} from '../services';


/**
 * Custom validation constraint for email uniqueness
 */

@Injectable()
@ValidatorConstraint({name: 'isEmailUnique', async: true})
export class IsEmailUniqueConstraint implements ValidatorConstraintInterface {
  static authUserService: AuthUserService;

  constructor(@Inject(AuthUserService)  authUserService: AuthUserService) {
    if (authUserService) {
      IsEmailUniqueConstraint.authUserService = authUserService
    }
  }

  /**
   * Check email address for uniqueness against existing user entities
   */
  async validate(propertyValue: string, args: ValidationArguments): Promise<boolean> {
    const {property} = args;
    const findOptions = {
      [property]: propertyValue
    };
    const userExists = await IsEmailUniqueConstraint.authUserService.findOneByPropertyValue(findOptions);
    // const userExists = undefined;
    if (userExists === undefined) {
      return true; //true : unique property
    } else if (args.object && args.object.hasOwnProperty('id')) {
      // Edit data
      return userExists.id === args.object['id'];
    } else {
      return false; // false : property
    }
  }

  /**
   * Default error message
   */
  public defaultMessage(args: ValidationArguments): string {
    return 'User with this email already exists.';
  }
}

export function IsEmailUnique(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsEmailUniqueConstraint
    });
  };
}