import {Repository} from 'typeorm';
import {AbstractEntity} from '../entities';

export  abstract class AbstractBaseRepository<T extends AbstractEntity> extends Repository<T> {

}