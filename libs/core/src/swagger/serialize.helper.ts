import {Expose, Type} from 'class-transformer';
import {ApiProperty} from '@nestjs/swagger';
import {IApiResponseInterface, IPaginationInterface} from '../interfaces';

export class SerializeHelper {
  static createGetManyDto(dto: any, resourceName: string): any {
    class PaginationResponseDto implements IPaginationInterface<any> {
      @ApiProperty({type: dto, isArray: true})
      @Type(() => dto)
      items: any[];

      @ApiProperty({example: 1})
      @Expose()
      total: number
    }

    class GetManyResponseDto implements IApiResponseInterface<any> {
      @ApiProperty({type: PaginationResponseDto, isArray: false})
      @Type(() => PaginationResponseDto)
      data: any;

      @ApiProperty({example: null})
      @Expose()
      errors: any
    }

    Object.defineProperty(GetManyResponseDto, 'name', {
      writable: false,
      value: `GetMany${resourceName}ResponseDto`,
    });

    return GetManyResponseDto;
  }

  static createGetOneDto(dto: any, resourceName: string): any {
    class GetOneResponseDto implements IApiResponseInterface<any> {
      @ApiProperty({type: dto, isArray: false})
      @Type(() => dto)
      @Expose()
      data: any;

      @ApiProperty({example: null})
      @Expose()
      errors: any
    }

    Object.defineProperty(GetOneResponseDto, 'name', {
      writable: false,
      value: resourceName ? `GetOne${resourceName}ResponseDto` : `ApiOkResponse${dto.name}`,
    });

    return GetOneResponseDto;
  }
}
