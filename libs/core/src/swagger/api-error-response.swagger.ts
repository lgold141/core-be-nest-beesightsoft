import {IApiResponseInterface} from '../interfaces';
import {ApiProperty} from '@nestjs/swagger';
import {ApiErrorCodeService, CodeEnum} from '@app/api-error-code';
import {Exclude, Expose} from 'class-transformer';

export class ApiBadRequestResponseExample implements IApiResponseInterface<any> {
  @ApiProperty({example: 'null'})
  data: any;

  @ApiProperty({
    example: [[
      {
        "code": 10004,
        "message": "FirstName's length must in a range [3, 100]"
      },
      {
        "code": 10006,
        "message": "LastName's length must in a range [3, 100]"
      },
      {
        "code": 10001,
        "message": "Email is invalid"
      }
    ]]
  })
  errors: any
}


export class ApiUnauthorizedResponseExample implements IApiResponseInterface<any> {
  @ApiProperty({example: null})
  @Expose()
  data: any;

  @ApiProperty({
    example: [ApiErrorCodeService.translate(CodeEnum.UNAUTHORIZED)]
  })
  @Expose()
  errors: any
}

export class ApiForbidenResponseExample implements IApiResponseInterface<any> {
  @ApiProperty({example: null})
  @Expose()
  data: any;

  @ApiProperty({
    example: [ApiErrorCodeService.translate(CodeEnum.FORBIDDEN)]
  })
  @Expose()
  errors: any
}

export class ApiNotfoundResponseExample implements IApiResponseInterface<any> {
  @ApiProperty({example: null})
  @Expose()
  data: any;

  @ApiProperty({
    example: [ApiErrorCodeService.translate(CodeEnum.NOT_FOUND)]
  })
  @Expose()
  errors: any
}