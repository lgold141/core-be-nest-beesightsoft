import {objKeys} from './obj.util';
import {R} from './reflection.helper';
import * as swaggerConst from '@nestjs/swagger/dist/constants';

export class SwaggerHelper {

  static setOperation(metadata: any, func: Function) {
    /* istanbul ignore else */
    if (swaggerConst) {
      R.set(swaggerConst.DECORATORS.API_OPERATION, metadata, func);
    }
  }

  static setParams(metadata: any, func: Function) {
    /* istanbul ignore else */
    if (swaggerConst) {
      R.set(swaggerConst.DECORATORS.API_PARAMETERS, metadata, func);
    }
  }

  static setExtraModels(swaggerModels: any) {
    /* istanbul ignore else */
    if (swaggerConst) {
      const meta = SwaggerHelper.getExtraModels(swaggerModels.get);
      const models: any[] = [
        ...meta,
        ...objKeys(swaggerModels)
        .map((name) => swaggerModels[name])
        .filter((one) => one && one.name !== swaggerModels.get.name),
      ];
      R.set(swaggerConst.DECORATORS.API_EXTRA_MODELS, models, swaggerModels.get);
    }
  }


  static setResponseOk(metadata: any, func: Function) {
    /* istanbul ignore else */
    if (swaggerConst) {
      R.set(swaggerConst.DECORATORS.API_RESPONSE, metadata, func);
    }
  }

  static getOperation(func: Function): any {
    /* istanbul ignore next */
    return swaggerConst ? R.get(swaggerConst.DECORATORS.API_OPERATION, func) || {} : {};
  }

  static getParams(func: Function): any[] {
    /* istanbul ignore next */
    return swaggerConst ? R.get(swaggerConst.DECORATORS.API_PARAMETERS, func) || [] : [];
  }

  static getExtraModels(target: any): any[] {
    /* istanbul ignore next */
    return swaggerConst ? R.get(swaggerConst.DECORATORS.API_EXTRA_MODELS, target) || [] : [];
  }

  static getResponseOk(func: Function): any {
    /* istanbul ignore next */
    return swaggerConst ? R.get(swaggerConst.DECORATORS.API_RESPONSE, func) || {} : {};
  }


  static createPathParasmMeta(options: any): any[] {
    return swaggerConst
      ? objKeys(options).map((param) => ({
        name: param,
        required: true,
        in: 'path',
        type: options[param].type === 'number' ? Number : String,
      }))
      : /* istanbul ignore next */ [];
  }

  private static getSwaggerVersion(): number {
    return 1;
  }

  static getApiTags(target: any): [] {
    return swaggerConst ? R.get(swaggerConst.DECORATORS.API_TAGS, target) || [] : [];
  }
}

