import {Exclude} from 'class-transformer';
import {BeforeInsert, BeforeUpdate, ManyToOne, VersionColumn,} from 'typeorm';

import {RequestContext} from '../context';
import {AbstractEntity} from './abstract.entity';
import {UserEntity} from './user.entity';

// TODO: Implement Soft Delete

export abstract class AbstractAuditEntity extends AbstractEntity {
  @ManyToOne(type => UserEntity, {
    onDelete: 'NO ACTION',
    onUpdate: 'CASCADE',
    nullable: false,
  })
  createdBy?: UserEntity;

  @ManyToOne(type => UserEntity, {
    onDelete: 'NO ACTION',
    onUpdate: 'CASCADE',
    nullable: true,
  })
  updatedBy?: UserEntity;

  @Exclude()
  @VersionColumn()
  version?: number;

  @BeforeInsert()
  setCreatedByUser() {
    const currentUser = RequestContext.currentUser();
    if (currentUser) {
      this.createdBy = currentUser;
    }
  }

  /**
   * NOTE: @BeforeUpdate won't run if you just call update(id, partialEntity)
   * https://github.com/typeorm/typeorm/blob/master/docs/listeners-and-subscribers.md#beforeupdate
   */
  @BeforeUpdate()
  setUpdatedByUser() {
    const currentUser = RequestContext.currentUser();
    if (currentUser) {
      this.updatedBy = currentUser;
    }
  }
}
