import {Entity} from 'typeorm';
import {AbstractUserEntity} from './abstract-user.entity';

@Entity('core_user')
export class UserEntity extends AbstractUserEntity {
}
