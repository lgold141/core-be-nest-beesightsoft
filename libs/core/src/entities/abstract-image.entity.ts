import {IImageInterface, ImageType} from '@nest-core/shared';
import {Transform} from 'class-transformer';
import {IsEnum, IsOptional, Length, MaxLength} from 'class-validator';
import {Column, ManyToOne} from 'typeorm';
import {AbstractEntity} from './abstract.entity';
import {UserEntity} from './user.entity';

export abstract class AbstractImageEntity extends AbstractEntity implements IImageInterface{
  @Length(1, 100)
  @Column({length: 100})
  title: string;

  @Column({type: 'enum', enum: ImageType, default: ImageType.Profile})
  @IsEnum(ImageType)
  type: ImageType;


  @ManyToOne(
    type => UserEntity,
    user => user.images,
    {onDelete: 'CASCADE', nullable: false},
  )
  user?: UserEntity;

  @IsOptional()
  @Transform(value => value.toString('base64'), {toPlainOnly: true})
  @Column({type: 'bytea', nullable: true})
  data?: Buffer;

  @IsOptional()
  @Column({nullable: true})
  checksum?: string;

  @IsOptional()
  @Column({nullable: true})
  mimeType?: string;

  @IsOptional()
  @Column({nullable: true})
  size?: number;

  @IsOptional()
  @MaxLength(500)
  @Column({length: 500, nullable: true})
  url?: string;
}
