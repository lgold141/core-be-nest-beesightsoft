import {Entity} from 'typeorm';
import {AbstractImageEntity} from './abstract-image.entity';

@Entity('core_image')
export class ImageEntity extends AbstractImageEntity {
}
