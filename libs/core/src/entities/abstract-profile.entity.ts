import {Gender, IProfileInterface} from '@nest-core/shared';
import {Exclude} from 'class-transformer';
import {Length} from 'class-validator';
import {Column, JoinColumn, OneToOne, VersionColumn} from 'typeorm';
import {AbstractEntity} from './abstract.entity';
import {ImageEntity} from './image.entity';

export abstract class AbstractProfileEntity extends AbstractEntity implements IProfileInterface{
  @OneToOne(_ => ImageEntity, {cascade: ['insert', 'update'], eager: true, nullable: true, onDelete: 'SET NULL'})
  @JoinColumn()
  avatar?: ImageEntity;

  @Column({type: 'enum', enum: Gender, default: Gender.UNKNOW})
  gender?: Gender = Gender.UNKNOW;

  @Column({nullable: true})
  @Length(10, 20)
  mobilePhone?: string;


  @Exclude()
  @VersionColumn()
  version?: number;
}
