import {Entity} from 'typeorm';
import {AbstractProfileEntity} from './abstract-profile.entity';

@Entity('core_profile')
export class ProfileEntity extends AbstractProfileEntity {

}
