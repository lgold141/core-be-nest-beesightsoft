import { IUserInterface } from '@nest-core/shared';
import { IsAscii, IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { Exclude } from 'class-transformer';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn, ManyToOne } from 'typeorm';
import { AbstractEntity } from './abstract.entity';
import { ImageEntity } from './image.entity';
import { ProfileEntity } from './profile.entity';
import { UserEntity } from './user.entity';

export abstract class AbstractUserEntity extends AbstractEntity implements IUserInterface {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Index({ unique: true })
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Column()
  email: string;

  @Column()
  verifiedEmail: boolean;

  @Index({ unique: true })
  @IsNotEmpty()
  @Length(6, 15)
  @Column()
  phoneNumber: string;

  @Column()
  verifiedPhoneNumber: boolean;

  @Index()
  @Column()
  address: string;

  @Index()
  @Column()
  language: string;

  @Index()
  @Column()
  date: string;

  @Column({ nullable: true })
  currencyId?: number;

  @Column()
  country: string;

  @Column()
  countryCode: string;

  @Column()
  theme: string;

  @IsAscii()
  @IsNotEmpty()
  @Length(6, 100)
  @Index({ unique: true })
  @Column()
  username: string;

  @VersionColumn()
  version?: number;

  @Column({
    nullable: true,
    // select: false
  })
  @Exclude()
  password: string;


  @OneToMany(
    _ => ImageEntity,
    image => image.user
  )
  images?: ImageEntity[];

  // FIXME: OneToOne downward cascade delete not implemented
  @OneToOne(type => ProfileEntity, { cascade: ['insert', 'remove'], nullable: true, onDelete: 'SET NULL' })
  @JoinColumn()
  profile?: ProfileEntity;

  @RelationId((user: UserEntity) => user.profile)
  readonly profileId?: string;

  @Column()
  companyId: string;

  @Column({
    type: 'timestamp without time zone',
    name: 'updatePasswordAt',
    nullable: true
  })
  updatePasswordAt: Date;

  @Column({
    default: false
  })
  registerCompleted: boolean;
}
