import {RequestContext} from '../context';
import {ApiValidatorService} from '../services';
import {
  IApiControllerInterface,
  IApiErrorInterface,
  IApiResponseInterface,
  IApiServiceResponseInterface
} from '../interfaces';
import {AllApiExceptionsFilter} from '../filters';
import {ApiErrorCodeService, CodeEnum} from '@app/api-error-code';
import {PlainToClassPipe} from '@nest-core/shared';
import {HttpStatus, UseFilters, UsePipes} from '@nestjs/common';
import {ApiBearerAuth} from '@nestjs/swagger';
import {isArray} from 'lodash';

@ApiBearerAuth()
@UsePipes(PlainToClassPipe)
@UseFilters(AllApiExceptionsFilter)
export abstract class AbstractApiController extends ApiValidatorService implements IApiControllerInterface {
  constructor() {
    super();
  }

  public respondWithSuccess<T>(data: T, options?: any): void {
    RequestContext.currentResponse()
      .status(HttpStatus.OK)
      .json({
        data: data,
        options: options,
        errors: null
      } as IApiResponseInterface<T>)
      .end();
  }

  public respondWithErrors(errors: IApiErrorInterface[]): void {
    RequestContext.currentResponse()
      .status(HttpStatus.BAD_REQUEST)
      .json({
        data: null,
        errors: errors
      } as IApiResponseInterface<null>);
  }

  public respondWithErrorCodeNumber(code: CodeEnum, options?: any): void {
    let codeError: any = options ? ApiErrorCodeService.translate(code, options) : ApiErrorCodeService.translate(code);
    if (!isArray(codeError)) {
      codeError = [codeError];
    }
    RequestContext.currentResponse()
      .status(HttpStatus.BAD_REQUEST)
      .json({
        data: null,
        errors: codeError
      } as IApiResponseInterface<null>);
  }

  public respondWithErrorsOrSuccess<T>(apiServiceResponse: IApiServiceResponseInterface<T>): void {
    const {code, options, data} = apiServiceResponse;
    if (code) {
      this.respondWithErrorCodeNumber(code, options);
    } else {
      this.respondWithSuccess<T>(data, options);
    }
  }

  public respondWithErrorResourceNotfound(): void {
    this.respondWithErrorCodeNumber(CodeEnum.NOT_FOUND);
  }
}

