import {ValueTransformer} from 'typeorm';
import {UtilsService} from '../services';

export class PasswordTransformer implements ValueTransformer {
  to(value) {
    return UtilsService.generateHashPassword(value);
  }

  from(value) {
    return value;
  }
}
