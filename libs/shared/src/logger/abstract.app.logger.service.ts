import {Logger, LoggerService} from '@nestjs/common';

export abstract class AbstractAppLoggerService extends Logger {
  constructor(private readonly logger: LoggerService) {
    super(null, true);
  }

  log(message: any) {
    this.logger.log(message, this.context);
  }

  error(message: any, trace: string) {
    this.logger.error(message, trace, this.context);
  }

  warn(message: any) {
    this.logger.warn(message, this.context);
  }

  debug(message: any) {
    this.logger.debug(message, this.context);
  }

  verbose(message: any) {
    this.logger.verbose(message, this.context);
  }

}
