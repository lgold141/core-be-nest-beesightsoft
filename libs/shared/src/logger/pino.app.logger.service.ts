import {Injectable} from '@nestjs/common';
import {PinoLogger, Logger, InjectPinoLogger} from 'nestjs-pino';
import {AbstractAppLoggerService} from './abstract.app.logger.service';

@Injectable()
export class PinoAppLoggerService extends AbstractAppLoggerService {
  constructor(logger: Logger) {
    super(logger);
  }
}
