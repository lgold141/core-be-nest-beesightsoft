export enum RolesEnum {
  SELF = 'SELF',
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
}
