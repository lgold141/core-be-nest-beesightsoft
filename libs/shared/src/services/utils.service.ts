import {classToPlain, ClassTransformOptions, plainToClass, serialize, classToPlainFromExist} from "class-transformer";
import {ConfigService} from '@nest-core/config';
import {ClassType} from '@nest-core/shared';
import * as _ from 'lodash';
import * as bcrypt from 'bcrypt';

export class UtilsService {

  /**
   * convert entity to dto class instance
   * @param {T} model
   * @param {Object | Object[]} fromEntity
   * @param {ClassTransformOptions} transformOptions
   * @returns {T[] | T}
   */
  public static plainToClass<T>(model: ClassType<T>,
                                fromEntity: Object | Object[],
                                transformOptions?: ClassTransformOptions): T;
  public static plainToClass<T>(model: ClassType<T>,
                                fromEntity: Object | Object[],
                                transformOptions?: ClassTransformOptions): T[];
  public static plainToClass<T>(model: ClassType<T>,
                                fromEntity: Object | Object[],
                                transformOptions?: ClassTransformOptions): T | T[] {
    if (_.isArray(fromEntity)) {
      return _.toArray(fromEntity).map(entity => <T>plainToClass(model, entity, transformOptions ? transformOptions : {excludeExtraneousValues: true}));
    }
    return <T>plainToClass(model, fromEntity, transformOptions ? transformOptions : {excludeExtraneousValues: true});
  }

  public static serialize<T>(object: T, options?: ClassTransformOptions): string;
  public static serialize<T>(object: T[], options?: ClassTransformOptions): string;
  public static serialize<T>(object: T | T[], options?: ClassTransformOptions): string {
    return serialize(object, options);
  }


  public static classToPlain<T>(object: T, options?: ClassTransformOptions): Object;
  public static classToPlain<T>(object: T[], options?: ClassTransformOptions): Object[];
  public static classToPlain<T>(object: T | T[], options?: ClassTransformOptions): Object | Object[] {
    return classToPlain(object, options);
  }


  /**
   * generate hash from password or string
   * @param {string} password
   * @returns {string}
   */
  static generateHashPassword(password: string): string {
    return bcrypt.hashSync(password, ConfigService.get('userPasswordBcryptSaltRounds'));
  }

  /**
   * validate text with hash
   * @param {string} password
   * @param {string} hash
   * @returns {Promise<boolean>}
   */
  static validateHashPassword(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash || '');
  }

  /**
   * generate random string
   * @param length
   */
  static generateRandomString(length: number): string {
    return Math.random()
      .toString(36)
      .replace(/[^a-zA-Z0-9]+/g, '')
      .substr(0, length);
  }

}
