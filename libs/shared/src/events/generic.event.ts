import {IEvent} from '@nestjs/cqrs';
import {IUserInterface} from '../interfaces';

export class GenericEvent implements IEvent {
  constructor(public readonly type: string, public readonly payload: any, public readonly user: IUserInterface) {
  }
}
