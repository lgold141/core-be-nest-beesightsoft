import {ArgumentMetadata, Injectable, PipeTransform} from '@nestjs/common';
import {UtilsService} from '../services';


@Injectable()
export class PlainToClassPipe implements PipeTransform {
  async transform(value, metadata: ArgumentMetadata) {
    // console.log([
    //   'PlainToClassPipe',
    //   {value},
    //   metadata.metatype
    // ]);
    return UtilsService.plainToClass(metadata.metatype, value);
  }
}
