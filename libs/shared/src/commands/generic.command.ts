import {ICommand} from '@nestjs/cqrs';
import {IUserInterface} from '@nest-core/shared';

export class GenericCommand implements ICommand {
  constructor(public readonly type: string, public readonly payload: any, public readonly user: IUserInterface) {
  }
}
