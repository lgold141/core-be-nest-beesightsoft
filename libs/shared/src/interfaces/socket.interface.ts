import {Socket} from 'socket.io';
// import {IJwtTokenInterface} from './jwt-token.interface';
// import {IUserInterface} from './user.interface';
import {JwtPayloadInterface} from './jwt-payload.interface';

export interface ISocket extends Socket {
  user: any;
  authInfo: JwtPayloadInterface;
}
