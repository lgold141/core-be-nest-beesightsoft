import {ImageType} from '../enums';
import {IUserInterface} from './user.interface';
import {IEntityInterface} from './entity.interface';

export interface IImageInterface extends IEntityInterface{
  title: string;
  type: ImageType;
  user?: IUserInterface;
  data?: Buffer;
  checksum?: string;
  mimeType?: string;
  size?: number;
  url?: string;
}
