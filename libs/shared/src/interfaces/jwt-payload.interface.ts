export interface JwtPayloadInterface {
  iat?: number; //(Issued At) Claim
  jti?: any; //"jti" (JWT ID) Claim
  exp?: number, // expired time

  id: string, // user id
  registerCompleted?: boolean;
  username: string, // username

  [key: string]: any;
}
//nasterblue:  https://www.iana.org/assignments/jwt/jwt.xhtml
