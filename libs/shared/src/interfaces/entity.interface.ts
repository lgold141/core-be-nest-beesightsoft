export interface IEntityInterface {
  id: string;
  createdAt: Date;
  updatedAt: Date;
}