import { IImageInterface } from './image.interface'
import { IProfileInterface } from './profile.interface';
import { IEntityInterface } from './entity.interface';
import { ICompanyInterface } from '../../../../src/app/companies/interfaces';


export interface IUserInterface extends IEntityInterface {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  username: string;
  version?: number;
  password?: string;
  verifiedEmail?: boolean;
  verifiedPhoneNumber?: boolean;
  country?: string;
  countryCode?: string;
  theme?: string;
  images?: IImageInterface[];
  profile?: IProfileInterface;
  readonly profileId?: string;
  company?: ICompanyInterface;
  readonly companyId?: string;
  registerCompleted?: boolean;
}
