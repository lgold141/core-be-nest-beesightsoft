import {Gender} from '../enums';
import {IImageInterface} from './image.interface';
import {IEntityInterface} from './entity.interface';

export interface IProfileInterface extends IEntityInterface{
  avatar?: IImageInterface;
  gender?: Gender;
  mobilePhone?: string;
}
