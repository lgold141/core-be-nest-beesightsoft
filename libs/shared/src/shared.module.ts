import {Module, Global} from '@nestjs/common';
import {CqrsModule} from '@nestjs/cqrs';
import {ConfigModule, ConfigService} from '@nest-core/config';
import {CQRSGateway} from './cqrs.gateway';
import {LoggerModule} from "nestjs-pino";
import {PinoAppLoggerService} from "./logger";

// import * as AuthGuards from './guards';
@Global()
@Module({
  imports: [
    CqrsModule,
    ConfigModule,
    LoggerModule.forRoot()
  ],
  providers: [
    CQRSGateway,
    ConfigService,
    PinoAppLoggerService
  ],
  exports: [
    CQRSGateway,
    ConfigService,
    PinoAppLoggerService
  ],
})
export class SharedModule {
}
