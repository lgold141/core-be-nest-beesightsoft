/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = __webpack_require__(1);
const app_module_1 = __webpack_require__(2);
const boostrap_1 = __webpack_require__(391);
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule, {
            cors: true,
            logger: ['log', 'error', 'warn']
        });
        yield boostrap_1.bootstrap(app);
    });
}
start();


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/core");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const platform_express_1 = __webpack_require__(4);
const app_bootstrap_module_1 = __webpack_require__(5);
const database_1 = __webpack_require__(130);
const core_1 = __webpack_require__(9);
const typeorm_1 = __webpack_require__(117);
const products_1 = __webpack_require__(322);
const suppliers_1 = __webpack_require__(364);
const stocks_1 = __webpack_require__(371);
const images_1 = __webpack_require__(190);
const categories_1 = __webpack_require__(327);
const locations_1 = __webpack_require__(348);
const variations_1 = __webpack_require__(354);
const code_1 = __webpack_require__(214);
const flag_1 = __webpack_require__(274);
const companies_1 = __webpack_require__(187);
const profile_entity_1 = __webpack_require__(259);
const image_entity_1 = __webpack_require__(260);
const user_entity_1 = __webpack_require__(200);
const device_1 = __webpack_require__(246);
const options_entity_1 = __webpack_require__(202);
const countries_entity_1 = __webpack_require__(204);
const currencies_entity_1 = __webpack_require__(201);
const barcodes_1 = __webpack_require__(361);
const optionValues_entity_1 = __webpack_require__(203);
const entities = [
    core_1.ImageEntity,
    core_1.ProfileEntity,
    core_1.UserEntity,
    products_1.ProductEntity,
    suppliers_1.SupplierEntity,
    stocks_1.StockEntity,
    images_1.ImagesEntity,
    categories_1.CategoryEntity,
    locations_1.LocationEntity,
    variations_1.VariationEntity,
    code_1.CodeEntity,
    flag_1.FlagEntity,
    companies_1.CompanyEntity,
    device_1.DevicesEntity,
    profile_entity_1.ProfileEntity,
    image_entity_1.ImageEntity,
    user_entity_1.UserEntity,
    options_entity_1.OptionEntity,
    countries_entity_1.CountryEntity,
    currencies_entity_1.CurrencyEntity,
    barcodes_1.BarcodeEntity,
    optionValues_entity_1.OptionValueEntity
];
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            platform_express_1.MulterModule.register({
                dest: '/images',
            }),
            database_1.DatabaseModule.forRootAsync({
                entities: entities
            }),
            typeorm_1.TypeOrmModule.forFeature(entities, database_1.DB_CONNECTION_DEFAULT),
            app_bootstrap_module_1.AppBootstrapModule,
        ]
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/common");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/platform-express");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const environment_1 = __webpack_require__(6);
const common_1 = __webpack_require__(3);
const nest_router_1 = __webpack_require__(8);
const core_1 = __webpack_require__(9);
const shared_1 = __webpack_require__(25);
const config_1 = __webpack_require__(52);
const auth_1 = __webpack_require__(166);
const user_1 = __webpack_require__(256);
const api_error_code_1 = __webpack_require__(94);
const example_1 = __webpack_require__(281);
const app_controller_1 = __webpack_require__(318);
const terminus_1 = __webpack_require__(320);
const app_health_controller_1 = __webpack_require__(321);
const images_1 = __webpack_require__(190);
const globalPrefix = environment_1.environment.server.globalPrefix;
const entities = [
    user_1.ImageEntity, user_1.ProfileEntity, user_1.UserEntity, images_1.ImagesEntity
];
const databaseConfig = __webpack_require__(130);
const { DB_CONNECTION_DEFAULT } = databaseConfig;
const products_1 = __webpack_require__(322);
const suppliers_1 = __webpack_require__(364);
const stocks_1 = __webpack_require__(371);
const categories_1 = __webpack_require__(327);
const locations_1 = __webpack_require__(348);
const twilio_1 = __webpack_require__(223);
const code_module_1 = __webpack_require__(216);
const flag_module_1 = __webpack_require__(276);
const companies_1 = __webpack_require__(187);
const device_1 = __webpack_require__(246);
const options_module_1 = __webpack_require__(378);
const countries_module_1 = __webpack_require__(253);
const currencies_module_1 = __webpack_require__(386);
const optionValues_module_1 = __webpack_require__(384);
let AppBootstrapModule = class AppBootstrapModule {
};
AppBootstrapModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [
            config_1.ConfigModule,
            core_1.CoreModule,
            nest_router_1.RouterModule.forRoutes([
                {
                    path: globalPrefix,
                    children: [
                        { path: '/auth', module: auth_1.AuthModule },
                        { path: '/user', module: user_1.UserModule },
                        { path: '/products', module: products_1.ProductModule },
                        { path: '/suppliers', module: suppliers_1.SupplierModule },
                        { path: '/stocks', module: stocks_1.StockModule },
                        { path: '/images', module: images_1.ImageModule },
                        { path: '/categories', module: categories_1.CategoryModule },
                        { path: '/locations', module: locations_1.LocationModule },
                        { path: '/code', module: code_module_1.CodeModule },
                        { path: '/flag', module: flag_module_1.FlagModule },
                        { path: '/company', module: companies_1.CompanyModule },
                        { path: '/devices', module: device_1.DeviceModule },
                        { path: '/options', module: options_module_1.OptionModule },
                        { path: '/countries', module: countries_module_1.CountryModule },
                        { path: '/currencies', module: currencies_module_1.CurrencyModule },
                        { path: '/optionValues', module: optionValues_module_1.OptionValueModule },
                    ],
                },
            ]),
            api_error_code_1.ApiErrorCodeModule,
            common_1.HttpModule,
            shared_1.SharedModule,
            auth_1.AuthModule,
            companies_1.CompanyModule,
            device_1.DeviceModule,
            user_1.UserModule,
            terminus_1.TerminusModule,
            example_1.ExampleModule,
            products_1.ProductModule,
            suppliers_1.SupplierModule,
            stocks_1.StockModule,
            images_1.ImageModule,
            categories_1.CategoryModule,
            locations_1.LocationModule,
            twilio_1.TwilioModule,
            flag_module_1.FlagModule,
            code_module_1.CodeModule,
            options_module_1.OptionModule,
            countries_module_1.CountryModule,
            currencies_module_1.CurrencyModule,
            optionValues_module_1.OptionValueModule,
        ],
        controllers: [app_controller_1.AppController, app_health_controller_1.HealthController],
    })
], AppBootstrapModule);
exports.AppBootstrapModule = AppBootstrapModule;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(7).config();
function getDatabaseConfig() {
    switch (process.env.DB_CONNECTION) {
        case 'postgres':
            const databaseConfig = {
                type: 'postgres',
                host: process.env.DB_HOST || 'localhost',
                port: process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : 5432,
                database: process.env.DB_DATABASE || 'postgres',
                username: process.env.DB_USERNAME || 'postgres',
                password: process.env.DB_PASSWORD || 'root',
                keepConnectionAlive: process.env.TYPEORM_KEEPCONNECTIONALIVE ? JSON.parse(process.env.TYPEORM_KEEPCONNECTIONALIVE) : false,
                logging: process.env.TYPEORM_LOGGING ? JSON.parse(process.env.TYPEORM_LOGGING) : false,
                synchronize: process.env.TYPEORM_SYNCHRONIZE ? JSON.parse(process.env.TYPEORM_SYNCHRONIZE) : false,
                uuidExtension: process.env.TYPEORM_UUID_EXTENSION
            };
            return databaseConfig;
            break;
    }
}
const databaseConfig = getDatabaseConfig();
console.info(`DB Config: ${JSON.stringify(databaseConfig)}`);
exports.environment = {
    production: process.env.APP_ENV === 'production',
    debug: process.env.APP_DEBUG ? JSON.parse(process.env.APP_DEBUG) : false,
    env: process.env.APP_ENV,
    ALLOW_WHITE_LIST: ['::ffff:127.0.0.1', '::1'],
    userPasswordBcryptSaltRounds: process.env.USER_PASSWORD_BCRYPT_SALT_ROUNDS ? parseInt(process.env.USER_PASSWORD_BCRYPT_SALT_ROUNDS, 10) : 12,
    jwt: {
        secretOrKey: process.env.JWT_SECRET,
        signOptions: {
            algorithm: process.env.JWT_SIGN_OPTIONS_ALGORITHM,
            expiresIn: process.env.JWT_SIGN_OPTIONS_EXPIRESIN
        },
    },
    server: {
        host: process.env.APP_URL,
        domainUrl: process.env.APP_URL + ':' + process.env.APP_PORT,
        port: process.env.APP_PORT || 3000,
        globalPrefix: process.env.APP_URL_PREFIX,
    },
    databases: [
        databaseConfig
    ],
    redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD
    },
    auth: {
        clientId: process.env.OIDC_CLIENT_ID || 'ngxapi',
        issuerExternalUrl: process.env.OIDC_ISSUER_EXTERNAL_URL || 'https://keycloak.traefik.k8s/auth/realms/ngx',
        issuerInternalUrl: process.env.OIDC_ISSUER_INTERNAL_URL || 'http://keycloak-headless:8080/auth/realms/ngx',
    },
    email: {
        transport: {
            host: process.env.MAIL_HOST || 'mail.google.com',
            port: process.env.MAIL_PORT ? Number(process.env.MAIL_PORT) : 25,
            secure: process.env.EMAIL_SECURE ? JSON.parse(process.env.MAIL_SECURE) : false,
            auth: {
                user: process.env.MAIL_USERNAME || 'auth_user',
                pass: process.env.MAIL_PASSWORD || 'auth_pass',
            },
        },
        defaults: {
            from: process.env.MAIL_FROM_ADDRESS ? process.env.MAIL_FROM_ADDRESS : '"sumo demo" <sumo@demo.com>',
        },
        templateDir: process.env.MAIL_TEMPLATE_DIR || `${__dirname}/assets/email-templates`,
    },
    weather: {
        baseUrl: 'https://samples.openweathermap.org/data/2.5',
        apiKey: 'b6907d289e10d714a6e88b30761fae22',
    },
    webPush: {
        subject: process.env.VAPID_SUBJECT || 'mailto: sumo@demo.com',
        publicKey: process.env.VAPID_PUBLIC_KEY ||
            'BAJq-yHlSNjUqKW9iMY0hG96X9WdVwetUFDa5rQIGRPqOHKAL_fkKUe_gUTAKnn9IPAltqmlNO2OkJrjdQ_MXNg',
        privateKey: process.env.VAPID_PRIVATE_KEY || 'cwh2CYK5h_B_Gobnv8Ym9x61B3qFE2nTeb9BeiZbtMI',
    },
    swagger: {
        enable: process.env.SWAGGER_ENABLED ? JSON.parse(process.env.SWAGGER_ENABLED) : false,
        title: 'Headless CMS API Docs',
        description: 'Headless CMS API Docs',
        welcomeText: 'Welcome to Headless API',
        path: process.env.SWAGGER_PATH
    },
    images: {
        path: process.env.IMAGE_PATH,
        api: process.env.IMAGE_API
    },
    barcode: process.env.BARCODE,
    twilio: {
        number: process.env.TWILIO_NUMBER,
        sid: process.env.TWILIO_SID,
        auth: process.env.TWILIO_AUTH,
        content: 'Your confirm number is '
    },
    productTemplate: {
        path: process.env.PRODUCT_TEMPLATES_PATH
    },
    productExport: {
        path: process.env.UPLOAD_FILE_PATH
    },
    code: {
        expireTime: eval(process.env.EXPIRE_TIME)
    }
};
exports.default = exports.environment;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("dotenv");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("nest-router");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(10));
__export(__webpack_require__(14));
__export(__webpack_require__(75));
__export(__webpack_require__(118));
__export(__webpack_require__(137));
__export(__webpack_require__(153));
__export(__webpack_require__(128));
__export(__webpack_require__(114));
__export(__webpack_require__(92));
__export(__webpack_require__(112));
__export(__webpack_require__(107));
__export(__webpack_require__(20));
__export(__webpack_require__(159));
__export(__webpack_require__(162));
__export(__webpack_require__(143));
__export(__webpack_require__(164));
__export(__webpack_require__(165));


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(11));
__export(__webpack_require__(13));


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const cls = __webpack_require__(12);
class RequestContext {
    constructor(request, response) {
        this.id = Math.random();
        this.request = request;
        this.response = response;
    }
    static currentRequestContext() {
        const session = cls.getNamespace(RequestContext.name);
        if (session && session.active) {
            return session.get(RequestContext.name);
        }
        return null;
    }
    static currentRequest() {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            return requestContext.request;
        }
        return null;
    }
    static currentResponse() {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            return requestContext.response;
        }
        return null;
    }
    static currentUser(throwError) {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            const user = requestContext.request['user'];
            if (user) {
                return user;
            }
        }
        if (throwError) {
            throw new common_1.HttpException('Unauthorized', common_1.HttpStatus.UNAUTHORIZED);
        }
        return null;
    }
    static currentToken(throwError) {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            return requestContext.request['token'];
        }
        if (throwError) {
            throw new common_1.HttpException('Unauthorized', common_1.HttpStatus.UNAUTHORIZED);
        }
        return null;
    }
}
exports.RequestContext = RequestContext;


/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("cls-hooked");

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const cls = __webpack_require__(12);
const request_context_1 = __webpack_require__(11);
let RequestContextMiddleware = class RequestContextMiddleware {
    use(req, res, next) {
        const requestContext = new request_context_1.RequestContext(req, res);
        const session = cls.getNamespace(request_context_1.RequestContext.name) || cls.createNamespace(request_context_1.RequestContext.name);
        session.run(() => __awaiter(this, void 0, void 0, function* () {
            session.set(request_context_1.RequestContext.name, requestContext);
            next();
        }));
    }
};
RequestContextMiddleware = __decorate([
    common_1.Injectable()
], RequestContextMiddleware);
exports.RequestContextMiddleware = RequestContextMiddleware;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(15));
__export(__webpack_require__(19));
__export(__webpack_require__(24));


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = __webpack_require__(16);
const class_validator_1 = __webpack_require__(17);
const swagger_1 = __webpack_require__(18);
class PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({ example: 10 }),
    class_validator_1.IsOptional(),
    class_transformer_1.Transform((val) => parseInt(val, 10) || 0),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], PaginationParams.prototype, "take", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 0 }),
    class_validator_1.IsOptional(),
    class_transformer_1.Transform((val) => parseInt(val, 10) || 0),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], PaginationParams.prototype, "skip", void 0);
exports.PaginationParams = PaginationParams;


/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("class-transformer");

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("class-validator");

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/swagger");

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const exceptions_1 = __webpack_require__(20);
class CrudService {
    constructor(repository) {
        this.repository = repository;
    }
    count(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.repository.count(filter);
        });
    }
    findAll(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const total = yield this.repository.count(filter);
            const items = yield this.repository.find(filter);
            return { items, total };
        });
    }
    findOneOrFail(id, options) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.findOneOrFail(id, options);
            }
            catch (err) {
                throw new exceptions_1.EntityNotfoundException(err);
            }
        });
    }
    findOne(id, options) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.findOne(id, options);
            }
            catch (err) {
                throw new exceptions_1.EntityNotfoundException(err);
            }
        });
    }
    create(entity, options) {
        return __awaiter(this, void 0, void 0, function* () {
            const obj = this.repository.create(entity);
            try {
                return yield this.repository.save(obj, options);
            }
            catch (err) {
                throw new exceptions_1.EntityWriteException(err);
            }
        });
    }
    update(id, partialEntity) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.repository.update(id, partialEntity);
            }
            catch (err) {
                throw new exceptions_1.EntityWriteException(err);
            }
        });
    }
    delete(criteria) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return this.repository.delete(criteria);
            }
            catch (err) {
                throw new exceptions_1.EntityNotfoundException(err);
            }
        });
    }
}
exports.CrudService = CrudService;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(21));
__export(__webpack_require__(22));
__export(__webpack_require__(23));


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
class ApiBadRequestException extends common_1.HttpException {
    constructor(objectOrError, description = 'Api Bad Request') {
        super(common_1.HttpException.createBody(objectOrError, description, common_1.HttpStatus.BAD_REQUEST), common_1.HttpStatus.BAD_REQUEST);
    }
}
exports.ApiBadRequestException = ApiBadRequestException;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
class EntityWriteException extends common_1.HttpException {
    constructor(objectOrError, description = 'Entity write exception') {
        super(common_1.HttpException.createBody(objectOrError, description, common_1.HttpStatus.BAD_REQUEST), common_1.HttpStatus.BAD_REQUEST);
    }
}
exports.EntityWriteException = EntityWriteException;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
class EntityNotfoundException extends common_1.HttpException {
    constructor(objectOrError, description = 'Entity notfound exception') {
        super(common_1.HttpException.createBody(objectOrError, description, common_1.HttpStatus.NOT_FOUND), common_1.HttpStatus.NOT_FOUND);
    }
}
exports.EntityNotfoundException = EntityNotfoundException;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const shared_1 = __webpack_require__(25);
const interfaces_1 = __webpack_require__(75);
const dto_1 = __webpack_require__(92);
const constants_1 = __webpack_require__(107);
const pagination_params_1 = __webpack_require__(15);
const controllers_1 = __webpack_require__(112);
const swagger_2 = __webpack_require__(143);
let CrudController = class CrudController extends controllers_1.AbstractApiController {
    constructor(crudService) {
        super();
        this.crudService = crudService;
        this.target = this.constructor;
        this.syncMetaData();
    }
    findAll(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            let { items, total } = yield this.crudService.findAll(filter);
            this.respondWithSuccess({
                items: this.transformData(items),
                total
            });
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield this.crudService.findOneOrFail(id);
                if (!data) {
                    this.respondWithErrorResourceNotfound();
                }
                this.respondWithSuccess(this.transformData(data));
            }
            catch (ex) {
                this.respondWithErrorResourceNotfound();
            }
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield this.crudService.findOneOrFail(id);
                this.respondWithSuccess(this.transformData(data));
            }
            catch (ex) {
                this.respondWithErrorResourceNotfound();
            }
        });
    }
    create(entity, ...options) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(entity);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.crudService.create(shared_1.UtilsService.classToPlain(entity));
                this.respondWithSuccess(this.transformData(data));
            }
        });
    }
    update(id, entity) {
        return __awaiter(this, void 0, void 0, function* () {
            Object.assign(entity, { id });
            const errors = yield this.validateWith(entity);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                yield this.crudService.update(id, shared_1.UtilsService.classToPlain(entity));
                const data = yield this.crudService.findOneOrFail(id);
                this.respondWithSuccess(this.transformData(data));
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.crudService.delete(id);
            this.respondWithSuccess(data);
        });
    }
    syncMetaData() {
        if (Reflect.hasMetadata(constants_1.CRUD_OPTIONS_DTO_METADATA, this.target)) {
            this[constants_1.CRUD_OPTIONS_DTO_METADATA] = Reflect.getMetadata(constants_1.CRUD_OPTIONS_DTO_METADATA, this.target);
            this.crudOptionsDto = Reflect.getMetadata(constants_1.CRUD_OPTIONS_DTO_METADATA, this.target);
        }
        this.bindingSwaggerResponse();
    }
    transformData(data) {
        const { transformDto } = this.crudOptionsDto;
        return transformDto ? shared_1.UtilsService.plainToClass(transformDto, data) : data;
    }
    bindingSwaggerResponse() {
        const { transformDto, createDto, updateDto, searchDto, resourceName } = this.crudOptionsDto;
        if (transformDto) {
            const GetOneDto = {
                [common_1.HttpStatus.OK]: {
                    description: 'OK',
                    type: swagger_2.SerializeHelper.createGetOneDto(transformDto, resourceName)
                },
            };
            const GetManyDto = {
                [common_1.HttpStatus.OK]: {
                    description: 'OK',
                    type: swagger_2.SerializeHelper.createGetManyDto(transformDto, resourceName)
                },
            };
            swagger_2.SwaggerHelper.setResponseOk(GetOneDto, this['findById']);
            swagger_2.SwaggerHelper.setResponseOk(GetOneDto, this['create']);
            swagger_2.SwaggerHelper.setResponseOk(GetOneDto, this['update']);
            swagger_2.SwaggerHelper.setResponseOk(GetManyDto, this['findAll']);
        }
        if (createDto) {
            swagger_2.R.setRouteArgsTypes([createDto], this, 'create');
        }
        if (updateDto) {
            swagger_2.R.setRouteArgsTypes(['', updateDto], this, 'update');
        }
        if (searchDto) {
            swagger_2.R.setRouteArgsTypes([searchDto], this, 'findAll');
        }
    }
};
__decorate([
    swagger_1.ApiOperation({ summary: 'Find all' }),
    common_1.Post('findAll'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof pagination_params_1.PaginationParams !== "undefined" && pagination_params_1.PaginationParams) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CrudController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    swagger_1.ApiOperation({ summary: 'Find one record by id' }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], CrudController.prototype, "findById", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'Create new record' }),
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_d = typeof dto_1.AbstractDto !== "undefined" && dto_1.AbstractDto) === "function" ? _d : Object, Object]),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], CrudController.prototype, "create", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'Update an existing record' }),
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, typeof (_f = typeof dto_1.AbstractDto !== "undefined" && dto_1.AbstractDto) === "function" ? _f : Object]),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], CrudController.prototype, "update", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'Delete a record' }),
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], CrudController.prototype, "delete", null);
CrudController = __decorate([
    swagger_1.ApiResponse({
        status: common_1.HttpStatus.BAD_REQUEST,
        description: 'Invalid request params',
        type: swagger_2.ApiBadRequestResponseExample
    }),
    swagger_1.ApiResponse({
        status: common_1.HttpStatus.UNAUTHORIZED, description: 'Caller is not authenticated',
        type: swagger_2.ApiUnauthorizedResponseExample
    }),
    swagger_1.ApiResponse({
        status: common_1.HttpStatus.FORBIDDEN, description: 'Forbidden',
        type: swagger_2.ApiForbidenResponseExample
    }),
    swagger_1.ApiResponse({
        status: common_1.HttpStatus.NOT_FOUND,
        description: 'Resource not found',
        type: swagger_2.ApiNotfoundResponseExample
    }),
    __metadata("design:paramtypes", [typeof (_j = typeof interfaces_1.ICrudServiceInterface !== "undefined" && interfaces_1.ICrudServiceInterface) === "function" ? _j : Object])
], CrudController);
exports.CrudController = CrudController;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(26));
var cqrs_gateway_1 = __webpack_require__(28);
exports.CQRSGateway = cqrs_gateway_1.CQRSGateway;
__export(__webpack_require__(44));
__export(__webpack_require__(46));
__export(__webpack_require__(58));
__export(__webpack_require__(50));
__export(__webpack_require__(33));
__export(__webpack_require__(66));
__export(__webpack_require__(71));
__export(__webpack_require__(73));
__export(__webpack_require__(68));


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(27));


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class GenericCommand {
    constructor(type, payload, user) {
        this.type = type;
        this.payload = payload;
        this.user = user;
    }
}
exports.GenericCommand = GenericCommand;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var CQRSGateway_1, _a, _b, _c, _d, _e, _f, _g, _h;
Object.defineProperty(exports, "__esModule", { value: true });
const websockets_1 = __webpack_require__(29);
const cqrs_1 = __webpack_require__(30);
const rxjs_1 = __webpack_require__(31);
const common_1 = __webpack_require__(3);
const operators_1 = __webpack_require__(32);
const interfaces_1 = __webpack_require__(33);
const socket_io_1 = __webpack_require__(40);
const guards_1 = __webpack_require__(41);
const commands_1 = __webpack_require__(26);
const events_1 = __webpack_require__(44);
let CQRSGateway = CQRSGateway_1 = class CQRSGateway {
    constructor(eventBus, commandBus) {
        this.eventBus = eventBus;
        this.commandBus = commandBus;
        this.logger = new common_1.Logger(CQRSGateway_1.name);
        this.clients = [];
    }
    afterInit(server) { }
    handleConnection(client) {
        this.clients.push(client);
    }
    handleDisconnect(client) {
        this.clients = this.clients.filter(c => c.id !== client.id);
    }
    onAuthenticate(client, data) {
        const event = 'auth';
        return { event, status: 'success' };
    }
    onTest(client, data) {
        const event = 'test';
        return rxjs_1.of({ event, data }).pipe(operators_1.delay(1000));
    }
    onEvent(client, event) {
        this.eventBus.publish(new events_1.GenericEvent(event.type, event.payload, client.user));
    }
    onCommand(client, command) {
        this.commandBus.execute(new commands_1.GenericCommand(command.type, command.payload, client.user));
    }
    sendCommandToUser(user, action) {
        const clients = this.getSocketsForUser(user);
        const type = this.getActionTypeFromInstance(action);
        clients.forEach(socket => socket.emit(CQRSGateway_1.COMMANDS, Object.assign(Object.assign({}, action), { type })));
    }
    sendCommandToAll(action) {
        const type = this.getActionTypeFromInstance(action);
        this.clients.forEach(socket => socket.emit(CQRSGateway_1.COMMANDS, Object.assign(Object.assign({}, action), { type })));
    }
    sendEventToUser(user, event) {
        const clients = this.getSocketsForUser(user);
        const type = this.getActionTypeFromInstance(event);
        clients.forEach(socket => socket.emit(CQRSGateway_1.EVENTS, Object.assign(Object.assign({}, event), { type })));
    }
    sendEventToAll(event) {
        const type = this.getActionTypeFromInstance(event);
        this.clients.forEach(socket => socket.emit(CQRSGateway_1.EVENTS, Object.assign(Object.assign({}, event), { type })));
    }
    getSocketsForUser(user) {
        return this.clients.filter(c => c.user && c.user.username === user.username);
    }
    getActionTypeFromInstance(action) {
        if (action.constructor && action.constructor.type) {
            return action.constructor.type;
        }
        return action.type;
    }
};
CQRSGateway.EVENTS = 'events';
CQRSGateway.COMMANDS = 'actions';
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", typeof (_a = typeof socket_io_1.Server !== "undefined" && socket_io_1.Server) === "function" ? _a : Object)
], CQRSGateway.prototype, "server", void 0);
__decorate([
    common_1.UseGuards(guards_1.WsJwtAuthGuard),
    websockets_1.SubscribeMessage('auth'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_b = typeof interfaces_1.ISocket !== "undefined" && interfaces_1.ISocket) === "function" ? _b : Object, Object]),
    __metadata("design:returntype", void 0)
], CQRSGateway.prototype, "onAuthenticate", null);
__decorate([
    websockets_1.SubscribeMessage('test'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_c = typeof interfaces_1.ISocket !== "undefined" && interfaces_1.ISocket) === "function" ? _c : Object, Object]),
    __metadata("design:returntype", typeof (_d = typeof rxjs_1.Observable !== "undefined" && rxjs_1.Observable) === "function" ? _d : Object)
], CQRSGateway.prototype, "onTest", null);
__decorate([
    websockets_1.SubscribeMessage(CQRSGateway_1.EVENTS),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_e = typeof interfaces_1.ISocket !== "undefined" && interfaces_1.ISocket) === "function" ? _e : Object, Object]),
    __metadata("design:returntype", void 0)
], CQRSGateway.prototype, "onEvent", null);
__decorate([
    websockets_1.SubscribeMessage(CQRSGateway_1.COMMANDS),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_f = typeof interfaces_1.ISocket !== "undefined" && interfaces_1.ISocket) === "function" ? _f : Object, Object]),
    __metadata("design:returntype", void 0)
], CQRSGateway.prototype, "onCommand", null);
CQRSGateway = CQRSGateway_1 = __decorate([
    websockets_1.WebSocketGateway({ namespace: 'eventbus' }),
    __metadata("design:paramtypes", [typeof (_g = typeof cqrs_1.EventBus !== "undefined" && cqrs_1.EventBus) === "function" ? _g : Object, typeof (_h = typeof cqrs_1.CommandBus !== "undefined" && cqrs_1.CommandBus) === "function" ? _h : Object])
], CQRSGateway);
exports.CQRSGateway = CQRSGateway;


/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/websockets");

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/cqrs");

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("rxjs");

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("rxjs/operators");

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(34));
__export(__webpack_require__(35));
__export(__webpack_require__(36));
__export(__webpack_require__(37));
__export(__webpack_require__(38));
__export(__webpack_require__(39));


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = require("socket.io");

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(42));


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const passport_1 = __webpack_require__(43);
let WsJwtAuthGuard = class WsJwtAuthGuard extends passport_1.AuthGuard('ws-jwt') {
};
WsJwtAuthGuard = __decorate([
    common_1.Injectable()
], WsJwtAuthGuard);
exports.WsJwtAuthGuard = WsJwtAuthGuard;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/passport");

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(45));


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class GenericEvent {
    constructor(type, payload, user) {
        this.type = type;
        this.payload = payload;
        this.user = user;
    }
}
exports.GenericEvent = GenericEvent;


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(47));
__export(__webpack_require__(48));
__export(__webpack_require__(49));


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class BoardTransformer {
    to(board) {
        return JSON.stringify(board);
    }
    from(stringified) {
        return JSON.parse(stringified);
    }
}
exports.BoardTransformer = BoardTransformer;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class LowerCaseTransformer {
    to(entityValue) {
        return entityValue.toLocaleLowerCase();
    }
    from(databaseValue) {
        return databaseValue;
    }
}
exports.LowerCaseTransformer = LowerCaseTransformer;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const services_1 = __webpack_require__(50);
class PasswordTransformer {
    to(value) {
        return services_1.UtilsService.generateHashPassword(value);
    }
    from(value) {
        return value;
    }
}
exports.PasswordTransformer = PasswordTransformer;


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(51));


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = __webpack_require__(16);
const config_1 = __webpack_require__(52);
const _ = __webpack_require__(55);
const bcrypt = __webpack_require__(57);
class UtilsService {
    static plainToClass(model, fromEntity, transformOptions) {
        if (_.isArray(fromEntity)) {
            return _.toArray(fromEntity).map(entity => class_transformer_1.plainToClass(model, entity, transformOptions ? transformOptions : { excludeExtraneousValues: true }));
        }
        return class_transformer_1.plainToClass(model, fromEntity, transformOptions ? transformOptions : { excludeExtraneousValues: true });
    }
    static serialize(object, options) {
        return class_transformer_1.serialize(object, options);
    }
    static classToPlain(object, options) {
        return class_transformer_1.classToPlain(object, options);
    }
    static generateHashPassword(password) {
        return bcrypt.hashSync(password, config_1.ConfigService.get('userPasswordBcryptSaltRounds'));
    }
    static validateHashPassword(password, hash) {
        return bcrypt.compare(password, hash || '');
    }
    static generateRandomString(length) {
        return Math.random()
            .toString(36)
            .replace(/[^a-zA-Z0-9]+/g, '')
            .substr(0, length);
    }
}
exports.UtilsService = UtilsService;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(53));
__export(__webpack_require__(54));


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const config_service_1 = __webpack_require__(54);
let ConfigModule = class ConfigModule {
};
ConfigModule = __decorate([
    common_1.Global(),
    common_1.Module({
        providers: [config_service_1.ConfigService],
        exports: [config_service_1.ConfigService],
    })
], ConfigModule);
exports.ConfigModule = ConfigModule;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ConfigService_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const lodash_1 = __webpack_require__(55);
const environment_1 = __webpack_require__(6);
const packageJson = __webpack_require__(56);
let ConfigService = ConfigService_1 = class ConfigService {
    constructor() {
        this.logger = new common_1.Logger(ConfigService_1.name);
        this.config = environment_1.environment;
        this.logger.log('Is Production: ' + environment_1.environment.production);
    }
    get(propertyPath, defaultValue) {
        const processValue = lodash_1.get(this.config, propertyPath);
        return lodash_1.isUndefined(processValue) ? defaultValue : processValue;
    }
    static get(propertyPath, defaultValue) {
        const processValue = lodash_1.get(this.configStatic, propertyPath);
        return lodash_1.isUndefined(processValue) ? defaultValue : processValue;
    }
    getVersion() {
        if (!process.env.APP_VERSION) {
            if (packageJson && packageJson.version) {
                process.env.APP_VERSION = packageJson.version;
            }
        }
        return process.env.APP_VERSION;
    }
    isProd() {
        return this.config.production;
    }
    getAllowWhitelist() {
        return this.config.ALLOW_WHITE_LIST ? this.config.ALLOW_WHITE_LIST : [];
    }
};
ConfigService.configStatic = environment_1.environment;
ConfigService = ConfigService_1 = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], ConfigService);
exports.ConfigService = ConfigService;


/***/ }),
/* 55 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 56 */
/***/ (function(module) {

module.exports = JSON.parse("{\"name\":\"nestjs-core\",\"version\":\"0.0.1\",\"description\":\"\",\"author\":\"\",\"private\":true,\"license\":\"UNLICENSED\",\"scripts\":{\"prebuild\":\"rimraf dist\",\"build\":\"nest build\",\"format\":\"prettier --write \\\"src/**/*.ts\\\" \\\"test/**/*.ts\\\" \\\"libs/**/*.ts\\\"\",\"start\":\"nest start\",\"start:dev\":\"nest start --watch\",\"start:debug\":\"nest start --debug --watch\",\"start:prod\":\"node dist/main\",\"lint\":\"eslint \\\"{src,apps,libs,test}/**/*.ts\\\" --fix\",\"test\":\"jest\",\"test:watch\":\"jest --watch\",\"test:cov\":\"jest --coverage\",\"test:debug\":\"node --inspect-brk -r tsconfig-paths/register -r ts-node/register node_modules/.bin/jest --runInBand\",\"test:e2e\":\"jest --config ./test/jest-e2e.json\"},\"dependencies\":{\"@godaddy/terminus\":\"4.3.1\",\"@nestjs/bull\":\"^0.1.2\",\"@nestjs/common\":\"7.0.7\",\"@nestjs/config\":\"^0.4.0\",\"@nestjs/core\":\"7.0.7\",\"@nestjs/cqrs\":\"6.1.0\",\"@nestjs/jwt\":\"^7.0.0\",\"@nestjs/passport\":\"^7.0.0\",\"@nestjs/platform-express\":\"7.0.7\",\"@nestjs/platform-socket.io\":\"7.0.7\",\"@nestjs/swagger\":\"4.5.1\",\"@nestjs/terminus\":\"7.0.0\",\"@nestjs/typeorm\":\"7.0.0\",\"@nestjs/websockets\":\"7.0.7\",\"aws-sdk\":\"^2.691.0\",\"bcrypt\":\"^4.0.1\",\"bull\":\"^3.18.0\",\"cache-manager\":\"3.2.1\",\"casbin\":\"^5.0.0\",\"class-transformer\":\"0.2.3\",\"class-validator\":\"0.12.0-refactor.5\",\"cli-ux\":\"4.9.3\",\"cls-hooked\":\"4.2.2\",\"convert-excel-to-json\":\"^1.7.0\",\"ean13-lib\":\"0.0.5\",\"excel4node\":\"^1.7.2\",\"faker\":\"^4.1.0\",\"helmet\":\"3.22.0\",\"kubernetes-client\":\"8.3.7\",\"moment\":\"^2.27.0\",\"multer\":\"^1.4.2\",\"multer-s3\":\"^2.9.0\",\"nest-router\":\"1.0.9\",\"nestjs-i18n\":\"^8.0.2\",\"nestjs-pino\":\"^1.2.0\",\"node-epc\":\"^0.1.1\",\"nodemailer\":\"6.4.6\",\"passport\":\"^0.4.1\",\"passport-jwt\":\"4.0.0\",\"passport-local\":\"^1.0.0\",\"pg\":\"8.0.2\",\"pug\":\"2.0.4\",\"reflect-metadata\":\"0.1.13\",\"rimraf\":\"^3.0.2\",\"rxjs\":\"6.5.5\",\"sharp\":\"^0.25.2\",\"swagger-ui-express\":\"4.1.4\",\"twilio\":\"^3.46.0\",\"typeorm\":\"0.2.24\",\"typeorm-adapter\":\"^1.1.0\",\"typescript\":\"~3.8.3\",\"typescript-require\":\"^0.2.10\",\"ua-parser-js\":\"^0.7.21\",\"web-push\":\"3.4.3\"},\"devDependencies\":{\"@nestjs/cli\":\"^7.0.0\",\"@nestjs/schematics\":\"^7.0.0\",\"@nestjs/testing\":\"^7.0.0\",\"@types/bull\":\"^3.14.1\",\"@types/express\":\"^4.17.3\",\"@types/helmet\":\"0.0.45\",\"@types/jest\":\"25.1.4\",\"@types/node\":\"^13.9.1\",\"@types/nodemailer\":\"6.4.0\",\"@types/passport\":\"1.0.3\",\"@types/passport-jwt\":\"3.0.3\",\"@types/passport-local\":\"^1.0.33\",\"@types/socket.io\":\"2.1.4\",\"@types/supertest\":\"2.0.8\",\"@types/web-push\":\"3.3.0\",\"@typescript-eslint/eslint-plugin\":\"^2.23.0\",\"@typescript-eslint/parser\":\"^2.23.0\",\"dotenv\":\"^8.2.0\",\"eslint\":\"^6.8.0\",\"eslint-config-prettier\":\"^6.10.0\",\"eslint-plugin-import\":\"^2.20.1\",\"jest\":\"^25.1.0\",\"prettier\":\"^1.19.1\",\"supertest\":\"^4.0.2\",\"ts-jest\":\"25.2.1\",\"ts-loader\":\"^6.2.1\",\"ts-node\":\"^8.8.2\",\"tsconfig-paths\":\"^3.9.0\",\"typescript\":\"^3.7.4\"},\"jest\":{\"moduleFileExtensions\":[\"js\",\"json\",\"ts\"],\"rootDir\":\".\",\"testRegex\":\".spec.ts$\",\"transform\":{\"^.+\\\\.(t|j)s$\":\"ts-jest\"},\"coverageDirectory\":\"./coverage\",\"testEnvironment\":\"node\",\"roots\":[\"<rootDir>/src/\",\"<rootDir>/libs/\"],\"moduleNameMapper\":{\"@nest-core/models/(.*)\":\"<rootDir>/libs/models/src/$1\",\"@nest-core/models\":\"<rootDir>/libs/models/src\",\"@nest-core/shared/(.*)\":\"<rootDir>/libs/shared/src/$1\",\"@nest-core/shared\":\"<rootDir>/libs/shared/src\",\"@nest-core/common/(.*)\":\"<rootDir>/libs/common/src/$1\",\"@nest-core/common\":\"<rootDir>/libs/common/src\",\"@nest-core/config/(.*)\":\"<rootDir>/libs/config/src/$1\",\"@nest-core/config\":\"<rootDir>/libs/config/src\",\"@nest-core/core/(.*)\":\"<rootDir>/libs/core/src/$1\",\"@nest-core/core\":\"<rootDir>/libs/core/src\"}}}");

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = require("bcrypt");

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(59));
__export(__webpack_require__(60));
__export(__webpack_require__(61));
__export(__webpack_require__(62));
__export(__webpack_require__(63));
__export(__webpack_require__(64));
__export(__webpack_require__(65));


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AllowEnum;
(function (AllowEnum) {
    AllowEnum["PUBLIC"] = "public";
    AllowEnum["WHITELIST"] = "whitelist";
})(AllowEnum = exports.AllowEnum || (exports.AllowEnum = {}));


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RolesEnum;
(function (RolesEnum) {
    RolesEnum["SELF"] = "SELF";
    RolesEnum["ADMIN"] = "ROLE_ADMIN";
    RolesEnum["USER"] = "ROLE_USER";
})(RolesEnum = exports.RolesEnum || (exports.RolesEnum = {}));


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Gender;
(function (Gender) {
    Gender["MALE"] = "male";
    Gender["FEMALE"] = "female";
    Gender["UNKNOW"] = "unknow";
})(Gender = exports.Gender || (exports.Gender = {}));


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ImageType;
(function (ImageType) {
    ImageType["Profile"] = "profile";
    ImageType["Icon"] = "icon";
    ImageType["Avatar"] = "avatar";
})(ImageType = exports.ImageType || (exports.ImageType = {}));


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ZoneType;
(function (ZoneType) {
    ZoneType["DMZ"] = "DMZ";
    ZoneType["Core"] = "Core";
})(ZoneType = exports.ZoneType || (exports.ZoneType = {}));


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var EnvironmentType;
(function (EnvironmentType) {
    EnvironmentType["Prod"] = "Prod";
    EnvironmentType["NonProd"] = "NonProd";
})(EnvironmentType = exports.EnvironmentType || (exports.EnvironmentType = {}));


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AccountSourceType;
(function (AccountSourceType) {
    AccountSourceType[AccountSourceType["bsId"] = 0] = "bsId";
    AccountSourceType[AccountSourceType["hsId"] = 1] = "hsId";
    AccountSourceType[AccountSourceType["gitHub"] = 2] = "gitHub";
})(AccountSourceType = exports.AccountSourceType || (exports.AccountSourceType = {}));


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const cqrs_1 = __webpack_require__(30);
const config_1 = __webpack_require__(52);
const cqrs_gateway_1 = __webpack_require__(28);
const nestjs_pino_1 = __webpack_require__(67);
const logger_1 = __webpack_require__(68);
let SharedModule = class SharedModule {
};
SharedModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [
            cqrs_1.CqrsModule,
            config_1.ConfigModule,
            nestjs_pino_1.LoggerModule.forRoot()
        ],
        providers: [
            cqrs_gateway_1.CQRSGateway,
            config_1.ConfigService,
            logger_1.PinoAppLoggerService
        ],
        exports: [
            cqrs_gateway_1.CQRSGateway,
            config_1.ConfigService,
            logger_1.PinoAppLoggerService
        ],
    })
], SharedModule);
exports.SharedModule = SharedModule;


/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports = require("nestjs-pino");

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(69));
__export(__webpack_require__(70));


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
class AbstractAppLoggerService extends common_1.Logger {
    constructor(logger) {
        super(null, true);
        this.logger = logger;
    }
    log(message) {
        this.logger.log(message, this.context);
    }
    error(message, trace) {
        this.logger.error(message, trace, this.context);
    }
    warn(message) {
        this.logger.warn(message, this.context);
    }
    debug(message) {
        this.logger.debug(message, this.context);
    }
    verbose(message) {
        this.logger.verbose(message, this.context);
    }
}
exports.AbstractAppLoggerService = AbstractAppLoggerService;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const nestjs_pino_1 = __webpack_require__(67);
const abstract_app_logger_service_1 = __webpack_require__(69);
let PinoAppLoggerService = class PinoAppLoggerService extends abstract_app_logger_service_1.AbstractAppLoggerService {
    constructor(logger) {
        super(logger);
    }
};
PinoAppLoggerService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof nestjs_pino_1.Logger !== "undefined" && nestjs_pino_1.Logger) === "function" ? _a : Object])
], PinoAppLoggerService);
exports.PinoAppLoggerService = PinoAppLoggerService;


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(72));


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(74));


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const services_1 = __webpack_require__(50);
let PlainToClassPipe = class PlainToClassPipe {
    transform(value, metadata) {
        return __awaiter(this, void 0, void 0, function* () {
            return services_1.UtilsService.plainToClass(metadata.metatype, value);
        });
    }
};
PlainToClassPipe = __decorate([
    common_1.Injectable()
], PlainToClassPipe);
exports.PlainToClassPipe = PlainToClassPipe;


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(76));
__export(__webpack_require__(79));
__export(__webpack_require__(85));


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(77));
__export(__webpack_require__(78));


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(80));
__export(__webpack_require__(81));
__export(__webpack_require__(82));
__export(__webpack_require__(83));
__export(__webpack_require__(84));


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(86));
__export(__webpack_require__(87));
__export(__webpack_require__(88));
__export(__webpack_require__(89));
__export(__webpack_require__(90));
__export(__webpack_require__(91));


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(93));


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const api_error_code_1 = __webpack_require__(94);
class AbstractDto {
    getErrorCodeMapping() {
        return api_error_code_1.ruleToCodeValidation[this.schemaCode] ? api_error_code_1.ruleToCodeValidation[this.schemaCode] : api_error_code_1.ruleToCodeValidation[api_error_code_1.SchemaCodeEnum.common];
    }
    ;
}
exports.AbstractDto = AbstractDto;


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(95));
__export(__webpack_require__(96));
__export(__webpack_require__(102));
__export(__webpack_require__(98));
__export(__webpack_require__(105));


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const api_error_code_service_1 = __webpack_require__(96);
let ApiErrorCodeModule = class ApiErrorCodeModule {
};
ApiErrorCodeModule = __decorate([
    common_1.Global(),
    common_1.Module({
        providers: [api_error_code_service_1.ApiErrorCodeService],
        exports: [api_error_code_service_1.ApiErrorCodeService],
    })
], ApiErrorCodeModule);
exports.ApiErrorCodeModule = ApiErrorCodeModule;


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ApiErrorCodeService_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const lodash_1 = __webpack_require__(55);
const format = __webpack_require__(97);
const enums_1 = __webpack_require__(98);
const resources = __webpack_require__(102);
const { en } = resources;
let ApiErrorCodeService = ApiErrorCodeService_1 = class ApiErrorCodeService {
    constructor() {
        this.logger = new common_1.Logger(ApiErrorCodeService_1.name);
        this.logger.log('ApiErrorCodeService');
    }
    static getInstance() {
        if (!ApiErrorCodeService_1.instance) {
            ApiErrorCodeService_1.instance = new ApiErrorCodeService_1();
        }
        return ApiErrorCodeService_1.instance;
    }
    static setSource(lang) {
        ApiErrorCodeService_1.lang = lang;
        ApiErrorCodeService_1.source = ApiErrorCodeService_1.getSource();
    }
    static getSource() {
        return resources[ApiErrorCodeService_1.lang];
    }
    static translate(data, options) {
        if (lodash_1.isArray(data)) {
            return ApiErrorCodeService_1.translateMulti(data);
        }
        else if (typeof data === 'object') {
            return ApiErrorCodeService_1.translateObject(data);
        }
        else {
            return ApiErrorCodeService_1.translateOne(data, options);
        }
    }
    static translateOne(code, options) {
        const message = options ? format(ApiErrorCodeService_1.source.get(code), options) : ApiErrorCodeService_1.source.get(code);
        return {
            code,
            message,
            options: options ? options : null
        };
    }
    static translateMulti(codes) {
        return codes.map(codeTranslate => {
            const [code, options] = codeTranslate;
            return ApiErrorCodeService_1.translateOne(code, options);
        });
    }
    static translateObject(codes) {
        let errors = [];
        const errorLine = ApiErrorCodeService_1.source.get(enums_1.CodeEnum.ERROR_LINE);
        codes.error.forEach(err => {
            const message = {
                code: err.errorCode,
                message: `${errorLine} ${err.line}: ${ApiErrorCodeService_1.source.get(err.errorCode)}`
            };
            errors.push(message);
        });
        return errors;
    }
    test() {
        ApiErrorCodeService_1.setSource('en');
        this.logger.log([
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.USER_EMAIL_INVALID),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.USER_EMAIL_REQUIRED),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.AUTH_PASSWORD_REQUIRED),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.X_RATELIMIT_LIMIT, { limit: '10 request/s' }),
        ]);
        ApiErrorCodeService_1.setSource('vi');
        this.logger.log([
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.USER_EMAIL_INVALID),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.USER_EMAIL_REQUIRED),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.AUTH_PASSWORD_REQUIRED),
            ApiErrorCodeService_1.translate(enums_1.CodeEnum.X_RATELIMIT_LIMIT, { limit: '10 truy cập /s' }),
        ]);
    }
    getErrorMessage(code, options) {
        ApiErrorCodeService_1.setSource(ApiErrorCodeService_1.lang);
        const message = options ? format(ApiErrorCodeService_1.source.get(code), options) : ApiErrorCodeService_1.source.get(code);
        return {
            code,
            message
        };
    }
};
ApiErrorCodeService.lang = 'en';
ApiErrorCodeService.source = en;
ApiErrorCodeService = ApiErrorCodeService_1 = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], ApiErrorCodeService);
exports.ApiErrorCodeService = ApiErrorCodeService;


/***/ }),
/* 97 */
/***/ (function(module, exports) {

module.exports = require("string-format");

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(99));
__export(__webpack_require__(100));
__export(__webpack_require__(101));


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CodeEnum;
(function (CodeEnum) {
    CodeEnum[CodeEnum["IS_UUID"] = 1] = "IS_UUID";
    CodeEnum[CodeEnum["QUERY_FAILED_ERROR"] = 2] = "QUERY_FAILED_ERROR";
    CodeEnum[CodeEnum["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    CodeEnum[CodeEnum["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    CodeEnum[CodeEnum["FORBIDDEN"] = 403] = "FORBIDDEN";
    CodeEnum[CodeEnum["NOT_FOUND"] = 404] = "NOT_FOUND";
    CodeEnum[CodeEnum["REQUEST_TIMEOUT"] = 408] = "REQUEST_TIMEOUT";
    CodeEnum[CodeEnum["INTERNAL_SERVER"] = 500] = "INTERNAL_SERVER";
    CodeEnum[CodeEnum["SERVICE_UNAVAILABLE"] = 503] = "SERVICE_UNAVAILABLE";
    CodeEnum[CodeEnum["USER_EMAIL_REQUIRED"] = 10000] = "USER_EMAIL_REQUIRED";
    CodeEnum[CodeEnum["USER_EMAIL_INVALID"] = 10001] = "USER_EMAIL_INVALID";
    CodeEnum[CodeEnum["USER_EMAIL_LENGTH"] = 10002] = "USER_EMAIL_LENGTH";
    CodeEnum[CodeEnum["USER_FIRSTNAME_REQUIRED"] = 10003] = "USER_FIRSTNAME_REQUIRED";
    CodeEnum[CodeEnum["USER_FIRSTNAME_LENGTH"] = 10004] = "USER_FIRSTNAME_LENGTH";
    CodeEnum[CodeEnum["USER_LASTNAME_REQUIRED"] = 10005] = "USER_LASTNAME_REQUIRED";
    CodeEnum[CodeEnum["USER_LASTNAME_LENGTH"] = 10006] = "USER_LASTNAME_LENGTH";
    CodeEnum[CodeEnum["USER_USERNAME_ASCII"] = 10007] = "USER_USERNAME_ASCII";
    CodeEnum[CodeEnum["USER_USERNAME_REQUIRED"] = 10008] = "USER_USERNAME_REQUIRED";
    CodeEnum[CodeEnum["USER_USERNAME_LENGTH"] = 10009] = "USER_USERNAME_LENGTH";
    CodeEnum[CodeEnum["USER_EMAIL_UNIQUE"] = 100010] = "USER_EMAIL_UNIQUE";
    CodeEnum[CodeEnum["USER_USERNAME_UNIQUE"] = 100011] = "USER_USERNAME_UNIQUE";
    CodeEnum[CodeEnum["USER_NOT_EXISTED"] = 100012] = "USER_NOT_EXISTED";
    CodeEnum[CodeEnum["NAME_LENGTH"] = 100013] = "NAME_LENGTH";
    CodeEnum[CodeEnum["NAME_REQUIRED"] = 100014] = "NAME_REQUIRED";
    CodeEnum[CodeEnum["USER_PHONENUMBER_UNIQUE"] = 100015] = "USER_PHONENUMBER_UNIQUE";
    CodeEnum[CodeEnum["USER_PHONENUMBER_INVALID"] = 100016] = "USER_PHONENUMBER_INVALID";
    CodeEnum[CodeEnum["AUTH_PASSWORD_REQUIRED"] = 11000] = "AUTH_PASSWORD_REQUIRED";
    CodeEnum[CodeEnum["AUTH_PASSWORD_LENGTH"] = 11001] = "AUTH_PASSWORD_LENGTH";
    CodeEnum[CodeEnum["AUTH_USER_NOTFOUND"] = 11002] = "AUTH_USER_NOTFOUND";
    CodeEnum[CodeEnum["AUTH_WRONG_PASSWORD"] = 11003] = "AUTH_WRONG_PASSWORD";
    CodeEnum[CodeEnum["CONFIRM_FIRST_LOGIN"] = 11004] = "CONFIRM_FIRST_LOGIN";
    CodeEnum[CodeEnum["AUTH_PASSWORD_ISSTRING"] = 11005] = "AUTH_PASSWORD_ISSTRING";
    CodeEnum[CodeEnum["AUTH_NEWPASSWORD_REQUIRED"] = 11006] = "AUTH_NEWPASSWORD_REQUIRED";
    CodeEnum[CodeEnum["AUTH_NEWPASSWORD_LENGTH"] = 11007] = "AUTH_NEWPASSWORD_LENGTH";
    CodeEnum[CodeEnum["AUTH_NEWPASSWORD_ISSTRING"] = 11008] = "AUTH_NEWPASSWORD_ISSTRING";
    CodeEnum[CodeEnum["AUTH_CONFIRMPASSWORD_REQUIRED"] = 11009] = "AUTH_CONFIRMPASSWORD_REQUIRED";
    CodeEnum[CodeEnum["AUTH_CONFIRMPASSWORD_LENGTH"] = 11010] = "AUTH_CONFIRMPASSWORD_LENGTH";
    CodeEnum[CodeEnum["AUTH_CONFIRMPASSWORD_ISSTRING"] = 11011] = "AUTH_CONFIRMPASSWORD_ISSTRING";
    CodeEnum[CodeEnum["AUTH_CONFIRMPASSWORD_INCORRECT"] = 11012] = "AUTH_CONFIRMPASSWORD_INCORRECT";
    CodeEnum[CodeEnum["AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD"] = 11013] = "AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD";
    CodeEnum[CodeEnum["AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL"] = 11014] = "AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL";
    CodeEnum[CodeEnum["AUTH_CURRENT_PASSWORD_INCORRECT"] = 11015] = "AUTH_CURRENT_PASSWORD_INCORRECT";
    CodeEnum[CodeEnum["AUTH_CONFIRM_FORGOT_PASSWORD_OTP"] = 11016] = "AUTH_CONFIRM_FORGOT_PASSWORD_OTP";
    CodeEnum[CodeEnum["AUTH_TOKEN_INVALID"] = 11017] = "AUTH_TOKEN_INVALID";
    CodeEnum[CodeEnum["X_RATELIMIT_LIMIT"] = 12000] = "X_RATELIMIT_LIMIT";
    CodeEnum[CodeEnum["CATEGORY_EXISTED"] = 13001] = "CATEGORY_EXISTED";
    CodeEnum[CodeEnum["CATEGORY_NOT_EXISTED"] = 13002] = "CATEGORY_NOT_EXISTED";
    CodeEnum[CodeEnum["SUBCATEGORY_EXISTED"] = 13003] = "SUBCATEGORY_EXISTED";
    CodeEnum[CodeEnum["BARCODE_EXISTED"] = 13004] = "BARCODE_EXISTED";
    CodeEnum[CodeEnum["BARCODE_LENGTH"] = 13005] = "BARCODE_LENGTH";
    CodeEnum[CodeEnum["SKU_EXISTED"] = 13006] = "SKU_EXISTED";
    CodeEnum[CodeEnum["PRODUCT_EXISTED"] = 13007] = "PRODUCT_EXISTED";
    CodeEnum[CodeEnum["PRODUCT_NOT_EXISTED"] = 13008] = "PRODUCT_NOT_EXISTED";
    CodeEnum[CodeEnum["CODE_NOT_EXISTED"] = 13009] = "CODE_NOT_EXISTED";
    CodeEnum[CodeEnum["EXPIRED_CODE"] = 13010] = "EXPIRED_CODE";
    CodeEnum[CodeEnum["FLAG_NOT_EXISTED"] = 13011] = "FLAG_NOT_EXISTED";
    CodeEnum[CodeEnum["COMPANY_ADDRESS_NOT_EXISTED"] = 13012] = "COMPANY_ADDRESS_NOT_EXISTED";
    CodeEnum[CodeEnum["QUANTITY_GREATER_ZERO"] = 13013] = "QUANTITY_GREATER_ZERO";
    CodeEnum[CodeEnum["PRICE_GREATER_ZERO"] = 13014] = "PRICE_GREATER_ZERO";
    CodeEnum[CodeEnum["BACKROOM_GREATER_ZERO"] = 13015] = "BACKROOM_GREATER_ZERO";
    CodeEnum[CodeEnum["LOCATION_NOT_EXISTED"] = 13016] = "LOCATION_NOT_EXISTED";
    CodeEnum[CodeEnum["COUNT_MUST_GREATER_1"] = 13017] = "COUNT_MUST_GREATER_1";
    CodeEnum[CodeEnum["COMPANY_NOT_EXISTED"] = 13018] = "COMPANY_NOT_EXISTED";
    CodeEnum[CodeEnum["ACCOUNT_MUST_PHONE_OR_EMAIL"] = 13019] = "ACCOUNT_MUST_PHONE_OR_EMAIL";
    CodeEnum[CodeEnum["OPTION_NOT_EXISTED"] = 13020] = "OPTION_NOT_EXISTED";
    CodeEnum[CodeEnum["NUMBER_PHONE_WRONG"] = 13021] = "NUMBER_PHONE_WRONG";
    CodeEnum[CodeEnum["EMAIL_WRONG"] = 13022] = "EMAIL_WRONG";
    CodeEnum[CodeEnum["BARCODE_NOT_EXISTED"] = 13023] = "BARCODE_NOT_EXISTED";
    CodeEnum[CodeEnum["VERIFIED_EMAIL"] = 13024] = "VERIFIED_EMAIL";
    CodeEnum[CodeEnum["VERIFIED_NUMBER_PHONE"] = 13025] = "VERIFIED_NUMBER_PHONE";
    CodeEnum[CodeEnum["CONFIRM_CHANGE_NUMBER_PHONE"] = 13026] = "CONFIRM_CHANGE_NUMBER_PHONE";
    CodeEnum[CodeEnum["CONFIRM_CHANGE_EMAIL"] = 13027] = "CONFIRM_CHANGE_EMAIL";
    CodeEnum[CodeEnum["PRODUCT_LIMIT_IMAGES"] = 13028] = "PRODUCT_LIMIT_IMAGES";
    CodeEnum[CodeEnum["IMPORT_FILE_ERROR"] = 13029] = "IMPORT_FILE_ERROR";
    CodeEnum[CodeEnum["COMPANY_LIMIT_LOGO"] = 13030] = "COMPANY_LIMIT_LOGO";
    CodeEnum[CodeEnum["CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL"] = 13031] = "CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL";
    CodeEnum[CodeEnum["LOCATION_NAME_EXISTED"] = 13032] = "LOCATION_NAME_EXISTED";
    CodeEnum[CodeEnum["LOCATION_ADDRESS_EXISTED"] = 13033] = "LOCATION_ADDRESS_EXISTED";
    CodeEnum[CodeEnum["ERROR_LINE"] = 13034] = "ERROR_LINE";
    CodeEnum[CodeEnum["EMAIL_EXISTED"] = 13035] = "EMAIL_EXISTED";
    CodeEnum[CodeEnum["NUMBER_PHONE_EXISTED"] = 13036] = "NUMBER_PHONE_EXISTED";
    CodeEnum[CodeEnum["VERIFIED_ACCOUNT"] = 13037] = "VERIFIED_ACCOUNT";
    CodeEnum[CodeEnum["OPTION_NAME_EXISTED"] = 13038] = "OPTION_NAME_EXISTED";
    CodeEnum[CodeEnum["COMPANY_BUSINESSNAME_EXISTED"] = 13039] = "COMPANY_BUSINESSNAME_EXISTED";
    CodeEnum[CodeEnum["LOCATION_INVALID_NAME"] = 13040] = "LOCATION_INVALID_NAME";
    CodeEnum[CodeEnum["CODE_STILL_VALID"] = 13041] = "CODE_STILL_VALID";
    CodeEnum[CodeEnum["COMPANY_NAME_EXISTED"] = 13042] = "COMPANY_NAME_EXISTED";
    CodeEnum[CodeEnum["BUSSINESS_NAME_LENGTH"] = 13043] = "BUSSINESS_NAME_LENGTH";
    CodeEnum[CodeEnum["LOCATION_NAME_LENGTH"] = 13044] = "LOCATION_NAME_LENGTH";
    CodeEnum[CodeEnum["LOCATION_ADDRESS_LENGTH"] = 13045] = "LOCATION_ADDRESS_LENGTH";
    CodeEnum[CodeEnum["NUMBER_PHONE_NOT_EXISTED"] = 13046] = "NUMBER_PHONE_NOT_EXISTED";
    CodeEnum[CodeEnum["EMAIL_NOT_EXISTED"] = 13047] = "EMAIL_NOT_EXISTED";
    CodeEnum[CodeEnum["CODE_INCORRECT"] = 13048] = "CODE_INCORRECT";
    CodeEnum[CodeEnum["LOGIN_USER_MUST_PHONE_OR_EMAIL"] = 13049] = "LOGIN_USER_MUST_PHONE_OR_EMAIL";
    CodeEnum[CodeEnum["FULLNAME_EXISTED"] = 13050] = "FULLNAME_EXISTED";
    CodeEnum[CodeEnum["SIGNUP_PASSWORD_INVALID"] = 13051] = "SIGNUP_PASSWORD_INVALID";
    CodeEnum[CodeEnum["BUSSINESS_NAME_REQUIRED"] = 13052] = "BUSSINESS_NAME_REQUIRED";
    CodeEnum[CodeEnum["BUSSINESS_NAME_ISSTRING"] = 13053] = "BUSSINESS_NAME_ISSTRING";
    CodeEnum[CodeEnum["CATEGORY_NAME_EXISTED"] = 13054] = "CATEGORY_NAME_EXISTED";
    CodeEnum[CodeEnum["NAME_INVALID"] = 13055] = "NAME_INVALID";
    CodeEnum[CodeEnum["COMPANY_CITY_LENGTH"] = 13056] = "COMPANY_CITY_LENGTH";
})(CodeEnum = exports.CodeEnum || (exports.CodeEnum = {}));


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LangEnum;
(function (LangEnum) {
    LangEnum[LangEnum["en"] = 0] = "en";
    LangEnum[LangEnum["vi"] = 1] = "vi";
})(LangEnum = exports.LangEnum || (exports.LangEnum = {}));


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SchemaCodeEnum;
(function (SchemaCodeEnum) {
    SchemaCodeEnum["common"] = "common";
})(SchemaCodeEnum = exports.SchemaCodeEnum || (exports.SchemaCodeEnum = {}));


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(103));
__export(__webpack_require__(104));


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const code_enum_1 = __webpack_require__(99);
exports.en = new Map([
    [code_enum_1.CodeEnum.IS_UUID, 'Checks if the ID is a UUID (version 3, 4, 5 or all )'],
    [code_enum_1.CodeEnum.FORBIDDEN, 'Access forbidden'],
    [code_enum_1.CodeEnum.UNAUTHORIZED, 'Caller is not authenticated'],
    [code_enum_1.CodeEnum.NOT_FOUND, 'Resource not found'],
    [code_enum_1.CodeEnum.REQUEST_TIMEOUT, 'Request time'],
    [code_enum_1.CodeEnum.INTERNAL_SERVER, 'Internal server'],
    [code_enum_1.CodeEnum.SERVICE_UNAVAILABLE, 'Service unavailable'],
    [code_enum_1.CodeEnum.USER_EMAIL_REQUIRED, 'Email is required'],
    [code_enum_1.CodeEnum.USER_EMAIL_INVALID, 'Your Email address is invalid. Please check again'],
    [code_enum_1.CodeEnum.USER_EMAIL_LENGTH, 'Email\'s length must in a range [{min}, {max}]'],
    [code_enum_1.CodeEnum.USER_FIRSTNAME_REQUIRED, 'The Full name is required'],
    [code_enum_1.CodeEnum.USER_FIRSTNAME_LENGTH, 'The Full name field must not be more 128 characters'],
    [code_enum_1.CodeEnum.USER_LASTNAME_REQUIRED, 'LastName is required'],
    [code_enum_1.CodeEnum.USER_LASTNAME_LENGTH, 'LastName\'s length must in a range [{min}, {max}]'],
    [code_enum_1.CodeEnum.USER_USERNAME_ASCII, 'UserName must be ascii'],
    [code_enum_1.CodeEnum.USER_USERNAME_REQUIRED, 'UserName is required'],
    [code_enum_1.CodeEnum.USER_USERNAME_LENGTH, 'UserName\'s length must in a range [{min}, {max}]'],
    [code_enum_1.CodeEnum.USER_EMAIL_UNIQUE, 'Your Email address is not exist. Please check again'],
    [code_enum_1.CodeEnum.USER_USERNAME_UNIQUE, 'This username already exists in system!'],
    [code_enum_1.CodeEnum.USER_NOT_EXISTED, 'User is not existed!'],
    [code_enum_1.CodeEnum.USER_PHONENUMBER_UNIQUE, 'This phone number already exists in system!'],
    [code_enum_1.CodeEnum.USER_PHONENUMBER_INVALID, 'Your Phone Number is invalid. Please check again'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_REQUIRED, 'Password is required'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_LENGTH, 'Your password is invalid. Please check again'],
    [code_enum_1.CodeEnum.AUTH_USER_NOTFOUND, 'User not found'],
    [code_enum_1.CodeEnum.AUTH_WRONG_PASSWORD, 'Your passowrd is invalid. Please check again'],
    [code_enum_1.CodeEnum.CONFIRM_FIRST_LOGIN, 'Please comfirm OTP in first login'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_ISSTRING, 'Password must be a string'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_REQUIRED, 'NewPassword is required'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_LENGTH, 'Your passowrd is invalid. Please check again'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_ISSTRING, 'NewPassword must be a string'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_REQUIRED, 'ConfirmPassword is required'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_LENGTH, 'ConfirmPassword\'s length must in a range [{min}, {max}]'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_ISSTRING, 'ConfirmPassword must be a string'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT, 'Confirm Password does not match'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD, 'NewPassword same with current Password'],
    [code_enum_1.CodeEnum.AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL, 'Resend OTP must email or number phone'],
    [code_enum_1.CodeEnum.AUTH_CURRENT_PASSWORD_INCORRECT, 'Your Current Password is incorrect. Please check again'],
    [code_enum_1.CodeEnum.AUTH_CONFIRM_FORGOT_PASSWORD_OTP, 'Please confirm OTP'],
    [code_enum_1.CodeEnum.AUTH_TOKEN_INVALID, 'Token is invalid or has expired'],
    [code_enum_1.CodeEnum.X_RATELIMIT_LIMIT, 'The throttle limit has been reached  {limit}'],
    [code_enum_1.CodeEnum.NAME_LENGTH, 'Name\'s length must in a range [{min}, {max}]'],
    [code_enum_1.CodeEnum.NAME_REQUIRED, 'Name is required'],
    [code_enum_1.CodeEnum.CATEGORY_EXISTED, 'The category is exist. Please check again'],
    [code_enum_1.CodeEnum.CATEGORY_NOT_EXISTED, 'Category is not existed'],
    [code_enum_1.CodeEnum.SUBCATEGORY_EXISTED, 'Sub-category is existed'],
    [code_enum_1.CodeEnum.BARCODE_EXISTED, 'The Barcode Number is exist. Please check again'],
    [code_enum_1.CodeEnum.BARCODE_LENGTH, 'Barcode must only 13 digits'],
    [code_enum_1.CodeEnum.SKU_EXISTED, 'SKU is existed'],
    [code_enum_1.CodeEnum.PRODUCT_EXISTED, 'Product is existed'],
    [code_enum_1.CodeEnum.PRODUCT_NOT_EXISTED, 'Product is not existed'],
    [code_enum_1.CodeEnum.CODE_NOT_EXISTED, 'Code is not existed'],
    [code_enum_1.CodeEnum.EXPIRED_CODE, 'The verification code is expired'],
    [code_enum_1.CodeEnum.FLAG_NOT_EXISTED, 'Flag is not existed'],
    [code_enum_1.CodeEnum.COMPANY_ADDRESS_NOT_EXISTED, 'Company address is not existed'],
    [code_enum_1.CodeEnum.QUANTITY_GREATER_ZERO, 'Quantity must greater than 0'],
    [code_enum_1.CodeEnum.PRICE_GREATER_ZERO, 'Price must greater than 0'],
    [code_enum_1.CodeEnum.BACKROOM_GREATER_ZERO, 'Backroom must greater than 0'],
    [code_enum_1.CodeEnum.LOCATION_NOT_EXISTED, 'Location is not existed'],
    [code_enum_1.CodeEnum.COUNT_MUST_GREATER_1, 'Count must greater than 1'],
    [code_enum_1.CodeEnum.COMPANY_NOT_EXISTED, 'Company is not existed'],
    [code_enum_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL, 'Register account must email or number phone'],
    [code_enum_1.CodeEnum.OPTION_NOT_EXISTED, 'Option is not existed'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_WRONG, 'Number phone is wrong format'],
    [code_enum_1.CodeEnum.EMAIL_WRONG, 'Email is wrong format'],
    [code_enum_1.CodeEnum.BARCODE_NOT_EXISTED, 'Barcode is not existed'],
    [code_enum_1.CodeEnum.VERIFIED_EMAIL, 'Please verify your email'],
    [code_enum_1.CodeEnum.VERIFIED_NUMBER_PHONE, 'Please verify your phone number'],
    [code_enum_1.CodeEnum.CONFIRM_CHANGE_EMAIL, 'Please confirm your change email'],
    [code_enum_1.CodeEnum.CONFIRM_CHANGE_NUMBER_PHONE, 'Please comfirm your change phone number'],
    [code_enum_1.CodeEnum.PRODUCT_LIMIT_IMAGES, 'Product only allow upload less 3 image'],
    [code_enum_1.CodeEnum.IMPORT_FILE_ERROR, 'Import file is corrupted'],
    [code_enum_1.CodeEnum.ERROR_LINE, 'Error in line'],
    [code_enum_1.CodeEnum.COMPANY_LIMIT_LOGO, 'Company logo only allow upload 1 image'],
    [code_enum_1.CodeEnum.CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL, 'Change profile user must email or number phone'],
    [code_enum_1.CodeEnum.LOCATION_NAME_EXISTED, 'The Location name is existed'],
    [code_enum_1.CodeEnum.LOCATION_ADDRESS_EXISTED, 'The Location address is existed'],
    [code_enum_1.CodeEnum.EMAIL_EXISTED, 'Your Email is existed. Please check again'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_EXISTED, 'Your Phone Number is existed. Please check again'],
    [code_enum_1.CodeEnum.VERIFIED_ACCOUNT, 'Please verify your account'],
    [code_enum_1.CodeEnum.OPTION_NAME_EXISTED, 'The Option name is existed'],
    [code_enum_1.CodeEnum.COMPANY_BUSINESSNAME_EXISTED, 'The Business name is exist. Please check again'],
    [code_enum_1.CodeEnum.COMPANY_NAME_EXISTED, 'The Company name is exist'],
    [code_enum_1.CodeEnum.LOCATION_INVALID_NAME, 'Please enter a valid Location name'],
    [code_enum_1.CodeEnum.CODE_STILL_VALID, 'Code is still valid'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_LENGTH, 'The Business name field must not be more 128 characters'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_REQUIRED, 'The Business name is required'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_ISSTRING, 'The Business name  must be a string'],
    [code_enum_1.CodeEnum.LOCATION_NAME_LENGTH, 'The Location name field must not be more 128 characters'],
    [code_enum_1.CodeEnum.LOCATION_ADDRESS_LENGTH, 'The Location address field must not be more 128 characters'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_NOT_EXISTED, 'Your Phone Number is not exist. Please check again.'],
    [code_enum_1.CodeEnum.EMAIL_NOT_EXISTED, 'Your Email address is not exist. Please check again.'],
    [code_enum_1.CodeEnum.CODE_INCORRECT, 'The verification code you entered is incorrect'],
    [code_enum_1.CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL, 'Login user must phone or email'],
    [code_enum_1.CodeEnum.FULLNAME_EXISTED, 'The Full name is exist. Please check again'],
    [code_enum_1.CodeEnum.SIGNUP_PASSWORD_INVALID, 'Password must be between 6 to 30 characters and at least 1 number'],
    [code_enum_1.CodeEnum.CATEGORY_NAME_EXISTED, 'The Name is exist. Please check again'],
    [code_enum_1.CodeEnum.NAME_INVALID, 'Please enter a valid Name'],
    [code_enum_1.CodeEnum.COMPANY_CITY_LENGTH, 'The City field must not be more 128 characters'],
]);


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const code_enum_1 = __webpack_require__(99);
exports.vi = new Map([
    [code_enum_1.CodeEnum.FORBIDDEN, 'FORBIDDEN 43'],
    [code_enum_1.CodeEnum.UNAUTHORIZED, 'UNAUTHORIZED 401'],
    [code_enum_1.CodeEnum.NOT_FOUND, 'NOT_FOUND 404'],
    [code_enum_1.CodeEnum.REQUEST_TIMEOUT, 'REQUEST_TIMEOUT 408'],
    [code_enum_1.CodeEnum.SERVICE_UNAVAILABLE, 'SERVICE_UNAVAILABLE 503'],
    [code_enum_1.CodeEnum.USER_EMAIL_REQUIRED, 'Email là bắt buộc'],
    [code_enum_1.CodeEnum.USER_EMAIL_INVALID, 'Địa chỉ email của bạn không hợp lệ. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.USER_EMAIL_LENGTH, 'Email length'],
    [code_enum_1.CodeEnum.USER_NOT_EXISTED, 'User không tồn tại!'],
    [code_enum_1.CodeEnum.USER_PHONENUMBER_INVALID, 'Số điện thoại của bạn không hợp lệ. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.X_RATELIMIT_LIMIT, 'Vượt quá số lượng request giới hạn {limit}'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_REQUIRED, 'Mật khẩu là bắt buộc'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_LENGTH, 'Mật khẩu dài giới hạn'],
    [code_enum_1.CodeEnum.CONFIRM_FIRST_LOGIN, 'Xác nhận OTP trong lần đầu đăng nhập'],
    [code_enum_1.CodeEnum.CATEGORY_EXISTED, 'Category đang tồn tại. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.CATEGORY_NOT_EXISTED, 'Category không tồn tại'],
    [code_enum_1.CodeEnum.SUBCATEGORY_EXISTED, 'Sub-category đã tồn tại'],
    [code_enum_1.CodeEnum.BARCODE_EXISTED, 'Barcode đang tồn tại. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.BARCODE_LENGTH, 'Barcode phải là 13 chữ số'],
    [code_enum_1.CodeEnum.AUTH_PASSWORD_ISSTRING, 'Mật khẩu phải là một chuỗi'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_REQUIRED, 'Mật khẩu mới là bắt buộc'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_LENGTH, 'Độ dài mật khẩu mới trong khoảng [{min}, {max}]'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_ISSTRING, 'Mật khẩu mới phải là một chuỗi'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_REQUIRED, 'Xác nhận mật khẩu là bắt buộc'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_LENGTH, 'Độ dài xác nhận mật khẩu trong khoảng [{min}, {max}]'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_ISSTRING, 'Xác nhận mật khẩu phải là một chuỗi'],
    [code_enum_1.CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT, 'Xác nhận mật khẩu không khớp'],
    [code_enum_1.CodeEnum.AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD, 'Mật khẩu mới trùng với mật khẩu hiện tại'],
    [code_enum_1.CodeEnum.AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL, 'Gửi lại OTP phải gửi email hoặc số điện thoại'],
    [code_enum_1.CodeEnum.AUTH_CURRENT_PASSWORD_INCORRECT, 'Mật khẩu hiện tại của bạn không chính xác. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.AUTH_CONFIRM_FORGOT_PASSWORD_OTP, 'Xác nhận OTP'],
    [code_enum_1.CodeEnum.AUTH_TOKEN_INVALID, 'Token thì không hợp lệ hoặc hết hạn.'],
    [code_enum_1.CodeEnum.SKU_EXISTED, 'SKU đã tồn tại'],
    [code_enum_1.CodeEnum.PRODUCT_EXISTED, 'Product đã tồn tại'],
    [code_enum_1.CodeEnum.PRODUCT_NOT_EXISTED, 'Product không tồn tại'],
    [code_enum_1.CodeEnum.CODE_NOT_EXISTED, 'Code không tồn tại'],
    [code_enum_1.CodeEnum.EXPIRED_CODE, 'Mã xác minh đã hết hạn'],
    [code_enum_1.CodeEnum.FLAG_NOT_EXISTED, 'Flag không tồn tại'],
    [code_enum_1.CodeEnum.COMPANY_ADDRESS_NOT_EXISTED, 'Compay address không tồn tại'],
    [code_enum_1.CodeEnum.QUANTITY_GREATER_ZERO, 'Số lượng phải lớn hơn 0'],
    [code_enum_1.CodeEnum.QUANTITY_GREATER_ZERO, 'Giá tiền phải lớn hơn 0'],
    [code_enum_1.CodeEnum.BACKROOM_GREATER_ZERO, 'Backroom phải lớn hơn 0'],
    [code_enum_1.CodeEnum.LOCATION_NOT_EXISTED, 'Vị trí không tồn tại'],
    [code_enum_1.CodeEnum.COUNT_MUST_GREATER_1, 'Count must greater than 1'],
    [code_enum_1.CodeEnum.COMPANY_NOT_EXISTED, 'Company không tồn tại'],
    [code_enum_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL, 'Tài khoản đăng ký phải là email hoặc số điện thoại'],
    [code_enum_1.CodeEnum.OPTION_NOT_EXISTED, 'Option không tồn tại'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_WRONG, 'Số điện thoại sai định dạng'],
    [code_enum_1.CodeEnum.EMAIL_WRONG, 'Email sai định dạng'],
    [code_enum_1.CodeEnum.BARCODE_NOT_EXISTED, 'Barcode không tồn tại'],
    [code_enum_1.CodeEnum.VERIFIED_EMAIL, 'Vui lòng xác minh email của bạn'],
    [code_enum_1.CodeEnum.VERIFIED_NUMBER_PHONE, 'Vui lòng xác minh số điện thoại của bạn'],
    [code_enum_1.CodeEnum.VERIFIED_EMAIL, 'Vui lòng xác nhận thay đổi email của bạn'],
    [code_enum_1.CodeEnum.VERIFIED_NUMBER_PHONE, 'Vui lòng xác nhận thay đổi số điện thoại của bạn'],
    [code_enum_1.CodeEnum.PRODUCT_LIMIT_IMAGES, 'Sản phẩm chỉ được tải lên tối đa 3 hình ảnh'],
    [code_enum_1.CodeEnum.IMPORT_FILE_ERROR, 'Nhập tập tin bị lỗi'],
    [code_enum_1.CodeEnum.ERROR_LINE, 'Lỗi dòng thứ'],
    [code_enum_1.CodeEnum.COMPANY_LIMIT_LOGO, 'Company logo chỉ được tải lên 1 hình ảnh'],
    [code_enum_1.CodeEnum.CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL, 'Thay đổi hồ sơ người dùng phải gửi email hoặc số điện thoại'],
    [code_enum_1.CodeEnum.LOCATION_NAME_EXISTED, 'Location name đã tồn tại.'],
    [code_enum_1.CodeEnum.LOCATION_ADDRESS_EXISTED, 'Location address đã tồn tại.'],
    [code_enum_1.CodeEnum.EMAIL_EXISTED, 'Email đã tồn tại.'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_EXISTED, 'Số điện thoại của bạn đã tồn tại. Vui lòng kiểm tra lại.'],
    [code_enum_1.CodeEnum.VERIFIED_ACCOUNT, 'Vui lòng xác minh tài khoản của bạn'],
    [code_enum_1.CodeEnum.OPTION_NAME_EXISTED, 'Option name đã tồn tại.'],
    [code_enum_1.CodeEnum.COMPANY_BUSINESSNAME_EXISTED, 'The Business name đã tồn tại'],
    [code_enum_1.CodeEnum.COMPANY_NAME_EXISTED, 'The Company name đã tồn tại'],
    [code_enum_1.CodeEnum.LOCATION_INVALID_NAME, 'Xin hãy điền Location name hợp lệ'],
    [code_enum_1.CodeEnum.CODE_STILL_VALID, 'Code vẫn còn hạn'],
    [code_enum_1.CodeEnum.NUMBER_PHONE_NOT_EXISTED, 'Số điện thoại của bạn không tồn tại. Vui lòng kiểm tra lại.'],
    [code_enum_1.CodeEnum.EMAIL_NOT_EXISTED, 'Địa chỉ email của bạn không tồn tại. Vui lòng kiểm tra lại.'],
    [code_enum_1.CodeEnum.CODE_INCORRECT, 'Mã xác minh bạn nhập không đúng'],
    [code_enum_1.CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL, 'Đăng nhập người dùng phải truyền số điện thoại hoặc email'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_LENGTH, 'The Business name field không được vượt quá 128 ký tự'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_REQUIRED, 'The Business name là bắt buộc'],
    [code_enum_1.CodeEnum.BUSSINESS_NAME_ISSTRING, 'The Business name phải là một chuỗi'],
    [code_enum_1.CodeEnum.LOCATION_NAME_LENGTH, 'The Location name field không được vượt quá 128 ký tự'],
    [code_enum_1.CodeEnum.LOCATION_ADDRESS_LENGTH, 'The Location address field không được vượt quá 128 ký tự'],
    [code_enum_1.CodeEnum.FULLNAME_EXISTED, 'The Full name đã tồn tại. Xin hãy kiểm tra lại'],
    [code_enum_1.CodeEnum.SIGNUP_PASSWORD_INVALID, 'Password phải từ 6 đến 30 ký tự và phải có ít nhất 1 số'],
    [code_enum_1.CodeEnum.CATEGORY_NAME_EXISTED, 'Tên là tồn tại. Vui lòng kiểm tra lại'],
    [code_enum_1.CodeEnum.NAME_INVALID, 'Xin vui lòng nhập vào một tên hợp lệ'],
    [code_enum_1.CodeEnum.COMPANY_CITY_LENGTH, 'The City field không được vượt quá 128 ký tự'],
]);


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(106));


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const enums_1 = __webpack_require__(98);
exports.ruleToCodeValidation = {
    [enums_1.SchemaCodeEnum.common]: {
        id: {
            isUuid: [enums_1.CodeEnum.IS_UUID, null]
        },
        email: {
            isNotEmpty: [enums_1.CodeEnum.USER_EMAIL_REQUIRED, null],
            isEmail: [enums_1.CodeEnum.USER_EMAIL_INVALID, null],
            length: [enums_1.CodeEnum.USER_EMAIL_LENGTH, { min: 6, max: 100 }],
            isEmailUnique: [enums_1.CodeEnum.USER_EMAIL_UNIQUE, null]
        },
        password: {
            isNotEmpty: [enums_1.CodeEnum.AUTH_PASSWORD_REQUIRED, null],
            length: [enums_1.CodeEnum.SIGNUP_PASSWORD_INVALID, null],
            isString: [enums_1.CodeEnum.AUTH_PASSWORD_ISSTRING, null]
        },
        newPassword: {
            isNotEmpty: [enums_1.CodeEnum.AUTH_NEWPASSWORD_REQUIRED, null],
            length: [enums_1.CodeEnum.SIGNUP_PASSWORD_INVALID, null],
            isString: [enums_1.CodeEnum.AUTH_NEWPASSWORD_ISSTRING, null]
        },
        confirmPassword: {
            isNotEmpty: [enums_1.CodeEnum.AUTH_CONFIRMPASSWORD_REQUIRED, null],
            length: [enums_1.CodeEnum.SIGNUP_PASSWORD_INVALID, null],
            isString: [enums_1.CodeEnum.AUTH_CONFIRMPASSWORD_ISSTRING, null]
        },
        firstName: {
            isNotEmpty: [enums_1.CodeEnum.USER_FIRSTNAME_REQUIRED, null],
            length: [enums_1.CodeEnum.USER_FIRSTNAME_LENGTH, { min: 1, max: 128 }]
        },
        lastName: {
            isNotEmpty: [enums_1.CodeEnum.USER_LASTNAME_REQUIRED, null],
            length: [enums_1.CodeEnum.USER_LASTNAME_LENGTH, { min: 6, max: 100 }]
        },
        username: {
            isAscii: [enums_1.CodeEnum.USER_USERNAME_ASCII, null],
            isNotEmpty: [enums_1.CodeEnum.USER_USERNAME_REQUIRED, null],
            length: [enums_1.CodeEnum.USER_USERNAME_LENGTH, { min: 6, max: 100 }],
            isUsernameUnique: [enums_1.CodeEnum.USER_USERNAME_UNIQUE, null]
        },
        name: {
            isNotEmpty: [enums_1.CodeEnum.NAME_REQUIRED, null],
            length: [enums_1.CodeEnum.NAME_INVALID, null]
        },
        businessName: {
            isNotEmpty: [enums_1.CodeEnum.BUSSINESS_NAME_REQUIRED, null],
            length: [enums_1.CodeEnum.BUSSINESS_NAME_LENGTH, { min: 1, max: 128 }],
            isString: [enums_1.CodeEnum.BUSSINESS_NAME_ISSTRING, null]
        },
        city: {
            maxLength: [enums_1.CodeEnum.COMPANY_CITY_LENGTH, null]
        },
    }
};


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(108));
__export(__webpack_require__(109));
__export(__webpack_require__(110));
__export(__webpack_require__(111));


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var OrderType;
(function (OrderType) {
    OrderType["ASC"] = "ASC";
    OrderType["DESC"] = "DESC";
})(OrderType = exports.OrderType || (exports.OrderType = {}));


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RoleType;
(function (RoleType) {
    RoleType["USER"] = "USER";
    RoleType["ADMIN"] = "ADMIN";
})(RoleType = exports.RoleType || (exports.RoleType = {}));


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.CRUD_OPTIONS_DTO_METADATA = 'crudOptionsDto';


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.FEAUTURE_NAME_METADATA = 'NESTJSX_FEAUTURE_NAME_METADATA';
exports.ACTION_NAME_METADATA = 'NESTJSX_ACTION_NAME_METADATA';
exports.OVERRIDE_METHOD_METADATA = 'NESTJSX_OVERRIDE_METHOD_METADATA';
exports.PARSED_BODY_METADATA = 'NESTJSX_PARSED_BODY_METADATA';
exports.PARSED_CRUD_REQUEST_KEY = 'NESTJSX_PARSED_CRUD_REQUEST_KEY';
exports.CRUD_OPTIONS_METADATA = 'NESTJSX_CRUD_OPTIONS_METADATA';
exports.CRUD_AUTH_OPTIONS_METADATA = 'NESTJSX_CRUD_AUTH_OPTIONS_METADATA';


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(113));


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const context_1 = __webpack_require__(10);
const services_1 = __webpack_require__(114);
const filters_1 = __webpack_require__(137);
const api_error_code_1 = __webpack_require__(94);
const shared_1 = __webpack_require__(25);
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const lodash_1 = __webpack_require__(55);
let AbstractApiController = class AbstractApiController extends services_1.ApiValidatorService {
    constructor() {
        super();
    }
    respondWithSuccess(data, options) {
        context_1.RequestContext.currentResponse()
            .status(common_1.HttpStatus.OK)
            .json({
            data: data,
            options: options,
            errors: null
        })
            .end();
    }
    respondWithErrors(errors) {
        context_1.RequestContext.currentResponse()
            .status(common_1.HttpStatus.BAD_REQUEST)
            .json({
            data: null,
            errors: errors
        });
    }
    respondWithErrorCodeNumber(code, options) {
        let codeError = options ? api_error_code_1.ApiErrorCodeService.translate(code, options) : api_error_code_1.ApiErrorCodeService.translate(code);
        if (!lodash_1.isArray(codeError)) {
            codeError = [codeError];
        }
        context_1.RequestContext.currentResponse()
            .status(common_1.HttpStatus.BAD_REQUEST)
            .json({
            data: null,
            errors: codeError
        });
    }
    respondWithErrorsOrSuccess(apiServiceResponse) {
        const { code, options, data } = apiServiceResponse;
        if (code) {
            this.respondWithErrorCodeNumber(code, options);
        }
        else {
            this.respondWithSuccess(data, options);
        }
    }
    respondWithErrorResourceNotfound() {
        this.respondWithErrorCodeNumber(api_error_code_1.CodeEnum.NOT_FOUND);
    }
};
AbstractApiController = __decorate([
    swagger_1.ApiBearerAuth(),
    common_1.UsePipes(shared_1.PlainToClassPipe),
    common_1.UseFilters(filters_1.AllApiExceptionsFilter),
    __metadata("design:paramtypes", [])
], AbstractApiController);
exports.AbstractApiController = AbstractApiController;


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(115));
__export(__webpack_require__(116));
__export(__webpack_require__(136));


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = __webpack_require__(31);
const common_1 = __webpack_require__(3);
class BaseRemoteService {
    constructor(http) {
        this.http = http;
        this.timeout = 10000;
    }
    unwrapData(response) {
        return response.data ? response.data : response;
    }
    handleError(error) {
        if (error.response) {
            this.logger.log(error.response.data);
            this.logger.log(error.response.status + '');
            console.log(error.response.headers);
            if (error.response.status === common_1.HttpStatus.NOT_FOUND) {
                return rxjs_1.throwError(new common_1.NotFoundException(error.response.data));
            }
            else if (error.response.status === common_1.HttpStatus.BAD_REQUEST) {
                return rxjs_1.throwError(new common_1.BadRequestException(error.response.data));
            }
            else {
                return rxjs_1.throwError(new common_1.HttpException(error.response.data, error.response.status));
            }
        }
        else if (error.request) {
            this.logger.log('Error' + error.request);
            return rxjs_1.throwError(new common_1.InternalServerErrorException(error.request));
        }
        else {
            return rxjs_1.throwError('Something bad happened; please try again later.');
        }
    }
    getHeaders(json = true) {
        const headers = {};
        if (json) {
            headers['Content-Type'] = 'application/json';
        }
        else {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
        return headers;
    }
}
exports.BaseRemoteService = BaseRemoteService;


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const crud_1 = __webpack_require__(14);
const entities_1 = __webpack_require__(118);
const repositories_1 = __webpack_require__(128);
const database_1 = __webpack_require__(130);
let AuthUserService = class AuthUserService extends crud_1.CrudService {
    constructor(userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }
    createOne(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.insert(user);
        });
    }
    changePassword(id, hashedPassword) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.findOne(id);
            user.password = hashedPassword;
            user.updatePasswordAt = new Date();
            return yield this.userRepository.save(user);
        });
    }
    findByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne({ email });
        });
    }
    findByUsername(username) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne({ username });
        });
    }
    findOneByPropertyValue(options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne(options);
        });
    }
};
AuthUserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(entities_1.UserEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof repositories_1.AbstractBaseRepository !== "undefined" && repositories_1.AbstractBaseRepository) === "function" ? _a : Object])
], AuthUserService);
exports.AuthUserService = AuthUserService;


/***/ }),
/* 117 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/typeorm");

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(119));
__export(__webpack_require__(121));
__export(__webpack_require__(125));
__export(__webpack_require__(127));
__export(__webpack_require__(123));
__export(__webpack_require__(122));
__export(__webpack_require__(126));
__export(__webpack_require__(124));


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
class AbstractEntity extends typeorm_1.BaseEntity {
}
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], AbstractEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.CreateDateColumn({
        type: 'timestamp without time zone',
        name: 'createdAt',
    }),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], AbstractEntity.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({
        type: 'timestamp without time zone',
        name: 'updatedAt',
    }),
    __metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], AbstractEntity.prototype, "updatedAt", void 0);
exports.AbstractEntity = AbstractEntity;


/***/ }),
/* 120 */
/***/ (function(module, exports) {

module.exports = require("typeorm");

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const context_1 = __webpack_require__(10);
const abstract_entity_1 = __webpack_require__(119);
const user_entity_1 = __webpack_require__(122);
class AbstractAuditEntity extends abstract_entity_1.AbstractEntity {
    setCreatedByUser() {
        const currentUser = context_1.RequestContext.currentUser();
        if (currentUser) {
            this.createdBy = currentUser;
        }
    }
    setUpdatedByUser() {
        const currentUser = context_1.RequestContext.currentUser();
        if (currentUser) {
            this.updatedBy = currentUser;
        }
    }
}
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserEntity, {
        onDelete: 'NO ACTION',
        onUpdate: 'CASCADE',
        nullable: false,
    }),
    __metadata("design:type", typeof (_a = typeof user_entity_1.UserEntity !== "undefined" && user_entity_1.UserEntity) === "function" ? _a : Object)
], AbstractAuditEntity.prototype, "createdBy", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserEntity, {
        onDelete: 'NO ACTION',
        onUpdate: 'CASCADE',
        nullable: true,
    }),
    __metadata("design:type", typeof (_b = typeof user_entity_1.UserEntity !== "undefined" && user_entity_1.UserEntity) === "function" ? _b : Object)
], AbstractAuditEntity.prototype, "updatedBy", void 0);
__decorate([
    class_transformer_1.Exclude(),
    typeorm_1.VersionColumn(),
    __metadata("design:type", Number)
], AbstractAuditEntity.prototype, "version", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AbstractAuditEntity.prototype, "setCreatedByUser", null);
__decorate([
    typeorm_1.BeforeUpdate(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AbstractAuditEntity.prototype, "setUpdatedByUser", null);
exports.AbstractAuditEntity = AbstractAuditEntity;


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const abstract_user_entity_1 = __webpack_require__(123);
let UserEntity = class UserEntity extends abstract_user_entity_1.AbstractUserEntity {
};
UserEntity = __decorate([
    typeorm_1.Entity('core_user')
], UserEntity);
exports.UserEntity = UserEntity;


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const abstract_entity_1 = __webpack_require__(119);
const image_entity_1 = __webpack_require__(124);
const profile_entity_1 = __webpack_require__(126);
class AbstractUserEntity extends abstract_entity_1.AbstractEntity {
}
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "firstName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Index({ unique: true }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], AbstractUserEntity.prototype, "verifiedEmail", void 0);
__decorate([
    typeorm_1.Index({ unique: true }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 15),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "phoneNumber", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], AbstractUserEntity.prototype, "verifiedPhoneNumber", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "address", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "language", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], AbstractUserEntity.prototype, "currencyId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "country", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "countryCode", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "theme", void 0);
__decorate([
    class_validator_1.IsAscii(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    typeorm_1.Index({ unique: true }),
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "username", void 0);
__decorate([
    typeorm_1.VersionColumn(),
    __metadata("design:type", Number)
], AbstractUserEntity.prototype, "version", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    class_transformer_1.Exclude(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "password", void 0);
__decorate([
    typeorm_1.OneToMany(_ => image_entity_1.ImageEntity, image => image.user),
    __metadata("design:type", Array)
], AbstractUserEntity.prototype, "images", void 0);
__decorate([
    typeorm_1.OneToOne(type => profile_entity_1.ProfileEntity, { cascade: ['insert', 'remove'], nullable: true, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn(),
    __metadata("design:type", typeof (_a = typeof profile_entity_1.ProfileEntity !== "undefined" && profile_entity_1.ProfileEntity) === "function" ? _a : Object)
], AbstractUserEntity.prototype, "profile", void 0);
__decorate([
    typeorm_1.RelationId((user) => user.profile),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "profileId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], AbstractUserEntity.prototype, "companyId", void 0);
__decorate([
    typeorm_1.Column({
        type: 'timestamp without time zone',
        name: 'updatePasswordAt',
        nullable: true
    }),
    __metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], AbstractUserEntity.prototype, "updatePasswordAt", void 0);
__decorate([
    typeorm_1.Column({
        default: false
    }),
    __metadata("design:type", Boolean)
], AbstractUserEntity.prototype, "registerCompleted", void 0);
exports.AbstractUserEntity = AbstractUserEntity;


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const abstract_image_entity_1 = __webpack_require__(125);
let ImageEntity = class ImageEntity extends abstract_image_entity_1.AbstractImageEntity {
};
ImageEntity = __decorate([
    typeorm_1.Entity('core_image')
], ImageEntity);
exports.ImageEntity = ImageEntity;


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const shared_1 = __webpack_require__(25);
const class_transformer_1 = __webpack_require__(16);
const class_validator_1 = __webpack_require__(17);
const typeorm_1 = __webpack_require__(120);
const abstract_entity_1 = __webpack_require__(119);
const user_entity_1 = __webpack_require__(122);
class AbstractImageEntity extends abstract_entity_1.AbstractEntity {
}
__decorate([
    class_validator_1.Length(1, 100),
    typeorm_1.Column({ length: 100 }),
    __metadata("design:type", String)
], AbstractImageEntity.prototype, "title", void 0);
__decorate([
    typeorm_1.Column({ type: 'enum', enum: shared_1.ImageType, default: shared_1.ImageType.Profile }),
    class_validator_1.IsEnum(shared_1.ImageType),
    __metadata("design:type", typeof (_a = typeof shared_1.ImageType !== "undefined" && shared_1.ImageType) === "function" ? _a : Object)
], AbstractImageEntity.prototype, "type", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserEntity, user => user.images, { onDelete: 'CASCADE', nullable: false }),
    __metadata("design:type", typeof (_b = typeof user_entity_1.UserEntity !== "undefined" && user_entity_1.UserEntity) === "function" ? _b : Object)
], AbstractImageEntity.prototype, "user", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_transformer_1.Transform(value => value.toString('base64'), { toPlainOnly: true }),
    typeorm_1.Column({ type: 'bytea', nullable: true }),
    __metadata("design:type", typeof (_c = typeof Buffer !== "undefined" && Buffer) === "function" ? _c : Object)
], AbstractImageEntity.prototype, "data", void 0);
__decorate([
    class_validator_1.IsOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], AbstractImageEntity.prototype, "checksum", void 0);
__decorate([
    class_validator_1.IsOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], AbstractImageEntity.prototype, "mimeType", void 0);
__decorate([
    class_validator_1.IsOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], AbstractImageEntity.prototype, "size", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(500),
    typeorm_1.Column({ length: 500, nullable: true }),
    __metadata("design:type", String)
], AbstractImageEntity.prototype, "url", void 0);
exports.AbstractImageEntity = AbstractImageEntity;


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const abstract_profile_entity_1 = __webpack_require__(127);
let ProfileEntity = class ProfileEntity extends abstract_profile_entity_1.AbstractProfileEntity {
};
ProfileEntity = __decorate([
    typeorm_1.Entity('core_profile')
], ProfileEntity);
exports.ProfileEntity = ProfileEntity;


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const shared_1 = __webpack_require__(25);
const class_transformer_1 = __webpack_require__(16);
const class_validator_1 = __webpack_require__(17);
const typeorm_1 = __webpack_require__(120);
const abstract_entity_1 = __webpack_require__(119);
const image_entity_1 = __webpack_require__(124);
class AbstractProfileEntity extends abstract_entity_1.AbstractEntity {
    constructor() {
        super(...arguments);
        this.gender = shared_1.Gender.UNKNOW;
    }
}
__decorate([
    typeorm_1.OneToOne(_ => image_entity_1.ImageEntity, { cascade: ['insert', 'update'], eager: true, nullable: true, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn(),
    __metadata("design:type", typeof (_a = typeof image_entity_1.ImageEntity !== "undefined" && image_entity_1.ImageEntity) === "function" ? _a : Object)
], AbstractProfileEntity.prototype, "avatar", void 0);
__decorate([
    typeorm_1.Column({ type: 'enum', enum: shared_1.Gender, default: shared_1.Gender.UNKNOW }),
    __metadata("design:type", typeof (_b = typeof shared_1.Gender !== "undefined" && shared_1.Gender) === "function" ? _b : Object)
], AbstractProfileEntity.prototype, "gender", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    class_validator_1.Length(10, 20),
    __metadata("design:type", String)
], AbstractProfileEntity.prototype, "mobilePhone", void 0);
__decorate([
    class_transformer_1.Exclude(),
    typeorm_1.VersionColumn(),
    __metadata("design:type", Number)
], AbstractProfileEntity.prototype, "version", void 0);
exports.AbstractProfileEntity = AbstractProfileEntity;


/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(129));


/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
class AbstractBaseRepository extends typeorm_1.Repository {
}
exports.AbstractBaseRepository = AbstractBaseRepository;


/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(131));
__export(__webpack_require__(132));
__export(__webpack_require__(135));


/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var DatabaseModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const databaseConfig = __webpack_require__(132);
const database_constants_1 = __webpack_require__(132);
const database_migrate_1 = __webpack_require__(133);
let DatabaseModule = DatabaseModule_1 = class DatabaseModule {
    static forRootAsync(options) {
        const connectionName = options.connectionName ? options.connectionName : database_constants_1.DB_CONNECTION_DEFAULT;
        return {
            module: DatabaseModule_1,
            imports: [
                database_migrate_1.MigrateDatabase,
                typeorm_1.TypeOrmModule.forRootAsync({
                    name: connectionName,
                    useFactory: () => {
                        const config = Object.assign(Object.assign({}, databaseConfig[connectionName]), { entities: options.entities || [] });
                        return config;
                    },
                }),
            ]
        };
    }
};
DatabaseModule = DatabaseModule_1 = __decorate([
    common_1.Global(),
    common_1.Module({})
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;


/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(52);
const databases = config_1.ConfigService.get('databases');
exports.DB_CONFIG_DEFAULT = databases[0];
exports.DB_CONFIG_1 = {};
exports.DB_CONFIG_2 = {};
exports.DB_CONNECTION_DEFAULT = 'DB_CONFIG_DEFAULT';
exports.DB_CONNECTION_1 = 'DB_CONFIG_1';
exports.DB_CONNECTION_2 = 'DB_CONFIG_2';


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const list_migrate_1 = __webpack_require__(134);
class MigrateDatabase {
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(exports.parseQuery(list_migrate_1.database));
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.MigrateDatabase = MigrateDatabase;
exports.parseQuery = (database) => {
    console.log(database);
    return database;
};


/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.database = [
    {
        table: 'products',
        column: [
            {
                name: 'id',
                conditions: ['uuid GENERATED DEFAULT uuid_generate_v4 ()', 'PRIMARY KEY', 'NOT NULL']
            },
            {
                name: 'createdAt',
                conditions: ['DATE', 'NOT NULL', 'NOT NULL DEFAULT CURRENT_DATE']
            },
            {
                name: 'updatedAt',
                conditions: ['DATE', 'NOT NULL', 'NOT NULL DEFAULT CURRENT_DATE']
            },
            {
                name: 'name',
                conditions: ['VARCHAR']
            },
            {
                name: 'categoryId',
                conditions: ['VARCHAR']
            },
            {
                name: 'sku',
                conditions: ['VARCHAR']
            },
            {
                name: 'barcode',
                conditions: ['VARCHAR', 'UNIQUE']
            },
            {
                name: 'rfid',
                conditions: ['VARCHAR']
            },
            {
                name: 'unit',
                conditions: ['VARCHAR']
            },
            {
                name: 'quantity',
                conditions: ['NUMERIC']
            },
            {
                name: 'price',
                conditions: ['NUMERIC']
            },
            {
                name: 'variationType',
                conditions: ['VARCHAR']
            },
            {
                name: 'variationName',
                conditions: ['VARCHAR']
            },
            {
                name: 'FOREIGN KEY',
                conditions: ['CONSTRAINT images FOREIGN KEY(images) REFERENCES customers(id) ON DELETE CASCADE']
            },
        ]
    }
];


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const lodash_1 = __webpack_require__(55);
const api_error_code_1 = __webpack_require__(94);
class ApiValidatorService {
    constructor() {
        console.log(['ApiValidatorService']);
    }
    validateWith(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield class_validator_1.validate(request);
            const errorsCodeMapping = request.getErrorCodeMapping();
            const data = errors.map(error => {
                return Object.keys(error.constraints).map((type) => {
                    return error.property + '.' + type;
                });
            }).map(types => {
                return types.map(type => {
                    const errorCodeMapping = lodash_1.get(errorsCodeMapping, type);
                    if (errorCodeMapping) {
                        const [code, options] = errorCodeMapping;
                        return api_error_code_1.ApiErrorCodeService.translate(code, options);
                    }
                    else {
                        throw Error(`Missing error code mapping for ${JSON.stringify(type)}`);
                    }
                });
            });
            return lodash_1.flatten(data);
        });
    }
}
exports.ApiValidatorService = ApiValidatorService;


/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(138));
__export(__webpack_require__(140));


/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const EntityNotFoundError_1 = __webpack_require__(139);
const common_1 = __webpack_require__(3);
let EntityNotFoundFilter = class EntityNotFoundFilter {
    catch(exception, host) {
        throw new common_1.NotFoundException(exception.message);
    }
};
EntityNotFoundFilter = __decorate([
    common_1.Catch(EntityNotFoundError_1.EntityNotFoundError)
], EntityNotFoundFilter);
exports.EntityNotFoundFilter = EntityNotFoundFilter;


/***/ }),
/* 139 */
/***/ (function(module, exports) {

module.exports = require("typeorm/error/EntityNotFoundError");

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const exceptions_1 = __webpack_require__(20);
const api_error_code_1 = __webpack_require__(94);
const EntityNotFoundError_1 = __webpack_require__(139);
const QueryFailedError_1 = __webpack_require__(141);
const jsonwebtoken_1 = __webpack_require__(142);
let AllApiExceptionsFilter = class AllApiExceptionsFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = exception instanceof common_1.HttpException
            ? exception.getStatus()
            : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        const errorResponse = exception instanceof common_1.HttpException ?
            exception.getResponse()
            : 'INTERNAL_SERVER_ERROR';
        console.error([
            'AllExceptionsFilter',
            {
                exception,
                status,
                errorResponse
            }
        ]);
        if (exception instanceof exceptions_1.ApiBadRequestException) {
            response
                .status(common_1.HttpStatus.BAD_REQUEST)
                .json(errorResponse);
        }
        else if (exception instanceof jsonwebtoken_1.JsonWebTokenError) {
            response
                .status(common_1.HttpStatus.BAD_REQUEST)
                .json({
                data: null,
                errors: [{
                        code: api_error_code_1.CodeEnum.BAD_REQUEST,
                        message: exception.message
                    }]
            });
        }
        else if (exception instanceof exceptions_1.EntityWriteException) {
            response
                .status(common_1.HttpStatus.BAD_REQUEST)
                .json({
                data: null,
                errors: [{
                        code: api_error_code_1.CodeEnum.BAD_REQUEST,
                        message: exception.message
                    }]
            });
        }
        else if (exception instanceof QueryFailedError_1.QueryFailedError) {
            response
                .status(common_1.HttpStatus.BAD_REQUEST)
                .json({
                data: null,
                errors: [{
                        code: api_error_code_1.CodeEnum.QUERY_FAILED_ERROR,
                        message: exception.message
                    }]
            });
        }
        else if (exception instanceof exceptions_1.EntityNotfoundException || exception instanceof EntityNotFoundError_1.EntityNotFoundError) {
            response
                .status(common_1.HttpStatus.NOT_FOUND)
                .json({
                data: null,
                errors: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.NOT_FOUND)]
            });
        }
        else if (exception instanceof common_1.UnauthorizedException) {
            response
                .status(common_1.HttpStatus.UNAUTHORIZED)
                .json({
                data: null,
                errors: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.UNAUTHORIZED)]
            });
        }
        else if (exception instanceof common_1.InternalServerErrorException) {
            response
                .status(common_1.HttpStatus.INTERNAL_SERVER_ERROR)
                .json({
                data: null,
                errors: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.INTERNAL_SERVER)]
            });
        }
        else {
            response
                .status(common_1.HttpStatus.BAD_REQUEST)
                .json({
                data: null,
                errors: [{
                        code: api_error_code_1.CodeEnum.BAD_REQUEST,
                        message: typeof errorResponse === 'string' ? errorResponse : errorResponse.message
                    }]
            });
        }
    }
};
AllApiExceptionsFilter = __decorate([
    common_1.Catch(common_1.HttpException, EntityNotFoundError_1.EntityNotFoundError, QueryFailedError_1.QueryFailedError, jsonwebtoken_1.JsonWebTokenError)
], AllApiExceptionsFilter);
exports.AllApiExceptionsFilter = AllApiExceptionsFilter;


/***/ }),
/* 141 */
/***/ (function(module, exports) {

module.exports = require("typeorm/error/QueryFailedError");

/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports = require("jsonwebtoken");

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(144));
__export(__webpack_require__(145));
__export(__webpack_require__(146));
__export(__webpack_require__(147));
__export(__webpack_require__(150));
__export(__webpack_require__(151));


/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const api_error_code_1 = __webpack_require__(94);
const class_transformer_1 = __webpack_require__(16);
class ApiBadRequestResponseExample {
}
__decorate([
    swagger_1.ApiProperty({ example: 'null' }),
    __metadata("design:type", Object)
], ApiBadRequestResponseExample.prototype, "data", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: [[
                {
                    "code": 10004,
                    "message": "FirstName's length must in a range [3, 100]"
                },
                {
                    "code": 10006,
                    "message": "LastName's length must in a range [3, 100]"
                },
                {
                    "code": 10001,
                    "message": "Email is invalid"
                }
            ]]
    }),
    __metadata("design:type", Object)
], ApiBadRequestResponseExample.prototype, "errors", void 0);
exports.ApiBadRequestResponseExample = ApiBadRequestResponseExample;
class ApiUnauthorizedResponseExample {
}
__decorate([
    swagger_1.ApiProperty({ example: null }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiUnauthorizedResponseExample.prototype, "data", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.UNAUTHORIZED)]
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiUnauthorizedResponseExample.prototype, "errors", void 0);
exports.ApiUnauthorizedResponseExample = ApiUnauthorizedResponseExample;
class ApiForbidenResponseExample {
}
__decorate([
    swagger_1.ApiProperty({ example: null }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiForbidenResponseExample.prototype, "data", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.FORBIDDEN)]
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiForbidenResponseExample.prototype, "errors", void 0);
exports.ApiForbidenResponseExample = ApiForbidenResponseExample;
class ApiNotfoundResponseExample {
}
__decorate([
    swagger_1.ApiProperty({ example: null }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiNotfoundResponseExample.prototype, "data", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: [api_error_code_1.ApiErrorCodeService.translate(api_error_code_1.CodeEnum.NOT_FOUND)]
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], ApiNotfoundResponseExample.prototype, "errors", void 0);
exports.ApiNotfoundResponseExample = ApiNotfoundResponseExample;


/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const obj_util_1 = __webpack_require__(146);
exports.isUndefined = (val) => typeof val === 'undefined';
exports.isNull = (val) => val === null;
exports.isNil = (val) => exports.isUndefined(val) || exports.isNull(val);
exports.isString = (val) => typeof val === 'string';
exports.hasLength = (val) => val.length > 0;
exports.isStringFull = (val) => exports.isString(val) && exports.hasLength(val);
exports.isArrayFull = (val) => Array.isArray(val) && exports.hasLength(val);
exports.isArrayStrings = (val) => exports.isArrayFull(val) && val.every((v) => exports.isStringFull(v));
exports.isObject = (val) => typeof val === 'object' && !exports.isNull(val);
exports.isObjectFull = (val) => exports.isObject(val) && exports.hasLength(obj_util_1.objKeys(val));
exports.isNumber = (val) => typeof val === 'number' && !Number.isNaN(val) && Number.isFinite(val);
exports.isEqual = (val, eq) => val === eq;
exports.isFalse = (val) => val === false;
exports.isTrue = (val) => val === true;
exports.isIn = (val, arr = []) => arr.some((o) => exports.isEqual(val, o));
exports.isBoolean = (val) => typeof val === 'boolean';
exports.isNumeric = (val) => /^[+-]?([0-9]*[.])?[0-9]+$/.test(val);
exports.isDateString = (val) => exports.isStringFull(val) &&
    /^\d{4}-[01]\d-[0-3]\d(?:T[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?(?:Z|[-+][0-2]\d(?::?[0-5]\d)?)?)?$/g.test(val);
exports.isDate = (val) => val instanceof Date;
exports.isValue = (val) => exports.isStringFull(val) || exports.isNumber(val) || exports.isBoolean(val) || exports.isDate(val);
exports.hasValue = (val) => exports.isArrayFull(val) ? val.every((o) => exports.isValue(o)) : exports.isValue(val);
exports.isFunction = (val) => typeof val === 'function';


/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.objKeys = (val) => Object.keys(val);
exports.getOwnPropNames = (val) => Object.getOwnPropertyNames(val);


/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const route_paramtypes_enum_1 = __webpack_require__(148);
const constants_1 = __webpack_require__(149);
const checks_util_1 = __webpack_require__(145);
const constants_2 = __webpack_require__(107);
class R {
    static set(metadataKey, metadataValue, target, propertyKey = undefined) {
        if (propertyKey) {
            Reflect.defineMetadata(metadataKey, metadataValue, target, propertyKey);
        }
        else {
            Reflect.defineMetadata(metadataKey, metadataValue, target);
        }
    }
    static get(metadataKey, target, propertyKey = undefined) {
        return propertyKey
            ? Reflect.getMetadata(metadataKey, target, propertyKey)
            : Reflect.getMetadata(metadataKey, target);
    }
    static createCustomRouteArg(paramtype, index, pipes = [], data = undefined) {
        return {
            [`${paramtype}${constants_1.CUSTOM_ROUTE_AGRS_METADATA}:${index}`]: {
                index,
                factory: (_, ctx) => R.getContextRequest(ctx)[paramtype],
                data,
                pipes,
            },
        };
    }
    static createRouteArg(paramtype, index, pipes = [], data = undefined) {
        return {
            [`${paramtype}:${index}`]: {
                index,
                pipes,
                data,
            },
        };
    }
    static setDecorators(decorators, target, name) {
        Reflect.defineProperty(target, name, Reflect.decorate(decorators, target, name, Reflect.getOwnPropertyDescriptor(target, name)));
        Reflect.decorate(decorators, target, name, Reflect.getOwnPropertyDescriptor(target, name));
    }
    static setParsedRequestArg(index) {
        return R.createCustomRouteArg(constants_2.PARSED_CRUD_REQUEST_KEY, index);
    }
    static setBodyArg(index, pipes = []) {
        return R.createRouteArg(route_paramtypes_enum_1.RouteParamtypes.BODY, index, pipes);
    }
    static setInterceptors(interceptors, func) {
        R.set(constants_1.INTERCEPTORS_METADATA, interceptors, func);
    }
    static setRouteArgs(metadata, target, name) {
        R.set(constants_1.ROUTE_ARGS_METADATA, metadata, target, name);
    }
    static setRouteArgsTypes(metadata, target, name) {
        R.set(constants_1.PARAMTYPES_METADATA, metadata, target, name);
    }
    static setCrudAuthOptions(metadata, target) {
        R.set(constants_2.CRUD_AUTH_OPTIONS_METADATA, metadata, target);
    }
    static getInterceptors(func) {
        return R.get(constants_1.INTERCEPTORS_METADATA, func) || [];
    }
    static getRouteArgs(target, name) {
        return R.get(constants_1.ROUTE_ARGS_METADATA, target, name);
    }
    static getRouteArgsTypes(target, name) {
        return R.get(constants_1.PARAMTYPES_METADATA, target, name) || [];
    }
    static getParsedBody(func) {
        return R.get(constants_2.PARSED_BODY_METADATA, func);
    }
    static getContextRequest(ctx) {
        return checks_util_1.isFunction(ctx.switchToHttp)
            ? ctx.switchToHttp().getRequest()
            : ctx;
    }
}
exports.R = R;


/***/ }),
/* 148 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/common/enums/route-paramtypes.enum");

/***/ }),
/* 149 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/common/constants");

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
class SerializeHelper {
    static createGetManyDto(dto, resourceName) {
        class PaginationResponseDto {
        }
        __decorate([
            swagger_1.ApiProperty({ type: dto, isArray: true }),
            class_transformer_1.Type(() => dto),
            __metadata("design:type", Array)
        ], PaginationResponseDto.prototype, "items", void 0);
        __decorate([
            swagger_1.ApiProperty({ example: 1 }),
            class_transformer_1.Expose(),
            __metadata("design:type", Number)
        ], PaginationResponseDto.prototype, "total", void 0);
        class GetManyResponseDto {
        }
        __decorate([
            swagger_1.ApiProperty({ type: PaginationResponseDto, isArray: false }),
            class_transformer_1.Type(() => PaginationResponseDto),
            __metadata("design:type", Object)
        ], GetManyResponseDto.prototype, "data", void 0);
        __decorate([
            swagger_1.ApiProperty({ example: null }),
            class_transformer_1.Expose(),
            __metadata("design:type", Object)
        ], GetManyResponseDto.prototype, "errors", void 0);
        Object.defineProperty(GetManyResponseDto, 'name', {
            writable: false,
            value: `GetMany${resourceName}ResponseDto`,
        });
        return GetManyResponseDto;
    }
    static createGetOneDto(dto, resourceName) {
        class GetOneResponseDto {
        }
        __decorate([
            swagger_1.ApiProperty({ type: dto, isArray: false }),
            class_transformer_1.Type(() => dto),
            class_transformer_1.Expose(),
            __metadata("design:type", Object)
        ], GetOneResponseDto.prototype, "data", void 0);
        __decorate([
            swagger_1.ApiProperty({ example: null }),
            class_transformer_1.Expose(),
            __metadata("design:type", Object)
        ], GetOneResponseDto.prototype, "errors", void 0);
        Object.defineProperty(GetOneResponseDto, 'name', {
            writable: false,
            value: resourceName ? `GetOne${resourceName}ResponseDto` : `ApiOkResponse${dto.name}`,
        });
        return GetOneResponseDto;
    }
}
exports.SerializeHelper = SerializeHelper;


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const obj_util_1 = __webpack_require__(146);
const reflection_helper_1 = __webpack_require__(147);
const swaggerConst = __webpack_require__(152);
class SwaggerHelper {
    static setOperation(metadata, func) {
        if (swaggerConst) {
            reflection_helper_1.R.set(swaggerConst.DECORATORS.API_OPERATION, metadata, func);
        }
    }
    static setParams(metadata, func) {
        if (swaggerConst) {
            reflection_helper_1.R.set(swaggerConst.DECORATORS.API_PARAMETERS, metadata, func);
        }
    }
    static setExtraModels(swaggerModels) {
        if (swaggerConst) {
            const meta = SwaggerHelper.getExtraModels(swaggerModels.get);
            const models = [
                ...meta,
                ...obj_util_1.objKeys(swaggerModels)
                    .map((name) => swaggerModels[name])
                    .filter((one) => one && one.name !== swaggerModels.get.name),
            ];
            reflection_helper_1.R.set(swaggerConst.DECORATORS.API_EXTRA_MODELS, models, swaggerModels.get);
        }
    }
    static setResponseOk(metadata, func) {
        if (swaggerConst) {
            reflection_helper_1.R.set(swaggerConst.DECORATORS.API_RESPONSE, metadata, func);
        }
    }
    static getOperation(func) {
        return swaggerConst ? reflection_helper_1.R.get(swaggerConst.DECORATORS.API_OPERATION, func) || {} : {};
    }
    static getParams(func) {
        return swaggerConst ? reflection_helper_1.R.get(swaggerConst.DECORATORS.API_PARAMETERS, func) || [] : [];
    }
    static getExtraModels(target) {
        return swaggerConst ? reflection_helper_1.R.get(swaggerConst.DECORATORS.API_EXTRA_MODELS, target) || [] : [];
    }
    static getResponseOk(func) {
        return swaggerConst ? reflection_helper_1.R.get(swaggerConst.DECORATORS.API_RESPONSE, func) || {} : {};
    }
    static createPathParasmMeta(options) {
        return swaggerConst
            ? obj_util_1.objKeys(options).map((param) => ({
                name: param,
                required: true,
                in: 'path',
                type: options[param].type === 'number' ? Number : String,
            }))
            : [];
    }
    static getSwaggerVersion() {
        return 1;
    }
    static getApiTags(target) {
        return swaggerConst ? reflection_helper_1.R.get(swaggerConst.DECORATORS.API_TAGS, target) || [] : [];
    }
}
exports.SwaggerHelper = SwaggerHelper;


/***/ }),
/* 152 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/swagger/dist/constants");

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(154));
__export(__webpack_require__(155));
__export(__webpack_require__(156));
__export(__webpack_require__(157));
__export(__webpack_require__(158));


/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const rxjs_1 = __webpack_require__(31);
const operators_1 = __webpack_require__(32);
let ErrorsInterceptor = class ErrorsInterceptor {
    intercept(context, next) {
        return next.handle().pipe(operators_1.catchError(err => rxjs_1.throwError(new common_1.HttpException('Message', common_1.HttpStatus.BAD_GATEWAY))));
    }
};
ErrorsInterceptor = __decorate([
    common_1.Injectable()
], ErrorsInterceptor);
exports.ErrorsInterceptor = ErrorsInterceptor;


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var LoggingInterceptor_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const operators_1 = __webpack_require__(32);
let LoggingInterceptor = LoggingInterceptor_1 = class LoggingInterceptor {
    constructor() {
        this.logger = new common_1.Logger(LoggingInterceptor_1.name, true);
    }
    intercept(context, next) {
        const request = context.switchToHttp().getRequest();
        const { url, method, params, query, headers } = request;
        this.logger.log(`Before: ${method} ${url} with :
     params: ${JSON.stringify(params)}, with query: ${JSON.stringify(query)}`);
        const now = Date.now();
        return next.handle().pipe(operators_1.tap(() => this.logger.log(`After: ${method} ${url} took ${Date.now() - now}ms`)));
    }
};
LoggingInterceptor = LoggingInterceptor_1 = __decorate([
    common_1.Injectable()
], LoggingInterceptor);
exports.LoggingInterceptor = LoggingInterceptor;


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const operators_1 = __webpack_require__(32);
let TimeoutInterceptor = class TimeoutInterceptor {
    intercept(context, next) {
        return next.handle().pipe(operators_1.timeout(5000));
    }
};
TimeoutInterceptor = __decorate([
    common_1.Injectable()
], TimeoutInterceptor);
exports.TimeoutInterceptor = TimeoutInterceptor;


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const operators_1 = __webpack_require__(32);
const class_transformer_1 = __webpack_require__(16);
let TransformInterceptor = class TransformInterceptor {
    intercept(context, next) {
        return next.handle().pipe(operators_1.map(data => class_transformer_1.classToPlain(data, { excludePrefixes: ['_id'] })));
    }
};
TransformInterceptor = __decorate([
    common_1.Injectable()
], TransformInterceptor);
exports.TransformInterceptor = TransformInterceptor;


/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const operators_1 = __webpack_require__(32);
let WrapInterceptor = class WrapInterceptor {
    intercept(context, next) {
        return next.handle().pipe(operators_1.map(response => ({ status: 'success', data: response })));
    }
};
WrapInterceptor = __decorate([
    common_1.Injectable()
], WrapInterceptor);
exports.WrapInterceptor = WrapInterceptor;


/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(160));
__export(__webpack_require__(161));


/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var IsEmailUniqueConstraint_1, _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const class_validator_1 = __webpack_require__(17);
const services_1 = __webpack_require__(114);
let IsEmailUniqueConstraint = IsEmailUniqueConstraint_1 = class IsEmailUniqueConstraint {
    constructor(authUserService) {
        if (authUserService) {
            IsEmailUniqueConstraint_1.authUserService = authUserService;
        }
    }
    validate(propertyValue, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { property } = args;
            const findOptions = {
                [property]: propertyValue
            };
            const userExists = yield IsEmailUniqueConstraint_1.authUserService.findOneByPropertyValue(findOptions);
            if (userExists === undefined) {
                return true;
            }
            else if (args.object && args.object.hasOwnProperty('id')) {
                return userExists.id === args.object['id'];
            }
            else {
                return false;
            }
        });
    }
    defaultMessage(args) {
        return 'User with this email already exists.';
    }
};
IsEmailUniqueConstraint = IsEmailUniqueConstraint_1 = __decorate([
    common_1.Injectable(),
    class_validator_1.ValidatorConstraint({ name: 'isEmailUnique', async: true }),
    __param(0, common_1.Inject(services_1.AuthUserService)),
    __metadata("design:paramtypes", [typeof (_a = typeof services_1.AuthUserService !== "undefined" && services_1.AuthUserService) === "function" ? _a : Object])
], IsEmailUniqueConstraint);
exports.IsEmailUniqueConstraint = IsEmailUniqueConstraint;
function IsEmailUnique(validationOptions) {
    return function (object, propertyName) {
        class_validator_1.registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsEmailUniqueConstraint
        });
    };
}
exports.IsEmailUnique = IsEmailUnique;


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var IsUsernameUniqueConstraint_1, _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const class_validator_1 = __webpack_require__(17);
const services_1 = __webpack_require__(114);
let IsUsernameUniqueConstraint = IsUsernameUniqueConstraint_1 = class IsUsernameUniqueConstraint {
    constructor(authUserService) {
        if (authUserService) {
            IsUsernameUniqueConstraint_1.authUserService = authUserService;
        }
    }
    validate(propertyValue, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { property } = args;
            const findOptions = {
                [property]: propertyValue
            };
            const userExists = yield IsUsernameUniqueConstraint_1.authUserService.findOneByPropertyValue(findOptions);
            if (userExists === undefined) {
                return true;
            }
            else if (args.object && args.object.hasOwnProperty('id')) {
                return userExists.id === args.object['id'];
            }
            else {
                return false;
            }
        });
    }
    defaultMessage(args) {
        return 'User with this username already exists.';
    }
};
IsUsernameUniqueConstraint = IsUsernameUniqueConstraint_1 = __decorate([
    common_1.Injectable(),
    class_validator_1.ValidatorConstraint({ name: 'isUsernameUnique', async: true }),
    __param(0, common_1.Inject(services_1.AuthUserService)),
    __metadata("design:paramtypes", [typeof (_a = typeof services_1.AuthUserService !== "undefined" && services_1.AuthUserService) === "function" ? _a : Object])
], IsUsernameUniqueConstraint);
exports.IsUsernameUniqueConstraint = IsUsernameUniqueConstraint;
function IsUsernameUnique(validationOptions) {
    return function (object, propertyName) {
        class_validator_1.registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsUsernameUniqueConstraint
        });
    };
}
exports.IsUsernameUnique = IsUsernameUnique;


/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(163));


/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = __webpack_require__(107);
function CrudOptionsDtoDecorator(crudOptionsDto) {
    return (target) => {
        Reflect.defineMetadata(constants_1.CRUD_OPTIONS_DTO_METADATA, crudOptionsDto, target);
    };
}
exports.CrudOptionsDtoDecorator = CrudOptionsDtoDecorator;


/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const entities_1 = __webpack_require__(118);
const validator_1 = __webpack_require__(159);
const typeorm_1 = __webpack_require__(117);
const database_1 = __webpack_require__(130);
const services_1 = __webpack_require__(114);
const entities = [
    entities_1.ImageEntity, entities_1.ProfileEntity, entities_1.UserEntity
];
let CoreBootstrapModule = class CoreBootstrapModule {
};
CoreBootstrapModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature(entities, database_1.DB_CONNECTION_DEFAULT),
        ],
        providers: [
            services_1.AuthUserService,
            validator_1.IsEmailUniqueConstraint,
            validator_1.IsUsernameUniqueConstraint
        ],
        exports: [
            services_1.AuthUserService,
            validator_1.IsEmailUniqueConstraint,
            validator_1.IsUsernameUniqueConstraint
        ]
    })
], CoreBootstrapModule);
exports.CoreBootstrapModule = CoreBootstrapModule;


/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_bootstrap_module_1 = __webpack_require__(164);
const context_1 = __webpack_require__(10);
let CoreModule = class CoreModule {
    configure(consumer) {
        consumer.apply(context_1.RequestContextMiddleware).forRoutes('*');
    }
};
CoreModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [
            core_bootstrap_module_1.CoreBootstrapModule,
        ]
    })
], CoreModule);
exports.CoreModule = CoreModule;


/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(167));
__export(__webpack_require__(181));
__export(__webpack_require__(178));
__export(__webpack_require__(263));
__export(__webpack_require__(171));
__export(__webpack_require__(169));


/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const cqrs_1 = __webpack_require__(30);
const typeorm_1 = __webpack_require__(117);
const passport_1 = __webpack_require__(43);
const jwt_1 = __webpack_require__(168);
const config_1 = __webpack_require__(52);
const core_1 = __webpack_require__(9);
const shared_1 = __webpack_require__(25);
const guards_1 = __webpack_require__(169);
const strategies_1 = __webpack_require__(178);
const auth_controller_1 = __webpack_require__(262);
const auth_service_1 = __webpack_require__(181);
const handlers_1 = __webpack_require__(272);
const database_1 = __webpack_require__(130);
const code_1 = __webpack_require__(214);
const twilio_1 = __webpack_require__(223);
const user_1 = __webpack_require__(256);
const flag_1 = __webpack_require__(274);
const device_1 = __webpack_require__(246);
const email_1 = __webpack_require__(227);
const logout_device_guard_1 = __webpack_require__(237);
let AuthModule = AuthModule_1 = class AuthModule {
};
AuthModule = AuthModule_1 = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => AuthModule_1),
            core_1.CoreModule,
            typeorm_1.TypeOrmModule.forFeature([core_1.ImageEntity, core_1.ProfileEntity, core_1.UserEntity, code_1.CodeEntity, flag_1.FlagEntity, device_1.DevicesEntity], database_1.DB_CONNECTION_DEFAULT),
            shared_1.SharedModule,
            config_1.ConfigModule,
            passport_1.PassportModule,
            jwt_1.JwtModule.registerAsync({
                useFactory: (configService) => {
                    return {
                        secret: configService.get('jwt.secretOrKey'),
                        signOptions: configService.get('jwt.signOptions'),
                    };
                },
                inject: [config_1.ConfigService]
            }),
            cqrs_1.CqrsModule,
            common_1.HttpModule
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [
            config_1.ConfigService,
            shared_1.UtilsService,
            strategies_1.JwtStrategy,
            strategies_1.WsJwtStrategy,
            guards_1.AllowGuard,
            guards_1.JwtAuthGuard,
            logout_device_guard_1.LogoutDeviceGuard,
            device_1.DevicesService,
            guards_1.WsJwtAuthGuard,
            core_1.AuthUserService,
            auth_service_1.AuthService,
            twilio_1.TwillioService,
            code_1.CodeService,
            user_1.UserService,
            flag_1.FlagService,
            email_1.EmailService,
            ...handlers_1.CommandHandlers,
            email_1.EmailCoreModule.getEmailConfig()
        ],
        exports: [
            guards_1.AllowGuard,
            guards_1.JwtAuthGuard,
            logout_device_guard_1.LogoutDeviceGuard,
            device_1.DevicesService,
            guards_1.WsJwtAuthGuard,
            core_1.AuthUserService,
            auth_service_1.AuthService,
            code_1.CodeService,
            user_1.UserService,
            flag_1.FlagService
        ],
    })
], AuthModule);
exports.AuthModule = AuthModule;


/***/ }),
/* 168 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/jwt");

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(170));
__export(__webpack_require__(174));
__export(__webpack_require__(175));
__export(__webpack_require__(176));
__export(__webpack_require__(177));


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_1 = __webpack_require__(1);
const config_1 = __webpack_require__(52);
const enums_1 = __webpack_require__(171);
let AllowGuard = class AllowGuard {
    constructor(reflector, config) {
        this.reflector = reflector;
        this.config = config;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const endpointAllow = this.reflector.get('allow', context.getHandler()) ||
                this.reflector.get('allow', context.getClass());
            if (endpointAllow) {
                if (endpointAllow.length === 0 || endpointAllow.includes(enums_1.AllowEnum.PUBLIC)) {
                    return true;
                }
                const request = context.switchToHttp().getRequest();
                if (endpointAllow.includes(enums_1.AllowEnum.WHITELIST)) {
                    if (this.config.getAllowWhitelist().includes(request.connection.remoteAddress)) {
                        return true;
                    }
                }
            }
        });
    }
};
AllowGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.Reflector !== "undefined" && core_1.Reflector) === "function" ? _a : Object, typeof (_b = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _b : Object])
], AllowGuard);
exports.AllowGuard = AllowGuard;


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(172));
__export(__webpack_require__(173));


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AllowEnum;
(function (AllowEnum) {
    AllowEnum["PUBLIC"] = "public";
    AllowEnum["WHITELIST"] = "whitelist";
})(AllowEnum = exports.AllowEnum || (exports.AllowEnum = {}));


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RolesEnum;
(function (RolesEnum) {
    RolesEnum["SELF"] = "SELF";
    RolesEnum["ADMIN"] = "ROLE_ADMIN";
    RolesEnum["USER"] = "ROLE_USER";
})(RolesEnum = exports.RolesEnum || (exports.RolesEnum = {}));


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const allow_guard_1 = __webpack_require__(170);
const role_guard_1 = __webpack_require__(175);
const jwt_auth_guard_1 = __webpack_require__(176);
let ComposeGuard = class ComposeGuard {
    constructor(allowGuard, jwtAuthGuard, roleGuard) {
        this.allowGuard = allowGuard;
        this.jwtAuthGuard = jwtAuthGuard;
        this.roleGuard = roleGuard;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            return ((yield this.allowGuard.canActivate(context)) ||
                ((yield this.jwtAuthGuard.canActivate(context)) &&
                    (yield this.roleGuard.canActivate(context))));
        });
    }
};
ComposeGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof allow_guard_1.AllowGuard !== "undefined" && allow_guard_1.AllowGuard) === "function" ? _a : Object, typeof (_b = typeof jwt_auth_guard_1.JwtAuthGuard !== "undefined" && jwt_auth_guard_1.JwtAuthGuard) === "function" ? _b : Object, typeof (_c = typeof role_guard_1.RoleGuard !== "undefined" && role_guard_1.RoleGuard) === "function" ? _c : Object])
], ComposeGuard);
exports.ComposeGuard = ComposeGuard;


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_1 = __webpack_require__(1);
const enums_1 = __webpack_require__(171);
const username = 'username';
let RoleGuard = class RoleGuard {
    constructor(reflector) {
        this.reflector = reflector;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const methodEndpointRoles = this.reflector.get('roles', context.getHandler()) || [];
            const classEndpointRoles = this.reflector.get('roles', context.getClass()) || [];
            const endpointRoles = [...methodEndpointRoles, ...classEndpointRoles];
            if (!endpointRoles || endpointRoles.length === 0) {
                return true;
            }
            const httpContext = context.switchToHttp();
            const [request, response] = [httpContext.getRequest(), httpContext.getResponse()];
            const token = request.authInfo.token;
            const userRoles = token.realm_access.roles;
            if (!token || !userRoles) {
                throw new common_1.UnauthorizedException('RoleGuard should have been executed after AuthGuard');
            }
            if (endpointRoles.includes(enums_1.RolesEnum.SELF)) {
                if (token.preferred_username === this.resolveUsername(request)) {
                    return true;
                }
                else {
                    throw new common_1.ForbiddenException(`SELF use only`);
                }
            }
            if (endpointRoles.includes(enums_1.RolesEnum.ADMIN)) {
                if (this.isRoleOverlay(token.realm_access.roles, [enums_1.RolesEnum.ADMIN])) {
                    return true;
                }
                else {
                    throw new common_1.ForbiddenException(`NgxWeb admin users only`);
                }
            }
            if (endpointRoles.includes(enums_1.RolesEnum.USER)) {
                if (this.isRoleOverlay(token.realm_access.roles, [enums_1.RolesEnum.USER])) {
                    return true;
                }
                else {
                    throw new common_1.ForbiddenException(`NgxWeb users only`);
                }
            }
            if (this.isRoleOverlay(userRoles, endpointRoles)) {
                return true;
            }
            else {
                throw new common_1.ForbiddenException(`${endpointRoles} roles only allowed`);
            }
        });
    }
    isRoleOverlay(userRoles, authRoles) {
        return authRoles.every(val => userRoles.includes(val));
    }
    resolveUsername(request) {
        if (request.method === 'GET' || request.method === 'DELETE') {
            return request.params[username] || request.query[username];
        }
        if (request.method === 'POST' || request.method === 'PATCH' || request.method === 'PUT') {
            return request.params[username] || request.body[username];
        }
        return null;
    }
};
RoleGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.Reflector !== "undefined" && core_1.Reflector) === "function" ? _a : Object])
], RoleGuard);
exports.RoleGuard = RoleGuard;


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const passport_1 = __webpack_require__(43);
let JwtAuthGuard = class JwtAuthGuard extends passport_1.AuthGuard('jwt') {
    canActivate(context) {
        return super.canActivate(context);
    }
    handleRequest(err, jwtPayload, info, context, status) {
        if (err || !jwtPayload) {
            throw new common_1.UnauthorizedException();
        }
        return jwtPayload;
    }
};
JwtAuthGuard = __decorate([
    common_1.Injectable()
], JwtAuthGuard);
exports.JwtAuthGuard = JwtAuthGuard;


/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const passport_1 = __webpack_require__(43);
let WsJwtAuthGuard = class WsJwtAuthGuard extends passport_1.AuthGuard('ws-jwt') {
};
WsJwtAuthGuard = __decorate([
    common_1.Injectable()
], WsJwtAuthGuard);
exports.WsJwtAuthGuard = WsJwtAuthGuard;


/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(179));
__export(__webpack_require__(261));


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const passport_jwt_1 = __webpack_require__(180);
const passport_1 = __webpack_require__(43);
const common_1 = __webpack_require__(3);
const config_1 = __webpack_require__(52);
const auth_service_1 = __webpack_require__(181);
let JwtStrategy = class JwtStrategy extends passport_1.PassportStrategy(passport_jwt_1.Strategy, 'jwt') {
    constructor(configService, authService) {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get('jwt.secretOrKey'),
            algorithm: [configService.get('jwt.signOptions.algorithm')],
        });
        this.configService = configService;
        this.authService = authService;
    }
    validate(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(['JwtStrategy', { payload }]);
            const { iat, exp, id: userId } = payload;
            const timeDiff = exp - iat;
            if (timeDiff <= 0) {
                throw new common_1.UnauthorizedException();
            }
            return payload;
        });
    }
};
JwtStrategy = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _a : Object, typeof (_b = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _b : Object])
], JwtStrategy);
exports.JwtStrategy = JwtStrategy;


/***/ }),
/* 180 */
/***/ (function(module, exports) {

module.exports = require("passport-jwt");

/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var _a, _b, _c, _d, _e, _f, _g;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const jwt_1 = __webpack_require__(168);
const core_1 = __webpack_require__(9);
const shared_1 = __webpack_require__(25);
const config_1 = __webpack_require__(52);
const email_utils_1 = __webpack_require__(182);
const phoneNumber_utils_1 = __webpack_require__(183);
const dto_1 = __webpack_require__(184);
const api_error_code_1 = __webpack_require__(94);
const code_1 = __webpack_require__(214);
const twilio_1 = __webpack_require__(223);
const code_utils_1 = __webpack_require__(220);
const device_1 = __webpack_require__(246);
const request_utils_1 = __webpack_require__(245);
const dto_2 = __webpack_require__(212);
const user_1 = __webpack_require__(256);
var Provider;
(function (Provider) {
    Provider["GOOGLE"] = "google";
    Provider["FACEBOOK"] = "facebook";
})(Provider = exports.Provider || (exports.Provider = {}));
let AuthService = class AuthService {
    constructor(jwtService, configService, twilioService, codeService, authUserService, devicesService, userService) {
        this.jwtService = jwtService;
        this.configService = configService;
        this.twilioService = twilioService;
        this.codeService = codeService;
        this.authUserService = authUserService;
        this.devicesService = devicesService;
        this.userService = userService;
    }
    findByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.authUserService.findOne({ email });
                if (!user) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                else {
                    return {
                        data: shared_1.UtilsService.plainToClass(dto_1.UserTransformDto, user)
                    };
                }
            }
            catch (ex) {
                console.error(['ex', ex]);
                return {
                    code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                };
            }
        });
    }
    getAuthenticatedUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.authUserService.findOne(id);
                if (!user) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                else {
                    return {
                        data: shared_1.UtilsService.plainToClass(dto_1.UserTransformDto, user)
                    };
                }
            }
            catch (ex) {
                console.error(['ex', ex]);
                return {
                    code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                };
            }
        });
    }
    hasNumber(myString) {
        return /\d/.test(myString);
    }
    register(userAuthRegisterDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const account = userAuthRegisterDto.account;
            if (yield email_utils_1.validateEmail(account)) {
                userAuthRegisterDto.email = account;
                userAuthRegisterDto.verifiedEmail = false;
            }
            else if (yield phoneNumber_utils_1.validatePhoneNumber(account)) {
                userAuthRegisterDto.phoneNumber = phoneNumber_utils_1.stripeZeroOut(account);
                userAuthRegisterDto.verifiedPhoneNumber = false;
            }
            else {
                return {
                    code: api_error_code_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
                };
            }
            delete userAuthRegisterDto.account;
            var checkAccount;
            if (userAuthRegisterDto.email) {
                checkAccount = yield this.userService.findOne({ email: userAuthRegisterDto.email });
                if (checkAccount && checkAccount.registerCompleted) {
                    return {
                        code: api_error_code_1.CodeEnum.EMAIL_EXISTED
                    };
                }
            }
            else if (userAuthRegisterDto.phoneNumber) {
                checkAccount = yield this.userService.findOne({ phoneNumber: userAuthRegisterDto.phoneNumber });
                if (checkAccount && checkAccount.registerCompleted) {
                    return {
                        code: api_error_code_1.CodeEnum.NUMBER_PHONE_EXISTED
                    };
                }
            }
            const checkPassword = userAuthRegisterDto.password;
            if (checkPassword.length < 6 || checkPassword.length > 30 || !this.hasNumber(checkPassword)) {
                return {
                    code: api_error_code_1.CodeEnum.SIGNUP_PASSWORD_INVALID
                };
            }
            var user;
            if (!checkAccount) {
                userAuthRegisterDto.registerCompleted = false;
                user = yield this.authUserService.create(Object.assign(Object.assign({}, userAuthRegisterDto), {
                    password: shared_1.UtilsService.generateHashPassword(userAuthRegisterDto.password)
                }));
            }
            else {
                userAuthRegisterDto.registerCompleted = false;
                userAuthRegisterDto.password = shared_1.UtilsService.generateHashPassword(userAuthRegisterDto.password);
                yield this.authUserService.update({ id: checkAccount.id }, userAuthRegisterDto);
                user = checkAccount;
            }
            if (userAuthRegisterDto.phoneNumber) {
                yield this.codeService.sendOTP(user, "register", false);
            }
            else {
                yield this.codeService.sendOTPByEmail(user, "register", false);
            }
            return {
                code: api_error_code_1.CodeEnum.VERIFIED_ACCOUNT
            };
        });
    }
    resendOTPRegister(userAuthRegisterDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const account = userAuthRegisterDto.account;
            if (yield email_utils_1.validateEmail(account)) {
                userAuthRegisterDto.email = account;
                userAuthRegisterDto.verifiedEmail = false;
            }
            else if (yield phoneNumber_utils_1.validatePhoneNumber(account)) {
                userAuthRegisterDto.phoneNumber = phoneNumber_utils_1.stripeZeroOut(account);
                userAuthRegisterDto.verifiedPhoneNumber = false;
            }
            else {
                return {
                    code: api_error_code_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
                };
            }
            delete userAuthRegisterDto.account;
            var checkAccount;
            if (userAuthRegisterDto.email) {
                checkAccount = yield this.userService.findOne({ email: userAuthRegisterDto.email });
                if (checkAccount && checkAccount.registerCompleted) {
                    return {
                        code: api_error_code_1.CodeEnum.EMAIL_EXISTED
                    };
                }
            }
            else if (userAuthRegisterDto.phoneNumber) {
                checkAccount = yield this.userService.findOne({ phoneNumber: userAuthRegisterDto.phoneNumber });
                if (checkAccount && checkAccount.registerCompleted) {
                    return {
                        code: api_error_code_1.CodeEnum.NUMBER_PHONE_EXISTED
                    };
                }
            }
            const checkPassword = userAuthRegisterDto.password;
            if (checkPassword.length < 6 || checkPassword.length > 30 || !this.hasNumber(checkPassword)) {
                return {
                    code: api_error_code_1.CodeEnum.SIGNUP_PASSWORD_INVALID
                };
            }
            var user;
            if (!checkAccount) {
                userAuthRegisterDto.registerCompleted = false;
                user = yield this.authUserService.create(Object.assign(Object.assign({}, userAuthRegisterDto), {
                    password: shared_1.UtilsService.generateHashPassword(userAuthRegisterDto.password)
                }));
            }
            else {
                userAuthRegisterDto.registerCompleted = false;
                userAuthRegisterDto.password = shared_1.UtilsService.generateHashPassword(userAuthRegisterDto.password);
                yield this.authUserService.update({ id: checkAccount.id }, userAuthRegisterDto);
                user = checkAccount;
            }
            if (userAuthRegisterDto.phoneNumber) {
                let result = yield this.codeService.sendOTP(user, "register", true);
                if (result.code) {
                    return {
                        code: result.code
                    };
                }
            }
            else {
                let result = yield this.codeService.sendOTPByEmail(user, "register", true);
                if (result.code) {
                    return {
                        code: result.code
                    };
                }
            }
            return {
                data: true
            };
        });
    }
    verifiedAccount(userAuthRegisterDto, otpCode, type) {
        return __awaiter(this, void 0, void 0, function* () {
            userAuthRegisterDto.phoneNumber = userAuthRegisterDto.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(userAuthRegisterDto.phoneNumber) : undefined;
            const validator = yield this.validateUser(userAuthRegisterDto);
            const { data: userData } = validator;
            const validateCode = {
                user: userData.id,
                code: otpCode,
                type: type
            };
            let checkCode = yield this.codeService.getCode(validateCode);
            const { code, options, data } = checkCode;
            if (code) {
                return {
                    code: code,
                    options,
                    data: null
                };
            }
            if (code_utils_1.checkExpiredTime(data.expiredAt)) {
                return {
                    code: api_error_code_1.CodeEnum.EXPIRED_CODE
                };
            }
            this.codeService.deleteCode(data.id);
            userData.verifiedPhoneNumber = userData.verifiedPhoneNumber === false ? true : null;
            userData.verifiedEmail = userData.verifiedEmail === false ? true : null;
            const updateUser = yield this.userService.updateUser(userData.id, shared_1.UtilsService.plainToClass(dto_2.UpdateUserDto, userData, { excludeExtraneousValues: false }));
            const permission = yield this.loginPermission(shared_1.UtilsService.plainToClass(core_1.UserEntity, updateUser.data, { excludeExtraneousValues: false }));
            return {
                data: shared_1.UtilsService.plainToClass(dto_1.JWTAuthDto, permission.data)
            };
        });
    }
    validateUser(input, options) {
        return __awaiter(this, void 0, void 0, function* () {
            const { password } = input, findData = __rest(input, ["password"]);
            Object.keys(findData).forEach(key => findData[key] === undefined && delete findData[key]);
            if (!findData.email && !findData.phoneNumber) {
                return {
                    code: api_error_code_1.CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL
                };
            }
            const user = yield this.authUserService.findOne(findData, options);
            if (!user) {
                if (input.phoneNumber) {
                    return {
                        code: api_error_code_1.CodeEnum.NUMBER_PHONE_NOT_EXISTED
                    };
                }
                if (input.email) {
                    return {
                        code: api_error_code_1.CodeEnum.EMAIL_NOT_EXISTED
                    };
                }
                return {
                    code: api_error_code_1.CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL
                };
            }
            else {
                if (!(yield shared_1.UtilsService.validateHashPassword(password, user.password))) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_WRONG_PASSWORD
                    };
                }
                else {
                    return {
                        data: user
                    };
                }
            }
        });
    }
    login(input, req) {
        return __awaiter(this, void 0, void 0, function* () {
            let ip = request_utils_1.getIpFromRequest(req);
            let userAgent = req.headers['user-agent'];
            let checkEmail = input.email ? email_utils_1.validateEmail(input.email) : true;
            if (!checkEmail) {
                return {
                    code: api_error_code_1.CodeEnum.USER_EMAIL_INVALID,
                    data: null
                };
            }
            let checkPhoneNumber = input.phoneNumber ? phoneNumber_utils_1.validatePhoneNumber(input.phoneNumber) : true;
            if (!checkPhoneNumber) {
                return {
                    code: api_error_code_1.CodeEnum.USER_PHONENUMBER_INVALID,
                    data: null
                };
            }
            input.phoneNumber = input.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(input.phoneNumber) : undefined;
            input.registerCompleted = true;
            const validator = yield this.validateUser(input);
            const { code, options, data } = validator;
            if (code) {
                return {
                    code,
                    options,
                    data: null
                };
            }
            if (input.email && !data.verifiedEmail) {
                yield this.codeService.sendOTPByEmail(data, "verify-user", false);
                return {
                    code: api_error_code_1.CodeEnum.VERIFIED_EMAIL,
                    options: {
                        email: data.email ? data.email : undefined
                    }
                };
            }
            else if (input.phoneNumber && !data.verifiedPhoneNumber) {
                yield this.codeService.sendOTP(data, "verify-user", false);
                return {
                    code: api_error_code_1.CodeEnum.VERIFIED_NUMBER_PHONE,
                    options: {
                        phoneNumber: data.phoneNumber ? `${data.countryCode}${data.phoneNumber}` : undefined,
                    }
                };
            }
            let device = yield this.devicesService.getCurrentDevice(ip, userAgent, data.id);
            if (!device) {
                if (input.email) {
                    yield this.codeService.sendOTPByEmail(data, "login", false);
                    return {
                        code: api_error_code_1.CodeEnum.CONFIRM_FIRST_LOGIN,
                        options: {
                            email: input.email
                        }
                    };
                }
                else if (input.phoneNumber) {
                    yield this.codeService.sendOTP(data, "login", false);
                    return {
                        code: api_error_code_1.CodeEnum.CONFIRM_FIRST_LOGIN,
                    };
                }
            }
            yield this.devicesService.createDevice(ip, userAgent, data.id);
            const permission = yield this.loginPermission(data);
            return {
                data: permission
            };
        });
    }
    generateJWTByPayload(jwtPayload) {
        return __awaiter(this, void 0, void 0, function* () {
            const accessToken = this.jwtService.sign(jwtPayload, this.configService.get('jwt.signOptions'));
            return {
                expiresIn: this.configService.get('jwt.signOptions.expiresIn'),
                accessToken: accessToken
            };
        });
    }
    requestPassword(userAuthRequestPasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, appSchema, webSchema } = userAuthRequestPasswordDto;
            const user = yield this.authUserService.findOne({ email }, {});
            let token;
            if (user && user.id) {
                const payload = { username: user.username, id: user.id };
                const jwt = yield this.generateJWTByPayload(payload);
                token = jwt.accessToken;
                if (token) {
                    const app = `${appSchema}?token=${token}`;
                    const web = `${webSchema}?token=${token}`;
                    console.log({ app, web });
                    return {
                        data: true
                    };
                }
            }
            else {
                return {
                    code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                };
            }
        });
    }
    resetPassword(userAuthResetPasswordDto, jwtPayload) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.authUserService.changePassword(jwtPayload.id, shared_1.UtilsService.generateHashPassword(userAuthResetPasswordDto.password));
            return {
                data: shared_1.UtilsService.plainToClass(dto_1.UserTransformDto, user)
            };
        });
    }
    confirmOTP(input, otpCode, req) {
        return __awaiter(this, void 0, void 0, function* () {
            let ip = request_utils_1.getIpFromRequest(req);
            let userAgent = req.headers['user-agent'];
            input.phoneNumber = input.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(input.phoneNumber) : undefined;
            const validator = yield this.validateUser(input);
            const { data: userData } = validator;
            const validateCode = {
                user: userData.id,
                code: otpCode,
                type: "login"
            };
            if (otpCode === 0) {
                yield this.devicesService.createDevice(ip, userAgent, userData.id);
                return {
                    data: yield this.loginPermission(userData)
                };
            }
            let checkCode = yield this.codeService.getCode(validateCode);
            const { code, options, data } = checkCode;
            if (code) {
                return {
                    code: code,
                    options,
                    data: null
                };
            }
            if (code_utils_1.checkExpiredTime(data.expiredAt)) {
                return {
                    code: api_error_code_1.CodeEnum.EXPIRED_CODE
                };
            }
            const permission = yield this.loginPermission(userData);
            yield this.devicesService.createDevice(ip, userAgent, userData.id);
            this.codeService.deleteCode(data.id);
            return {
                data: permission
            };
        });
    }
    loginPermission(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { password } = data, currentUser = __rest(data, ["password"]);
            const payload = { username: null, id: data.id, registerCompleted: data.registerCompleted };
            const jwt = yield this.generateJWTByPayload(payload);
            const userDto = shared_1.UtilsService.plainToClass(dto_1.UserTransformDto, currentUser);
            return {
                data: {
                    user: userDto,
                    jwt: jwt
                }
            };
        });
    }
    checkCompanyAddress(address) {
        return __awaiter(this, void 0, void 0, function* () {
            let companyAddress = yield this.authUserService.findOne({ address: address });
            if (!companyAddress) {
                return {
                    code: api_error_code_1.CodeEnum.COMPANY_ADDRESS_NOT_EXISTED
                };
            }
            return {
                data: true
            };
        });
    }
    resendOTP(input) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const validator = yield this.validateUser(input);
                const { code, options, data } = validator;
                if (code) {
                    return {
                        code,
                        options,
                        data: false
                    };
                }
                if (input.phoneNumber) {
                    data.phoneNumber = phoneNumber_utils_1.stripeZeroOut(input.phoneNumber);
                    let result = yield this.codeService.sendOTP(data, "login", true);
                    if (result.code) {
                        return {
                            code: result.code
                        };
                    }
                    return {
                        data: true
                    };
                }
                else if (input.email) {
                    let result = yield this.codeService.sendOTPByEmail(data, "login", true);
                    if (result.code) {
                        return {
                            code: result.code
                        };
                    }
                    return {
                        data: true
                    };
                }
                return {
                    code: api_error_code_1.CodeEnum.AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL
                };
            }
            catch (e) {
                throw e;
            }
        });
    }
    changePassword(req, userChangePasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!/\d/.test(userChangePasswordDto.newPassword)) {
                    return {
                        code: api_error_code_1.CodeEnum.SIGNUP_PASSWORD_INVALID
                    };
                }
                if (userChangePasswordDto.password === userChangePasswordDto.newPassword) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD
                    };
                }
                if (userChangePasswordDto.newPassword !== userChangePasswordDto.confirmPassword) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT
                    };
                }
                let id = req.user.id;
                let userExist = yield this.authUserService.findOne(id);
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                if (!(yield shared_1.UtilsService.validateHashPassword(userChangePasswordDto.password, userExist.password))) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_CURRENT_PASSWORD_INCORRECT
                    };
                }
                const changePassword = yield this.authUserService.changePassword(id, shared_1.UtilsService.generateHashPassword(userChangePasswordDto.newPassword));
                let ip = request_utils_1.getIpFromRequest(req);
                let userAgent = req.headers['user-agent'];
                yield this.devicesService.logoutOtherDevices(ip, userAgent, id);
                return {
                    data: changePassword
                };
            }
            catch (e) {
                throw e;
            }
        });
    }
    forgotPassword(userAuthForgotPasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const account = userAuthForgotPasswordDto.account;
            if (yield email_utils_1.validateEmail(account)) {
                userAuthForgotPasswordDto.email = account;
            }
            else if (yield phoneNumber_utils_1.validatePhoneNumber(account)) {
                userAuthForgotPasswordDto.phoneNumber = phoneNumber_utils_1.stripeZeroOut(account);
            }
            else {
                return {
                    code: api_error_code_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
                };
            }
            delete userAuthForgotPasswordDto.account;
            let user;
            if (userAuthForgotPasswordDto.phoneNumber) {
                user = yield this.userService.findByPhoneNumber(userAuthForgotPasswordDto.phoneNumber);
                if (!user) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                yield this.codeService.sendOTP(user, "forgot-password", false);
            }
            else {
                user = yield this.userService.findByEmail(userAuthForgotPasswordDto.email);
                if (!user) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                yield this.codeService.sendOTPByEmail(user, "forgot-password", false);
            }
            return {
                code: api_error_code_1.CodeEnum.AUTH_CONFIRM_FORGOT_PASSWORD_OTP,
            };
        });
    }
    confirmForgotPasswordOTP(input, otpCode, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const ip = request_utils_1.getIpFromRequest(req);
            const userAgent = req.headers['user-agent'];
            const account = input.account;
            let userForgot;
            if (yield email_utils_1.validateEmail(account)) {
                input.email = account;
                userForgot = yield this.userService.findByEmail(account);
                if (!userForgot) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
            }
            else if (yield phoneNumber_utils_1.validatePhoneNumber(account)) {
                input.phoneNumber = phoneNumber_utils_1.stripeZeroOut(account);
                userForgot = yield this.userService.findByPhoneNumber(account);
                if (!userForgot) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
            }
            else {
                return {
                    code: api_error_code_1.CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
                };
            }
            const validateCode = {
                user: userForgot.id,
                code: otpCode,
                type: "forgot-password"
            };
            const checkCode = yield this.codeService.getCode(validateCode);
            const { code, options, data } = checkCode;
            if (code) {
                return {
                    code: code,
                    options,
                    data: null
                };
            }
            if (code_utils_1.checkExpiredTime(data.expiredAt)) {
                return {
                    code: api_error_code_1.CodeEnum.EXPIRED_CODE
                };
            }
            const permission = yield this.loginPermission(userForgot);
            yield this.devicesService.createDevice(ip, userAgent, userForgot.id);
            this.codeService.deleteCode(data.id);
            return {
                data: permission
            };
        });
    }
    changeForgotPassword(req, userChangePasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.user.id;
                const userExist = yield this.authUserService.findOne(id);
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_USER_NOTFOUND
                    };
                }
                const updatePasswordAt = Date.parse(userExist.updatePasswordAt.toLocaleString());
                let token = req.headers['x-access-token'] || req.headers['authorization'];
                if (token.startsWith('Bearer ')) {
                    token = token.slice(7, token.length);
                }
                if (token) {
                    const decoded = yield this.jwtService.verifyAsync(token);
                    const iat = decoded.iat * 1000;
                    if (updatePasswordAt > iat) {
                        return {
                            code: api_error_code_1.CodeEnum.AUTH_TOKEN_INVALID
                        };
                    }
                }
                if (!/\d/.test(userChangePasswordDto.newPassword)) {
                    return {
                        code: api_error_code_1.CodeEnum.SIGNUP_PASSWORD_INVALID
                    };
                }
                if (userChangePasswordDto.newPassword !== userChangePasswordDto.confirmPassword) {
                    return {
                        code: api_error_code_1.CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT
                    };
                }
                const changePassword = yield this.authUserService.changePassword(id, shared_1.UtilsService.generateHashPassword(userChangePasswordDto.newPassword));
                const ip = request_utils_1.getIpFromRequest(req);
                const userAgent = req.headers['user-agent'];
                yield this.devicesService.logoutOtherDevices(ip, userAgent, id);
                return {
                    data: changePassword
                };
            }
            catch (e) {
                throw e;
            }
        });
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(4, common_1.Inject(core_1.AuthUserService)),
    __metadata("design:paramtypes", [typeof (_a = typeof jwt_1.JwtService !== "undefined" && jwt_1.JwtService) === "function" ? _a : Object, typeof (_b = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _b : Object, typeof (_c = typeof twilio_1.TwillioService !== "undefined" && twilio_1.TwillioService) === "function" ? _c : Object, typeof (_d = typeof code_1.CodeService !== "undefined" && code_1.CodeService) === "function" ? _d : Object, typeof (_e = typeof core_1.AuthUserService !== "undefined" && core_1.AuthUserService) === "function" ? _e : Object, typeof (_f = typeof device_1.DevicesService !== "undefined" && device_1.DevicesService) === "function" ? _f : Object, typeof (_g = typeof user_1.UserService !== "undefined" && user_1.UserService) === "function" ? _g : Object])
], AuthService);
exports.AuthService = AuthService;


/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.validateEmail = (email) => {
    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {
        return false;
    }
    return true;
};


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePhoneNumber = (phoneNumber) => {
    let reg = /(([0-9]{8,12})\b)/g;
    if (reg.test(phoneNumber) == false) {
        return false;
    }
    return true;
};
exports.stripeZeroOut = (phoneNumber) => {
    return phoneNumber.replace(/^0+/, '');
};


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(185));
__export(__webpack_require__(186));


/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const crud_options_dto_1 = __webpack_require__(186);
const class_transformer_1 = __webpack_require__(16);
class JWTAuthPayloadDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '1d'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], JWTAuthPayloadDto.prototype, "expiresIn", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im5hc3RlcmJsdWUiLCJpZCI6IjYwNTk0MWVkLTZmMGUtNDcxZi04Y2NjLTUyM2Q0YzBjOWI0ZiIsImlhdCI6MTU4ODQ0MTQ1MSwiZXhwIjoxNTg4NTI3ODUxfQ.cTO9BMwWzkCD3htjTOoDlhud0sJYgdB4MBdAkauLRso'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], JWTAuthPayloadDto.prototype, "accessToken", void 0);
exports.JWTAuthPayloadDto = JWTAuthPayloadDto;
class JWTAuthDto {
}
__decorate([
    swagger_1.ApiProperty({ type: crud_options_dto_1.UserTransformDto }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof crud_options_dto_1.UserTransformDto !== "undefined" && crud_options_dto_1.UserTransformDto) === "function" ? _a : Object)
], JWTAuthDto.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ type: JWTAuthPayloadDto }),
    class_transformer_1.Expose(),
    __metadata("design:type", JWTAuthPayloadDto)
], JWTAuthDto.prototype, "jwt", void 0);
exports.JWTAuthDto = JWTAuthDto;


/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const typeorm_1 = __webpack_require__(120);
const companies_1 = __webpack_require__(187);
class UserTransformDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'John' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Doe' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "countryCode", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_validator_1.IsEmail(),
    core_1.IsEmailUnique(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserTransformDto.prototype, "verifiedEmail", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+84868964539' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(8, 15),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserTransformDto.prototype, "verifiedPhoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'beesight.alpha.com' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'nasterblue' }),
    class_validator_1.IsAscii(),
    core_1.IsUsernameUnique(),
    class_validator_1.Length(6, 100),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "username", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Vietnamese' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(3, 15),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'DD/MM/YYYY' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UserTransformDto.prototype, "currencyId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "theme", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserTransformDto.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_1.CompanyEntity, company => company.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: 'companyId' }),
    __metadata("design:type", typeof (_a = typeof companies_1.CompanyEntity !== "undefined" && companies_1.CompanyEntity) === "function" ? _a : Object)
], UserTransformDto.prototype, "company", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], UserTransformDto.prototype, "updatePasswordAt", void 0);
exports.UserTransformDto = UserTransformDto;
let UserAuthLoginDto = class UserAuthLoginDto extends core_1.AbstractDto {
};
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthLoginDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '0868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthLoginDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '123456' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(6, 30),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthLoginDto.prototype, "password", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserAuthLoginDto.prototype, "registerCompleted", void 0);
UserAuthLoginDto = __decorate([
    class_transformer_1.Exclude()
], UserAuthLoginDto);
exports.UserAuthLoginDto = UserAuthLoginDto;
class UserAuthRegisterDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Nghia Vo' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 128),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "firstName", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com || 868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "account", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Vietnam' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+84' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "countryCode", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "email", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserAuthRegisterDto.prototype, "verifiedEmail", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "phoneNumber", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserAuthRegisterDto.prototype, "verifiedPhoneNumber", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "address", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "username", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "language", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "date", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UserAuthRegisterDto.prototype, "currencyId", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "theme", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "companyId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '123456' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRegisterDto.prototype, "password", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UserAuthRegisterDto.prototype, "registerCompleted", void 0);
exports.UserAuthRegisterDto = UserAuthRegisterDto;
class UserAuthRequestPasswordDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRequestPasswordDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'beesightsoft://reqset-password' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRequestPasswordDto.prototype, "appSchema", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'http://beesightsoft.com/reset-password' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthRequestPasswordDto.prototype, "webSchema", void 0);
exports.UserAuthRequestPasswordDto = UserAuthRequestPasswordDto;
let UserAuthResetPasswordDto = class UserAuthResetPasswordDto extends core_1.AbstractDto {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.Length(6, 100),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthResetPasswordDto.prototype, "password", void 0);
UserAuthResetPasswordDto = __decorate([
    class_transformer_1.Exclude()
], UserAuthResetPasswordDto);
exports.UserAuthResetPasswordDto = UserAuthResetPasswordDto;
let UserChangePasswordDto = class UserChangePasswordDto extends core_1.AbstractDto {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 30),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserChangePasswordDto.prototype, "password", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 30),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserChangePasswordDto.prototype, "newPassword", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 30),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserChangePasswordDto.prototype, "confirmPassword", void 0);
UserChangePasswordDto = __decorate([
    class_transformer_1.Exclude()
], UserChangePasswordDto);
exports.UserChangePasswordDto = UserChangePasswordDto;
let UserConfirmChangePasswordDto = class UserConfirmChangePasswordDto extends core_1.AbstractDto {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserConfirmChangePasswordDto.prototype, "password", void 0);
UserConfirmChangePasswordDto = __decorate([
    class_transformer_1.Exclude()
], UserConfirmChangePasswordDto);
exports.UserConfirmChangePasswordDto = UserConfirmChangePasswordDto;
class UserAuthForgotPasswordDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com || 868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthForgotPasswordDto.prototype, "account", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthForgotPasswordDto.prototype, "email", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserAuthForgotPasswordDto.prototype, "phoneNumber", void 0);
exports.UserAuthForgotPasswordDto = UserAuthForgotPasswordDto;
let UserChangeForgotPasswordDto = class UserChangeForgotPasswordDto extends core_1.AbstractDto {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 30),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserChangeForgotPasswordDto.prototype, "newPassword", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 30),
    swagger_1.ApiProperty({ example: '123456' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UserChangeForgotPasswordDto.prototype, "confirmPassword", void 0);
UserChangeForgotPasswordDto = __decorate([
    class_transformer_1.Exclude()
], UserChangeForgotPasswordDto);
exports.UserChangeForgotPasswordDto = UserChangeForgotPasswordDto;


/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var companies_module_1 = __webpack_require__(188);
exports.CompanyModule = companies_module_1.CompanyModule;
var companies_service_1 = __webpack_require__(208);
exports.CompanyService = companies_service_1.CompanyService;
var companies_entity_1 = __webpack_require__(189);
exports.CompanyEntity = companies_entity_1.CompanyEntity;
var companies_controller_1 = __webpack_require__(207);
exports.CompanyController = companies_controller_1.CompanyController;


/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const companies_entity_1 = __webpack_require__(189);
const companies_controller_1 = __webpack_require__(207);
const companies_service_1 = __webpack_require__(208);
const images_1 = __webpack_require__(190);
const countries_module_1 = __webpack_require__(253);
const PROVIDERS = [
    companies_service_1.CompanyService
];
const ENTITY = [
    companies_entity_1.CompanyEntity
];
let CompanyModule = class CompanyModule {
};
CompanyModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            common_1.forwardRef(() => countries_module_1.CountryModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
            images_1.ImageModule
        ],
        controllers: [companies_controller_1.CompanyController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], CompanyModule);
exports.CompanyModule = CompanyModule;


/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_1 = __webpack_require__(120);
const images_1 = __webpack_require__(190);
const locations_entity_1 = __webpack_require__(199);
const user_entity_1 = __webpack_require__(200);
const options_entity_1 = __webpack_require__(202);
const countries_entity_1 = __webpack_require__(204);
const products_entity_1 = __webpack_require__(205);
let CompanyEntity = class CompanyEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    typeorm_1.Index(),
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CompanyEntity.prototype, "address", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CompanyEntity.prototype, "phoneCountry", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_1.Index(),
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "phoneNumber", void 0);
__decorate([
    class_validator_1.IsEmail(),
    typeorm_1.Index(),
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "email", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_1.Index(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "businessType", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "businessName", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    typeorm_1.Index(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "city", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_1.Index(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], CompanyEntity.prototype, "countryId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CompanyEntity.prototype, "barcodeCountry", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CompanyEntity.prototype, "barcodeCompany", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CompanyEntity.prototype, "language", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "timezone", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CompanyEntity.prototype, "logoId", void 0);
__decorate([
    typeorm_1.OneToOne(type => images_1.ImagesEntity, logo => logo.id, { nullable: true, onDelete: 'SET NULL', eager: true }),
    typeorm_1.JoinColumn({ name: "logoId" }),
    __metadata("design:type", typeof (_a = typeof images_1.ImagesEntity !== "undefined" && images_1.ImagesEntity) === "function" ? _a : Object)
], CompanyEntity.prototype, "logo", void 0);
__decorate([
    typeorm_1.OneToMany(type => locations_entity_1.LocationEntity, location => location.company),
    __metadata("design:type", Array)
], CompanyEntity.prototype, "locations", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_entity_1.UserEntity, user => user.company),
    __metadata("design:type", Array)
], CompanyEntity.prototype, "users", void 0);
__decorate([
    typeorm_1.OneToMany(type => options_entity_1.OptionEntity, option => option.company),
    __metadata("design:type", Array)
], CompanyEntity.prototype, "options", void 0);
__decorate([
    typeorm_1.ManyToOne(type => countries_entity_1.CountryEntity, country => country.id, { nullable: true, onDelete: 'SET NULL', eager: true }),
    typeorm_1.JoinColumn({ name: "countryId" }),
    __metadata("design:type", typeof (_b = typeof countries_entity_1.CountryEntity !== "undefined" && countries_entity_1.CountryEntity) === "function" ? _b : Object)
], CompanyEntity.prototype, "country", void 0);
__decorate([
    typeorm_1.OneToMany(type => products_entity_1.ProductEntity, product => product.company),
    __metadata("design:type", Array)
], CompanyEntity.prototype, "products", void 0);
CompanyEntity = __decorate([
    typeorm_1.Entity('companies')
], CompanyEntity);
exports.CompanyEntity = CompanyEntity;


/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var images_module_1 = __webpack_require__(191);
exports.ImageModule = images_module_1.ImageModule;
var images_entity_1 = __webpack_require__(192);
exports.ImagesEntity = images_entity_1.ImagesEntity;
var images_service_1 = __webpack_require__(196);
exports.ImageService = images_service_1.ImageService;
var images_controller_1 = __webpack_require__(193);
exports.ImageController = images_controller_1.ImageController;


/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const images_entity_1 = __webpack_require__(192);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const images_controller_1 = __webpack_require__(193);
const images_service_1 = __webpack_require__(196);
const PROVIDERS = [
    images_service_1.ImageService
];
const ENTITY = [
    images_entity_1.ImagesEntity
];
let ImageModule = class ImageModule {
};
ImageModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [images_controller_1.ImageController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], ImageModule);
exports.ImageModule = ImageModule;


/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
let ImagesEntity = class ImagesEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], ImagesEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], ImagesEntity.prototype, "path", void 0);
ImagesEntity = __decorate([
    typeorm_1.Entity('images')
], ImagesEntity);
exports.ImagesEntity = ImagesEntity;


/***/ }),
/* 193 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(194);
const images_service_1 = __webpack_require__(196);
let ImageController = class ImageController extends core_1.CrudController {
    constructor(imageService) {
        super(imageService);
        this.imageService = imageService;
    }
    getImage(image, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.imageService.getImage(image, res);
            }
            catch (e) {
                return e;
            }
        });
    }
};
__decorate([
    common_1.Get(':imgpath'),
    __param(0, common_1.Param('imgpath')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ImageController.prototype, "getImage", null);
ImageController = __decorate([
    swagger_1.ApiTags('Images'),
    common_1.Controller(),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Image',
        createDto: dto_1.CreateImageDto,
        updateDto: dto_1.UpdateImageDto,
        transformDto: dto_1.TransformImageDto,
        searchDto: dto_1.SearchImageDto
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof images_service_1.ImageService !== "undefined" && images_service_1.ImageService) === "function" ? _a : Object])
], ImageController);
exports.ImageController = ImageController;


/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(195));


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TransformImageDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformImageDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'nghia vo' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformImageDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformImageDto.prototype, "path", void 0);
exports.TransformImageDto = TransformImageDto;
class CreateImageDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'nghia vo' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateImageDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateImageDto.prototype, "path", void 0);
exports.CreateImageDto = CreateImageDto;
class UpdateImageDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateImageDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'nghia vo' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateImageDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateImageDto.prototype, "path", void 0);
exports.UpdateImageDto = UpdateImageDto;
class SearchImageDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['name', 'createdAt', 'path']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchImageDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformImageDto, ['name', 'path'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchImageDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchImageDto.prototype, "order", void 0);
exports.SearchImageDto = SearchImageDto;


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const images_entity_1 = __webpack_require__(192);
const AWS = __webpack_require__(197);
var fs = __webpack_require__(198);
let ImageService = class ImageService extends core_1.CrudService {
    constructor(imageRepository) {
        super(imageRepository);
        this.imageRepository = imageRepository;
        this.s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
        });
    }
    upload(file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let image = {
                    name: file.filename,
                    path: file.path
                };
                let createImage = yield this.imageRepository.save(image);
                return {
                    data: createImage
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    uploadArrayImages(files) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let response = [];
                files.forEach(file => {
                    const fileReponse = {
                        originalname: file.originalname,
                        filename: file.filename,
                        path: file.path
                    };
                    response.push(fileReponse);
                });
                let images = [];
                for (let i = 0; i < response.length; i++) {
                    let img = yield this.upload(response[i]);
                    images.push(img.data);
                }
                return {
                    data: images
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    delte(file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return fs.unlinkSync(file.path);
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteImageLocal(path) {
        return __awaiter(this, void 0, void 0, function* () {
            fs.unlink(path, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
            });
        });
    }
    deleteImageS3(file, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = {
                    Bucket: process.env.AWS_BUCKET_NAME,
                    Key: file.path
                };
                yield this.s3.deleteObject(params);
                return {
                    data: null
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getImage(path, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return res.sendFile(path, { root: './' });
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
ImageService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(images_entity_1.ImagesEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object])
], ImageService);
exports.ImageService = ImageService;


/***/ }),
/* 197 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 198 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
const companies_entity_1 = __webpack_require__(189);
let LocationEntity = class LocationEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column({ unique: true }),
    __metadata("design:type", String)
], LocationEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], LocationEntity.prototype, "type", void 0);
__decorate([
    typeorm_2.Column({ unique: true }),
    __metadata("design:type", String)
], LocationEntity.prototype, "address", void 0);
__decorate([
    typeorm_2.Column(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], LocationEntity.prototype, "backroom", void 0);
__decorate([
    typeorm_2.Column(),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], LocationEntity.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_entity_1.CompanyEntity, company => company.locations),
    typeorm_2.JoinColumn({ name: 'companyId', referencedColumnName: 'id' }),
    __metadata("design:type", typeof (_a = typeof companies_entity_1.CompanyEntity !== "undefined" && companies_entity_1.CompanyEntity) === "function" ? _a : Object)
], LocationEntity.prototype, "company", void 0);
LocationEntity = __decorate([
    typeorm_1.Entity('locations')
], LocationEntity);
exports.LocationEntity = LocationEntity;


/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const companies_1 = __webpack_require__(187);
const currencies_entity_1 = __webpack_require__(201);
let UserEntity = class UserEntity extends core_1.UserEntity {
};
__decorate([
    typeorm_1.ManyToOne(type => companies_1.CompanyEntity, company => company.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_a = typeof companies_1.CompanyEntity !== "undefined" && companies_1.CompanyEntity) === "function" ? _a : Object)
], UserEntity.prototype, "company", void 0);
__decorate([
    typeorm_1.ManyToOne(type => currencies_entity_1.CurrencyEntity, currency => currency.id, { nullable: true, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "currencyId" }),
    __metadata("design:type", typeof (_b = typeof currencies_entity_1.CurrencyEntity !== "undefined" && currencies_entity_1.CurrencyEntity) === "function" ? _b : Object)
], UserEntity.prototype, "currency", void 0);
UserEntity = __decorate([
    typeorm_1.Entity('core_user')
], UserEntity);
exports.UserEntity = UserEntity;


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const typeorm_2 = __webpack_require__(120);
const user_entity_1 = __webpack_require__(200);
let CurrencyEntity = class CurrencyEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CurrencyEntity.prototype, "id", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "currencyCode", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "currencyName", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_entity_1.UserEntity, user => user.currency),
    __metadata("design:type", Array)
], CurrencyEntity.prototype, "users", void 0);
CurrencyEntity = __decorate([
    typeorm_1.Entity('currencies')
], CurrencyEntity);
exports.CurrencyEntity = CurrencyEntity;


/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
const companies_entity_1 = __webpack_require__(189);
const optionValues_entity_1 = __webpack_require__(203);
let OptionEntity = class OptionEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], OptionEntity.prototype, "name", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], OptionEntity.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_entity_1.CompanyEntity, company => company.options),
    typeorm_1.JoinColumn({ name: 'companyId', referencedColumnName: 'id' }),
    __metadata("design:type", typeof (_a = typeof companies_entity_1.CompanyEntity !== "undefined" && companies_entity_1.CompanyEntity) === "function" ? _a : Object)
], OptionEntity.prototype, "company", void 0);
__decorate([
    typeorm_1.OneToMany(type => optionValues_entity_1.OptionValueEntity, optionValue => optionValue.option),
    __metadata("design:type", Array)
], OptionEntity.prototype, "optionValues", void 0);
OptionEntity = __decorate([
    typeorm_1.Entity('options')
], OptionEntity);
exports.OptionEntity = OptionEntity;


/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
const options_entity_1 = __webpack_require__(202);
let OptionValueEntity = class OptionValueEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], OptionValueEntity.prototype, "value", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], OptionValueEntity.prototype, "optionId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => options_entity_1.OptionEntity, option => option.optionValues, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'optionId', referencedColumnName: 'id' }),
    __metadata("design:type", typeof (_a = typeof options_entity_1.OptionEntity !== "undefined" && options_entity_1.OptionEntity) === "function" ? _a : Object)
], OptionValueEntity.prototype, "option", void 0);
OptionValueEntity = __decorate([
    typeorm_1.Entity('optionValues')
], OptionValueEntity);
exports.OptionValueEntity = OptionValueEntity;


/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const typeorm_2 = __webpack_require__(120);
const companies_entity_1 = __webpack_require__(189);
let CountryEntity = class CountryEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CountryEntity.prototype, "id", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], CountryEntity.prototype, "iso", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], CountryEntity.prototype, "name", void 0);
__decorate([
    typeorm_2.Column(),
    __metadata("design:type", String)
], CountryEntity.prototype, "nicename", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", String)
], CountryEntity.prototype, "iso3", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", Number)
], CountryEntity.prototype, "numcode", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", String)
], CountryEntity.prototype, "phonecode", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", String)
], CountryEntity.prototype, "barcode", void 0);
__decorate([
    typeorm_1.OneToMany(type => companies_entity_1.CompanyEntity, company => company.country),
    __metadata("design:type", Array)
], CountryEntity.prototype, "companies", void 0);
CountryEntity = __decorate([
    typeorm_1.Entity('countries')
], CountryEntity);
exports.CountryEntity = CountryEntity;


/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
const images_1 = __webpack_require__(190);
const categories_entity_1 = __webpack_require__(206);
const companies_entity_1 = __webpack_require__(189);
let ProductEntity = class ProductEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    class_validator_1.Length(1, 128),
    typeorm_1.Column('text', { nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "categoryId", void 0);
__decorate([
    typeorm_2.ManyToOne(type => categories_entity_1.CategoryEntity, category => category.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_2.JoinColumn({ name: "categoryId" }),
    __metadata("design:type", typeof (_a = typeof categories_entity_1.CategoryEntity !== "undefined" && categories_entity_1.CategoryEntity) === "function" ? _a : Object)
], ProductEntity.prototype, "category", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_2.Index(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "sku", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "barcode", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "rfid", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "unit", void 0);
__decorate([
    class_validator_1.IsNumber(),
    typeorm_1.Column({ nullable: true, type: 'numeric' }),
    __metadata("design:type", Number)
], ProductEntity.prototype, "quantity", void 0);
__decorate([
    class_validator_1.IsNumber(),
    typeorm_1.Column({ nullable: true, type: 'numeric' }),
    __metadata("design:type", Number)
], ProductEntity.prototype, "price", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "option", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    typeorm_1.Column({ nullable: true, default: false }),
    __metadata("design:type", Boolean)
], ProductEntity.prototype, "isVariation", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "variationType", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], ProductEntity.prototype, "variationName", void 0);
__decorate([
    typeorm_2.ManyToMany(type => images_1.ImagesEntity),
    typeorm_2.JoinTable(),
    __metadata("design:type", Array)
], ProductEntity.prototype, "images", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ProductEntity.prototype, "companyId", void 0);
__decorate([
    typeorm_2.ManyToOne(type => companies_entity_1.CompanyEntity, company => company.products, { eager: true }),
    typeorm_2.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_b = typeof companies_entity_1.CompanyEntity !== "undefined" && companies_entity_1.CompanyEntity) === "function" ? _b : Object)
], ProductEntity.prototype, "company", void 0);
__decorate([
    typeorm_1.TreeChildren(),
    __metadata("design:type", Array)
], ProductEntity.prototype, "children", void 0);
__decorate([
    typeorm_1.TreeParent(),
    __metadata("design:type", ProductEntity)
], ProductEntity.prototype, "parent", void 0);
ProductEntity = __decorate([
    typeorm_1.Entity('products'),
    typeorm_1.Tree("nested-set")
], ProductEntity);
exports.ProductEntity = ProductEntity;


/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const companies_1 = __webpack_require__(187);
let CategoryEntity = class CategoryEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], CategoryEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CategoryEntity.prototype, "key", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], CategoryEntity.prototype, "numberOfProduct", void 0);
__decorate([
    typeorm_1.TreeChildren(),
    __metadata("design:type", Array)
], CategoryEntity.prototype, "children", void 0);
__decorate([
    typeorm_1.TreeParent(),
    __metadata("design:type", CategoryEntity)
], CategoryEntity.prototype, "parent", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CategoryEntity.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_1.CompanyEntity, company => company.products, { eager: true }),
    typeorm_1.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_a = typeof companies_1.CompanyEntity !== "undefined" && companies_1.CompanyEntity) === "function" ? _a : Object)
], CategoryEntity.prototype, "company", void 0);
CategoryEntity = __decorate([
    typeorm_1.Entity('categories'),
    typeorm_1.Tree("nested-set")
], CategoryEntity);
exports.CategoryEntity = CategoryEntity;


/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const companies_service_1 = __webpack_require__(208);
const dto_1 = __webpack_require__(209);
const platform_express_1 = __webpack_require__(4);
const composeSecurity_guard_1 = __webpack_require__(236);
let CompanyController = class CompanyController extends core_1.CrudController {
    constructor(companyService) {
        super(companyService);
        this.companyService = companyService;
    }
    create(data, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    console.log(errors);
                    return this.respondWithErrors(errors);
                }
                let company = yield this.companyService.createCompany(userId, data);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateCompany(id, data, files) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                let company = yield this.companyService.updateCompany(id, data, files);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateUserCompany(req, data, files) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const company = yield this.companyService.updateUserCompany(userId, data, files);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getCompany(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let company = yield this.companyService.findCompany(id);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getUserCompany(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const company = yield this.companyService.findUserCompany(userId);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllCompanies(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let company = yield this.companyService.findAllCompanies(filter);
                this.respondWithErrorsOrSuccess(company);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof dto_1.CreateCompanyDto !== "undefined" && dto_1.CreateCompanyDto) === "function" ? _a : Object, Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CompanyController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseInterceptors(platform_express_1.AnyFilesInterceptor()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()), __param(2, common_1.UploadedFiles()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, typeof (_c = typeof dto_1.UpdateCompanyDto !== "undefined" && dto_1.UpdateCompanyDto) === "function" ? _c : Object, Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], CompanyController.prototype, "updateCompany", null);
__decorate([
    common_1.Put(),
    common_1.UseInterceptors(platform_express_1.AnyFilesInterceptor()),
    __param(0, common_1.Req()), __param(1, common_1.Body()), __param(2, common_1.UploadedFiles()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_e = typeof dto_1.UpdateCompanyDto !== "undefined" && dto_1.UpdateCompanyDto) === "function" ? _e : Object, Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], CompanyController.prototype, "updateUserCompany", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], CompanyController.prototype, "getCompany", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], CompanyController.prototype, "getUserCompany", null);
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_j = typeof dto_1.SearchCompanyDto !== "undefined" && dto_1.SearchCompanyDto) === "function" ? _j : Object]),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], CompanyController.prototype, "getAllCompanies", null);
CompanyController = __decorate([
    swagger_1.ApiTags('Companies'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Company',
        createDto: dto_1.CreateCompanyDto,
        updateDto: dto_1.UpdateCompanyDto,
        transformDto: dto_1.TransformCompanyDto,
        searchDto: dto_1.SearchCompanyDto
    }),
    __metadata("design:paramtypes", [typeof (_l = typeof companies_service_1.CompanyService !== "undefined" && companies_service_1.CompanyService) === "function" ? _l : Object])
], CompanyController);
exports.CompanyController = CompanyController;


/***/ }),
/* 208 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const companies_entity_1 = __webpack_require__(189);
const dto_1 = __webpack_require__(209);
const shared_1 = __webpack_require__(25);
const enums_1 = __webpack_require__(98);
const typeorm_2 = __webpack_require__(120);
const email_utils_1 = __webpack_require__(182);
const phoneNumber_utils_1 = __webpack_require__(183);
const images_service_1 = __webpack_require__(196);
const user_service_1 = __webpack_require__(211);
const file_uploading_utils_1 = __webpack_require__(249);
const countries_service_1 = __webpack_require__(252);
let CompanyService = class CompanyService extends core_1.CrudService {
    constructor(companyRepository, imageService, userService, countryService) {
        super(companyRepository);
        this.companyRepository = companyRepository;
        this.imageService = imageService;
        this.userService = userService;
        this.countryService = countryService;
    }
    createCompany(userId, company) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let checkCompany = yield this.checkValidCompany(company);
                if (checkCompany) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
                    };
                }
                const checkEmail = company.email ? yield email_utils_1.validateEmail(company.email) : true;
                if (!checkEmail) {
                    return {
                        code: enums_1.CodeEnum.USER_EMAIL_INVALID,
                        data: null
                    };
                }
                const checkPhoneNumber = company.phoneNumber ? yield phoneNumber_utils_1.validatePhoneNumber(company.phoneNumber) : true;
                if (!checkPhoneNumber) {
                    return {
                        code: enums_1.CodeEnum.NUMBER_PHONE_WRONG,
                        data: null
                    };
                }
                if (company.phoneNumber) {
                    company.phoneNumber = phoneNumber_utils_1.stripeZeroOut(company.phoneNumber);
                }
                const userExist = yield this.userService.findUserNoRelations(userId);
                const country = yield this.countryService.getCountry(userExist.data.country);
                company.countryId = country.id;
                let createCompany = yield this.companyRepository.save(company);
                let updateCompanyForUser = {
                    companyId: createCompany.id,
                    phoneNumber: company.phoneNumber ? company.phoneNumber : undefined,
                    email: company.email ? company.email : undefined,
                    registerCompleted: true
                };
                let { code } = yield this.userService.updateUser(userId, updateCompanyForUser);
                if (code) {
                    yield this.companyRepository.delete({ id: createCompany.id });
                    return {
                        code: code
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, createCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateUserCompany(userId, company, images) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const companyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                company.id = companyId;
                const checkCompanyId = yield this.companyRepository.findOne({ id: companyId });
                if (!checkCompanyId) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_NOT_EXISTED
                    };
                }
                let logo;
                if (images) {
                    if (images.length > 1) {
                        return {
                            code: enums_1.CodeEnum.COMPANY_LIMIT_LOGO
                        };
                    }
                    logo = yield this.uploadLogoCompany(images);
                    if ((_b = checkCompanyId.logo) === null || _b === void 0 ? void 0 : _b.id) {
                        yield this.imageService.delete(checkCompanyId.logo.id);
                        yield this.imageService.deleteImageLocal(checkCompanyId.logo.path);
                    }
                    company.logoId = logo.id;
                }
                const checkCompany = yield this.checkValidCompany(company, companyId);
                if (checkCompany) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
                    };
                }
                const updateCompany = yield this.companyRepository.save(company);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, updateCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateCompany(id, company, images) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                company.id = id;
                const checkCompanyId = yield this.companyRepository.findOne({ id: id });
                if (!checkCompanyId) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_NOT_EXISTED
                    };
                }
                let logo;
                if (images) {
                    if (images.length > 1) {
                        return {
                            code: enums_1.CodeEnum.COMPANY_LIMIT_LOGO
                        };
                    }
                    logo = yield this.uploadLogoCompany(images);
                    if ((_a = checkCompanyId.logo) === null || _a === void 0 ? void 0 : _a.id) {
                        this.imageService.delete(checkCompanyId.logo.id);
                        yield this.imageService.deleteImageLocal(checkCompanyId.logo.path);
                    }
                    company.logoId = logo.id;
                }
                const checkCompany = yield this.checkValidCompany(company, id);
                if (checkCompany) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
                    };
                }
                const updateCompany = yield this.companyRepository.save(company);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, updateCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findUserCompany(userId) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const companyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                const checkCompany = yield this.companyRepository.findOne({ id: companyId });
                if (!checkCompany) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_NOT_EXISTED
                    };
                }
                if (checkCompany.logo) {
                    checkCompany.logo = yield file_uploading_utils_1.getImageUrl(checkCompany.logo, false);
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, checkCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findCompany(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let checkCompany = yield this.companyRepository.findOne({ id: id });
                if (!checkCompany) {
                    return {
                        code: enums_1.CodeEnum.COMPANY_NOT_EXISTED
                    };
                }
                if (checkCompany.logo) {
                    checkCompany.logo = yield file_uploading_utils_1.getImageUrl(checkCompany.logo, false);
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, checkCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findAllCompanies(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (filter) {
                    if (filter.where) {
                        let value = filter.where.search || '';
                        let queryName = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                        filter.where =
                            {
                                name: queryName
                            };
                    }
                }
                const findAllCompany = yield this.companyRepository.find(filter);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCompanyDto, findAllCompany, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    uploadLogoCompany(images) {
        return __awaiter(this, void 0, void 0, function* () {
            const updateImg = [];
            for (let i = 0; i < images.length; i++) {
                if (images[i].size > 0 && images[i].mimetype !== 'application/octet-stream' && images[i].mimetype !== 'application/json') {
                    const img = yield this.imageService.upload(file_uploading_utils_1.saveImage(images[i]));
                    updateImg.push(img.data);
                }
            }
            return updateImg[0];
        });
    }
    checkValidCompany(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { businessName } = data;
            let countBusinessName;
            if (id) {
                countBusinessName = yield this.companyRepository.createQueryBuilder("companies")
                    .where("LOWER(companies.businessName) = LOWER(:businessName) AND id != :id", { businessName, id })
                    .getCount();
            }
            else {
                countBusinessName = yield this.companyRepository.createQueryBuilder("companies")
                    .where("LOWER(companies.businessName) = LOWER(:businessName)", { businessName })
                    .getCount();
            }
            if (countBusinessName > 0)
                return true;
            return false;
        });
    }
};
CompanyService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(companies_entity_1.CompanyEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof images_service_1.ImageService !== "undefined" && images_service_1.ImageService) === "function" ? _b : Object, typeof (_c = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" ? _c : Object, typeof (_d = typeof countries_service_1.CountryService !== "undefined" && countries_service_1.CountryService) === "function" ? _d : Object])
], CompanyService);
exports.CompanyService = CompanyService;


/***/ }),
/* 209 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(210));


/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const images_1 = __webpack_require__(190);
const typeorm_1 = __webpack_require__(120);
class TransformCompanyDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "phoneCountry", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+84868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "barcodeCountry", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "barcodeCompany", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "timezone", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformCompanyDto.prototype, "countryId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "industry", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "businessName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "businessType", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCompanyDto.prototype, "logoId", void 0);
__decorate([
    typeorm_1.OneToOne(type => images_1.ImagesEntity, logo => logo.id),
    typeorm_1.JoinColumn({ name: "logoId" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof images_1.ImagesEntity !== "undefined" && images_1.ImagesEntity) === "function" ? _a : Object)
], TransformCompanyDto.prototype, "logo", void 0);
exports.TransformCompanyDto = TransformCompanyDto;
class CreateCompanyDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Beesight Soft' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'beesight.alpha.com' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+84' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "phoneCountry", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '0868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '893' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "barcodeCountry", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '00000' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "barcodeCompany", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'en' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+7' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "timezone", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 232 }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateCompanyDto.prototype, "countryId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 128),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "businessName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "businessType", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '84bde5cc-3ab4-4598-bb16-9a13f1d30e9e' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCompanyDto.prototype, "logoId", void 0);
exports.CreateCompanyDto = CreateCompanyDto;
class UpdateCompanyDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "phoneCountry", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "barcodeCountry", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "barcodeCompany", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "timezone", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "logoId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateCompanyDto.prototype, "countryId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "businessName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "businessType", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.MaxLength(128),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCompanyDto.prototype, "city", void 0);
exports.UpdateCompanyDto = UpdateCompanyDto;
class SearchCompanyDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'name']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCompanyDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformCompanyDto, ['name'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCompanyDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCompanyDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: ['logo', 'options', 'country']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCompanyDto.prototype, "relations", void 0);
exports.SearchCompanyDto = SearchCompanyDto;


/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const user_entity_1 = __webpack_require__(200);
const api_error_code_1 = __webpack_require__(94);
const shared_1 = __webpack_require__(25);
const dto_1 = __webpack_require__(212);
const typeorm_2 = __webpack_require__(120);
const code_1 = __webpack_require__(214);
const phoneNumber_utils_1 = __webpack_require__(183);
const email_utils_1 = __webpack_require__(182);
const code_utils_1 = __webpack_require__(220);
const device_1 = __webpack_require__(246);
const request_utils_1 = __webpack_require__(245);
let UserService = class UserService extends core_1.CrudService {
    constructor(userRepository, codeService, devicesService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.codeService = codeService;
        this.devicesService = devicesService;
    }
    findByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne({ email });
        });
    }
    findByPhoneNumber(phoneNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne({ phoneNumber });
        });
    }
    updateUser(id, user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userRepository.findOne({ id: id });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                if (user.phoneNumber) {
                    let checkPhone = yield phoneNumber_utils_1.validatePhoneNumber(user.phoneNumber);
                    if (!checkPhone) {
                        return {
                            code: api_error_code_1.CodeEnum.NUMBER_PHONE_WRONG
                        };
                    }
                    let checkPhoneExisted = yield this.userRepository.findOne({ phoneNumber: user.phoneNumber });
                    if (checkPhoneExisted && checkPhoneExisted.registerCompleted) {
                        return {
                            code: api_error_code_1.CodeEnum.NUMBER_PHONE_EXISTED
                        };
                    }
                    else {
                        if (checkPhoneExisted && checkPhoneExisted.id !== id) {
                            yield this.deleteUser(checkPhoneExisted.id);
                        }
                    }
                    userExist.phoneNumber = phoneNumber_utils_1.stripeZeroOut(user.phoneNumber);
                    userExist.verifiedPhoneNumber = false;
                }
                if (user.email) {
                    let checkMail = yield email_utils_1.validateEmail(user.email);
                    if (!checkMail) {
                        return {
                            code: api_error_code_1.CodeEnum.EMAIL_WRONG
                        };
                    }
                    let checkEmailExisted = yield this.userRepository.findOne({ email: user.email });
                    if (checkEmailExisted && checkEmailExisted.registerCompleted) {
                        return {
                            code: api_error_code_1.CodeEnum.EMAIL_EXISTED
                        };
                    }
                    else {
                        if (checkEmailExisted && checkEmailExisted.id !== id) {
                            yield this.deleteUser(checkEmailExisted.id);
                        }
                    }
                    userExist.email = user.email;
                    userExist.verifiedEmail = false;
                }
                let updateUser = yield this.userRepository.merge(userExist, user);
                yield this.userRepository.save(updateUser);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.UpdateUserDto, user, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateProfile(id, user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userRepository.findOne({ id: id });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                if (user.phoneNumber) {
                    let checkPhone = phoneNumber_utils_1.validatePhoneNumber(user.phoneNumber);
                    if (!checkPhone) {
                        return {
                            code: api_error_code_1.CodeEnum.USER_PHONENUMBER_INVALID
                        };
                    }
                    let phoneNumberExist = yield this.userRepository.findOne({
                        phoneNumber: phoneNumber_utils_1.stripeZeroOut(user.phoneNumber),
                        registerCompleted: true
                    });
                    if (phoneNumberExist) {
                        return {
                            code: api_error_code_1.CodeEnum.USER_PHONENUMBER_UNIQUE
                        };
                    }
                    userExist.phoneNumber = phoneNumber_utils_1.stripeZeroOut(user.phoneNumber);
                    yield this.codeService.sendOTP(userExist, "change-profile", false);
                    userExist.verifiedPhoneNumber = false;
                    return {
                        code: api_error_code_1.CodeEnum.CONFIRM_CHANGE_NUMBER_PHONE,
                        options: { phoneNumber: `${userExist.countryCode}${userExist.phoneNumber}` }
                    };
                }
                if (user.email) {
                    let checkMail = email_utils_1.validateEmail(user.email);
                    if (!checkMail) {
                        return {
                            code: api_error_code_1.CodeEnum.EMAIL_WRONG
                        };
                    }
                    let emailExist = yield this.userRepository.findOne({
                        email: user.email,
                        registerCompleted: true
                    });
                    if (emailExist) {
                        return {
                            code: api_error_code_1.CodeEnum.USER_EMAIL_UNIQUE
                        };
                    }
                    userExist.email = user.email;
                    yield this.codeService.sendOTPByEmail(userExist, "change-profile", false);
                    userExist.verifiedEmail = false;
                    return {
                        code: api_error_code_1.CodeEnum.CONFIRM_CHANGE_EMAIL,
                        options: { email: `${user.email}` }
                    };
                }
                let updateUser = yield this.userRepository.merge(userExist, user);
                yield this.userRepository.save(updateUser);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.UpdateUserDto, user, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updatePreference(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const checkUser = yield this.userRepository.findOne({ id: id });
                if (!checkUser) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                data.id = id;
                const updateUser = yield this.userRepository.save(data);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.UpdatePreferenceDto, updateUser, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userRepository.findOne({ id: id });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                this.userRepository.delete({ id: id });
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformUserDto, {})
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userRepository.findOne({
                    where: { id: id },
                    relations: ['company', 'currency'],
                });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformUserDto, userExist, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findAllUser(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (filter) {
                    for (var name in filter.where) {
                        let value = filter.where[`${name}`];
                        filter.where[`${name}`] = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                    }
                }
                const findAllUsers = yield this.userRepository.find(filter);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformUserDto, findAllUsers, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    confirmChangeUser(req, user, otpCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                user.phoneNumber = user.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(user.phoneNumber) : undefined;
                const validateCode = {
                    user: req.user.id,
                    code: otpCode,
                    type: "change-profile"
                };
                let checkCode = yield this.codeService.getCode(validateCode);
                const { code, options, data } = checkCode;
                if (code) {
                    return {
                        code: code,
                        options,
                        data: null
                    };
                }
                if (code_utils_1.checkExpiredTime(data.expiredAt)) {
                    return {
                        code: api_error_code_1.CodeEnum.EXPIRED_CODE
                    };
                }
                this.codeService.deleteCode(data.id);
                user.phoneNumber = user.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(user.phoneNumber) : undefined;
                user.id = req.user.id;
                if (user.phoneNumber) {
                    yield this.userRepository.delete({ phoneNumber: user.phoneNumber, registerCompleted: false });
                }
                if (user.email) {
                    yield this.userRepository.delete({ email: user.email, registerCompleted: false });
                }
                const updateProfile = yield this.userRepository.save(user);
                let id = req.user.id;
                let ip = request_utils_1.getIpFromRequest(req);
                let userAgent = req.headers['user-agent'];
                yield this.devicesService.logoutOtherDevices(ip, userAgent, id);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformUserDto, updateProfile, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    resendOTPChangeProfile(id, user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userRepository.findOne({ id: id });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                if (user.phoneNumber) {
                    let checkPhone = phoneNumber_utils_1.validatePhoneNumber(user.phoneNumber);
                    if (!checkPhone) {
                        return {
                            code: api_error_code_1.CodeEnum.NUMBER_PHONE_WRONG
                        };
                    }
                    let phoneNumberExist = yield this.userRepository.findOne({
                        phoneNumber: phoneNumber_utils_1.stripeZeroOut(user.phoneNumber),
                        registerCompleted: true
                    });
                    if (phoneNumberExist) {
                        return {
                            code: api_error_code_1.CodeEnum.USER_PHONENUMBER_UNIQUE
                        };
                    }
                    userExist.phoneNumber = phoneNumber_utils_1.stripeZeroOut(user.phoneNumber);
                    let result = yield this.codeService.sendOTP(userExist, "change-profile", true);
                    if (result.code) {
                        return {
                            code: result.code
                        };
                    }
                    return {
                        data: true
                    };
                }
                else if (user.email) {
                    let checkMail = email_utils_1.validateEmail(user.email);
                    if (!checkMail) {
                        return {
                            code: api_error_code_1.CodeEnum.EMAIL_WRONG
                        };
                    }
                    let emailExist = yield this.userRepository.findOne({
                        email: user.email,
                        registerCompleted: true
                    });
                    if (emailExist) {
                        return {
                            code: api_error_code_1.CodeEnum.USER_EMAIL_UNIQUE
                        };
                    }
                    userExist.email = user.email;
                    let result = yield this.codeService.sendOTPByEmail(userExist, "change-profile", true);
                    if (result.code) {
                        return {
                            code: result.code
                        };
                    }
                    return {
                        data: true
                    };
                }
                return {
                    code: api_error_code_1.CodeEnum.CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findUserNoRelations(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userRepository.findOne({ id: id });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformUserDto, userExist, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.UserEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof code_1.CodeService !== "undefined" && code_1.CodeService) === "function" ? _b : Object, typeof (_c = typeof device_1.DevicesService !== "undefined" && device_1.DevicesService) === "function" ? _c : Object])
], UserService);
exports.UserService = UserService;


/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(213));


/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const typeorm_1 = __webpack_require__(120);
const companies_1 = __webpack_require__(187);
class TransformUserDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'John' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Doe' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_validator_1.IsEmail(),
    core_1.IsEmailUnique(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'nasterblue' }),
    class_validator_1.IsAscii(),
    core_1.IsUsernameUnique(),
    class_validator_1.Length(6, 100),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "username", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Vietnamese' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(3, 15),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Viet Nam' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'DD/MM/YYYY' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformUserDto.prototype, "currencyId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformUserDto.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_1.CompanyEntity, company => company.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_a = typeof companies_1.CompanyEntity !== "undefined" && companies_1.CompanyEntity) === "function" ? _a : Object)
], TransformUserDto.prototype, "company", void 0);
exports.TransformUserDto = TransformUserDto;
class CreateUserDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'John' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Doe' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_validator_1.IsEmail(),
    core_1.IsEmailUnique(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], CreateUserDto.prototype, "verifiedEmail", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'nasterblue' }),
    class_validator_1.IsAscii(),
    core_1.IsUsernameUnique(),
    class_validator_1.Length(6, 100),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "username", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Vietnamese' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(3, 15),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'DD/MM/YYYY' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateUserDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateUserDto.prototype, "currencyId", void 0);
exports.CreateUserDto = CreateUserDto;
class UpdateUserDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'John' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Doe' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'dev@beesightsoft.com' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UpdateUserDto.prototype, "verifiedEmail", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+84868964539' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], UpdateUserDto.prototype, "verifiedPhoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Dark' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "theme", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Vietnamese' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(3, 15),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'DD/MM/YYYY' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateUserDto.prototype, "currencyId", void 0);
exports.UpdateUserDto = UpdateUserDto;
class UpdatePreferenceDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdatePreferenceDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Dark' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdatePreferenceDto.prototype, "theme", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'en' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdatePreferenceDto.prototype, "language", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'DD/MM/YYYY' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdatePreferenceDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 1 }),
    class_validator_1.IsInt(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdatePreferenceDto.prototype, "currencyId", void 0);
exports.UpdatePreferenceDto = UpdatePreferenceDto;
class SearchUserDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'email', 'firstName', 'username']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchUserDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformUserDto, ['id', 'email', 'firstName', 'username'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchUserDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            username: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchUserDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: ['company', 'company.locations', 'currency']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchUserDto.prototype, "relations", void 0);
exports.SearchUserDto = SearchUserDto;


/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var code_entity_1 = __webpack_require__(215);
exports.CodeEntity = code_entity_1.CodeEntity;
var code_module_1 = __webpack_require__(216);
exports.CodeModule = code_module_1.CodeModule;
var code_service_1 = __webpack_require__(217);
exports.CodeService = code_service_1.CodeService;
var code_controller_1 = __webpack_require__(235);
exports.CodeController = code_controller_1.CodeController;


/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
let CodeEntity = class CodeEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(1, 4),
    class_validator_1.IsNumber(),
    typeorm_1.Index(),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], CodeEntity.prototype, "code", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], CodeEntity.prototype, "expiredAt", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CodeEntity.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: "user" }),
    __metadata("design:type", typeof (_b = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _b : Object)
], CodeEntity.prototype, "userData", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CodeEntity.prototype, "type", void 0);
CodeEntity = __decorate([
    typeorm_1.Entity('code')
], CodeEntity);
exports.CodeEntity = CodeEntity;


/***/ }),
/* 216 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const code_service_1 = __webpack_require__(217);
const code_controller_1 = __webpack_require__(235);
const code_entity_1 = __webpack_require__(215);
const twilio_1 = __webpack_require__(223);
const email_1 = __webpack_require__(227);
const PROVIDERS = [
    code_service_1.CodeService,
    email_1.EmailService,
    twilio_1.TwillioService
];
const ENTITY = [
    code_entity_1.CodeEntity,
];
let CodeModule = class CodeModule {
};
CodeModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        controllers: [code_controller_1.CodeController],
        providers: [
            ...PROVIDERS,
            email_1.EmailCoreModule.getEmailConfig()
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], CodeModule);
exports.CodeModule = CodeModule;


/***/ }),
/* 217 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_1 = __webpack_require__(9);
const api_error_code_1 = __webpack_require__(94);
const database_1 = __webpack_require__(130);
const typeorm_1 = __webpack_require__(117);
const code_entity_1 = __webpack_require__(215);
const dto_1 = __webpack_require__(218);
const code_utils_1 = __webpack_require__(220);
const moment = __webpack_require__(222);
const environment_1 = __webpack_require__(6);
const shared_1 = __webpack_require__(25);
const twilio_1 = __webpack_require__(223);
const email_1 = __webpack_require__(227);
let CodeService = class CodeService extends core_1.CrudService {
    constructor(codeRepository, twilioService, emailService) {
        super(codeRepository);
        this.codeRepository = codeRepository;
        this.twilioService = twilioService;
        this.emailService = emailService;
    }
    createCode(id, type, isCheckCodeExpire) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                isCheckCodeExpire = false;
                let checkCodeExpire = yield this.codeRepository.findOne({
                    user: id,
                    type: type
                });
                let createCode;
                if (checkCodeExpire) {
                    if (!code_utils_1.checkExpiredTime(checkCodeExpire.expiredAt) && isCheckCodeExpire) {
                        return {
                            code: api_error_code_1.CodeEnum.CODE_STILL_VALID
                        };
                    }
                    checkCodeExpire.code = code_utils_1.generateVerifyCode();
                    checkCodeExpire.expiredAt = moment().add(environment_1.default.code.expireTime, 'minutes').toDate();
                    createCode = yield this.codeRepository.save(checkCodeExpire);
                }
                else {
                    const code = {
                        user: id,
                        code: code_utils_1.generateVerifyCode(),
                        expiredAt: moment().add(environment_1.default.code.expireTime, 'minutes').toDate(),
                        type
                    };
                    createCode = yield this.codeRepository.save(code);
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCodeDto, createCode)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getCode(code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getCode = yield this.codeRepository.findOne(code);
                if (!getCode) {
                    return {
                        code: api_error_code_1.CodeEnum.CODE_INCORRECT
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCodeDto, getCode)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getALlCode() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getAllCode = yield this.codeRepository.find();
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCodeDto, getAllCode)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteCode(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getCode = yield this.codeRepository.findOne(id);
                if (!getCode) {
                    return {
                        code: api_error_code_1.CodeEnum.CODE_NOT_EXISTED
                    };
                }
                let deleteCode = yield this.codeRepository.delete(id);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCodeDto, deleteCode)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    sendOTP(user, type, isCheckCodeExpire) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let createCode = yield this.createCode(user.id, type, isCheckCodeExpire);
                if (createCode.code) {
                    return {
                        code: createCode.code
                    };
                }
                const otp = {
                    to: `${user.countryCode}${user.phoneNumber}`,
                    message: createCode.data.code.toString()
                };
                this.twilioService.sendSMS(otp);
                return {
                    code: null
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    sendOTPByEmail(user, type, isCheckCodeExpire) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let createCode = yield this.createCode(user.id, type, isCheckCodeExpire);
                if (createCode.code) {
                    return {
                        code: createCode.code
                    };
                }
                const mailOptions = {
                    from: process.env.MAIL_HOST,
                    to: user.email,
                    subject: 'OTP',
                    template: 'sendOTP',
                    context: {
                        otp: createCode.data.code.toString()
                    }
                };
                this.emailService.sendMail(mailOptions);
                return {
                    code: null
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    reSendOTP(user, type, isCheckCodeExpire) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (user.phoneNumber) {
                    return yield this.sendOTP(user, type, isCheckCodeExpire);
                }
                else if (user.email) {
                    return yield this.sendOTPByEmail(user, type, isCheckCodeExpire);
                }
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
CodeService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(code_entity_1.CodeEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof twilio_1.TwillioService !== "undefined" && twilio_1.TwillioService) === "function" ? _b : Object, typeof (_c = typeof email_1.EmailService !== "undefined" && email_1.EmailService) === "function" ? _c : Object])
], CodeService);
exports.CodeService = CodeService;


/***/ }),
/* 218 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(219));


/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c, _d, _e, _f;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const typeorm_1 = __webpack_require__(120);
class TransformCodeDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCodeDto.prototype, "id", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformCodeDto.prototype, "code", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], TransformCodeDto.prototype, "expiredAt", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCodeDto.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "user" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _b : Object)
], TransformCodeDto.prototype, "userData", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCodeDto.prototype, "type", void 0);
exports.TransformCodeDto = TransformCodeDto;
class CreateCodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: '1111' }),
    class_validator_1.Length(1, 4),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateCodeDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '2020-07-24 17:14:50.477395' }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_c = typeof Date !== "undefined" && Date) === "function" ? _c : Object)
], CreateCodeDto.prototype, "expiredAt", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCodeDto.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "user" }),
    __metadata("design:type", typeof (_d = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _d : Object)
], CreateCodeDto.prototype, "userData", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'login' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCodeDto.prototype, "type", void 0);
exports.CreateCodeDto = CreateCodeDto;
class UpdateCodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: '1111' }),
    class_validator_1.Length(1, 4),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateCodeDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '2020-07-24 17:14:50.477395' }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_e = typeof Date !== "undefined" && Date) === "function" ? _e : Object)
], UpdateCodeDto.prototype, "expiredAt", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCodeDto.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_1.JoinColumn({ name: "user" }),
    __metadata("design:type", typeof (_f = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _f : Object)
], UpdateCodeDto.prototype, "userData", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'login' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCodeDto.prototype, "type", void 0);
exports.UpdateCodeDto = UpdateCodeDto;
class SearchCodeDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['code', 'user']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCodeDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformCodeDto, ['code', 'user'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCodeDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCodeDto.prototype, "order", void 0);
exports.SearchCodeDto = SearchCodeDto;
class ValiDateCodeDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 8888 }),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], ValiDateCodeDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], ValiDateCodeDto.prototype, "user", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], ValiDateCodeDto.prototype, "type", void 0);
exports.ValiDateCodeDto = ValiDateCodeDto;


/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const faker = __webpack_require__(221);
const moment = __webpack_require__(222);
exports.generateVerifyCode = () => {
    return faker.random.number({ min: 1000, max: 9999, precision: 4 });
};
exports.checkExpiredTime = (expiredAt) => {
    if (moment(expiredAt).isBefore(moment())) {
        return true;
    }
    return false;
};


/***/ }),
/* 221 */
/***/ (function(module, exports) {

module.exports = require("faker");

/***/ }),
/* 222 */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var twilio_service_1 = __webpack_require__(224);
exports.TwillioService = twilio_service_1.TwillioService;
var twilio_module_1 = __webpack_require__(226);
exports.TwilioModule = twilio_module_1.TwilioModule;


/***/ }),
/* 224 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const environment_1 = __webpack_require__(6);
const twilio = __webpack_require__(225);
class TwillioService {
    constructor() {
        this.twilioClient = twilio(environment_1.environment.twilio.sid, environment_1.environment.twilio.auth);
    }
    sendSMS(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const vObject = {
                    body: `${environment_1.environment.twilio.content}${data.message}`,
                    from: environment_1.environment.twilio.number,
                    to: data.to,
                };
                return this.twilioClient.messages.create(vObject);
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
}
exports.TwillioService = TwillioService;


/***/ }),
/* 225 */
/***/ (function(module, exports) {

module.exports = require("twilio");

/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const shared_1 = __webpack_require__(25);
const auth_1 = __webpack_require__(166);
const twilio_1 = __webpack_require__(223);
const PROVIDERS = [
    twilio_1.TwillioService
];
let TwilioModule = class TwilioModule {
};
TwilioModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
        ],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], TwilioModule);
exports.TwilioModule = TwilioModule;


/***/ }),
/* 227 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var email_module_1 = __webpack_require__(228);
exports.EmailModule = email_module_1.EmailModule;
var email_options_interface_1 = __webpack_require__(234);
exports.EmailModuleOptions = email_options_interface_1.EmailModuleOptions;
var email_service_1 = __webpack_require__(230);
exports.EmailService = email_service_1.EmailService;
var email_core_module_1 = __webpack_require__(229);
exports.EmailCoreModule = email_core_module_1.EmailCoreModule;


/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var EmailModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const email_core_module_1 = __webpack_require__(229);
let EmailModule = EmailModule_1 = class EmailModule {
    static forRoot(config) {
        return {
            module: EmailModule_1,
            imports: [email_core_module_1.EmailCoreModule.forRoot(config)],
        };
    }
};
EmailModule = EmailModule_1 = __decorate([
    common_1.Module({})
], EmailModule);
exports.EmailModule = EmailModule;


/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var EmailCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const email_service_1 = __webpack_require__(230);
const defaultConfig = {
    transport: {
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.MAIL_SECURE,
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD
        }
    },
    defaults: {
        forceEmbeddedImages: false,
        from: undefined,
    },
    templateDir: process.env.MAIL_TEMPLATE_DIR,
};
let EmailCoreModule = EmailCoreModule_1 = class EmailCoreModule {
    static forRoot(userConfig) {
        const EmailConfig = EmailCoreModule_1.getEmailConfig(userConfig);
        return {
            module: EmailCoreModule_1,
            providers: [email_service_1.EmailService, EmailConfig],
            exports: [email_service_1.EmailService],
        };
    }
    static getEmailConfig(userConfig) {
        const config = Object.assign(Object.assign({}, defaultConfig), userConfig);
        return {
            name: 'EMAIL_CONFIG',
            provide: 'EMAIL_CONFIG',
            useValue: {
                transport: config.transport,
                defaults: config.defaults,
                templateDir: config.templateDir,
            },
        };
    }
};
EmailCoreModule = EmailCoreModule_1 = __decorate([
    common_1.Global(),
    common_1.Module({})
], EmailCoreModule);
exports.EmailCoreModule = EmailCoreModule;


/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __webpack_require__(231);
const pug_1 = __webpack_require__(232);
const common_1 = __webpack_require__(3);
const nodemailer_1 = __webpack_require__(233);
const email_options_interface_1 = __webpack_require__(234);
let EmailService = class EmailService {
    constructor(mailerConfig) {
        this.mailerConfig = mailerConfig;
        if (!mailerConfig.transport || Object.keys(mailerConfig.transport).length < 1) {
            throw new Error('Make sure to provide a nodemailer transport configuration object, connection url or a transport plugin instance');
        }
        this.setupTransporter(mailerConfig.transport, mailerConfig.defaults, mailerConfig.templateDir);
    }
    setupTransporter(transport, defaults, templateDir) {
        this.transporter = nodemailer_1.createTransport(transport, defaults);
        this.transporter.use('compile', this.renderTemplate(templateDir));
    }
    sendMail(sendMailOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.transporter.sendMail(sendMailOptions);
        });
    }
    renderTemplate(templateDir) {
        return (mail, callback) => {
            if (mail.data.html) {
                return callback();
            }
            pug_1.renderFile(path_1.join(process.cwd(), templateDir || './public/templates', mail.data.template + '.pug'), mail.data.context, (err, body) => {
                if (err) {
                    return callback(err);
                }
                mail.data.html = body;
                return callback();
            });
        };
    }
};
EmailService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject('EMAIL_CONFIG')),
    __metadata("design:paramtypes", [typeof (_a = typeof email_options_interface_1.EmailModuleOptions !== "undefined" && email_options_interface_1.EmailModuleOptions) === "function" ? _a : Object])
], EmailService);
exports.EmailService = EmailService;


/***/ }),
/* 231 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 232 */
/***/ (function(module, exports) {

module.exports = require("pug");

/***/ }),
/* 233 */
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),
/* 234 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class EmailModuleOptions {
}
exports.EmailModuleOptions = EmailModuleOptions;


/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(218);
const code_service_1 = __webpack_require__(217);
const composeSecurity_guard_1 = __webpack_require__(236);
let CodeController = class CodeController extends core_1.CrudController {
    constructor(codeService) {
        super(codeService);
        this.codeService = codeService;
    }
    reSendOTP(user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let code = yield this.codeService.reSendOTP(user, "login", true);
                this.respondWithErrorsOrSuccess(code);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllCode() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let code = yield this.codeService.getALlCode();
                this.respondWithErrorsOrSuccess(code);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post('resendOTP'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CodeController.prototype, "reSendOTP", null);
__decorate([
    common_1.Post('findAll'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], CodeController.prototype, "getAllCode", null);
CodeController = __decorate([
    swagger_1.ApiTags('Code'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Code',
        createDto: dto_1.CreateCodeDto,
        updateDto: dto_1.UpdateCodeDto,
        transformDto: dto_1.TransformCodeDto,
        searchDto: dto_1.SearchCodeDto
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof code_service_1.CodeService !== "undefined" && code_service_1.CodeService) === "function" ? _d : Object])
], CodeController);
exports.CodeController = CodeController;


/***/ }),
/* 236 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const jwt_auth_guard_1 = __webpack_require__(176);
const logout_device_guard_1 = __webpack_require__(237);
let ComposeSecurity = class ComposeSecurity {
    constructor(jwtAuthGuard, logoutDeviceGuard) {
        this.jwtAuthGuard = jwtAuthGuard;
        this.logoutDeviceGuard = logoutDeviceGuard;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            return ((yield this.jwtAuthGuard.canActivate(context)) &&
                (yield this.logoutDeviceGuard.canActivate(context)));
        });
    }
};
ComposeSecurity = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof jwt_auth_guard_1.JwtAuthGuard !== "undefined" && jwt_auth_guard_1.JwtAuthGuard) === "function" ? _a : Object, typeof (_b = typeof logout_device_guard_1.LogoutDeviceGuard !== "undefined" && logout_device_guard_1.LogoutDeviceGuard) === "function" ? _b : Object])
], ComposeSecurity);
exports.ComposeSecurity = ComposeSecurity;


/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const devices_service_1 = __webpack_require__(238);
const request_utils_1 = __webpack_require__(245);
let LogoutDeviceGuard = class LogoutDeviceGuard {
    constructor(devicesService) {
        this.devicesService = devicesService;
    }
    canActivate(context) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            const httpContext = context.switchToHttp();
            const request = httpContext.getRequest();
            let ip = request_utils_1.getIpFromRequest(request);
            let userAgent = request.headers['user-agent'];
            let userId = (_a = request.user) === null || _a === void 0 ? void 0 : _a.id;
            if ((_b = request.user) === null || _b === void 0 ? void 0 : _b.registerCompleted) {
                if (!userId) {
                    return true;
                }
                if (yield this.devicesService.getCurrentDevice(ip, userAgent, userId)) {
                    return true;
                }
                else {
                    throw new common_1.UnauthorizedException();
                }
            }
            else {
                return true;
            }
        });
    }
};
LogoutDeviceGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof devices_service_1.DevicesService !== "undefined" && devices_service_1.DevicesService) === "function" ? _a : Object])
], LogoutDeviceGuard);
exports.LogoutDeviceGuard = LogoutDeviceGuard;


/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const devices_entity_1 = __webpack_require__(239);
const dto_1 = __webpack_require__(240);
const xml2js_1 = __webpack_require__(242);
const devices_utils_1 = __webpack_require__(243);
const typeorm_2 = __webpack_require__(120);
let DevicesService = class DevicesService extends core_1.CrudService {
    constructor(devicesRepository, httpService) {
        super(devicesRepository);
        this.devicesRepository = devicesRepository;
        this.httpService = httpService;
    }
    createDevice(ip, userAgent, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let deviceExist = yield this.getCurrentDevice(ip, userAgent, userId);
            let newDevice;
            if (deviceExist) {
                deviceExist.loginAt = new Date();
                newDevice = yield this.devicesRepository.save(deviceExist);
            }
            else {
                newDevice = yield this.devicesRepository.save(yield this.getNewDevice(ip, userAgent, userId));
            }
            return {
                data: newDevice
            };
        });
    }
    getLocationByIP(ip) {
        return __awaiter(this, void 0, void 0, function* () {
            let ipExist = yield this.devicesRepository.findOne({ deviceAddress: ip });
            if (ipExist) {
                return {
                    country: ipExist.country,
                    region: ipExist.region,
                    city: ipExist.city
                };
            }
            let get = yield this.httpService.get('http://www.geoplugin.net/xml.gp?ip=' + ip).toPromise();
            let country;
            let region;
            let city;
            xml2js_1.parseString(get.data, (err, result) => {
                if (err) {
                    throw err;
                }
                const json = JSON.parse(JSON.stringify(result, null, 4));
                country = json.geoPlugin.geoplugin_countryName[0] || null;
                region = json.geoPlugin.geoplugin_region[0] || null;
                city = json.geoPlugin.geoplugin_city[0] || null;
            });
            return {
                country,
                region,
                city
            };
        });
    }
    getNewDevice(ip, userAgent, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let location = yield this.getLocationByIP(ip);
            let _userAgent = devices_utils_1.formatUserAgent(userAgent);
            let newDevice = new dto_1.CreateDeviceDto();
            newDevice.userId = userId;
            newDevice.name = _userAgent.name;
            newDevice.country = location.country;
            newDevice.region = location.region;
            newDevice.city = location.city;
            newDevice.deviceAddress = ip;
            newDevice.loginAt = new Date();
            newDevice.browserName = _userAgent.browserName;
            newDevice.browserVersion = _userAgent.browserVersion;
            newDevice.osName = _userAgent.osName;
            newDevice.osVersion = _userAgent.osVersion;
            newDevice.deviceVendor = _userAgent.deviceVendor;
            newDevice.deviceModel = _userAgent.deviceModel;
            return newDevice;
        });
    }
    getCurrentDevice(ip, userAgent, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let device = yield this.getNewDevice(ip, userAgent, userId);
            let deviceExist = yield this.devicesRepository.findOne({
                userId: device.userId,
                name: device.name,
                country: device.country,
                region: device.region,
                city: device.city,
                deviceAddress: device.deviceAddress,
                browserName: device.browserName,
                browserVersion: device.browserVersion,
                osName: device.osName,
                osVersion: device.osVersion,
                deviceVendor: device.deviceVendor,
                deviceModel: device.deviceModel
            });
            return deviceExist;
        });
    }
    findAllDevices(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let findAllDevices = yield this.devicesRepository.find({ where: { userId }, order: { "loginAt": "DESC" } });
            return {
                data: findAllDevices
            };
        });
    }
    logoutOtherDevices(ip, userAgent, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let device = yield this.getCurrentDevice(ip, userAgent, userId);
            if (device) {
                let deviceId = device.id;
                yield this.devicesRepository.delete({ id: typeorm_2.Not(deviceId), userId });
            }
        });
    }
};
DevicesService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(devices_entity_1.DevicesEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof common_1.HttpService !== "undefined" && common_1.HttpService) === "function" ? _b : Object])
], DevicesService);
exports.DevicesService = DevicesService;


/***/ }),
/* 239 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
let DevicesEntity = class DevicesEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], DevicesEntity.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id),
    typeorm_1.JoinColumn({ name: "userId" }),
    __metadata("design:type", typeof (_a = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _a : Object)
], DevicesEntity.prototype, "user", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], DevicesEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "country", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "region", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "city", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], DevicesEntity.prototype, "deviceAddress", void 0);
__decorate([
    typeorm_1.Column({
        type: 'timestamp without time zone'
    }),
    __metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], DevicesEntity.prototype, "loginAt", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "browserName", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "browserVersion", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "osName", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "osVersion", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "deviceVendor", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DevicesEntity.prototype, "deviceModel", void 0);
DevicesEntity = __decorate([
    typeorm_1.Entity('devices')
], DevicesEntity);
exports.DevicesEntity = DevicesEntity;


/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(241));


/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const core_1 = __webpack_require__(9);
class TransformDeviceDto {
}
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "userId", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "country", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "region", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "city", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "deviceAddress", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], TransformDeviceDto.prototype, "loginAt", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "browserName", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "browserVersion", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "osName", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "osVersion", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "deviceVendor", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformDeviceDto.prototype, "deviceModel", void 0);
exports.TransformDeviceDto = TransformDeviceDto;
class CreateDeviceDto extends core_1.AbstractDto {
}
exports.CreateDeviceDto = CreateDeviceDto;


/***/ }),
/* 242 */
/***/ (function(module, exports) {

module.exports = require("xml2js");

/***/ }),
/* 243 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const ua_parser_js_1 = __webpack_require__(244);
exports.formatUserAgent = (userAgent) => {
    const ua = new ua_parser_js_1.UAParser(userAgent).getResult();
    let name;
    if (!ua.browser.name) {
        name = userAgent;
    }
    else {
        if (ua.device.vendor) {
            name = ua.device.vendor;
        }
        else {
            name = ua.browser.name + ' on ' + ua.os.name;
        }
    }
    return {
        name: name,
        browserName: ua.browser.name ? ua.browser.name : null,
        browserVersion: ua.browser.version ? ua.browser.version : null,
        osName: ua.os.name ? ua.os.name : null,
        osVersion: ua.os.version ? ua.os.version : null,
        deviceVendor: ua.device.vendor ? ua.device.vendor : null,
        deviceModel: ua.device.model ? ua.device.model : null
    };
};


/***/ }),
/* 244 */
/***/ (function(module, exports) {

module.exports = require("ua-parser-js");

/***/ }),
/* 245 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.getIpFromRequest = (request) => {
    let ip = request.headers['x-forwarded-for'] || (request === null || request === void 0 ? void 0 : request.ip);
    ip = ip.split(',')[0];
    ip = ip.split(':').slice(-1)[0];
    return ip;
};


/***/ }),
/* 246 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var devices_module_1 = __webpack_require__(247);
exports.DeviceModule = devices_module_1.DeviceModule;
var devices_service_1 = __webpack_require__(238);
exports.DevicesService = devices_service_1.DevicesService;
var devices_controller_1 = __webpack_require__(248);
exports.DeviceController = devices_controller_1.DeviceController;
var devices_entity_1 = __webpack_require__(239);
exports.DevicesEntity = devices_entity_1.DevicesEntity;


/***/ }),
/* 247 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const devices_entity_1 = __webpack_require__(239);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const devices_controller_1 = __webpack_require__(248);
const devices_service_1 = __webpack_require__(238);
const PROVIDERS = [
    devices_service_1.DevicesService
];
const ENTITY = [
    devices_entity_1.DevicesEntity
];
let DeviceModule = class DeviceModule {
};
DeviceModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
            common_1.HttpModule
        ],
        controllers: [devices_controller_1.DeviceController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], DeviceModule);
exports.DeviceModule = DeviceModule;


/***/ }),
/* 248 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const devices_service_1 = __webpack_require__(238);
const composeSecurity_guard_1 = __webpack_require__(236);
let DeviceController = class DeviceController extends core_1.CrudController {
    constructor(devicesService) {
        super(devicesService);
        this.devicesService = devicesService;
    }
    findAllDevices(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let category = yield this.devicesService.findAllDevices(req.user.id);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_a = typeof Promise !== "undefined" && Promise) === "function" ? _a : Object)
], DeviceController.prototype, "findAllDevices", null);
DeviceController = __decorate([
    swagger_1.ApiTags('Devices'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Device',
        createDto: null,
        updateDto: null,
        transformDto: null,
        searchDto: null
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof devices_service_1.DevicesService !== "undefined" && devices_service_1.DevicesService) === "function" ? _b : Object])
], DeviceController);
exports.DeviceController = DeviceController;


/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __webpack_require__(231);
const multer_1 = __webpack_require__(250);
const multer = __webpack_require__(250);
const AWS = __webpack_require__(197);
const multerS3 = __webpack_require__(251);
const environment_1 = __webpack_require__(6);
const common_1 = __webpack_require__(3);
const fs_1 = __webpack_require__(198);
const fs = __webpack_require__(198);
const path = __webpack_require__(231);
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});
var TypeServer;
(function (TypeServer) {
    TypeServer[TypeServer["s3"] = 0] = "s3";
    TypeServer[TypeServer["server"] = 1] = "server";
})(TypeServer || (TypeServer = {}));
exports.fileInterceptor = (type) => {
    var date = new Date();
    var upload;
    if (type === 's3') {
        upload = multer({
            storage: multerS3({
                s3: new AWS.S3(),
                bucket: process.env.AWS_BUCKET,
                acl: 'public-read',
                key: function (request, file, cb) {
                    cb(null, `${Date.now().toString()}-${file.originalname}`);
                },
            }),
        }).array('upload', 1);
    }
    else {
        upload = {
            storage: multer_1.diskStorage({
                destination: `${environment_1.environment.images.path}/${exports.genratePath()}/`,
                filename: exports.editFileName,
            }),
            fileFilter: exports.imageFileFilter,
            limits: {
                fileSize: eval(process.env.IMAGE_FILE_SIZE)
            },
        };
    }
    return upload;
};
exports.imageFileFilter = (req, file, callback) => {
    file.originalname = file.originalname.toLowerCase();
    if (!file.originalname.match(/\.(jpg|JPG|webp)$/)) {
        return callback(new Error('Type of photos allow ".jpg, .webp"!'), false);
    }
    callback(null, true);
};
exports.editFileName = (req, file, callback) => {
    var name = file.originalname.split('.')[0];
    name = name.replace(/ /g, '-');
    const fileExtName = path_1.extname(file.originalname);
    const randomName = Array(4)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    callback(null, `${name}-${randomName}${fileExtName}`);
};
exports.genratePath = () => {
    var date = new Date();
    return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;
    ;
};
exports.getImageUrl = (images, isMultiple) => {
    const host = `/${process.env.IMAGE_API}`;
    let resData;
    if (isMultiple) {
        resData = [];
        for (let i = 0; i < images.length; i++) {
            if (typeof images[i] === 'string') {
                resData.push(images[i]);
            }
            else {
                images[i].path = `${host}/${encodeURIComponent(images[i].path)}`;
                resData.push(images[i]);
            }
        }
    }
    else {
        if (typeof images === 'string') {
            resData = images;
        }
        else {
            images.path = `${host}/${encodeURIComponent(images.path)}`;
            resData = images;
        }
    }
    return resData;
};
exports.excelFileFilter = (req, file, cb) => {
    const fileExension = path_1.extname(file.originalname);
    if (fileExension === '.xlsx' || fileExension === '.xls') {
        cb(null, true);
    }
    else {
        cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
    }
};
exports.excelMulterOptions = {
    fileFilter: exports.excelFileFilter,
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = process.env.UPLOAD_FILE_PATH;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath);
            }
            cb(null, uploadPath);
        },
        filename: exports.editFileName
    }),
    limits: {
        fileSize: eval(process.env.UPLOAD_FILE_SIZE)
    },
};
exports.imageFilter = (file) => {
    file.originalname = file.originalname.toLowerCase();
    if (!file.originalname.match(/\.(jpg|JPG|webp|null)$/)) {
        return new Error('Only image files are allowed!'), false;
    }
    return;
};
exports.editImageName = (file) => {
    var name = file.originalname.split('.')[0];
    name = name.replace(/ /g, '-');
    const fileExtName = path_1.extname(file.originalname);
    const randomName = Array(4)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    return `${name}-${randomName}${fileExtName}`;
};
exports.saveImage = (file) => {
    try {
        exports.imageFilter(file);
        file.originalname = exports.editImageName(file);
        const imgPath = `./${environment_1.environment.images.path}/${exports.genratePath()}/${file.originalname}`;
        const imgToSavePath = path.join(__dirname.replace("dist", ""), imgPath);
        fs.writeFile(imgToSavePath, file.buffer, err => {
            if (err) {
                throw err;
            }
        });
        var image = {
            filename: file.originalname,
            path: imgPath.replace("./", "")
        };
        return image;
    }
    catch (e) {
        throw e;
    }
};


/***/ }),
/* 250 */
/***/ (function(module, exports) {

module.exports = require("multer");

/***/ }),
/* 251 */
/***/ (function(module, exports) {

module.exports = require("multer-s3");

/***/ }),
/* 252 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const database_1 = __webpack_require__(130);
const typeorm_2 = __webpack_require__(120);
const countries_entity_1 = __webpack_require__(204);
let CountryService = class CountryService {
    constructor(countryRepository) {
        this.countryRepository = countryRepository;
    }
    getAllCountries(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (filter) {
                    if (filter.where) {
                        const value = filter.where.search || '';
                        if (typeof value === 'number') {
                            filter.where = { id: value };
                        }
                        else {
                            const queryName = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                            filter.where =
                                {
                                    name: queryName
                                };
                        }
                    }
                }
                const countries = yield this.countryRepository.find(filter);
                return countries;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getCountry(name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const country = yield this.countryRepository.findOne({ where: { nicename: name } });
                return country;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
CountryService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(countries_entity_1.CountryEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object])
], CountryService);
exports.CountryService = CountryService;


/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const countries_service_1 = __webpack_require__(252);
const countries_controller_1 = __webpack_require__(254);
const countries_entity_1 = __webpack_require__(204);
const auth_1 = __webpack_require__(166);
const shared_1 = __webpack_require__(25);
const typeorm_1 = __webpack_require__(117);
const database_1 = __webpack_require__(130);
const PROVIDERS = [
    countries_service_1.CountryService
];
const ENTITY = [
    countries_entity_1.CountryEntity
];
let CountryModule = class CountryModule {
};
CountryModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [countries_controller_1.CountryController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], CountryModule);
exports.CountryModule = CountryModule;


/***/ }),
/* 254 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const countries_service_1 = __webpack_require__(252);
const countries_dto_1 = __webpack_require__(255);
let CountryController = class CountryController {
    constructor(countryService) {
        this.countryService = countryService;
    }
    getAllCountries(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const countries = yield this.countryService.getAllCountries(filter);
                return countries;
            }
            catch (e) {
                console.log(e);
                throw (e);
            }
        });
    }
};
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof countries_dto_1.SearchCountryDto !== "undefined" && countries_dto_1.SearchCountryDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CountryController.prototype, "getAllCountries", null);
CountryController = __decorate([
    swagger_1.ApiTags('Countries'),
    common_1.Controller(),
    __metadata("design:paramtypes", [typeof (_c = typeof countries_service_1.CountryService !== "undefined" && countries_service_1.CountryService) === "function" ? _c : Object])
], CountryController);
exports.CountryController = CountryController;


/***/ }),
/* 255 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const class_transformer_1 = __webpack_require__(16);
class SearchCountryDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'iso', 'nicename', 'phonecode', 'barcode']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCountryDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Viet Nam" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCountryDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCountryDto.prototype, "order", void 0);
exports.SearchCountryDto = SearchCountryDto;


/***/ }),
/* 256 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var user_module_1 = __webpack_require__(257);
exports.UserModule = user_module_1.UserModule;
var user_entity_1 = __webpack_require__(200);
exports.UserEntity = user_entity_1.UserEntity;
var profile_entity_1 = __webpack_require__(259);
exports.ProfileEntity = profile_entity_1.ProfileEntity;
var image_entity_1 = __webpack_require__(260);
exports.ImageEntity = image_entity_1.ImageEntity;
var user_service_1 = __webpack_require__(211);
exports.UserService = user_service_1.UserService;


/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const user_controller_1 = __webpack_require__(258);
const user_service_1 = __webpack_require__(211);
const user_entity_1 = __webpack_require__(200);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const PROVIDERS = [
    user_service_1.UserService,
];
const ENTITIES = [
    user_entity_1.UserEntity,
];
let UserModule = class UserModule {
};
UserModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITIES, database_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [user_controller_1.UserController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS,
        ],
    })
], UserModule);
exports.UserModule = UserModule;


/***/ }),
/* 258 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(212);
const user_service_1 = __webpack_require__(211);
const composeSecurity_guard_1 = __webpack_require__(236);
let UserController = class UserController extends core_1.CrudController {
    constructor(userService) {
        super(userService);
        this.userService = userService;
    }
    updateProfile(req, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = req.user.id;
                let user = yield this.userService.updateProfile(id, data);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updatePreference(req, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const id = req.user.id;
                const user = yield this.userService.updatePreference(id, data);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateUser(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                let user = yield this.userService.updateUser(id, data);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getProfile(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = req.user.id;
                let user = yield this.userService.findUser(id);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllUser(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield this.userService.findAllUser(filter);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    confirmProfile(req, user, code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let confirmProfile = yield this.userService.confirmChangeUser(req, user, code);
                this.respondWithErrorsOrSuccess(confirmProfile);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    resendOTPChangeProfile(req, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = req.user.id;
                let user = yield this.userService.resendOTPChangeProfile(id, data);
                this.respondWithErrorsOrSuccess(user);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Put(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_a = typeof dto_1.UpdateUserDto !== "undefined" && dto_1.UpdateUserDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], UserController.prototype, "updateProfile", null);
__decorate([
    common_1.Put('settingPreference'),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_c = typeof dto_1.UpdatePreferenceDto !== "undefined" && dto_1.UpdatePreferenceDto) === "function" ? _c : Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], UserController.prototype, "updatePreference", null);
__decorate([
    common_1.Put(':id'),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, typeof (_e = typeof dto_1.UpdateUserDto !== "undefined" && dto_1.UpdateUserDto) === "function" ? _e : Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], UserController.prototype, "updateUser", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], UserController.prototype, "getProfile", null);
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_h = typeof dto_1.SearchUserDto !== "undefined" && dto_1.SearchUserDto) === "function" ? _h : Object]),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], UserController.prototype, "getAllUser", null);
__decorate([
    common_1.Post('confirmChangeProfile'),
    __param(0, common_1.Req()), __param(1, common_1.Body()), __param(2, common_1.Query('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_k = typeof dto_1.UpdateUserDto !== "undefined" && dto_1.UpdateUserDto) === "function" ? _k : Object, Number]),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], UserController.prototype, "confirmProfile", null);
__decorate([
    common_1.Post('resendOTPChangeProfile'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_m = typeof dto_1.UpdateUserDto !== "undefined" && dto_1.UpdateUserDto) === "function" ? _m : Object]),
    __metadata("design:returntype", typeof (_o = typeof Promise !== "undefined" && Promise) === "function" ? _o : Object)
], UserController.prototype, "resendOTPChangeProfile", null);
UserController = __decorate([
    swagger_1.ApiTags('User'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'User',
        createDto: dto_1.CreateUserDto,
        updateDto: dto_1.UpdateUserDto,
        transformDto: dto_1.TransformUserDto,
        searchDto: dto_1.SearchUserDto
    }),
    __metadata("design:paramtypes", [typeof (_p = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" ? _p : Object])
], UserController);
exports.UserController = UserController;


/***/ }),
/* 259 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = __webpack_require__(9);
const typeorm_1 = __webpack_require__(120);
let ProfileEntity = class ProfileEntity extends core_1.AbstractProfileEntity {
};
ProfileEntity = __decorate([
    typeorm_1.Entity('core_profile')
], ProfileEntity);
exports.ProfileEntity = ProfileEntity;


/***/ }),
/* 260 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
let ImageEntity = class ImageEntity extends core_1.AbstractImageEntity {
};
ImageEntity = __decorate([
    typeorm_1.Entity('core_image')
], ImageEntity);
exports.ImageEntity = ImageEntity;


/***/ }),
/* 261 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const passport_jwt_1 = __webpack_require__(180);
const passport_1 = __webpack_require__(43);
const common_1 = __webpack_require__(3);
const websockets_1 = __webpack_require__(29);
const config_1 = __webpack_require__(52);
const auth_service_1 = __webpack_require__(181);
const extractJwtFromWsQuery = req => {
    let token = null;
    if (req.handshake.query && req.handshake.query.token) {
        {
            token = req.handshake.query.token;
        }
        return token;
    }
};
let WsJwtStrategy = class WsJwtStrategy extends passport_1.PassportStrategy(passport_jwt_1.Strategy, 'ws-jwt') {
    constructor(configService, authService) {
        super({
            jwtFromRequest: extractJwtFromWsQuery,
            secretOrKey: configService.get('jwt.secretOrKey'),
            algorithm: [configService.get('jwt.signOptions.algorithm')],
        });
        this.configService = configService;
        this.authService = authService;
    }
    validate(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(['WsJwtStrategy', { payload }]);
            const { iat, exp, id: userId } = payload;
            const timeDiff = exp - iat;
            if (timeDiff <= 0) {
                throw new websockets_1.WsException('UnauthorizedException');
            }
            return payload;
        });
    }
};
WsJwtStrategy = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _a : Object, typeof (_b = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _b : Object])
], WsJwtStrategy);
exports.WsJwtStrategy = WsJwtStrategy;


/***/ }),
/* 262 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var AuthController_1, _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5, _6;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const cqrs_1 = __webpack_require__(30);
const core_1 = __webpack_require__(9);
const shared_1 = __webpack_require__(25);
const decorators_1 = __webpack_require__(263);
const auth_service_1 = __webpack_require__(181);
const commands_1 = __webpack_require__(270);
const dto_1 = __webpack_require__(184);
const composeSecurity_guard_1 = __webpack_require__(236);
const phoneNumber_utils_1 = __webpack_require__(183);
const crud_options_dto_1 = __webpack_require__(186);
let AuthController = AuthController_1 = class AuthController extends core_1.AbstractApiController {
    constructor(authService, commandBus, logger) {
        super();
        this.authService = authService;
        this.commandBus = commandBus;
        this.logger = logger;
        this.logger.setContext(AuthController_1.name);
    }
    register(userRegisterDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(userRegisterDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.commandBus.execute(new commands_1.AuthRegisterCommand(userRegisterDto));
                this.logger.debug(`[register] User ${userRegisterDto.email} register`);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    resendOTPReister(resendOTPDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(resendOTPDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                resendOTPDto.phoneNumber = resendOTPDto.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(resendOTPDto.phoneNumber) : undefined;
                let data = yield this.authService.resendOTPRegister(resendOTPDto);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    verifiedAccount(userAuthRegister, code, type) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(userAuthRegister);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.authService.verifiedAccount(userAuthRegister, code, type);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    login(userLoginDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(userLoginDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.authService.login(userLoginDto, core_1.RequestContext.currentRequest());
                this.logger.debug(`[login] User ${userLoginDto.email} logging`);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    confirmOTP(userLoginDto, code) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(userLoginDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.authService.confirmOTP(userLoginDto, code, core_1.RequestContext.currentRequest());
                this.logger.debug(`[login] User ${userLoginDto.email} logging`);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    requestPass(userAuthRequestPasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.authService.requestPassword(userAuthRequestPasswordDto);
            this.respondWithErrorsOrSuccess(data);
        });
    }
    resetPassword(userAuthResetPasswordDto, user) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.authService.resetPassword(userAuthResetPasswordDto, user);
            this.respondWithErrorsOrSuccess(data);
        });
    }
    getProfile(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.authService.getAuthenticatedUser(req.user.id);
            this.respondWithErrorsOrSuccess(data);
        });
    }
    getProfile2(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.authService.getAuthenticatedUser(user.id);
            this.respondWithErrorsOrSuccess(data);
        });
    }
    checkCompanyAddress(companyAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.authService.checkCompanyAddress(companyAddress);
        });
    }
    resendOTP(resendOTPDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(resendOTPDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                resendOTPDto.phoneNumber = resendOTPDto.phoneNumber ? phoneNumber_utils_1.stripeZeroOut(resendOTPDto.phoneNumber) : undefined;
                let data = yield this.authService.resendOTP(resendOTPDto);
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    changePassword(req, userChangePasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(userChangePasswordDto);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                let changePassword = yield this.authService.changePassword(req, userChangePasswordDto);
                this.respondWithErrorsOrSuccess(changePassword);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    forgotPassword(userAuthForgotPasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(userAuthForgotPasswordDto);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const forgotPassword = yield this.authService.forgotPassword(userAuthForgotPasswordDto);
                this.respondWithErrorsOrSuccess(forgotPassword);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    confirmForgotPasswordOTP(userAuthForgotPasswordDto, code) {
        return __awaiter(this, void 0, void 0, function* () {
            const errors = yield this.validateWith(userAuthForgotPasswordDto);
            if (errors.length > 0) {
                this.respondWithErrors(errors);
            }
            else {
                const data = yield this.authService.confirmForgotPasswordOTP(userAuthForgotPasswordDto, code, core_1.RequestContext.currentRequest());
                this.respondWithErrorsOrSuccess(data);
            }
        });
    }
    changeForgotPassword(req, userChangePasswordDto) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(userChangePasswordDto);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const changePassword = yield this.authService.changeForgotPassword(req, userChangePasswordDto);
                this.respondWithErrorsOrSuccess(changePassword);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post('register'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.UserAuthRegisterDto, null)
    }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof dto_1.UserAuthRegisterDto !== "undefined" && dto_1.UserAuthRegisterDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], AuthController.prototype, "register", null);
__decorate([
    common_1.Post('resendOTPRegister'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_c = typeof dto_1.UserAuthRegisterDto !== "undefined" && dto_1.UserAuthRegisterDto) === "function" ? _c : Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], AuthController.prototype, "resendOTPReister", null);
__decorate([
    common_1.Post('verifiedAccount'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.JWTAuthDto, null),
        description: 'User info with access token',
    }),
    __param(0, common_1.Body()), __param(1, common_1.Query('code')), __param(2, common_1.Query('type')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_e = typeof dto_1.UserAuthRegisterDto !== "undefined" && dto_1.UserAuthRegisterDto) === "function" ? _e : Object, Number, String]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], AuthController.prototype, "verifiedAccount", null);
__decorate([
    common_1.Post('login'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.JWTAuthDto, null),
        description: 'User info with access token',
    }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_g = typeof dto_1.UserAuthLoginDto !== "undefined" && dto_1.UserAuthLoginDto) === "function" ? _g : Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], AuthController.prototype, "login", null);
__decorate([
    common_1.Post('comfirmOTP'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.JWTAuthDto, null),
        description: 'User info with access token',
    }),
    __param(0, common_1.Body()), __param(1, common_1.Query('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_j = typeof dto_1.UserAuthLoginDto !== "undefined" && dto_1.UserAuthLoginDto) === "function" ? _j : Object, Number]),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], AuthController.prototype, "confirmOTP", null);
__decorate([
    common_1.Post('request-password'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(Boolean, 'RequestPassword'),
        description: 'User info with access token',
    }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_l = typeof dto_1.UserAuthRequestPasswordDto !== "undefined" && dto_1.UserAuthRequestPasswordDto) === "function" ? _l : Object]),
    __metadata("design:returntype", typeof (_m = typeof Promise !== "undefined" && Promise) === "function" ? _m : Object)
], AuthController.prototype, "requestPass", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.UserTransformDto, null),
        description: 'User info with access token',
    }),
    common_1.Post('reset-password'),
    __param(0, common_1.Body()), __param(1, decorators_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_o = typeof dto_1.UserAuthResetPasswordDto !== "undefined" && dto_1.UserAuthResetPasswordDto) === "function" ? _o : Object, typeof (_p = typeof shared_1.JwtPayloadInterface !== "undefined" && shared_1.JwtPayloadInterface) === "function" ? _p : Object]),
    __metadata("design:returntype", typeof (_q = typeof Promise !== "undefined" && Promise) === "function" ? _q : Object)
], AuthController.prototype, "resetPassword", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.UserTransformDto, null),
        description: 'Me',
    }),
    common_1.Get('me'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_r = typeof Promise !== "undefined" && Promise) === "function" ? _r : Object)
], AuthController.prototype, "getProfile", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.UserTransformDto, null),
        description: 'Me2',
    }),
    common_1.Get('me2'),
    __param(0, decorators_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_s = typeof shared_1.JwtPayloadInterface !== "undefined" && shared_1.JwtPayloadInterface) === "function" ? _s : Object]),
    __metadata("design:returntype", typeof (_t = typeof Promise !== "undefined" && Promise) === "function" ? _t : Object)
], AuthController.prototype, "getProfile2", null);
__decorate([
    common_1.Get('checkCompanyAddress'),
    __param(0, common_1.Query("companyAddress")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "checkCompanyAddress", null);
__decorate([
    common_1.Post('resendOTP'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_u = typeof dto_1.UserAuthLoginDto !== "undefined" && dto_1.UserAuthLoginDto) === "function" ? _u : Object]),
    __metadata("design:returntype", typeof (_v = typeof Promise !== "undefined" && Promise) === "function" ? _v : Object)
], AuthController.prototype, "resendOTP", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post('changePassword'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_w = typeof dto_1.UserChangePasswordDto !== "undefined" && dto_1.UserChangePasswordDto) === "function" ? _w : Object]),
    __metadata("design:returntype", typeof (_x = typeof Promise !== "undefined" && Promise) === "function" ? _x : Object)
], AuthController.prototype, "changePassword", null);
__decorate([
    common_1.Post('forgotPassword'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_y = typeof crud_options_dto_1.UserAuthForgotPasswordDto !== "undefined" && crud_options_dto_1.UserAuthForgotPasswordDto) === "function" ? _y : Object]),
    __metadata("design:returntype", typeof (_z = typeof Promise !== "undefined" && Promise) === "function" ? _z : Object)
], AuthController.prototype, "forgotPassword", null);
__decorate([
    common_1.Post('confirmForgotPasswordOTP'),
    swagger_1.ApiOkResponse({
        type: core_1.SerializeHelper.createGetOneDto(dto_1.JWTAuthDto, null),
        description: 'User info with access token',
    }),
    __param(0, common_1.Body()), __param(1, common_1.Query('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_0 = typeof crud_options_dto_1.UserAuthForgotPasswordDto !== "undefined" && crud_options_dto_1.UserAuthForgotPasswordDto) === "function" ? _0 : Object, Number]),
    __metadata("design:returntype", typeof (_1 = typeof Promise !== "undefined" && Promise) === "function" ? _1 : Object)
], AuthController.prototype, "confirmForgotPasswordOTP", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post('changeForgotPassword'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_2 = typeof crud_options_dto_1.UserChangeForgotPasswordDto !== "undefined" && crud_options_dto_1.UserChangeForgotPasswordDto) === "function" ? _2 : Object]),
    __metadata("design:returntype", typeof (_3 = typeof Promise !== "undefined" && Promise) === "function" ? _3 : Object)
], AuthController.prototype, "changeForgotPassword", null);
AuthController = AuthController_1 = __decorate([
    swagger_1.ApiTags('Auth'),
    common_1.Controller(),
    __metadata("design:paramtypes", [typeof (_4 = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _4 : Object, typeof (_5 = typeof cqrs_1.CommandBus !== "undefined" && cqrs_1.CommandBus) === "function" ? _5 : Object, typeof (_6 = typeof shared_1.PinoAppLoggerService !== "undefined" && shared_1.PinoAppLoggerService) === "function" ? _6 : Object])
], AuthController);
exports.AuthController = AuthController;


/***/ }),
/* 263 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(264));
__export(__webpack_require__(265));
__export(__webpack_require__(266));
__export(__webpack_require__(267));
__export(__webpack_require__(268));
__export(__webpack_require__(269));


/***/ }),
/* 264 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
exports.Roles = (...roles) => common_1.SetMetadata('roles', roles);


/***/ }),
/* 265 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
exports.Allow = (...roles) => common_1.SetMetadata('allow', roles);


/***/ }),
/* 266 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
exports.Claims = (...claims) => common_1.SetMetadata('claims', claims);


/***/ }),
/* 267 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
exports.CurrentUser = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const _a = request.user, { password } = _a, currentUser = __rest(_a, ["password"]);
    return currentUser;
});


/***/ }),
/* 268 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
exports.Token = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.authInfo.token;
});


/***/ }),
/* 269 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const passport_jwt_1 = __webpack_require__(180);
const extractor = passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken();
exports.RawToken = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return extractor(request);
});


/***/ }),
/* 270 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var auth_register_command_1 = __webpack_require__(271);
exports.AuthRegisterCommand = auth_register_command_1.AuthRegisterCommand;


/***/ }),
/* 271 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class AuthRegisterCommand {
    constructor(input) {
        this.input = input;
    }
}
exports.AuthRegisterCommand = AuthRegisterCommand;
AuthRegisterCommand.type = '[Auth] Register';


/***/ }),
/* 272 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const auth_register_handler_1 = __webpack_require__(273);
exports.CommandHandlers = [auth_register_handler_1.AuthRegisterHandler];


/***/ }),
/* 273 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const cqrs_1 = __webpack_require__(30);
const auth_register_command_1 = __webpack_require__(271);
const auth_service_1 = __webpack_require__(181);
let AuthRegisterHandler = class AuthRegisterHandler {
    constructor(authService, publisher) {
        this.authService = authService;
        this.publisher = publisher;
    }
    execute(command) {
        return __awaiter(this, void 0, void 0, function* () {
            const { input } = command;
            return yield this.authService.register(input);
        });
    }
};
AuthRegisterHandler = __decorate([
    cqrs_1.CommandHandler(auth_register_command_1.AuthRegisterCommand),
    __metadata("design:paramtypes", [typeof (_a = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _a : Object, typeof (_b = typeof cqrs_1.EventPublisher !== "undefined" && cqrs_1.EventPublisher) === "function" ? _b : Object])
], AuthRegisterHandler);
exports.AuthRegisterHandler = AuthRegisterHandler;


/***/ }),
/* 274 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var flag_entity_1 = __webpack_require__(275);
exports.FlagEntity = flag_entity_1.FlagEntity;
var flag_module_1 = __webpack_require__(276);
exports.FlagModule = flag_module_1.FlagModule;
var flag_service_1 = __webpack_require__(277);
exports.FlagService = flag_service_1.FlagService;
var flag_controller_1 = __webpack_require__(280);
exports.FlagController = flag_controller_1.FlagController;


/***/ }),
/* 275 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
let FlagEntity = class FlagEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], FlagEntity.prototype, "user", void 0);
__decorate([
    typeorm_2.ManyToOne(type => core_1.UserEntity, user => user.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_2.JoinColumn({ name: "user" }),
    __metadata("design:type", typeof (_a = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _a : Object)
], FlagEntity.prototype, "userDate", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], FlagEntity.prototype, "address", void 0);
FlagEntity = __decorate([
    typeorm_1.Entity('flag')
], FlagEntity);
exports.FlagEntity = FlagEntity;


/***/ }),
/* 276 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const flag_service_1 = __webpack_require__(277);
const flag_entity_1 = __webpack_require__(275);
const flag_controller_1 = __webpack_require__(280);
const PROVIDERS = [
    flag_service_1.FlagService
];
const ENTITY = [
    flag_entity_1.FlagEntity
];
let FlagModule = class FlagModule {
};
FlagModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [flag_controller_1.FlagController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], FlagModule);
exports.FlagModule = FlagModule;


/***/ }),
/* 277 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const flag_entity_1 = __webpack_require__(275);
const dto_1 = __webpack_require__(278);
const user_1 = __webpack_require__(256);
const api_error_code_1 = __webpack_require__(94);
const shared_1 = __webpack_require__(25);
let FlagService = class FlagService extends core_1.CrudService {
    constructor(flagRepository, userService) {
        super(flagRepository);
        this.flagRepository = flagRepository;
        this.userService = userService;
    }
    createFlag(flag) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let userExist = yield this.userService.findOne({ id: flag.user });
                if (!userExist) {
                    return {
                        code: api_error_code_1.CodeEnum.USER_NOT_EXISTED
                    };
                }
                let createFlag = yield this.flagRepository.save(flag);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformFlagDto, createFlag)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getFlag(flag) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getFlag = yield this.flagRepository.findOne(flag);
                if (!getFlag) {
                    return {
                        code: api_error_code_1.CodeEnum.FLAG_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformFlagDto, getFlag)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteFlag(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getFlag = yield this.flagRepository.findOne(id);
                if (!getFlag) {
                    return {
                        code: api_error_code_1.CodeEnum.FLAG_NOT_EXISTED
                    };
                }
                let deleteFLag = yield this.flagRepository.delete(id);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformFlagDto, deleteFLag)
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
FlagService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(flag_entity_1.FlagEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof user_1.UserService !== "undefined" && user_1.UserService) === "function" ? _b : Object])
], FlagService);
exports.FlagService = FlagService;


/***/ }),
/* 278 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(279));


/***/ }),
/* 279 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TransformFlagDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformFlagDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformFlagDto.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(type => core_1.UserEntity, user => user.id),
    typeorm_1.JoinColumn({ name: "user" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof core_1.UserEntity !== "undefined" && core_1.UserEntity) === "function" ? _a : Object)
], TransformFlagDto.prototype, "userData", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformFlagDto.prototype, "address", void 0);
exports.TransformFlagDto = TransformFlagDto;
class CreateFlagDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateFlagDto.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'AAAA-BBBB-CCCC-DDDD' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateFlagDto.prototype, "address", void 0);
exports.CreateFlagDto = CreateFlagDto;
class UpdateFlagDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateFlagDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateFlagDto.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'AAAA-BBBB-CCCC-DDDD' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateFlagDto.prototype, "address", void 0);
exports.UpdateFlagDto = UpdateFlagDto;
class SearchFlagDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['user', 'address']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchFlagDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformFlagDto, ['user', 'address'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchFlagDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchFlagDto.prototype, "order", void 0);
exports.SearchFlagDto = SearchFlagDto;


/***/ }),
/* 280 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(278);
const flag_service_1 = __webpack_require__(277);
let FlagController = class FlagController extends core_1.CrudController {
    constructor(flagService) {
        super(flagService);
        this.flagService = flagService;
    }
};
FlagController = __decorate([
    swagger_1.ApiTags('Flags'),
    common_1.Controller(),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Flag',
        createDto: dto_1.CreateFlagDto,
        updateDto: dto_1.UpdateFlagDto,
        transformDto: dto_1.TransformFlagDto,
        searchDto: dto_1.SearchFlagDto
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof flag_service_1.FlagService !== "undefined" && flag_service_1.FlagService) === "function" ? _a : Object])
], FlagController);
exports.FlagController = FlagController;


/***/ }),
/* 281 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(282));


/***/ }),
/* 282 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const access_control_1 = __webpack_require__(283);
let ExampleModule = class ExampleModule {
};
ExampleModule = __decorate([
    common_1.Module({
        imports: [
            access_control_1.AccessControlModule
        ],
    })
], ExampleModule);
exports.ExampleModule = ExampleModule;


/***/ }),
/* 283 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(284));
__export(__webpack_require__(316));


/***/ }),
/* 284 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(285));
__export(__webpack_require__(307));
__export(__webpack_require__(309));
__export(__webpack_require__(310));


/***/ }),
/* 285 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const casbin_1 = __webpack_require__(286);
const access_control_contstant_1 = __webpack_require__(306);
let AbacController = class AbacController {
    constructor(casbinService) {
        this.casbinService = casbinService;
    }
    reloadPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.reloadPolicy();
        });
    }
    getPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getPolicy();
        });
    }
    getAllRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllRoles();
        });
    }
    getAllObjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllObjects();
        });
    }
    getAllSubjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllSubjects();
        });
    }
    getAllActions() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllActions();
        });
    }
    addRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    getRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getImplicitRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getPermissionsForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_PRIMARY)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_SECONDARY)]
            ];
        });
    }
    hasRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    addPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'create'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'update'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'delete'])],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_SECONDARY, 'user', 'create'])],
            ];
        });
    }
    checkPermission() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'create')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'update')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'delete')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'create')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'update')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'delete')],
            ];
        });
    }
    enforceWithSyncCompile() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'create')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'update')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'delete')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'create')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'update')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'delete')],
            ];
        });
    }
};
__decorate([
    common_1.Get('reloadPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_a = typeof Promise !== "undefined" && Promise) === "function" ? _a : Object)
], AbacController.prototype, "reloadPolicy", null);
__decorate([
    common_1.Get('getPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], AbacController.prototype, "getPolicy", null);
__decorate([
    common_1.Get('getAllRoles'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], AbacController.prototype, "getAllRoles", null);
__decorate([
    common_1.Get('getAllObjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], AbacController.prototype, "getAllObjects", null);
__decorate([
    common_1.Get('getAllSubjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], AbacController.prototype, "getAllSubjects", null);
__decorate([
    common_1.Get('getAllActions'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], AbacController.prototype, "getAllActions", null);
__decorate([
    common_1.Get('addRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], AbacController.prototype, "addRoleForUser", null);
__decorate([
    common_1.Get('getRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], AbacController.prototype, "getRolesForUser", null);
__decorate([
    common_1.Get('getImplicitRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], AbacController.prototype, "getImplicitRolesForUser", null);
__decorate([
    common_1.Get('getPermissionsForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], AbacController.prototype, "getPermissionsForUser", null);
__decorate([
    common_1.Get('hasRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], AbacController.prototype, "hasRoleForUser", null);
__decorate([
    common_1.Get('addPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_m = typeof Promise !== "undefined" && Promise) === "function" ? _m : Object)
], AbacController.prototype, "addPolicy", null);
__decorate([
    common_1.Get('checkPermission'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_o = typeof Promise !== "undefined" && Promise) === "function" ? _o : Object)
], AbacController.prototype, "checkPermission", null);
__decorate([
    common_1.Get('enforceWithSyncCompile'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_p = typeof Promise !== "undefined" && Promise) === "function" ? _p : Object)
], AbacController.prototype, "enforceWithSyncCompile", null);
AbacController = __decorate([
    swagger_1.ApiTags('ABAC (Attribute-Based Access Control)'),
    common_1.Controller('abac'),
    __metadata("design:paramtypes", [typeof (_q = typeof casbin_1.CasbinService !== "undefined" && casbin_1.CasbinService) === "function" ? _q : Object])
], AbacController);
exports.AbacController = AbacController;


/***/ }),
/* 286 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(287));
__export(__webpack_require__(288));
__export(__webpack_require__(291));
__export(__webpack_require__(292));
__export(__webpack_require__(303));


/***/ }),
/* 287 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.CASBIN_ENFORCER = 'CASBIN_ENFORCER';
exports.CASBIN_ACL_RESOURCE_TOKEN = 'CASBIN_ACL_RESOURCE_TOKEN';
exports.CASBIN_RBAC_RESOURCE_TOKEN = 'CASBIN_RBAC_RESOURCE_TOKEN';
var AccessActionEnum;
(function (AccessActionEnum) {
    AccessActionEnum["create"] = "create";
    AccessActionEnum["read"] = "read";
    AccessActionEnum["update"] = "update";
    AccessActionEnum["delete"] = "delete";
})(AccessActionEnum = exports.AccessActionEnum || (exports.AccessActionEnum = {}));


/***/ }),
/* 288 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var CasbinModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const casbin_1 = __webpack_require__(289);
const typeorm_adapter_1 = __webpack_require__(290);
const casbin_constants_1 = __webpack_require__(287);
const casbin_service_1 = __webpack_require__(291);
const role_1 = __webpack_require__(292);
let CasbinModule = CasbinModule_1 = class CasbinModule {
    static forRootAsync(casbinModelPath, dbConnectionOptions) {
        const casbinEnforcerProvider = {
            provide: casbin_constants_1.CASBIN_ENFORCER,
            useFactory: () => __awaiter(this, void 0, void 0, function* () {
                const adapter = yield typeorm_adapter_1.default.newAdapter(dbConnectionOptions);
                const enforcer = yield casbin_1.newEnforcer(casbinModelPath, adapter);
                yield enforcer.loadPolicy();
                return enforcer;
            })
        };
        return {
            module: CasbinModule_1,
            providers: [
                casbinEnforcerProvider,
                casbin_service_1.CasbinService,
                role_1.RoleService,
                role_1.AclResourceGuard,
                role_1.RbacResourceGuard
            ],
            exports: [
                casbinEnforcerProvider,
                casbin_service_1.CasbinService,
                role_1.RoleService,
                role_1.AclResourceGuard,
                role_1.RbacResourceGuard
            ],
        };
    }
};
CasbinModule = CasbinModule_1 = __decorate([
    common_1.Global(),
    common_1.Module({})
], CasbinModule);
exports.CasbinModule = CasbinModule;


/***/ }),
/* 289 */
/***/ (function(module, exports) {

module.exports = require("casbin");

/***/ }),
/* 290 */
/***/ (function(module, exports) {

module.exports = require("typeorm-adapter");

/***/ }),
/* 291 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const casbin_1 = __webpack_require__(289);
const casbin_constants_1 = __webpack_require__(287);
let CasbinService = class CasbinService {
    constructor(enforcer) {
        this.enforcer = enforcer;
    }
    reloadPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.enforcer.loadPolicy();
        });
    }
    addPolicy(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            const added = yield this.enforcer.addPolicy(...params);
            if (added) {
                return yield this.enforcer.savePolicy();
            }
            return false;
        });
    }
    removePolicy(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            const removed = yield this.enforcer.removePolicy(...params);
            if (removed) {
                return yield this.enforcer.savePolicy();
            }
            return false;
        });
    }
    getPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getPolicy();
        });
    }
    enforce(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.enforce(params);
        });
    }
    getAllRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getAllRoles();
        });
    }
    getAllObjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getAllObjects();
        });
    }
    getAllSubjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getAllSubjects();
        });
    }
    getUsersForRole(name, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getUsersForRole(name, domain);
        });
    }
    hasRoleForUser(user, role, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.hasRoleForUser(user, role, domain);
        });
    }
    addRoleForUser(user, role, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.addRoleForUser(user, role, domain);
        });
    }
    deleteRoleForUser(user, role, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deleteRoleForUser(user, role, domain);
        });
    }
    deleteRolesForUser(user, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deleteRolesForUser(user, domain);
        });
    }
    deleteUser(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deleteUser(user);
        });
    }
    deleteRole(role) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deleteRole(role);
        });
    }
    deletePermission(...permissions) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deletePermission(...permissions);
        });
    }
    addPermissionForUser(user, ...permissions) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.addPermissionForUser(user, ...permissions);
        });
    }
    deletePermissionForUser(user, ...permissions) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deletePermissionForUser(user, ...permissions);
        });
    }
    deletePermissionsForUser(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.deletePermissionsForUser(user);
        });
    }
    getPermissionsForUser(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getPermissionsForUser(user);
        });
    }
    hasPermissionForUser(user, ...permissions) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.hasPermissionForUser(user, ...permissions);
        });
    }
    getAllActions() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.getAllActions();
        });
    }
    hasPolicy(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.hasPolicy(...params);
        });
    }
    hasNamedPolicy(p, ...params) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.hasNamedPolicy(p, ...params);
        });
    }
    getRolesForUser(name, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.getRolesForUser(name, domain);
        });
    }
    getImplicitPermissionsForUser(name, ...domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.getImplicitPermissionsForUser(name, ...domain);
        });
    }
    getImplicitRolesForUser(name, ...domain) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.getImplicitRolesForUser(name, ...domain);
        });
    }
    getNamedPolicy(name) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.getNamedPolicy(name);
        });
    }
    addFunction(name, fn) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.addFunction(name, fn);
        });
    }
    loadFilteredPolicy(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enforcer.loadFilteredPolicy(filter);
        });
    }
    enableAutoBuildRoleLinks(autoBuildRoleLinks) {
        return this.enforcer.enableAutoBuildRoleLinks(autoBuildRoleLinks);
    }
    isFiltered() {
        return this.enforcer.isFiltered();
    }
    enableAutoSave(autoSave) {
        return this.enforcer.enableAutoSave(autoSave);
    }
    setWatcher(watcher) {
        return this.enforcer.setWatcher(watcher);
    }
    enableLog(enable) {
        return this.enforcer.enableLog(enable);
    }
    enableEnforce(enable) {
        return this.enforcer.enableEnforce(enable);
    }
    setEffector(eft) {
        return this.enforcer.setEffector(eft);
    }
    clearPolicy() {
        return this.enforcer.clearPolicy();
    }
    addGroupingPolicy() {
        return this.enforcer.addGroupingPolicy();
    }
    enforceWithSyncCompile(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.enforceWithSyncCompile(...params);
        });
    }
    checkPermission(...params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enforcer.enforce(...params);
        });
    }
};
CasbinService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(casbin_constants_1.CASBIN_ENFORCER)),
    __metadata("design:paramtypes", [typeof (_a = typeof casbin_1.Enforcer !== "undefined" && casbin_1.Enforcer) === "function" ? _a : Object])
], CasbinService);
exports.CasbinService = CasbinService;


/***/ }),
/* 292 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(293));
__export(__webpack_require__(296));
__export(__webpack_require__(300));


/***/ }),
/* 293 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const casbin_service_1 = __webpack_require__(291);
const helper_1 = __webpack_require__(294);
let RoleService = class RoleService {
    constructor(casbinService) {
        this.casbinService = casbinService;
        this.logger = new common_1.Logger(this.constructor.name);
    }
    populateRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            const resources = helper_1.defaultResourceBuilder();
            for (const res of resources) {
                if (res.actions.read.length > 0) {
                    for (const a of res.actions.read) {
                        try {
                            const check = yield this.casbinService.hasPolicy(a, res.name, 'read');
                            if (!check) {
                                yield this.casbinService.addPolicy(a, res.name, 'read');
                            }
                        }
                        catch (e) {
                            this.logger.error(e);
                        }
                    }
                }
                if (res.actions.create.length > 0) {
                    for (const a of res.actions.create) {
                        try {
                            const check = yield this.casbinService.hasPolicy(a, res.name, 'create');
                            if (!check) {
                                yield this.casbinService.addPolicy(a, res.name, 'create');
                            }
                        }
                        catch (e) {
                            this.logger.error(e);
                        }
                    }
                }
                if (res.actions.delete.length > 0) {
                    for (const a of res.actions.delete) {
                        try {
                            const check = yield this.casbinService.hasPolicy(a, res.name, 'delete');
                            if (!check) {
                                yield this.casbinService.checkPermission(a, res.name, 'delete');
                            }
                        }
                        catch (e) {
                            this.logger.error(e);
                        }
                    }
                }
                if (res.actions.update.length > 0) {
                    for (const a of res.actions.update) {
                        try {
                            const check = yield this.casbinService.hasPolicy(a, res.name, 'update');
                            if (!check) {
                                yield this.casbinService.addPolicy(a, res.name, 'update');
                            }
                        }
                        catch (e) {
                            this.logger.error(e);
                        }
                    }
                }
            }
        });
    }
    onModuleInit() {
    }
};
RoleService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof casbin_service_1.CasbinService !== "undefined" && casbin_service_1.CasbinService) === "function" ? _a : Object])
], RoleService);
exports.RoleService = RoleService;


/***/ }),
/* 294 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(295));


/***/ }),
/* 295 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function defaultResourceBuilder() {
    return [
        {
            name: 'user',
            domain: '*',
            actions: {
                read: ['customer'],
                update: ['customer'],
                delete: ['customer'],
                create: ['customer'],
            },
        },
        {
            name: 'tenant',
            domain: '*',
            actions: {
                read: ['customer'],
                update: [],
                delete: [],
                create: ['customer'],
            },
        },
        {
            name: 'billing:card',
            domain: '*',
            actions: {
                read: ['customer'],
                update: ['customer'],
                delete: ['customer'],
                create: ['customer'],
            },
        },
        {
            name: 'billing:subscription',
            domain: '*',
            actions: {
                read: ['customer'],
                update: ['customer'],
                delete: ['customer'],
                create: ['customer'],
            },
        },
        {
            name: 'billing:invoice',
            domain: '*',
            actions: {
                read: ['customer'],
                update: ['customer'],
                delete: ['customer'],
                create: ['customer'],
            },
        },
    ];
}
exports.defaultResourceBuilder = defaultResourceBuilder;


/***/ }),
/* 296 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(297));
__export(__webpack_require__(298));
__export(__webpack_require__(299));


/***/ }),
/* 297 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 298 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 299 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });


/***/ }),
/* 300 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(301));
__export(__webpack_require__(302));


/***/ }),
/* 301 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var AclResourceGuard_1, _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_1 = __webpack_require__(1);
const shared_1 = __webpack_require__(25);
const casbin_constants_1 = __webpack_require__(287);
const casbin_service_1 = __webpack_require__(291);
let AclResourceGuard = AclResourceGuard_1 = class AclResourceGuard {
    constructor(reflector, casbinService, logger) {
        this.reflector = reflector;
        this.casbinService = casbinService;
        this.logger = logger;
        this.logger.setContext(AclResourceGuard_1.name);
    }
    getUser(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const request = context.switchToHttp().getRequest();
            const user = request.user;
            if (!user)
                throw new common_1.UnauthorizedException();
            return user;
        });
    }
    isGranted(sub, aclResource) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!aclResource || (aclResource.roles && aclResource.roles.length === 0)) {
                return true;
            }
            const roles = aclResource.roles;
            const domain = aclResource.domain || null;
            const userRoles = yield this.casbinService.getRolesForUser(sub, domain);
            if (userRoles.length === 0) {
                return false;
            }
            else {
                const hasRole = () => roles.every(role => !!userRoles.find(item => item === role));
                return hasRole();
            }
        });
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const methodAclResource = this.reflector.get(casbin_constants_1.CASBIN_ACL_RESOURCE_TOKEN, context.getHandler()) || {
                roles: [],
                domain: null
            };
            const classAclResource = this.reflector.get(casbin_constants_1.CASBIN_ACL_RESOURCE_TOKEN, context.getClass()) || {
                roles: [],
                domain: null
            };
            const aclResource = Object.assign(Object.assign({}, methodAclResource), classAclResource);
            const user = yield this.getUser(context);
            const sub = user.username || '';
            return yield this.isGranted(sub, aclResource);
        });
    }
};
AclResourceGuard = AclResourceGuard_1 = __decorate([
    common_1.Injectable(),
    __param(2, common_1.Inject(shared_1.PinoAppLoggerService)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.Reflector !== "undefined" && core_1.Reflector) === "function" ? _a : Object, typeof (_b = typeof casbin_service_1.CasbinService !== "undefined" && casbin_service_1.CasbinService) === "function" ? _b : Object, typeof (_c = typeof shared_1.PinoAppLoggerService !== "undefined" && shared_1.PinoAppLoggerService) === "function" ? _c : Object])
], AclResourceGuard);
exports.AclResourceGuard = AclResourceGuard;


/***/ }),
/* 302 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var RbacResourceGuard_1, _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const core_1 = __webpack_require__(1);
const shared_1 = __webpack_require__(25);
const casbin_constants_1 = __webpack_require__(287);
const casbin_service_1 = __webpack_require__(291);
const lodash_1 = __webpack_require__(55);
let RbacResourceGuard = RbacResourceGuard_1 = class RbacResourceGuard {
    constructor(reflector, casbinService, logger) {
        this.reflector = reflector;
        this.casbinService = casbinService;
        this.logger = logger;
        this.logger.setContext(RbacResourceGuard_1.name);
    }
    getUser(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const request = context.switchToHttp().getRequest();
            const user = request.user;
            if (!user)
                throw new common_1.UnauthorizedException();
            return user;
        });
    }
    isGranted(sub, rbacResources) {
        return __awaiter(this, void 0, void 0, function* () {
            const permissions = lodash_1.flatten(rbacResources.map((rbacResource) => {
                return rbacResource.actions.map((action) => {
                    return [
                        sub,
                        rbacResource.resource,
                        action
                    ];
                });
            }));
            for (let permission of permissions) {
                const hasPermission = yield this.casbinService.checkPermission(...permission);
                if (!hasPermission) {
                    return false;
                }
            }
            return true;
        });
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const methodRbacResources = this.reflector.get(casbin_constants_1.CASBIN_RBAC_RESOURCE_TOKEN, context.getHandler()) || [];
            const classRbacResources = this.reflector.get(casbin_constants_1.CASBIN_RBAC_RESOURCE_TOKEN, context.getClass()) || [];
            const rbacResources = [...methodRbacResources, ...classRbacResources];
            const user = yield this.getUser(context);
            const sub = user.username || '';
            return yield this.isGranted(sub, rbacResources);
        });
    }
};
RbacResourceGuard = RbacResourceGuard_1 = __decorate([
    common_1.Injectable(),
    __param(2, common_1.Inject(shared_1.PinoAppLoggerService)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.Reflector !== "undefined" && core_1.Reflector) === "function" ? _a : Object, typeof (_b = typeof casbin_service_1.CasbinService !== "undefined" && casbin_service_1.CasbinService) === "function" ? _b : Object, typeof (_c = typeof shared_1.PinoAppLoggerService !== "undefined" && shared_1.PinoAppLoggerService) === "function" ? _c : Object])
], RbacResourceGuard);
exports.RbacResourceGuard = RbacResourceGuard;


/***/ }),
/* 303 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(304));
__export(__webpack_require__(305));


/***/ }),
/* 304 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const casbin_constants_1 = __webpack_require__(287);
exports.UseACL = (acl) => common_1.SetMetadata(casbin_constants_1.CASBIN_ACL_RESOURCE_TOKEN, acl);


/***/ }),
/* 305 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const casbin_constants_1 = __webpack_require__(287);
exports.UseRBAC = (...rbac) => common_1.SetMetadata(casbin_constants_1.CASBIN_RBAC_RESOURCE_TOKEN, rbac);


/***/ }),
/* 306 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_PRIMARY = 'nasterblue';
exports.USER_SECONDARY = 'lucifer';
exports.USER_DOMAIN = null;
exports.ROLE_ADMIN = 'ROLE_ADMIN';
exports.ROLE_USER = 'ROLE_USER';
exports.RESOURCE_USER = 'user';


/***/ }),
/* 307 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const casbin_1 = __webpack_require__(286);
const fake_user_guard_1 = __webpack_require__(308);
const access_control_contstant_1 = __webpack_require__(306);
const rbac = [
    {
        resource: access_control_contstant_1.RESOURCE_USER,
        actions: [
            casbin_1.AccessActionEnum.create
        ]
    }
];
let RbacController = class RbacController {
    constructor(casbinService) {
        this.casbinService = casbinService;
    }
    reloadPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.reloadPolicy();
        });
    }
    getPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getPolicy();
        });
    }
    getAllRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllRoles();
        });
    }
    getAllObjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllObjects();
        });
    }
    getAllSubjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllSubjects();
        });
    }
    getAllActions() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllActions();
        });
    }
    addRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    getRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getImplicitRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getPermissionsForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_PRIMARY)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_SECONDARY)]
            ];
        });
    }
    hasRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    addPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'create'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'update'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'delete'])],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_SECONDARY, 'user', 'create'])],
            ];
        });
    }
    checkPermission() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'create')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'update')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'delete')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'create')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'update')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'delete')],
            ];
        });
    }
};
__decorate([
    common_1.Get('reloadPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_a = typeof Promise !== "undefined" && Promise) === "function" ? _a : Object)
], RbacController.prototype, "reloadPolicy", null);
__decorate([
    common_1.Get('getPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], RbacController.prototype, "getPolicy", null);
__decorate([
    common_1.Get('getAllRoles'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], RbacController.prototype, "getAllRoles", null);
__decorate([
    common_1.Get('getAllObjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], RbacController.prototype, "getAllObjects", null);
__decorate([
    common_1.Get('getAllSubjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], RbacController.prototype, "getAllSubjects", null);
__decorate([
    common_1.Get('getAllActions'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], RbacController.prototype, "getAllActions", null);
__decorate([
    common_1.Get('addRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], RbacController.prototype, "addRoleForUser", null);
__decorate([
    common_1.Get('getRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], RbacController.prototype, "getRolesForUser", null);
__decorate([
    common_1.Get('getImplicitRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], RbacController.prototype, "getImplicitRolesForUser", null);
__decorate([
    common_1.Get('getPermissionsForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], RbacController.prototype, "getPermissionsForUser", null);
__decorate([
    common_1.Get('hasRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], RbacController.prototype, "hasRoleForUser", null);
__decorate([
    common_1.Get('addPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_m = typeof Promise !== "undefined" && Promise) === "function" ? _m : Object)
], RbacController.prototype, "addPolicy", null);
__decorate([
    common_1.Get('checkPermission'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_o = typeof Promise !== "undefined" && Promise) === "function" ? _o : Object)
], RbacController.prototype, "checkPermission", null);
RbacController = __decorate([
    swagger_1.ApiTags('RBAC (Role-Based Access Control)'),
    casbin_1.UseRBAC(...rbac),
    common_1.UseGuards(fake_user_guard_1.FakeUserGuard, casbin_1.RbacResourceGuard),
    common_1.Controller('rbac'),
    __metadata("design:paramtypes", [typeof (_p = typeof casbin_1.CasbinService !== "undefined" && casbin_1.CasbinService) === "function" ? _p : Object])
], RbacController);
exports.RbacController = RbacController;


/***/ }),
/* 308 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
let FakeUserGuard = class FakeUserGuard {
    canActivate(context) {
        const req = context.switchToHttp().getRequest();
        const fakeUser = {
            id: 123,
            username: 'nasterblue'
        };
        req.user = fakeUser;
        console.log({ fakeUser });
        return true;
    }
};
FakeUserGuard = __decorate([
    common_1.Injectable()
], FakeUserGuard);
exports.FakeUserGuard = FakeUserGuard;


/***/ }),
/* 309 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const casbin_1 = __webpack_require__(286);
const fake_user_guard_1 = __webpack_require__(308);
const access_control_contstant_1 = __webpack_require__(306);
const acl = {
    roles: [access_control_contstant_1.ROLE_ADMIN]
};
let AclController = class AclController {
    constructor(casbinService) {
        this.casbinService = casbinService;
    }
    reloadPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.reloadPolicy();
        });
    }
    getPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getPolicy();
        });
    }
    getAllRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllRoles();
        });
    }
    getAllObjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllObjects();
        });
    }
    getAllSubjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllSubjects();
        });
    }
    getAllActions() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.casbinService.getAllActions();
        });
    }
    addRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    getRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getImplicitRolesForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getImplicitRolesForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.USER_DOMAIN)]
            ];
        });
    }
    getPermissionsForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_PRIMARY)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.getPermissionsForUser(access_control_contstant_1.USER_SECONDARY)]
            ];
        });
    }
    hasRoleForUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_PRIMARY, access_control_contstant_1.ROLE_ADMIN, access_control_contstant_1.USER_DOMAIN)],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.hasRoleForUser(access_control_contstant_1.USER_SECONDARY, access_control_contstant_1.ROLE_USER, access_control_contstant_1.USER_DOMAIN)],
            ];
        });
    }
    addPolicy() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'create'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'update'])],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_PRIMARY, 'user', 'delete'])],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.addPolicy(...[access_control_contstant_1.USER_SECONDARY, 'user', 'create'])],
            ];
        });
    }
    checkPermission() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'create')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'update')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_PRIMARY, 'user', 'delete')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'create')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'update')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.checkPermission(access_control_contstant_1.USER_SECONDARY, 'user', 'delete')],
            ];
        });
    }
    enforceWithSyncCompile() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'create')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'update')],
                [access_control_contstant_1.USER_PRIMARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_PRIMARY, 'user', 'delete')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'create')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'update')],
                [access_control_contstant_1.USER_SECONDARY, yield this.casbinService.enforceWithSyncCompile(access_control_contstant_1.USER_SECONDARY, 'user', 'delete')],
            ];
        });
    }
};
__decorate([
    common_1.Get('reloadPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_a = typeof Promise !== "undefined" && Promise) === "function" ? _a : Object)
], AclController.prototype, "reloadPolicy", null);
__decorate([
    common_1.Get('getPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], AclController.prototype, "getPolicy", null);
__decorate([
    common_1.Get('getAllRoles'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], AclController.prototype, "getAllRoles", null);
__decorate([
    common_1.Get('getAllObjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], AclController.prototype, "getAllObjects", null);
__decorate([
    common_1.Get('getAllSubjects'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], AclController.prototype, "getAllSubjects", null);
__decorate([
    common_1.Get('getAllActions'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], AclController.prototype, "getAllActions", null);
__decorate([
    common_1.Get('addRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], AclController.prototype, "addRoleForUser", null);
__decorate([
    common_1.Get('getRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], AclController.prototype, "getRolesForUser", null);
__decorate([
    common_1.Get('getImplicitRolesForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], AclController.prototype, "getImplicitRolesForUser", null);
__decorate([
    common_1.Get('getPermissionsForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], AclController.prototype, "getPermissionsForUser", null);
__decorate([
    common_1.Get('hasRoleForUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], AclController.prototype, "hasRoleForUser", null);
__decorate([
    common_1.Get('addPolicy'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_m = typeof Promise !== "undefined" && Promise) === "function" ? _m : Object)
], AclController.prototype, "addPolicy", null);
__decorate([
    common_1.Get('checkPermission'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_o = typeof Promise !== "undefined" && Promise) === "function" ? _o : Object)
], AclController.prototype, "checkPermission", null);
__decorate([
    common_1.Get('enforceWithSyncCompile'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", typeof (_p = typeof Promise !== "undefined" && Promise) === "function" ? _p : Object)
], AclController.prototype, "enforceWithSyncCompile", null);
AclController = __decorate([
    swagger_1.ApiTags('ACL (Access Control List)'),
    casbin_1.UseACL(acl),
    common_1.UseGuards(fake_user_guard_1.FakeUserGuard, casbin_1.AclResourceGuard),
    common_1.Controller('acl'),
    __metadata("design:paramtypes", [typeof (_q = typeof casbin_1.CasbinService !== "undefined" && casbin_1.CasbinService) === "function" ? _q : Object])
], AclController);
exports.AclController = AclController;


/***/ }),
/* 310 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const barcode_epc_1 = __webpack_require__(311);
const zpl_printer_1 = __webpack_require__(313);
const index_dto_1 = __webpack_require__(315);
let PrinterController = class PrinterController {
    constructor() {
    }
    toSgtin96TagUri(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const sgtin96TagUri = yield barcode_epc_1.toSgtin96TagUri(data.CompanyPrefix, data.ItemReference, data.SerialNumber);
            return sgtin96TagUri;
        });
    }
    sgtin96TagUriToHex(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const sgtin96TagUri = yield barcode_epc_1.toSgtin96TagUri(data.CompanyPrefix, data.ItemReference, data.SerialNumber);
            return yield barcode_epc_1.sgtin96TagUriToHex(sgtin96TagUri);
        });
    }
    hexToSgtin96TagUri(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const epcHexadecimal = data.epcHexadecimal;
            console.log({ epcHexadecimal });
            return yield barcode_epc_1.hexToSgtin96TagUri(epcHexadecimal);
        });
    }
    toZpl(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield zpl_printer_1.toZpl(data);
        });
    }
    printZpl(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const zpl = yield zpl_printer_1.printZpl(data);
            return zpl;
        });
    }
};
__decorate([
    common_1.Post('toSgtin96TagUri'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof index_dto_1.toSgtin96TagUriDto !== "undefined" && index_dto_1.toSgtin96TagUriDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], PrinterController.prototype, "toSgtin96TagUri", null);
__decorate([
    common_1.Post('sgtin96TagUriToHex'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_c = typeof index_dto_1.toSgtin96TagUriDto !== "undefined" && index_dto_1.toSgtin96TagUriDto) === "function" ? _c : Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], PrinterController.prototype, "sgtin96TagUriToHex", null);
__decorate([
    common_1.Post('hexToSgtin96TagUri'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_e = typeof index_dto_1.hexToSgtin96TagUriDto !== "undefined" && index_dto_1.hexToSgtin96TagUriDto) === "function" ? _e : Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], PrinterController.prototype, "hexToSgtin96TagUri", null);
__decorate([
    common_1.Post('toZpl'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_g = typeof index_dto_1.toZplDto !== "undefined" && index_dto_1.toZplDto) === "function" ? _g : Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], PrinterController.prototype, "toZpl", null);
__decorate([
    common_1.Post('printZpl'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_j = typeof index_dto_1.toZplDto !== "undefined" && index_dto_1.toZplDto) === "function" ? _j : Object]),
    __metadata("design:returntype", typeof (_k = typeof Promise !== "undefined" && Promise) === "function" ? _k : Object)
], PrinterController.prototype, "printZpl", null);
PrinterController = __decorate([
    swagger_1.ApiTags('Printer'),
    common_1.Controller('printer'),
    __metadata("design:paramtypes", [])
], PrinterController);
exports.PrinterController = PrinterController;


/***/ }),
/* 311 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const epc = __webpack_require__(312);
const Filter = 3;
exports.toSgtin96TagUri = (CompanyPrefix, ItemReference, SerialNumber) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        try {
            if (CompanyPrefix.length + ItemReference.length === 13) {
                const sgtin96TagUri = `urn:epc:tag:sgtin-96:${Filter}.${CompanyPrefix}.${ItemReference}.${SerialNumber}`;
                resolve(sgtin96TagUri);
            }
            else {
                throw new Error('CompanyPrefix.length +ItemReference.length must be 13');
            }
        }
        catch (e) {
            reject(e);
        }
    });
});
exports.sgtin96TagUriToHex = (epcTagUri) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        try {
            let headerValue = '48';
            let fiterValue = epcTagUri.split(':')[4].split('.')[0];
            let companyPrefix = epcTagUri.split(':')[4].split('.')[1];
            let reference = epcTagUri.split(':')[4].split('.')[2];
            let serial = epcTagUri.split(':')[4].split('.')[3];
            let companyPrefixLen = companyPrefix.length;
            const partionValueMap = {
                12: 0,
                11: 1,
                10: 2,
                9: 3,
                8: 4,
                7: 5,
                6: 6,
            };
            const companyBitMap = {
                12: 40,
                11: 37,
                10: 34,
                9: 30,
                8: 27,
                7: 24,
                6: 20,
            };
            const referenceBitMap = {
                12: 4,
                11: 7,
                10: 10,
                9: 14,
                8: 17,
                7: 20,
                6: 24,
            };
            let partion = partionValueMap[companyPrefixLen];
            let epcHeader = 96;
            let epcHeaderBin = parseInt(headerValue, 10).toString(2).padStart(8, '0');
            let filterBin = parseInt(fiterValue, 10).toString(2).padStart(3, '0');
            let partionBin = parseInt(partion, 10).toString(2).padStart(3, '0');
            let companyPrefixBin = parseInt(companyPrefix, 10).toString(2).padStart(companyBitMap[companyPrefixLen], '0');
            let referenceBin = parseInt(reference, 10).toString(2).padStart(referenceBitMap[companyPrefixLen], '0');
            let serialBinLength = epcHeader - epcHeaderBin.length - filterBin.length - partionBin.length - companyPrefixBin.length - referenceBin.length;
            let serialBin = parseInt(serial, 10).toString(2).padStart(serialBinLength, '0');
            let epcBinary = `${epcHeaderBin}${filterBin}${partionBin}${companyPrefixBin}${referenceBin}${serialBin}`;
            console.warn({
                headerValue,
                epcHeaderBin,
                epcHeaderBinLength: epcHeaderBin.length,
                fiterValue,
                filterBin,
                filterBinLength: filterBin.length,
                partion,
                partionBin,
                companyPrefix,
                companyPrefixBin,
                companyPrefixBinLength: companyPrefixBin.length,
                reference,
                referenceBin,
                referenceBinLength: referenceBin.length,
                serial,
                serialBin,
                serialBinLength: serialBin.length,
                epcBinary,
            });
            var middlelength = Math.ceil(epcBinary.length / 2);
            var firstHalfBin = epcBinary.slice(0, middlelength);
            var secondHalfBin = epcBinary.slice(middlelength);
            var hexa1 = parseInt(firstHalfBin, 2).toString(16).toUpperCase();
            var hexa2 = parseInt(secondHalfBin, 2).toString(16).toUpperCase();
            const epcHex = `${hexa1}${hexa2}`;
            resolve(epcHex);
        }
        catch (e) {
            reject(e);
        }
    });
});
exports.hexToSgtin96TagUri = (epcHexadecimal) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        try {
            epc.getParser('SGTIN')
                .then(function (sgtin) {
                sgtin.parse(epcHexadecimal)
                    .then(function (parsed) {
                    const { Filter, CompanyPrefix, ItemReference, SerialNumber, } = parsed.parts;
                    const sgtin96TagUri = `urn:epc:tag:sgtin-96:${Filter}.${CompanyPrefix}.${ItemReference}.${SerialNumber}`;
                    resolve(sgtin96TagUri);
                })
                    .fail(function (err) {
                    reject(err);
                });
            });
        }
        catch (e) {
            reject(e);
        }
    });
});


/***/ }),
/* 312 */
/***/ (function(module, exports) {

module.exports = require("node-epc");

/***/ }),
/* 313 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios = __webpack_require__(314);
const instanceAxios = axios.create({
    baseURL: process.env.PRINTER_API,
    timeout: 1000,
    headers: { 'X-Requested-With': 'XMLHttpRequest' },
    withCredentials: false,
});
exports.toZpl = (data) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        try {
            const price = `^FO300,100^FD${data.price}^FS\n`;
            const name = `^FO300,130^FD${data.name}^FS\n`;
            const variationName = `^FO300,150^FD${data.variationName}^FS\n`;
            const barcode = `^FD${data.barcode}^FS\n`;
            const rfid = `^FD${data.rfid}\n`;
            const zplTemplate = '^XA\n' +
                '\n' +
                '^FX Product info is here\n' +
                '^CF0,30\n' +
                price +
                '^CFA,15\n' +
                name +
                variationName +
                '\n' +
                '^FX Barcode EAN-8 is here\n' +
                '^FO300,180^BY3\n' +
                '^B8N,60,Y,N\n' +
                barcode +
                '\n' +
                '^FX EPC hexadecimal is here\n' +
                '^RFW,H\n' +
                rfid +
                '^FS\n' +
                '\n' +
                '^XZ';
            resolve(zplTemplate);
        }
        catch (e) {
            reject(e);
        }
    });
});
exports.printZpl = (data) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield exports.toZpl(data).then(function (response) {
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                });
            }
            catch (e) {
                reject(e);
            }
        });
    });
});


/***/ }),
/* 314 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 315 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const class_transformer_1 = __webpack_require__(16);
class toSgtin96TagUriDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '0614141'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "CompanyPrefix", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '812345'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "ItemReference", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '6789'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "SerialNumber", void 0);
exports.toSgtin96TagUriDto = toSgtin96TagUriDto;
class hexToSgtin96TagUriDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '3074257BF7194E4000001A85'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], hexToSgtin96TagUriDto.prototype, "epcHexadecimal", void 0);
exports.hexToSgtin96TagUriDto = hexToSgtin96TagUriDto;
class toZplDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '$35.00'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'Flower Dress'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'Small'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "variationName", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '1234567'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '3074257BF7194E4000001A85'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "rfid", void 0);
exports.toZplDto = toZplDto;


/***/ }),
/* 316 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const controllers_1 = __webpack_require__(284);
const access_control_service_1 = __webpack_require__(317);
const shared_1 = __webpack_require__(25);
const databaseConfig = __webpack_require__(130);
const casbin_1 = __webpack_require__(286);
const { DB_CONNECTION_DEFAULT } = databaseConfig;
const path_1 = __webpack_require__(231);
let AccessControlModule = class AccessControlModule {
};
AccessControlModule = __decorate([
    common_1.Module({
        imports: [
            shared_1.SharedModule,
            casbin_1.CasbinModule.forRootAsync(path_1.join(__dirname, '..', 'src/app/example/access-control/rbac_model.conf'), databaseConfig[DB_CONNECTION_DEFAULT])
        ],
        controllers: [
            controllers_1.PrinterController
        ],
        providers: [access_control_service_1.AccessControlService]
    })
], AccessControlModule);
exports.AccessControlModule = AccessControlModule;


/***/ }),
/* 317 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
let AccessControlService = class AccessControlService {
};
AccessControlService = __decorate([
    common_1.Injectable()
], AccessControlService);
exports.AccessControlService = AccessControlService;


/***/ }),
/* 318 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const environment_1 = __webpack_require__(6);
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const https = __webpack_require__(319);
const rxjs_1 = __webpack_require__(31);
const operators_1 = __webpack_require__(32);
const auth_1 = __webpack_require__(166);
const config_1 = __webpack_require__(52);
const url = 'https://httpbin.org/anything';
const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
});
let AppController = class AppController {
    constructor(config, http) {
        this.config = config;
        this.http = http;
    }
    root() {
        return `<h3>${environment_1.environment.swagger.welcomeText}</h3>
            <br/>Checkout <a href="${environment_1.environment.swagger.path}">API Docs</a>
            <br/><code>Version: ${this.config.getVersion()}</code>`;
    }
    echo(req) {
        const { headers, ip, method, params, body } = req;
        return this.http
            .request({ url, headers, method, params, data: body, httpsAgent })
            .pipe(operators_1.map(res => ({ httpbin: res.data, ip })));
    }
};
__decorate([
    swagger_1.ApiOperation({ summary: 'Welcome' }),
    auth_1.Allow(),
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], AppController.prototype, "root", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'echo service for testing' }),
    auth_1.Allow(),
    common_1.All('echo'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_a = typeof rxjs_1.Observable !== "undefined" && rxjs_1.Observable) === "function" ? _a : Object)
], AppController.prototype, "echo", null);
AppController = __decorate([
    common_1.Controller(),
    __metadata("design:paramtypes", [typeof (_b = typeof config_1.ConfigService !== "undefined" && config_1.ConfigService) === "function" ? _b : Object, typeof (_c = typeof common_1.HttpService !== "undefined" && common_1.HttpService) === "function" ? _c : Object])
], AppController);
exports.AppController = AppController;


/***/ }),
/* 319 */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ }),
/* 320 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/terminus");

/***/ }),
/* 321 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const terminus_1 = __webpack_require__(320);
let HealthController = class HealthController {
    constructor(health, dns, disk, memory, db) {
        this.health = health;
        this.dns = dns;
        this.disk = disk;
        this.memory = memory;
        this.db = db;
    }
    ready() {
        return this.health.check([
            () => __awaiter(this, void 0, void 0, function* () { return this.dns.pingCheck('google', 'https://google.com'); }),
            () => __awaiter(this, void 0, void 0, function* () { return this.memory.checkHeap('memory_heap', 200 * 1024 * 1024); }),
            () => __awaiter(this, void 0, void 0, function* () { return this.memory.checkRSS('memory_rss', 3000 * 1024 * 1024); }),
            () => __awaiter(this, void 0, void 0, function* () { return this.disk.checkStorage('storage', { thresholdPercent: 0.8, path: '/' }); })
        ]);
    }
    live() {
        return this.health.check([
            () => __awaiter(this, void 0, void 0, function* () { return this.db.pingCheck('database', { timeout: 300 }); })
        ]);
    }
};
__decorate([
    common_1.Get('ready'),
    terminus_1.HealthCheck(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], HealthController.prototype, "ready", null);
__decorate([
    common_1.Get('live'),
    terminus_1.HealthCheck(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], HealthController.prototype, "live", null);
HealthController = __decorate([
    common_1.Controller('health'),
    __metadata("design:paramtypes", [typeof (_a = typeof terminus_1.HealthCheckService !== "undefined" && terminus_1.HealthCheckService) === "function" ? _a : Object, typeof (_b = typeof terminus_1.DNSHealthIndicator !== "undefined" && terminus_1.DNSHealthIndicator) === "function" ? _b : Object, typeof (_c = typeof terminus_1.DiskHealthIndicator !== "undefined" && terminus_1.DiskHealthIndicator) === "function" ? _c : Object, typeof (_d = typeof terminus_1.MemoryHealthIndicator !== "undefined" && terminus_1.MemoryHealthIndicator) === "function" ? _d : Object, typeof (_e = typeof terminus_1.TypeOrmHealthIndicator !== "undefined" && terminus_1.TypeOrmHealthIndicator) === "function" ? _e : Object])
], HealthController);
exports.HealthController = HealthController;


/***/ }),
/* 322 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var products_module_1 = __webpack_require__(323);
exports.ProductModule = products_module_1.ProductModule;
var products_service_1 = __webpack_require__(336);
exports.ProductService = products_service_1.ProductService;
var products_controller_1 = __webpack_require__(324);
exports.ProductController = products_controller_1.ProductController;
var products_entity_1 = __webpack_require__(205);
exports.ProductEntity = products_entity_1.ProductEntity;


/***/ }),
/* 323 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const products_entity_1 = __webpack_require__(205);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const products_controller_1 = __webpack_require__(324);
const products_service_1 = __webpack_require__(336);
const images_1 = __webpack_require__(190);
const locations_1 = __webpack_require__(348);
const variations_1 = __webpack_require__(354);
const twilio_1 = __webpack_require__(223);
const code_1 = __webpack_require__(214);
const user_1 = __webpack_require__(256);
const email_1 = __webpack_require__(227);
const categories_1 = __webpack_require__(327);
const barcodes_1 = __webpack_require__(361);
const device_1 = __webpack_require__(246);
const bull_1 = __webpack_require__(338);
const environment_1 = __webpack_require__(6);
const products_consumer_1 = __webpack_require__(363);
const PROVIDERS = [
    products_service_1.ProductService,
    images_1.ImageService,
    locations_1.LocationService,
    variations_1.VariationService,
    twilio_1.TwillioService,
    user_1.UserService,
    code_1.CodeService,
    email_1.EmailService,
    categories_1.CategoryService,
    barcodes_1.BarcodeService,
    products_consumer_1.ProductConsumer
];
const ENTITY = [
    products_entity_1.ProductEntity,
    images_1.ImagesEntity,
    locations_1.LocationEntity,
    variations_1.VariationEntity,
    user_1.UserEntity,
    code_1.CodeEntity,
    categories_1.CategoryEntity,
    barcodes_1.BarcodeEntity
];
let ProductModule = class ProductModule {
};
ProductModule = __decorate([
    common_1.Module({
        imports: [
            bull_1.BullModule.registerQueue({
                name: 'products',
                redis: {
                    host: environment_1.default.redis.host,
                    port: parseInt(environment_1.default.redis.port),
                },
            }),
            common_1.forwardRef(() => auth_1.AuthModule),
            common_1.forwardRef(() => images_1.ImagesEntity),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
            device_1.DeviceModule,
            bull_1.BullModule
        ],
        controllers: [products_controller_1.ProductController],
        providers: [
            ...PROVIDERS,
            email_1.EmailCoreModule.getEmailConfig()
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], ProductModule);
exports.ProductModule = ProductModule;


/***/ }),
/* 324 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const platform_express_1 = __webpack_require__(4);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(325);
const products_service_1 = __webpack_require__(336);
const file_uploading_utils_1 = __webpack_require__(249);
const images_service_1 = __webpack_require__(196);
const platform_express_2 = __webpack_require__(4);
const composeSecurity_guard_1 = __webpack_require__(236);
let ProductController = class ProductController extends core_1.CrudController {
    constructor(productService, imageService) {
        super(productService);
        this.productService = productService;
        this.imageService = imageService;
    }
    checkBarcode(barcode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.productService.checkBarcode(barcode);
            }
            catch (e) {
                return e;
            }
        });
    }
    checkSKU(req, sku) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.productService.checkSKU(req.user.id, sku);
            }
            catch (e) {
                return e;
            }
        });
    }
    downloadTemplate(res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const downloadPath = yield this.productService.downloadTemplate();
                return res.download(downloadPath);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    generateRFID(barcode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.productService.generateRFID(barcode);
            }
            catch (e) {
                return e;
            }
        });
    }
    generateBarcode(req, count) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.productService.generateBarcode(req.user.id, count);
            }
            catch (e) {
                return e;
            }
        });
    }
    generateSKU(productName, category, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.productService.generateSKU(productName, category, req.user.id);
            }
            catch (e) {
                return e;
            }
        });
    }
    importFile(req, file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let importFile = yield this.productService.importFile(req, file.path);
                this.respondWithErrorsOrSuccess(importFile);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    exportFile(res, req, productIds) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let exportProduct = yield this.productService.exportFile(req, productIds);
                if (exportProduct.code) {
                    this.respondWithErrorsOrSuccess(exportProduct);
                }
                else {
                    return res.download(exportProduct);
                }
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    createProduct(req, data, files) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let product = yield this.productService.createProduct(req.user.id, data, files);
                this.respondWithErrorsOrSuccess(product);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getProduct(req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                let product = yield this.productService.getProduct(userId, id);
                this.respondWithErrorsOrSuccess(product);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllProduct(req, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                let products = yield this.productService.getAllProduct(userId, filter);
                this.respondWithErrorsOrSuccess(products);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateProduct(req, id, product, files) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                product.id = id;
                let update = yield this.productService.updateProduct(req, product, files);
                this.respondWithErrorsOrSuccess(update);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    deleteProduct(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let product = yield this.productService.deleteProduct(ids);
                this.respondWithErrorsOrSuccess(product);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    printZpl(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const zpl = yield this.productService.printLabel(data);
                this.respondWithErrorsOrSuccess(zpl);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Get('checkBarcode'),
    __param(0, common_1.Query("barcode")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "checkBarcode", null);
__decorate([
    common_1.Get('checkSKU'),
    __param(0, common_1.Req()), __param(1, common_1.Query("sku")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "checkSKU", null);
__decorate([
    common_1.Get('downloadTemplate'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", typeof (_a = typeof Promise !== "undefined" && Promise) === "function" ? _a : Object)
], ProductController.prototype, "downloadTemplate", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Get('generateRFID'),
    __param(0, common_1.Query("barcode")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "generateRFID", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Get('generateBarcode'),
    __param(0, common_1.Req()), __param(1, common_1.Query("count")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "generateBarcode", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Get('generateSKU'),
    __param(0, common_1.Query('productName')), __param(1, common_1.Query('category')), __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "generateSKU", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post('importFile'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', file_uploading_utils_1.excelMulterOptions)),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], ProductController.prototype, "importFile", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post('exportFile'),
    __param(0, common_1.Res()), __param(1, common_1.Req()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, typeof (_c = typeof dto_1.IProductIds !== "undefined" && dto_1.IProductIds) === "function" ? _c : Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], ProductController.prototype, "exportFile", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post(),
    common_1.UseInterceptors(platform_express_2.AnyFilesInterceptor()),
    __param(0, common_1.Req()), __param(1, common_1.Body()), __param(2, common_1.UploadedFiles()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_e = typeof dto_1.CreateProductDto !== "undefined" && dto_1.CreateProductDto) === "function" ? _e : Object, Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], ProductController.prototype, "createProduct", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Get(":id"),
    __param(0, common_1.Req()), __param(1, common_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], ProductController.prototype, "getProduct", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post("findAll"),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_h = typeof dto_1.SearchProductDto !== "undefined" && dto_1.SearchProductDto) === "function" ? _h : Object]),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], ProductController.prototype, "getAllProduct", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Put(":id"),
    common_1.UseInterceptors(platform_express_2.AnyFilesInterceptor()),
    __param(0, common_1.Req()), __param(1, common_1.Param("id")), __param(2, common_1.Body()), __param(3, common_1.UploadedFiles()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, typeof (_k = typeof dto_1.UpdateProductDto !== "undefined" && dto_1.UpdateProductDto) === "function" ? _k : Object, Object]),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], ProductController.prototype, "updateProduct", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Delete(":id"),
    __param(0, common_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", typeof (_m = typeof Promise !== "undefined" && Promise) === "function" ? _m : Object)
], ProductController.prototype, "deleteProduct", null);
__decorate([
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    common_1.Post('printLabel'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_o = typeof dto_1.PrintLabelDto !== "undefined" && dto_1.PrintLabelDto) === "function" ? _o : Object]),
    __metadata("design:returntype", typeof (_p = typeof Promise !== "undefined" && Promise) === "function" ? _p : Object)
], ProductController.prototype, "printZpl", null);
ProductController = __decorate([
    swagger_1.ApiTags('Products'),
    common_1.Controller(),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Product',
        createDto: dto_1.CreateProductDto,
        updateDto: dto_1.UpdateProductDto,
        transformDto: dto_1.TransformProductDto,
        searchDto: dto_1.SearchProductDto
    }),
    __metadata("design:paramtypes", [typeof (_q = typeof products_service_1.ProductService !== "undefined" && products_service_1.ProductService) === "function" ? _q : Object, typeof (_r = typeof images_service_1.ImageService !== "undefined" && images_service_1.ImageService) === "function" ? _r : Object])
], ProductController);
exports.ProductController = ProductController;


/***/ }),
/* 325 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(326));
__export(__webpack_require__(334));
__export(__webpack_require__(335));


/***/ }),
/* 326 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const products_entity_1 = __webpack_require__(205);
const images_1 = __webpack_require__(190);
const categories_1 = __webpack_require__(327);
const companies_entity_1 = __webpack_require__(189);
class variationModel {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "type", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "variationName", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "barcode", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "rfid", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "sku", void 0);
__decorate([
    class_transformer_1.Expose(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], variationModel.prototype, "quantity", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], variationModel.prototype, "image", void 0);
exports.variationModel = variationModel;
class optionModel {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], optionModel.prototype, "type", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], optionModel.prototype, "value", void 0);
exports.optionModel = optionModel;
class TransformProductDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "categoryId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => categories_1.CategoryEntity, category => category.id),
    typeorm_1.JoinColumn({ name: "categoryId" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof categories_1.CategoryEntity !== "undefined" && categories_1.CategoryEntity) === "function" ? _a : Object)
], TransformProductDto.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "rfid", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "unit", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformProductDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformProductDto.prototype, "price", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], TransformProductDto.prototype, "images", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], TransformProductDto.prototype, "children", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _b : Object)
], TransformProductDto.prototype, "parent", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], TransformProductDto.prototype, "isVariation", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_entity_1.CompanyEntity, company => company.id),
    typeorm_1.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_c = typeof companies_entity_1.CompanyEntity !== "undefined" && companies_entity_1.CompanyEntity) === "function" ? _c : Object)
], TransformProductDto.prototype, "company", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformProductDto.prototype, "total", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformProductDto.prototype, "option", void 0);
exports.TransformProductDto = TransformProductDto;
class CreateProductDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' }),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 128),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "categoryId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'SKU 345-091' }),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_validator_1.Length(1, 999999999999),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "barcode", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "rfid", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "unit", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 40 }),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateProductDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 593 }),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateProductDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "option", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "variations", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], CreateProductDto.prototype, "images", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], CreateProductDto.prototype, "children", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_d = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _d : Object)
], CreateProductDto.prototype, "parent", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateProductDto.prototype, "companyId", void 0);
exports.CreateProductDto = CreateProductDto;
class UpdateProductDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "categoryId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'SKU 345-091' }),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "unit", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 40 }),
    class_validator_1.IsNumber(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateProductDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 593 }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateProductDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "option", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateProductDto.prototype, "variations", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], UpdateProductDto.prototype, "images", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], UpdateProductDto.prototype, "children", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_e = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _e : Object)
], UpdateProductDto.prototype, "parent", void 0);
exports.UpdateProductDto = UpdateProductDto;
class SearchProductDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'name', 'sku', 'barcode', 'price', 'quantity', 'unit', "isVariation", "option"]
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchProductDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            search: "UND-PRO-NGH--NON-1594723735718",
            categoryName: "Uncategorized",
            categoryId: [
                "ff9c4678-82ad-4d83-be79-df037ff93313",
                "77cbc722-a005-4a36-953d-0a458d81dfd1"
            ],
            quantity: 1
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchProductDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchProductDto.prototype, "order", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchProductDto.prototype, "relations", void 0);
exports.SearchProductDto = SearchProductDto;
class IProductIds extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: [
            "ff9c4678-82ad-4d83-be79-df037ff93313",
            "77cbc722-a005-4a36-953d-0a458d81dfd1"
        ]
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], IProductIds.prototype, "productIds", void 0);
exports.IProductIds = IProductIds;


/***/ }),
/* 327 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var categories_module_1 = __webpack_require__(328);
exports.CategoryModule = categories_module_1.CategoryModule;
var categories_service_1 = __webpack_require__(332);
exports.CategoryService = categories_service_1.CategoryService;
var categories_entity_1 = __webpack_require__(206);
exports.CategoryEntity = categories_entity_1.CategoryEntity;
var categories_controller_1 = __webpack_require__(329);
exports.CategoryController = categories_controller_1.CategoryController;


/***/ }),
/* 328 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const categories_entity_1 = __webpack_require__(206);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const categories_controller_1 = __webpack_require__(329);
const categories_service_1 = __webpack_require__(332);
const twilio_1 = __webpack_require__(223);
const code_1 = __webpack_require__(214);
const user_1 = __webpack_require__(256);
const products_1 = __webpack_require__(322);
const PROVIDERS = [
    categories_service_1.CategoryService,
    twilio_1.TwillioService,
    user_1.UserService,
    code_1.CodeService
];
const ENTITY = [
    categories_entity_1.CategoryEntity,
    user_1.UserEntity,
    code_1.CodeEntity
];
let CategoryModule = class CategoryModule {
};
CategoryModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            common_1.forwardRef(() => products_1.ProductModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
            twilio_1.TwillioService
        ],
        controllers: [categories_controller_1.CategoryController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], CategoryModule);
exports.CategoryModule = CategoryModule;


/***/ }),
/* 329 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(330);
const categories_service_1 = __webpack_require__(332);
const composeSecurity_guard_1 = __webpack_require__(236);
let CategoryController = class CategoryController extends core_1.CrudController {
    constructor(categoryService) {
        super(categoryService);
        this.categoryService = categoryService;
    }
    create(data, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                let category = yield this.categoryService.createCategory(data, req.user.id);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateCategory(id, data, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                let category = yield this.categoryService.updateCategory(id, data, req.user.id);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    deleteCategory(id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let category = yield this.categoryService.deleteCategory(id, req.user.id);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getCategory(id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let category = yield this.categoryService.findCategory(id, req.user.id);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllCategories(req, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let category = yield this.categoryService.findAllCategories(req.user.id, filter);
                this.respondWithErrorsOrSuccess(category);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof dto_1.CreateCategoryDto !== "undefined" && dto_1.CreateCategoryDto) === "function" ? _a : Object, Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CategoryController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()), __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, typeof (_c = typeof dto_1.UpdateCategoryDto !== "undefined" && dto_1.UpdateCategoryDto) === "function" ? _c : Object, Object]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], CategoryController.prototype, "updateCategory", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], CategoryController.prototype, "deleteCategory", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], CategoryController.prototype, "getCategory", null);
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_g = typeof dto_1.SearchCategoryDto !== "undefined" && dto_1.SearchCategoryDto) === "function" ? _g : Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], CategoryController.prototype, "getAllCategories", null);
CategoryController = __decorate([
    swagger_1.ApiTags('Categories'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Category',
        createDto: dto_1.CreateCategoryDto,
        updateDto: dto_1.UpdateCategoryDto,
        transformDto: dto_1.TransformCategoryDto,
        searchDto: dto_1.SearchCategoryDto
    }),
    __metadata("design:paramtypes", [typeof (_j = typeof categories_service_1.CategoryService !== "undefined" && categories_service_1.CategoryService) === "function" ? _j : Object])
], CategoryController);
exports.CategoryController = CategoryController;


/***/ }),
/* 330 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(331));


/***/ }),
/* 331 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const categories_entity_1 = __webpack_require__(206);
const typeorm_1 = __webpack_require__(120);
const companies_1 = __webpack_require__(187);
class TransformCategoryDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCategoryDto.prototype, "id", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCategoryDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCategoryDto.prototype, "key", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformCategoryDto.prototype, "numberOfProduct", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], TransformCategoryDto.prototype, "children", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof categories_entity_1.CategoryEntity !== "undefined" && categories_entity_1.CategoryEntity) === "function" ? _a : Object)
], TransformCategoryDto.prototype, "parent", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformCategoryDto.prototype, "companyId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => companies_1.CompanyEntity, company => company.id),
    typeorm_1.JoinColumn({ name: "companyId" }),
    __metadata("design:type", typeof (_b = typeof companies_1.CompanyEntity !== "undefined" && companies_1.CompanyEntity) === "function" ? _b : Object)
], TransformCategoryDto.prototype, "company", void 0);
exports.TransformCategoryDto = TransformCategoryDto;
class CreateCategoryDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Men' }),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 128),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCategoryDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCategoryDto.prototype, "key", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], CreateCategoryDto.prototype, "children", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'd8cf855b-a2a0-4ca6-9c16-36da1f1b4040' }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_c = typeof categories_entity_1.CategoryEntity !== "undefined" && categories_entity_1.CategoryEntity) === "function" ? _c : Object)
], CreateCategoryDto.prototype, "parent", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateCategoryDto.prototype, "companyId", void 0);
exports.CreateCategoryDto = CreateCategoryDto;
class UpdateCategoryDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCategoryDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Men' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateCategoryDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], UpdateCategoryDto.prototype, "children", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_d = typeof categories_entity_1.CategoryEntity !== "undefined" && categories_entity_1.CategoryEntity) === "function" ? _d : Object)
], UpdateCategoryDto.prototype, "parent", void 0);
exports.UpdateCategoryDto = UpdateCategoryDto;
class SearchCategoryDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'name']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCategoryDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Men" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCategoryDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCategoryDto.prototype, "order", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCategoryDto.prototype, "relations", void 0);
exports.SearchCategoryDto = SearchCategoryDto;


/***/ }),
/* 332 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const api_error_code_1 = __webpack_require__(94);
const database_1 = __webpack_require__(130);
const categories_entity_1 = __webpack_require__(206);
const dto_1 = __webpack_require__(330);
const shared_1 = __webpack_require__(25);
const typeorm_2 = __webpack_require__(120);
const products_1 = __webpack_require__(322);
const user_1 = __webpack_require__(256);
const filter_utils_1 = __webpack_require__(333);
let CategoryService = class CategoryService extends core_1.CrudService {
    constructor(categoryRepository, productService, userService) {
        super(categoryRepository);
        this.categoryRepository = categoryRepository;
        this.productService = productService;
        this.userService = userService;
    }
    createCategory(category, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (category.parent && category.parent.toString() === '') {
                    delete category.parent;
                }
                const user = yield this.userService.findOne({ id: userId });
                category.companyId = user.companyId;
                if (!category.parent) {
                    let checkNameCategoryParent = yield this.categoryRepository.findOne({
                        name: typeorm_2.Raw(alias => `${alias} ILIKE '${category.name}'`),
                        parent: null,
                        companyId: category.companyId
                    });
                    if (checkNameCategoryParent) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_NAME_EXISTED
                        };
                    }
                    category.key = `${category.name}`;
                }
                else {
                    let parent = yield this.categoryRepository.findOne({
                        id: category.parent.toString(),
                        companyId: category.companyId
                    });
                    if (!parent) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                        };
                    }
                    let checkNameCategoryChildren = yield this.categoryRepository.findOne({
                        name: typeorm_2.Raw(alias => `${alias} ILIKE '${category.name}'`),
                        parent: parent,
                        companyId: category.companyId
                    });
                    if (checkNameCategoryChildren) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_EXISTED
                        };
                    }
                    category.key = `${parent.key}.${category.name}`;
                }
                let createCategory = yield this.categoryRepository.save(category);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, createCategory, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateCategory(id, category, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                category.id = id;
                const user = yield this.userService.findOne({ id: userId });
                const companyId = user.companyId;
                let checkCategoryId = yield this.categoryRepository.findOne({
                    id: id,
                    companyId: companyId
                }, { relations: ["parent", "children"] });
                if (!checkCategoryId) {
                    return {
                        code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                    };
                }
                if (checkCategoryId.parent) {
                    let parent = yield this.categoryRepository.findOne({
                        id: checkCategoryId.parent.id,
                        companyId: companyId
                    });
                    if (!parent) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                        };
                    }
                    let categoryExist = yield this.categoryRepository.findOne({
                        name: typeorm_2.Raw(alias => `${alias} ILIKE '${category.name}'`),
                        parent: parent,
                        companyId: companyId
                    });
                    if (categoryExist) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_EXISTED
                        };
                    }
                    category.key = `${parent.key}.${category.name}`;
                }
                else {
                    let categoryExist = yield this.categoryRepository.findOne({
                        name: typeorm_2.Raw(alias => `${alias} ILIKE '${category.name}'`),
                        parent: null,
                        companyId: companyId
                    });
                    if (categoryExist) {
                        return {
                            code: api_error_code_1.CodeEnum.CATEGORY_EXISTED
                        };
                    }
                    category.key = category.name;
                    yield this.changeKeyChildrens(category.name, checkCategoryId.children);
                }
                let updateCategory = yield this.categoryRepository.save(category);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, updateCategory, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findCategory(id, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.userService.findOne({ id: userId });
                const companyId = user.companyId;
                let checkCategoryId = yield this.categoryRepository.findOne({
                    id: id,
                    companyId
                });
                if (!checkCategoryId) {
                    return {
                        code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                    };
                }
                let findCategory = yield this.categoryRepository.findOne({
                    id: id,
                    companyId
                }, { relations: ["children", "parent"] });
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, findCategory, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findAllCategories(userId, filter) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findOne({ id: userId });
                const companyId = userExist === null || userExist === void 0 ? void 0 : userExist.companyId;
                let findAllCategory;
                const table = 'category';
                if (filter) {
                    let select = filter_utils_1.formatSelect(filter.select, table);
                    let order = filter_utils_1.formatOrder(filter.order, table);
                    let value = (_b = (_a = filter.where) === null || _a === void 0 ? void 0 : _a.search) !== null && _b !== void 0 ? _b : '';
                    findAllCategory = yield this.categoryRepository.createQueryBuilder(table)
                        .select(select)
                        .leftJoinAndSelect(`${table}.children`, 'children')
                        .leftJoinAndSelect('children.children', 'children_children')
                        .where(`${table}.parent IS NULL`)
                        .andWhere(`${table}.companyId = '${companyId}'`)
                        .andWhere(`${table}.name ILIKE '%${value}%'`)
                        .take(filter.take)
                        .skip(filter.skip)
                        .orderBy(order)
                        .getMany();
                }
                else {
                    findAllCategory = yield this.categoryRepository.createQueryBuilder(table)
                        .leftJoinAndSelect(`${table}.children`, 'children')
                        .leftJoinAndSelect('children.children', 'children_children')
                        .where(`${table}.parent IS NULL`)
                        .andWhere(`${table}.companyId = '${companyId}'`)
                        .getMany();
                }
                yield this.productService.addTotalProductToCategories(findAllCategory);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, findAllCategory, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteCategory(id, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findOne({ id: userId });
                const companyId = userExist === null || userExist === void 0 ? void 0 : userExist.companyId;
                let categoryExist = yield this.categoryRepository.findOne({
                    id: id,
                    companyId
                }, { relations: ['children'] });
                if (!categoryExist) {
                    return {
                        code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                    };
                }
                const unCategory = yield this.getUncategory(userId);
                if (unCategory.id === id) {
                    return {
                        data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, {})
                    };
                }
                let categoriesList = [categoryExist.id];
                if (categoryExist.children.length > 0) {
                    for (let i = 0; i < categoryExist.children.length; i++) {
                        categoriesList.push(categoryExist.children[i].id);
                    }
                }
                for (let i = 0; i < categoriesList.length; i++) {
                    let productCondition = {
                        where: {
                            categoryId: categoriesList[i],
                            companyId
                        }
                    };
                    let productOfCategory = yield this.productService.findAll(productCondition);
                    for (let j = 0; j < productOfCategory.items.length; j++) {
                        productOfCategory.items[j].categoryId = unCategory.id;
                        yield this.productService.create(productOfCategory.items[j]);
                    }
                }
                yield this.categoryRepository.delete({ parent: { id } });
                yield this.categoryRepository.delete({ id: id });
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, {})
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findOneCategory(conditions, options) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let category = yield this.categoryRepository.findOne(conditions, options);
                if (!category) {
                    return {
                        code: api_error_code_1.CodeEnum.CATEGORY_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformCategoryDto, category, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getUncategory(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findOne({ id: userId });
                const companyId = userExist === null || userExist === void 0 ? void 0 : userExist.companyId;
                let unCategory = yield this.categoryRepository.findOne({ name: 'Uncategorized', companyId });
                if (!unCategory) {
                    return (yield this.createCategory({ name: 'Uncategorized' }, userId)).data;
                }
                return unCategory;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    mergeCategory(id, category) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let updateCategory = yield this.categoryRepository.findOne({ id: id });
                return yield this.categoryRepository.merge(updateCategory, category);
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    changeKeyChildrens(parentName, childrens) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                childrens.forEach(element => {
                    element.key = `${parentName}.${element.name}`;
                });
                this.categoryRepository.save(childrens);
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
CategoryService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(categories_entity_1.CategoryEntity, database_1.DB_CONNECTION_DEFAULT)),
    __param(1, common_1.Inject(common_1.forwardRef(() => products_1.ProductService))),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof products_1.ProductService !== "undefined" && products_1.ProductService) === "function" ? _b : Object, typeof (_c = typeof user_1.UserService !== "undefined" && user_1.UserService) === "function" ? _c : Object])
], CategoryService);
exports.CategoryService = CategoryService;


/***/ }),
/* 333 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Quantity;
(function (Quantity) {
    Quantity[Quantity["ALL_INVENTORY"] = 0] = "ALL_INVENTORY";
    Quantity[Quantity["LOW_OF_STOCK"] = 1] = "LOW_OF_STOCK";
    Quantity[Quantity["OUT_OF_STOCK"] = 2] = "OUT_OF_STOCK";
})(Quantity = exports.Quantity || (exports.Quantity = {}));
exports.formatSelect = (select, table) => {
    if (!select || !Array.isArray(select) || !select.length)
        return null;
    const newSelect = select.map(fieldName => `${table}.${fieldName}`);
    return newSelect;
};
exports.formatOrder = (order, table) => {
    let newOrder = {};
    if (!order)
        return newOrder;
    for (let propertyName in order) {
        newOrder[`${table}.${propertyName}`] = order[`${propertyName}`];
    }
    return newOrder;
};


/***/ }),
/* 334 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const products_entity_1 = __webpack_require__(205);
const images_1 = __webpack_require__(190);
class TransformVariationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "rfid", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "variationType", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "variationName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformVariationDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "option", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], TransformVariationDto.prototype, "variation", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], TransformVariationDto.prototype, "images", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], TransformVariationDto.prototype, "children", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _a : Object)
], TransformVariationDto.prototype, "parent", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformVariationDto.prototype, "index", void 0);
exports.TransformVariationDto = TransformVariationDto;
class CreateVariationDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Boolean)
], CreateVariationDto.prototype, "isVariation", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'SKU 345-091' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_validator_1.Length(1, 999999999999),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "rfid", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "categoryId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'size' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "variationType", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'S' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "variationName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateVariationDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "option", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], CreateVariationDto.prototype, "images", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], CreateVariationDto.prototype, "children", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _b : Object)
], CreateVariationDto.prototype, "parent", void 0);
exports.CreateVariationDto = CreateVariationDto;
class UpdateVariationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "id", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "categoryId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'size' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "variationType", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'S' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "variationName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateVariationDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "option", void 0);
__decorate([
    typeorm_1.ManyToMany(type => images_1.ImagesEntity),
    typeorm_1.JoinTable(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], UpdateVariationDto.prototype, "images", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Array)
], UpdateVariationDto.prototype, "children", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_c = typeof products_entity_1.ProductEntity !== "undefined" && products_entity_1.ProductEntity) === "function" ? _c : Object)
], UpdateVariationDto.prototype, "parent", void 0);
exports.UpdateVariationDto = UpdateVariationDto;
class SearchVariationDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['sku', 'barcode']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformVariationDto, ['sku', 'barcode'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: ['images']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "relations", void 0);
exports.SearchVariationDto = SearchVariationDto;


/***/ }),
/* 335 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const class_transformer_1 = __webpack_require__(16);
class toSgtin96TagUriDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '0614141'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "CompanyPrefix", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '812345'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "ItemReference", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '6789'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toSgtin96TagUriDto.prototype, "SerialNumber", void 0);
exports.toSgtin96TagUriDto = toSgtin96TagUriDto;
class hexToSgtin96TagUriDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '3074257BF7194E4000001A85'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], hexToSgtin96TagUriDto.prototype, "epcHexadecimal", void 0);
exports.hexToSgtin96TagUriDto = hexToSgtin96TagUriDto;
class toZplDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '$35.00'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'Flower Dress'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'Small'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "variationName", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '1234567'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '3074257BF7194E4000001A85'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], toZplDto.prototype, "rfid", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 10
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], toZplDto.prototype, "quantity", void 0);
exports.toZplDto = toZplDto;
class PrintLabelDto {
}
__decorate([
    swagger_1.ApiProperty({ type: toZplDto, isArray: true }),
    class_transformer_1.Type(() => toZplDto),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], PrintLabelDto.prototype, "data", void 0);
exports.PrintLabelDto = PrintLabelDto;


/***/ }),
/* 336 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const bull_1 = __webpack_require__(337);
const bull_2 = __webpack_require__(338);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const products_entity_1 = __webpack_require__(205);
const dto_1 = __webpack_require__(325);
const api_error_code_1 = __webpack_require__(94);
const images_1 = __webpack_require__(190);
const shared_1 = __webpack_require__(25);
const file_uploading_utils_1 = __webpack_require__(249);
const barcode_epc_1 = __webpack_require__(311);
const zpl_printer_1 = __webpack_require__(313);
const readFile_utils_1 = __webpack_require__(339);
const compareArray_utils_1 = __webpack_require__(341);
const categories_1 = __webpack_require__(327);
const typeorm_2 = __webpack_require__(120);
const filter_utils_1 = __webpack_require__(333);
const barcodes_service_1 = __webpack_require__(342);
const dto_2 = __webpack_require__(343);
const exportExcel_utils_1 = __webpack_require__(346);
const user_1 = __webpack_require__(256);
const environment_1 = __webpack_require__(6);
const path = __webpack_require__(231);
const fs = __webpack_require__(198);
let ProductService = class ProductService extends core_1.CrudService {
    constructor(productRepository, productQueue, categoryService, imageService, barcodeService, userService) {
        super(productRepository);
        this.productRepository = productRepository;
        this.productQueue = productQueue;
        this.categoryService = categoryService;
        this.imageService = imageService;
        this.barcodeService = barcodeService;
        this.userService = userService;
    }
    createNewProduct(userId, product, images) {
        return __awaiter(this, void 0, void 0, function* () {
            this.productQueue.add('createProduct', { userId: userId, data: product, images: images });
            return {
                data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, product, { excludeExtraneousValues: false })
            };
        });
    }
    createProduct(userId, product, images) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.validateProduct(userId, product);
                let { code } = yield this.validateProduct(userId, product);
                if (code) {
                    return {
                        code: code
                    };
                }
                if (!product.sku) {
                    product.sku = yield this.generateSKU(product.name);
                }
                const user = yield this.userService.findUser(userId);
                product.companyId = user.data.company.id;
                let createProduct;
                if (!product.variations) {
                    if (images) {
                        if (images.length > 4) {
                            return {
                                code: api_error_code_1.CodeEnum.PRODUCT_LIMIT_IMAGES
                            };
                        }
                        product.images = yield this.createImageProduct(images);
                    }
                    if (product.barcode) {
                        if (product.barcode.length != 8) {
                            return {
                                code: api_error_code_1.CodeEnum.BARCODE_LENGTH
                            };
                        }
                        if (yield this.checkBarcode(product.barcode)) {
                            return {
                                code: api_error_code_1.CodeEnum.BARCODE_EXISTED
                            };
                        }
                    }
                    else {
                        product.barcode = yield this.generateBarcode(userId);
                    }
                    product.rfid = yield this.generateRFID(product.barcode);
                    const getBarcodeCountry = yield this.getBarcodeCountry(userId);
                    const getLastestBarcode = yield this.getLastedBarcode(getBarcodeCountry);
                    if (getLastestBarcode && !getLastestBarcode.code) {
                        yield this.barcodeService.update(getLastestBarcode.data.id, { lastedBarcode: product.barcode });
                    }
                    else {
                        let barcode = {
                            countryCode: getBarcodeCountry,
                            lastedBarcode: product.barcode
                        };
                        yield this.barcodeService.createBarcode(barcode);
                    }
                    createProduct = yield this.productRepository.save(product);
                }
                else {
                    delete product.barcode;
                    product.quantity = 0;
                    JSON.parse(product.variations).forEach(variation => product.quantity += parseInt(variation.quantity));
                    createProduct = yield this.productRepository.save(product);
                    yield this.createVariation(userId, createProduct, JSON.parse(JSON.stringify(product.variations)), images);
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, createProduct, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getProduct(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = userExist.data.companyId;
                let getProduct = yield this.findData({
                    where: { id: id, companyId: userCompanyId },
                    relations: ["images", "children", "category"]
                });
                if (!getProduct) {
                    return {
                        code: api_error_code_1.CodeEnum.PRODUCT_NOT_EXISTED
                    };
                }
                getProduct.images = getProduct.images.length > 0 ? yield file_uploading_utils_1.getImageUrl(getProduct.images, true) : [];
                getProduct.option = getProduct.option ? JSON.parse(getProduct.option) : null;
                if (getProduct.children) {
                    for (let i = 0; i < getProduct.children.length; i++) {
                        getProduct.children[i].option = getProduct.children[i].option ? JSON.parse(getProduct.children[i].option) : null;
                        getProduct.children[i].images = yield file_uploading_utils_1.getImageUrl(getProduct.children[i].images, true);
                    }
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, getProduct, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getAllProduct(userId, filter) {
        var _a, _b, _c, _d, _e, _f, _g;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                let getAllProduct;
                const table = 'product';
                let total = 0;
                if (filter) {
                    let searchProduct = (_c = (_b = filter.where) === null || _b === void 0 ? void 0 : _b.search) !== null && _c !== void 0 ? _c : '';
                    let categoryName = (_e = (_d = filter.where) === null || _d === void 0 ? void 0 : _d.categoryName) !== null && _e !== void 0 ? _e : '';
                    let categoryId = (_f = filter.where) === null || _f === void 0 ? void 0 : _f.categoryId;
                    let quantity = (_g = filter.where) === null || _g === void 0 ? void 0 : _g.quantity;
                    let select = filter_utils_1.formatSelect(filter.select, table);
                    let order = filter_utils_1.formatOrder(filter.order, table);
                    [getAllProduct, total] = yield this.productRepository.createQueryBuilder(table)
                        .select(select)
                        .leftJoinAndSelect(`${table}.children`, 'children')
                        .leftJoinAndSelect('children.images', 'children_images')
                        .leftJoinAndSelect(`${table}.category`, 'category')
                        .leftJoinAndSelect(`${table}.images`, 'images')
                        .leftJoinAndSelect(`${table}.company`, 'company')
                        .where(new typeorm_2.Brackets(qb => {
                        qb.where(`${table}.isVariation = false`);
                        if (searchProduct) {
                            qb.andWhere(new typeorm_2.Brackets(qbSearchProduct => {
                                qbSearchProduct.where(`${table}.name ILIKE '%${searchProduct}%'`)
                                    .orWhere(`${table}.sku ILIKE '%${searchProduct}%'`)
                                    .orWhere(`${table}.barcode ILIKE '%${searchProduct}%'`);
                            }));
                        }
                        if (Array.isArray(categoryId) && categoryId.length) {
                            qb.andWhere(`${table}.categoryId IN(:...categoryId)`, { categoryId });
                        }
                        if (userCompanyId) {
                            qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
                        }
                        if (categoryName) {
                            qb.andWhere(`category.name ILIKE '%${categoryName}%'`);
                        }
                        if (quantity) {
                            switch (quantity) {
                                case filter_utils_1.Quantity.LOW_OF_STOCK:
                                    qb.andWhere(`${table}.quantity > 0`);
                                    break;
                                case filter_utils_1.Quantity.OUT_OF_STOCK:
                                    qb.andWhere(`${table}.quantity = 0`);
                                    break;
                            }
                        }
                    }))
                        .take(filter.take)
                        .skip(filter.skip)
                        .orderBy(order)
                        .getManyAndCount();
                }
                else {
                    [getAllProduct, total] = yield this.productRepository.createQueryBuilder(table)
                        .leftJoinAndSelect(`${table}.children`, 'children')
                        .leftJoinAndSelect('children.images', 'children_images')
                        .leftJoinAndSelect(`${table}.category`, 'category')
                        .leftJoinAndSelect(`${table}.images`, 'images')
                        .leftJoinAndSelect(`${table}.company`, 'company')
                        .where(new typeorm_2.Brackets(qb => {
                        qb.where(`${table}.isVariation = false`);
                        if (userCompanyId) {
                            qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
                        }
                    }))
                        .getManyAndCount();
                }
                for (let i = 0; i < getAllProduct.length; i++) {
                    getAllProduct[i].images = getAllProduct[i].images ? yield file_uploading_utils_1.getImageUrl(getAllProduct[i].images, true) : [];
                    getAllProduct[i].option = getAllProduct[i].option ? JSON.parse(getAllProduct[i].option) : null;
                    if (getAllProduct[i].children) {
                        for (let j = 0; j < getAllProduct[i].children.length; j++) {
                            getAllProduct[i].children[j].option = getAllProduct[i].children[j].option ? JSON.parse(getAllProduct[i].children[j].option) : null;
                            getAllProduct[i].children[j].images = getAllProduct[i].children[j].images ? yield file_uploading_utils_1.getImageUrl(getAllProduct[i].children[j].images, true) : [];
                        }
                    }
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, getAllProduct, { excludeExtraneousValues: false }),
                    options: { total: total }
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateProduct(req, product, images) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (product.sku) {
                    let user = yield this.userService.findUser(req.user.id);
                    let checkSKU = yield this.productRepository.findOne({ sku: product.sku, companyId: (_a = user.data) === null || _a === void 0 ? void 0 : _a.companyId });
                    if (checkSKU && checkSKU.id !== product.id) {
                        return {
                            code: api_error_code_1.CodeEnum.SKU_EXISTED
                        };
                    }
                }
                let productExisted = yield this.findData({ id: product.id }, { relations: ['images', 'children', 'children.images'] });
                if (!productExisted) {
                    return {
                        code: api_error_code_1.CodeEnum.PRODUCT_NOT_EXISTED
                    };
                }
                if (!product.variations) {
                    if (productExisted.children.length > 0) {
                        for (let i = 0; i < productExisted.children.length; i++) {
                            yield this.productRepository.delete({ id: productExisted.children[i].id });
                        }
                    }
                    else {
                        if (images) {
                            if (images.length > 4) {
                                return {
                                    code: api_error_code_1.CodeEnum.PRODUCT_LIMIT_IMAGES
                                };
                            }
                            product.images = yield this.uploadImageProduct(productExisted, images);
                        }
                        else {
                            for (let i = 0; i < productExisted.images.length; i++) {
                                yield this.imageService.delete(productExisted.images[i].id);
                            }
                        }
                    }
                }
                else {
                    product.quantity = 0;
                    const updateVariations = product.variations ? JSON.parse(product.variations) : [];
                    updateVariations.forEach((variation, idx) => {
                        product.quantity += parseInt(variation.quantity);
                        variation.index = idx;
                    });
                    yield this.updateVariation(req, productExisted, updateVariations, images);
                }
                let updateProduct = yield this.productRepository.save(product);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, updateProduct, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteProduct(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                ids = ids.toString().split(',');
                yield this.productRepository.delete({ parent: typeorm_2.In(ids) });
                yield this.productRepository.delete({ id: typeorm_2.In(ids) });
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, {}, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    checkBarcode(barcode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcodeExist = yield this.findData({ barcode: barcode });
                if (barcodeExist) {
                    return true;
                }
                return false;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    checkSKU(userId, sku) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield this.userService.findUser(userId);
                let skuExist = yield this.findData({ sku: sku, companyId: (_a = user.data) === null || _a === void 0 ? void 0 : _a.companyId });
                if (skuExist) {
                    return true;
                }
                return false;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    generateRFID(barcode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let itemRef;
                const serial = Math.floor(10000 + Math.random() * 999999);
                switch (barcode.length) {
                    case 8: itemRef = barcode.slice(2, 7);
                }
                const sgtin96TagUri = yield barcode_epc_1.toSgtin96TagUri(barcode.toString(), itemRef.toString(), serial.toString());
                const generateRFID = yield barcode_epc_1.sgtin96TagUriToHex(sgtin96TagUri);
                return generateRFID;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    generateBarcode(userId, count) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcodeOfUser = yield this.getBarcodeCountry(userId);
                barcodeOfUser = barcodeOfUser.split("–")[0];
                let barcodeExist = true;
                let barcode;
                let generateBarcode;
                let generateCode = [];
                const lastedBarcode = yield this.getLastedBarcode(barcodeOfUser);
                if (lastedBarcode && !lastedBarcode.code) {
                    barcode = lastedBarcode.data.lastedBarcode.slice(0, 7);
                    barcode = parseInt(barcode) + 1;
                }
                else {
                    for (let i = 0; i < (7 - barcodeOfUser.length); i++) {
                        generateCode.push(0);
                    }
                    barcode = `${barcodeOfUser}${generateCode.join('')}`;
                }
                if (!count) {
                    while (barcodeExist) {
                        generateBarcode = yield this.eanCheckDigit(barcode.toString());
                        barcodeExist = (yield this.findData({ barcode: generateBarcode })) === undefined ? false : true;
                        barcode = parseInt(barcode) + 1;
                    }
                }
                else {
                    if (count < 1) {
                        return {
                            code: api_error_code_1.CodeEnum.COUNT_MUST_GREATER_1
                        };
                    }
                    generateBarcode = [];
                    for (let i = 0; i < count; i++) {
                        barcodeExist = true;
                        let genBarcode = yield this.eanCheckDigit(barcode.toString());
                        while (barcodeExist) {
                            barcodeExist = (yield this.findData({ barcode: generateBarcode })) === undefined ? false : true;
                            if (barcodeExist) {
                                barcode = parseInt(barcode) + 1;
                            }
                        }
                        generateBarcode.push(genBarcode);
                        barcode = parseInt(barcode) + 1;
                    }
                }
                return generateBarcode;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    generateSKU(productName, category, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield this.userService.findOne({ id: userId });
                if (user.language === 'ar') {
                    productName = productName.slice(-2);
                    category = category.slice(-2);
                }
                else {
                    productName = productName.slice(0, 2);
                    category = category.slice(0, 2);
                }
                if (productName.length === 1) {
                    productName = `${productName}${productName}`;
                }
                if (category.length === 1) {
                    category = `${category}${category}`;
                }
                let skuExist = true;
                let generateSKU;
                let uniqueCode = 90000;
                while (skuExist) {
                    let rawString = `${category}${productName}`;
                    generateSKU = `${rawString.slice(0, 4)}${uniqueCode.toString().slice(1, 5)}`;
                    generateSKU = generateSKU.toUpperCase();
                    skuExist = (yield this.findData({ sku: generateSKU })) === undefined ? false : true;
                    if (skuExist) {
                        uniqueCode = uniqueCode + 1;
                    }
                }
                return generateSKU;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    printLabel(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let zplTemplates = [];
                let zplData = data.data;
                for (let i = 0; i < zplData.length; i++) {
                    for (let j = 0; j < zplData[i].quantity; j++) {
                        if (!zplData[i].variationName) {
                            delete zplData[i].variationName;
                        }
                        let template = yield zpl_printer_1.printZpl(zplData[i]);
                        zplTemplates.push(template);
                    }
                }
                return {
                    data: zplTemplates
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    importFile(req, path) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let products = yield readFile_utils_1.readFileExcell(path);
                let errorCodes = [];
                const userExist = yield this.userService.findUserNoRelations(req.user.id);
                const userCompanyId = userExist.data.companyId;
                const categoryConditions = {
                    where: { companyId: userCompanyId }
                };
                const allCategories = yield this.categoryService.findAll(categoryConditions);
                const getUncategorized = yield this.categoryService.getUncategory(req.user.id);
                let createProductWithoutVariant = [];
                let createProductWithVariant = [];
                for (let i = 0; i < products.length; i++) {
                    let { code } = yield this.validateProduct(req.user.id, products[i]);
                    if (code) {
                        errorCodes.push({ line: i + 1, errorCode: code });
                        continue;
                    }
                    let getCategory = allCategories.items.filter(category => category.key === products[i].category);
                    if (getCategory.length > 0) {
                        products[i].categoryId = getCategory[0].id;
                    }
                    else {
                        products[i].categoryId = getUncategorized.id;
                    }
                    if (products[i].variations) {
                        products[i].variations = JSON.stringify(products[i].variations);
                        createProductWithVariant.push(products[i]);
                    }
                    else {
                        createProductWithoutVariant.push(products[i]);
                    }
                }
                if (errorCodes.length > 0) {
                    return {
                        code: { error: errorCodes }
                    };
                }
                for (let i = 0; i < createProductWithoutVariant.length; i++) {
                    this.productQueue.add('createProduct', { userId: req.user.id, data: createProductWithoutVariant[i] });
                }
                for (let i = 0; i < createProductWithVariant.length; i++) {
                    this.productQueue.add('createProduct', { userId: req.user.id, data: createProductWithVariant[i] });
                }
                fs.unlink(path, (e) => {
                    if (e) {
                        console.log(e);
                        throw e;
                    }
                });
                return {
                    data: products
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    downloadTemplate() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const filePath = `${__dirname.replace("dist", "")}${process.env.PRODUCT_TEMPLATES_PATH}`;
                return filePath;
            }
            catch (e) {
                throw e;
            }
        });
    }
    exportFile(req, ids) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let productIds = ids.productIds;
                const productPath = path.join(__dirname.replace("dist", ""), `${environment_1.default.productExport.path}/product-list-${new Date().getTime()}.xlsx`);
                var exportProduct;
                let allProduct = yield this.getAllProduct(req.user.id);
                if (productIds === null || productIds === void 0 ? void 0 : productIds.length) {
                    let products = yield this.filterProduct(productIds, allProduct.data);
                    exportProduct = yield exportExcel_utils_1.exportProductsToExcel(products);
                }
                else {
                    exportProduct = yield exportExcel_utils_1.exportProductsToExcel(allProduct.data);
                }
                yield fs.writeFileSync(productPath, exportProduct);
                return yield productPath;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    countProductWithOptionId(userCompanyId, optionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const table = 'product';
            const products = yield this.productRepository.createQueryBuilder(table)
                .leftJoinAndSelect(`${table}.children`, 'children')
                .where(new typeorm_2.Brackets(qb => {
                qb.where(`${table}.isVariation = false`);
                if (userCompanyId) {
                    qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
                }
            }))
                .getMany();
            let total = 0;
            products.forEach(product => {
                const productOptions = JSON.parse(product.option);
                productOptions === null || productOptions === void 0 ? void 0 : productOptions.forEach(option => {
                    if (option.id === optionId) {
                        total += 1;
                    }
                });
            });
            return total;
        });
    }
    getCountVariation(userCompanyId, value) {
        return __awaiter(this, void 0, void 0, function* () {
            const table = 'product';
            const products = yield this.productRepository.createQueryBuilder(table)
                .leftJoinAndSelect(`${table}.parent`, 'parent')
                .where(`parent.companyId = '${userCompanyId}'`)
                .getMany();
            let total = 0;
            products.forEach(product => {
                const arrVariationNames = product.variationName.split(',');
                arrVariationNames.forEach(name => {
                    if (name.toLowerCase() === value.toLowerCase()) {
                        total += 1;
                    }
                });
            });
            return total;
        });
    }
    countVariation(userCompanyId, optionName, value) {
        return __awaiter(this, void 0, void 0, function* () {
            const table = 'product';
            const products = yield this.productRepository.createQueryBuilder(table)
                .leftJoinAndSelect(`${table}.parent`, 'parent')
                .where(`parent.companyId = '${userCompanyId}'`)
                .getMany();
            let total = 0;
            products.forEach(product => {
                const productOptions = JSON.parse(product.parent.option);
                if (productOptions === null || productOptions === void 0 ? void 0 : productOptions.some(option => option.name.toLowerCase() === optionName.toLowerCase())) {
                    const arrVariationNames = product.variationName.split(',');
                    arrVariationNames.forEach(name => {
                        if (name.toLowerCase() === value.toLowerCase()) {
                            total += 1;
                        }
                    });
                }
            });
            return total;
        });
    }
    createVariation(userId, product, variationsString, variationImages) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let variations = JSON.parse((variationsString));
                let bulk = [];
                for (let i = 0; i < variations.length; i++) {
                    let variation = new dto_1.CreateVariationDto();
                    variation.isVariation = true;
                    if (variationImages && variationImages[i]) {
                        variation.images = yield this.createImageVariant(variationImages[i]);
                    }
                    if (variations[i].sku) {
                        delete variations[i].sku;
                    }
                    if (variations[i].barcode) {
                        if (variations[i].barcode.length != 8) {
                            return {
                                code: api_error_code_1.CodeEnum.BARCODE_LENGTH
                            };
                        }
                        if (yield this.checkBarcode(variations[i].barcode)) {
                            return {
                                code: api_error_code_1.CodeEnum.BARCODE_EXISTED
                            };
                        }
                        variation.barcode = variations[i].barcode;
                    }
                    else {
                        variation.barcode = yield this.generateBarcode(userId);
                    }
                    variation.rfid = yield this.generateRFID(variation.barcode);
                    variation.variationName = variations[i].variationName;
                    variation.quantity = variations[i].quantity;
                    variation.option = JSON.stringify(variations[i].option);
                    variation.parent = product;
                    bulk.push(variation);
                    yield this.updateLastedBarcode(userId, variation.barcode);
                }
                yield this.productRepository.save(bulk);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, null, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateVariation(req, product, variations, variationImages) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var i;
                if (product.children.length === 0) {
                    for (let i = 0; i < product.images.length; i++) {
                        yield this.imageService.delete({ id: product.images[i].id });
                    }
                    yield this.createVariation(req.user.id, product, JSON.stringify(variations), variationImages);
                }
                else {
                    let deletedVariation = product.children.filter(compareArray_utils_1.findDifferentArray(variations));
                    let createNewVariation = variations.filter(compareArray_utils_1.findDifferentArray(product.children));
                    var updateVariration = variations.filter(compareArray_utils_1.findMatchArray(product.children));
                    for (i = 0; i < deletedVariation.length; i++) {
                        yield this.productRepository.delete({ barcode: deletedVariation[i].barcode });
                        yield this.deleteProductOptions(deletedVariation, product);
                    }
                    if (createNewVariation.length > 0) {
                        let createImages = [];
                        createNewVariation.forEach(variation => {
                            createImages.push(variationImages[variation.index]);
                        });
                        yield this.createVariation(req.user.id, product, JSON.stringify(createNewVariation), createImages);
                    }
                    let updateImages = [];
                    if (updateVariration.length > 0) {
                        updateVariration.forEach(variation => {
                            updateImages.push(variationImages[variation.index]);
                        });
                    }
                    for (let i = 0; i < updateVariration.length; i++) {
                        let variation = new dto_1.UpdateVariationDto();
                        if (updateImages && updateImages[i]) {
                            variation.images = yield this.uploadImageVariant(updateVariration[i], updateImages[i]);
                        }
                        let getVariationId = yield this.productRepository.findOne({ barcode: updateVariration[i].barcode });
                        if (!getVariationId) {
                            return {
                                code: api_error_code_1.CodeEnum.PRODUCT_NOT_EXISTED
                            };
                        }
                        variation.id = getVariationId.id;
                        variation.variationName = updateVariration[i].variationName;
                        variation.quantity = updateVariration[i].quantity;
                        variation.option = JSON.stringify(updateVariration[i].option);
                        yield this.productRepository.save(variation);
                    }
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformProductDto, null, { excludeExtraneousValues: false })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findData(conditions, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.productRepository.findOne(conditions, options);
        });
    }
    eanCheckDigit(s) {
        let result = 0;
        let i = 1;
        for (let counter = s.length - 1; counter >= 0; counter--) {
            result = result + parseInt(s.charAt(counter)) * (1 + (2 * (i % 2)));
            i++;
        }
        return `${s}${(10 - (result % 10)) % 10}`;
    }
    getBarcodeCountry(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.userService.findUser(userId);
            const barcodeOfUser = user.data.company.barcodeCountry ? user.data.company.barcodeCountry : process.env.BARCODE;
            return barcodeOfUser;
        });
    }
    getLastedBarcode(barcodeCountry) {
        return __awaiter(this, void 0, void 0, function* () {
            let getCode = new dto_2.FindBarcodeDto();
            getCode.countryCode = barcodeCountry;
            let lastedBarcode = yield this.barcodeService.findBarcode(getCode);
            return lastedBarcode;
        });
    }
    updateLastedBarcode(userId, barcode) {
        return __awaiter(this, void 0, void 0, function* () {
            const getBarcodeCountry = yield this.getBarcodeCountry(userId);
            const getLastestBarcode = yield this.getLastedBarcode(getBarcodeCountry);
            let updateLastedBarcode;
            if (getLastestBarcode && !getLastestBarcode.code) {
                updateLastedBarcode = yield this.barcodeService.update(getLastestBarcode.data.id, { lastedBarcode: barcode });
            }
            else {
                let updateBarcode = {
                    countryCode: getBarcodeCountry,
                    lastedBarcode: barcode
                };
                updateLastedBarcode = yield this.barcodeService.createBarcode(updateBarcode);
            }
            return updateLastedBarcode;
        });
    }
    createImageProduct(images) {
        return __awaiter(this, void 0, void 0, function* () {
            let updateImg = [];
            for (let i = 0; i < images.length; i++) {
                if (images[i].size > 0 && images[i].mimetype !== 'application/octet-stream' && images[i].mimetype !== 'application/json') {
                    let img = yield this.imageService.upload(file_uploading_utils_1.saveImage(images[i]));
                    updateImg.push(img.data);
                }
            }
            return updateImg;
        });
    }
    createImageVariant(image) {
        return __awaiter(this, void 0, void 0, function* () {
            let updateImg = [];
            if (image.size > 0 && image.mimetype !== 'application/octet-stream' && image.mimetype !== 'application/json') {
                let img = yield this.imageService.upload(file_uploading_utils_1.saveImage(image));
                updateImg.push(img.data);
            }
            return updateImg;
        });
    }
    uploadImageProduct(product, images) {
        return __awaiter(this, void 0, void 0, function* () {
            let updateImg = [];
            for (let i = 0; i < images.length; i++) {
                if (images[i].size === 0 || images[i].mimetype === 'application/octet-stream') {
                    for (let i = 0; i < product.images.length; i++) {
                        this.imageService.delete(product.images[i].id);
                    }
                }
                else if (images[i].mimetype === 'application/json') {
                    updateImg.push(product.images[i]);
                }
                else {
                    let img = yield this.imageService.upload(file_uploading_utils_1.saveImage(images[i]));
                    updateImg.push(img.data);
                }
            }
            return updateImg;
        });
    }
    uploadImageVariant(variation, image) {
        return __awaiter(this, void 0, void 0, function* () {
            let updateImg = [];
            if (image.size === 0 || image.mimetype === 'application/octet-stream') {
                for (let i = 0; i < variation.images.length; i++) {
                    this.imageService.delete(variation.images[i].id);
                }
            }
            else if (image.mimetype === 'application/json') {
                let variationImage = yield this.productRepository.findOne({ barcode: variation.barcode }, { relations: ["images"] });
                updateImg = variationImage.images;
            }
            else {
                let img = yield this.imageService.upload(file_uploading_utils_1.saveImage(image));
                updateImg.push(img.data);
            }
            return updateImg;
        });
    }
    validateProduct(userId, product) {
        return __awaiter(this, void 0, void 0, function* () {
            if (product.quantity < 0) {
                return {
                    code: api_error_code_1.CodeEnum.QUANTITY_GREATER_ZERO
                };
            }
            if (product.price < 0) {
                return {
                    code: api_error_code_1.CodeEnum.PRICE_GREATER_ZERO
                };
            }
            if (product.sku) {
                if (yield this.checkSKU(userId, product.sku)) {
                    return {
                        code: api_error_code_1.CodeEnum.SKU_EXISTED
                    };
                }
            }
            return false;
        });
    }
    filterProduct(productIds, products) {
        return __awaiter(this, void 0, void 0, function* () {
            let productFilter = [];
            for (let i = 0; i < products.length; i++) {
                productIds.forEach((productId) => __awaiter(this, void 0, void 0, function* () {
                    if (productId === products[i].id) {
                        productFilter = yield this.checkProductDuplicate(productFilter, products[i]);
                    }
                    else {
                        products[i].children.forEach((variant) => __awaiter(this, void 0, void 0, function* () {
                            if (variant.id === productId) {
                                productFilter = yield this.checkVariantDuplicate(productFilter, products[i], variant);
                            }
                        }));
                    }
                }));
            }
            return productFilter;
        });
    }
    checkProductDuplicate(productFilter, product) {
        return __awaiter(this, void 0, void 0, function* () {
            let duplicateProduct = -1;
            productFilter.forEach((filter, idx) => {
                if (product.id === filter.id) {
                    duplicateProduct = idx;
                }
            });
            if (duplicateProduct == -1) {
                productFilter.push(product);
            }
            else {
                productFilter[duplicateProduct] = product;
            }
            return productFilter;
        });
    }
    checkVariantDuplicate(productFilter, product, variation) {
        return __awaiter(this, void 0, void 0, function* () {
            let duplicateProduct = -1;
            productFilter.forEach((product, idx) => {
                if (product.id === product.id) {
                    duplicateProduct = idx;
                }
            });
            if (duplicateProduct == -1) {
                let data = Object.assign({}, product);
                data.children = [variation];
                productFilter.push(data);
            }
            else {
                productFilter[duplicateProduct].children.forEach(variant => {
                    if (variation.id !== variant.id) {
                        productFilter[duplicateProduct].children.push(variation);
                    }
                });
            }
            return productFilter;
        });
    }
    deleteProductOptions(variations, product) {
        return __awaiter(this, void 0, void 0, function* () {
            return null;
        });
    }
    addTotalProductToCategories(categories) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!categories.length) {
                return;
            }
            let idsCategory = [];
            categories.forEach(category => {
                idsCategory.push(category.id);
            });
            const table = 'product';
            let getAllProduct = yield this.productRepository.createQueryBuilder(table)
                .select(`${table}.categoryId as category_id`)
                .where(new typeorm_2.Brackets(qb => {
                if (Array.isArray(idsCategory) && idsCategory.length) {
                    qb.andWhere(`${table}.categoryId IN(:...idsCategory)`, { idsCategory });
                }
            }))
                .getRawMany();
            categories.forEach(category => {
                getAllProduct.forEach(product => {
                    if (product.category_id === category.id) {
                        category.numberOfProduct++;
                    }
                });
            });
        });
    }
};
ProductService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(products_entity_1.ProductEntity, database_1.DB_CONNECTION_DEFAULT)),
    __param(1, bull_2.InjectQueue('products')),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof bull_1.Queue !== "undefined" && bull_1.Queue) === "function" ? _b : Object, typeof (_c = typeof categories_1.CategoryService !== "undefined" && categories_1.CategoryService) === "function" ? _c : Object, typeof (_d = typeof images_1.ImageService !== "undefined" && images_1.ImageService) === "function" ? _d : Object, typeof (_e = typeof barcodes_service_1.BarcodeService !== "undefined" && barcodes_service_1.BarcodeService) === "function" ? _e : Object, typeof (_f = typeof user_1.UserService !== "undefined" && user_1.UserService) === "function" ? _f : Object])
], ProductService);
exports.ProductService = ProductService;


/***/ }),
/* 337 */
/***/ (function(module, exports) {

module.exports = require("bull");

/***/ }),
/* 338 */
/***/ (function(module, exports) {

module.exports = require("@nestjs/bull");

/***/ }),
/* 339 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const excelToJson = __webpack_require__(340);
exports.readFileExcell = (path) => {
    let data = excelToJson({
        sourceFile: path,
        columnToKey: {
            '*': '{{columnHeader}}'
        }
    }).Sheet1;
    data.shift();
    var createProduct = [];
    for (let i = 0; i < data.length; i++) {
        if (data[i].variation ? data[i].variation === "TRUE" : false) {
            data[i].variations = [];
            let keys = Object.keys(data[i]);
            let type = 'variation_name_';
            let value = 'variation_value_';
            let quantity = 'variation_quantity_';
            let count = 1;
            let checkConvertVariant = true;
            while (checkConvertVariant) {
                let obj = keys.filter((el) => {
                    return el === type + count || el === value + count || el === quantity + count;
                });
                if (obj.length == 0) {
                    createProduct = data;
                    checkConvertVariant = false;
                }
                let element = {};
                for (let k in obj) {
                    element[exports.changeNameOfKey(obj[k])] = data[i][obj[k]];
                    delete data[i][obj[k]];
                }
                data[i].variations[count - 1] = element;
                count += 1;
            }
        }
    }
    createProduct.forEach(item => {
        if (item.variations) {
            let options = exports.createProductOptions(item.variations);
            item.option = JSON.stringify(options);
            item.variations = exports.createVariationOptions(item.variations, options);
        }
    });
    return createProduct;
};
exports.changeNameOfKey = (rawKey) => {
    let key;
    if (rawKey.includes('name')) {
        key = 'name';
    }
    else if (rawKey.includes('value')) {
        key = 'variationName';
    }
    else
        key = 'quantity';
    return key;
};
exports.createProductOptions = (variations) => {
    var productOptions = [];
    const variationName = variations[0].name.split(",");
    for (let i = 0; i < variationName.length; i++) {
        let tmp = [];
        let options = {
            id: generateNextOptionId(),
            name: variationName[i],
            value: []
        };
        for (let j = 0; j < variations.length; j++) {
            if (variations[j].variationName) {
                let value = variations[j].variationName.split(",");
                if (!tmp.includes(value[i])) {
                    options.value.push({
                        id: generateNextOptionId(),
                        value: value[i]
                    });
                    tmp.push(value[i]);
                }
            }
        }
        productOptions.push(options);
    }
    return productOptions;
};
exports.createVariationOptions = (variations, options) => {
    for (let i = 0; i < variations.length; i++) {
        if (variations[i].name || variations[i].variationName) {
            variations[i].option = [];
            const names = variations[i].name.split(",");
            const value = variations[i].variationName.split(",");
            names.forEach((name, idx) => {
                let optionOfNames;
                optionOfNames = options.filter(option => name === option.name);
                optionOfNames[0].value.forEach(optionOfName => {
                    if (value[idx] === optionOfName.value) {
                        variations[i].option.push(optionOfName.id);
                    }
                });
            });
        }
        else {
            variations.splice(i, 1);
        }
    }
    return variations;
};
function generateNextOptionId() {
    return Math.random().toString(36).substring(2, 15);
}
exports.generateNextOptionId = generateNextOptionId;


/***/ }),
/* 340 */
/***/ (function(module, exports) {

module.exports = require("convert-excel-to-json");

/***/ }),
/* 341 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.findDifferentArray = (preVariations) => {
    return function (current) {
        return preVariations.filter(function (newVariations) {
            return newVariations.barcode === current.barcode;
        }).length == 0;
    };
};
exports.findMatchArray = (preVariations) => {
    return function (current) {
        return preVariations.some(function (newVariations) {
            return newVariations.barcode === current.barcode;
        });
    };
};


/***/ }),
/* 342 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const dto_1 = __webpack_require__(343);
const barcodes_entity_1 = __webpack_require__(345);
const shared_1 = __webpack_require__(25);
const api_error_code_1 = __webpack_require__(94);
let BarcodeService = class BarcodeService extends core_1.CrudService {
    constructor(barcodeRepository) {
        super(barcodeRepository);
        this.barcodeRepository = barcodeRepository;
    }
    createBarcode(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcode = yield this.barcodeRepository.save(data);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getBarcode(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcode = yield this.barcodeRepository.findOne({ id: id });
                if (!barcode) {
                    return {
                        code: api_error_code_1.CodeEnum.BARCODE_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findBarcode(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcode = yield this.barcodeRepository.findOne(filter);
                if (!barcode) {
                    return {
                        code: api_error_code_1.CodeEnum.BARCODE_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateBarcode(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let barcodeExisted = yield this.barcodeRepository.findOne({ id: data.id });
                if (!barcodeExisted) {
                    return {
                        code: api_error_code_1.CodeEnum.BARCODE_NOT_EXISTED
                    };
                }
                let updateBarcode = yield this.barcodeRepository.update({ id: data.id }, data);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformBarcodeDto, updateBarcode, { excludeExtraneousValues: false }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
BarcodeService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(barcodes_entity_1.BarcodeEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object])
], BarcodeService);
exports.BarcodeService = BarcodeService;


/***/ }),
/* 343 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(344));


/***/ }),
/* 344 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TransformBarcodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformBarcodeDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformBarcodeDto.prototype, "countryCode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformBarcodeDto.prototype, "lastedBarcode", void 0);
exports.TransformBarcodeDto = TransformBarcodeDto;
class CreateBarcodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateBarcodeDto.prototype, "countryCode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateBarcodeDto.prototype, "lastedBarcode", void 0);
exports.CreateBarcodeDto = CreateBarcodeDto;
class UpdateBarcodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateBarcodeDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateBarcodeDto.prototype, "countryCode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateBarcodeDto.prototype, "lastedBarcode", void 0);
exports.UpdateBarcodeDto = UpdateBarcodeDto;
class FindBarcodeDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], FindBarcodeDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], FindBarcodeDto.prototype, "countryCode", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], FindBarcodeDto.prototype, "lastedBarcode", void 0);
exports.FindBarcodeDto = FindBarcodeDto;
class SearchBarcodeDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['countryCode', 'lastedBarcode']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchBarcodeDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Cong Hoa" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchBarcodeDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchBarcodeDto.prototype, "order", void 0);
exports.SearchBarcodeDto = SearchBarcodeDto;


/***/ }),
/* 345 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
let BarcodeEntity = class BarcodeEntity extends core_1.AbstractEntity {
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BarcodeEntity.prototype, "countryCode", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BarcodeEntity.prototype, "lastedBarcode", void 0);
BarcodeEntity = __decorate([
    typeorm_1.Entity('barcode')
], BarcodeEntity);
exports.BarcodeEntity = BarcodeEntity;


/***/ }),
/* 346 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const xl = __webpack_require__(347);
exports.exportProductsToExcel = (products) => {
    const wb = new xl.Workbook();
    const ws = wb.addWorksheet('Sheet1');
    const headingColumnNames = exports.getHeadingProduct(products);
    const data = exports.convertProduct(headingColumnNames, products);
    let headingColumnIndex = 1;
    headingColumnNames.forEach(heading => {
        if (typeof heading === 'string') {
            ws.cell(1, headingColumnIndex++)
                .string(heading);
        }
        else if (typeof heading === 'number') {
            ws.cell(1, headingColumnIndex++)
                .number(heading);
        }
    });
    let rowIndex = 2;
    data.forEach(record => {
        let columnIndex = 1;
        Object.keys(record).forEach(columnName => {
            if (typeof record[columnName] === 'string') {
                ws.cell(rowIndex, columnIndex++)
                    .string(record[columnName]);
            }
            else if (typeof record[columnName] === 'number') {
                ws.cell(rowIndex, columnIndex++)
                    .number(record[columnName]);
            }
            else {
                ws.cell(rowIndex, columnIndex++)
                    .string("");
            }
        });
        rowIndex++;
    });
    return wb.writeToBuffer();
};
exports.getHeadingProduct = (products) => {
    let heading = ['no', 'name', 'category', 'sku', 'quantity', 'price', 'variation'];
    let max = 0;
    products.forEach(product => {
        max = product.children.length > max ? product.children.length : max;
    });
    for (let i = 0; i < max; i++) {
        heading.push(`variation_name_${i + 1}`, `variation_value_${i + 1}`, `variation_quantity_${i + 1}`);
    }
    return heading;
};
exports.convertProduct = (headers, products) => {
    let productsConverted = [];
    for (let i = 0; i < products.length; i++) {
        let product = {};
        headers.forEach(header => {
            switch (header) {
                case "no":
                    product[header] = i + 1;
                    break;
                case "name":
                    product[header] = products[i].name;
                    break;
                case "category":
                    product[header] = products[i].category.key;
                    break;
                case "sku":
                    product[header] = products[i].sku;
                    break;
                case "quantity":
                    product[header] = parseInt(products[i].quantity);
                    break;
                case "price":
                    product[header] = parseInt(products[i].price);
                    break;
                case "variation":
                    product[header] = products[i].children.length > 0 ? 'TRUE' : 'FALSE';
                    break;
                default:
                    product[header] = undefined;
            }
        });
        if (products[i].children.length > 0) {
            let productQuantity = 0;
            products[i].children.forEach((variant, idx) => {
                product[`variation_name_${idx + 1}`] = null;
                product[`variation_value_${idx + 1}`] = variant.variationName;
                product[`variation_quantity_${idx + 1}`] = parseInt(variant.quantity);
                productQuantity += parseInt(variant.quantity);
            });
            product['quantity'] = productQuantity;
        }
        productsConverted.push(product);
    }
    return productsConverted;
};


/***/ }),
/* 347 */
/***/ (function(module, exports) {

module.exports = require("excel4node");

/***/ }),
/* 348 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var locations_module_1 = __webpack_require__(349);
exports.LocationModule = locations_module_1.LocationModule;
var locations_service_1 = __webpack_require__(353);
exports.LocationService = locations_service_1.LocationService;
var locations_entity_1 = __webpack_require__(199);
exports.LocationEntity = locations_entity_1.LocationEntity;
var locations_controller_1 = __webpack_require__(350);
exports.LocationController = locations_controller_1.LocationController;


/***/ }),
/* 349 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const locations_entity_1 = __webpack_require__(199);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const locations_controller_1 = __webpack_require__(350);
const locations_service_1 = __webpack_require__(353);
const PROVIDERS = [
    locations_service_1.LocationService
];
const ENTITY = [
    locations_entity_1.LocationEntity
];
let LocationModule = class LocationModule {
};
LocationModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        controllers: [locations_controller_1.LocationController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], LocationModule);
exports.LocationModule = LocationModule;


/***/ }),
/* 350 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(351);
const locations_service_1 = __webpack_require__(353);
const composeSecurity_guard_1 = __webpack_require__(236);
let LocationController = class LocationController extends core_1.CrudController {
    constructor(locationService) {
        super(locationService);
        this.locationService = locationService;
    }
    createLocation(req, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const location = yield this.locationService.createLocation(userId, data);
                this.respondWithErrorsOrSuccess(location);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getLocation(req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const location = yield this.locationService.getLocation(userId, id);
                this.respondWithErrorsOrSuccess(location);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllLocations(req, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const locations = yield this.locationService.getAllLocations(userId, filter);
                this.respondWithErrorsOrSuccess(locations);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    deleteLocation(req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const location = yield this.locationService.deleteLocation(userId, id);
                this.respondWithErrorsOrSuccess(location);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateLocation(req, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const location = yield this.locationService.updateLocation(userId, id, data);
                this.respondWithErrorsOrSuccess(location);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post(),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_a = typeof dto_1.CreateLocationDto !== "undefined" && dto_1.CreateLocationDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], LocationController.prototype, "createLocation", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], LocationController.prototype, "getLocation", null);
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_d = typeof dto_1.SearchLocationDto !== "undefined" && dto_1.SearchLocationDto) === "function" ? _d : Object]),
    __metadata("design:returntype", typeof (_e = typeof Promise !== "undefined" && Promise) === "function" ? _e : Object)
], LocationController.prototype, "getAllLocations", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], LocationController.prototype, "deleteLocation", null);
__decorate([
    common_1.Put(':id'),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, typeof (_g = typeof dto_1.UpdateLocationDto !== "undefined" && dto_1.UpdateLocationDto) === "function" ? _g : Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], LocationController.prototype, "updateLocation", null);
LocationController = __decorate([
    swagger_1.ApiTags('Locations'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Location',
        createDto: dto_1.CreateLocationDto,
        updateDto: dto_1.UpdateLocationDto,
        transformDto: dto_1.TransformLocationDto,
        searchDto: dto_1.SearchLocationDto
    }),
    __metadata("design:paramtypes", [typeof (_j = typeof locations_service_1.LocationService !== "undefined" && locations_service_1.LocationService) === "function" ? _j : Object])
], LocationController);
exports.LocationController = LocationController;


/***/ }),
/* 351 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(352));


/***/ }),
/* 352 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TransformLocationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformLocationDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformLocationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformLocationDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformLocationDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformLocationDto.prototype, "backroom", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformLocationDto.prototype, "companyId", void 0);
exports.TransformLocationDto = TransformLocationDto;
class CreateLocationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Beesight Cong Hoa' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateLocationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Store' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateLocationDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '18E Cong Hoa' }),
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateLocationDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 1 }),
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateLocationDto.prototype, "backroom", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateLocationDto.prototype, "companyId", void 0);
exports.CreateLocationDto = CreateLocationDto;
class UpdateLocationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateLocationDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Beesight Cong Hoa' }),
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateLocationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Store' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateLocationDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '18E Cong Hoa' }),
    class_validator_1.IsString(),
    class_validator_1.MaxLength(128),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateLocationDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 1 }),
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateLocationDto.prototype, "backroom", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateLocationDto.prototype, "companyId", void 0);
exports.UpdateLocationDto = UpdateLocationDto;
class SearchLocationDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['name', 'type', 'address', 'backroom']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchLocationDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Cong Hoa" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchLocationDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchLocationDto.prototype, "order", void 0);
exports.SearchLocationDto = SearchLocationDto;


/***/ }),
/* 353 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const locations_entity_1 = __webpack_require__(199);
const dto_1 = __webpack_require__(351);
const shared_1 = __webpack_require__(25);
const api_error_code_1 = __webpack_require__(94);
const typeorm_2 = __webpack_require__(120);
const user_service_1 = __webpack_require__(211);
let LocationService = class LocationService extends core_1.CrudService {
    constructor(locationRepository, userService) {
        super(locationRepository);
        this.locationRepository = locationRepository;
        this.userService = userService;
    }
    createLocation(userId, data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (data.backroom && data.backroom < 0) {
                    return {
                        code: api_error_code_1.CodeEnum.BACKROOM_GREATER_ZERO,
                    };
                }
                data = this.removeMultipleSpace(data);
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                const checkLocation = yield this.checkValidLocation(data, userCompanyId);
                if (checkLocation === 'invalid') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_INVALID_NAME,
                    };
                }
                if (checkLocation === 'name') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_NAME_EXISTED,
                    };
                }
                if (checkLocation === 'address') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_ADDRESS_EXISTED,
                    };
                }
                data.companyId = userCompanyId;
                const location = yield this.locationRepository.save(data);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformLocationDto, location, {
                        excludeExtraneousValues: false,
                    }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getLocation(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const location = yield this.findExistLocation(userId, id);
                if (!location) {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_NOT_EXISTED
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformLocationDto, location, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getAllLocations(userId, filter) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                if (filter) {
                    if (filter.where) {
                        const value = filter.where.search || '';
                        if (typeof value === "number") {
                            filter.where = { backroom: value, companyId: userCompanyId };
                        }
                        else {
                            const queryName = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                            filter.where = [
                                { name: queryName, companyId: userCompanyId },
                                { type: queryName, companyId: userCompanyId },
                                { address: queryName, companyId: userCompanyId },
                            ];
                        }
                    }
                    else {
                        filter.where = { companyId: userCompanyId };
                    }
                }
                const locations = yield this.locationRepository.find(filter);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformLocationDto, locations, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteLocation(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!(yield this.findExistLocation(userId, id))) {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_NOT_EXISTED
                    };
                }
                yield this.locationRepository.delete({
                    id: id
                });
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformLocationDto, {})
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateLocation(userId, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!(yield this.findExistLocation(userId, id))) {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_NOT_EXISTED
                    };
                }
                if (data.backroom && data.backroom < 0) {
                    return {
                        code: api_error_code_1.CodeEnum.BACKROOM_GREATER_ZERO,
                    };
                }
                data = this.removeMultipleSpace(data);
                const checkLocation = yield this.checkValidLocation(data, id);
                if (checkLocation === 'invalid') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_INVALID_NAME,
                    };
                }
                if (checkLocation === 'name') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_NAME_EXISTED,
                    };
                }
                if (checkLocation === 'address') {
                    return {
                        code: api_error_code_1.CodeEnum.LOCATION_ADDRESS_EXISTED,
                    };
                }
                data.id = id;
                const updatedLocation = yield this.locationRepository.save(data);
                return {
                    data: shared_1.UtilsService.plainToClass(dto_1.TransformLocationDto, updatedLocation, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findExistLocation(userId, id) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const userExist = yield this.userService.findUserNoRelations(userId);
            const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
            const location = yield this.locationRepository.findOne({
                where: {
                    id: id,
                    companyId: userCompanyId
                }
            });
            return location;
        });
    }
    checkValidLocation(data, companyId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name, address } = data;
            if (!name.replace(/\s/g, '').length) {
                return 'invalid';
            }
            let locationsWithName, locationsWithAddress;
            if (id) {
                locationsWithName = yield this.locationRepository.createQueryBuilder("locations")
                    .where("LOWER(locations.name) = LOWER(:name) AND id != :id", { name, id })
                    .andWhere("locations.companyId = :companyId", { companyId })
                    .getCount();
                locationsWithAddress = yield this.locationRepository.createQueryBuilder("locations")
                    .where("LOWER(locations.address) = LOWER(:address) AND id != :id", { address, id })
                    .andWhere("locations.companyId = :companyId", { companyId })
                    .getCount();
            }
            else {
                locationsWithName = yield this.locationRepository.createQueryBuilder("locations")
                    .where("LOWER(locations.name) = LOWER(:name) AND locations.companyId = :companyId", { name, companyId })
                    .getCount();
                locationsWithAddress = yield this.locationRepository.createQueryBuilder("locations")
                    .where("LOWER(locations.address) = LOWER(:address) AND locations.companyId = :companyId", { address, companyId })
                    .getCount();
            }
            if (locationsWithName > 0)
                return 'name';
            if (address === '')
                return 'valid';
            if (locationsWithAddress > 0)
                return 'address';
            return 'valid';
        });
    }
    removeMultipleSpace(data) {
        if (data.address) {
            if (!data.address.replace(/\s/g, '').length) {
                data.address = data.address.replace(/\s/g, '');
            }
            else {
                data.address = data.address.trim().replace(/\s\s+/g, ' ');
            }
        }
        if (data.name) {
            data.name = data.name.trim().replace(/\s\s+/g, ' ');
        }
        return data;
    }
};
LocationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(locations_entity_1.LocationEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" ? _b : Object])
], LocationService);
exports.LocationService = LocationService;


/***/ }),
/* 354 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var variations_module_1 = __webpack_require__(355);
exports.VariationModule = variations_module_1.VariationModule;
var variations_service_1 = __webpack_require__(360);
exports.VariationService = variations_service_1.VariationService;
var variations_controller_1 = __webpack_require__(357);
exports.VariationController = variations_controller_1.VariationController;
var variations_entity_1 = __webpack_require__(356);
exports.VariationEntity = variations_entity_1.VariationEntity;


/***/ }),
/* 355 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const variations_entity_1 = __webpack_require__(356);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const variations_controller_1 = __webpack_require__(357);
const variations_service_1 = __webpack_require__(360);
const images_1 = __webpack_require__(190);
const categories_1 = __webpack_require__(327);
const locations_1 = __webpack_require__(348);
const twilio_1 = __webpack_require__(223);
const code_1 = __webpack_require__(214);
const core_1 = __webpack_require__(9);
const user_1 = __webpack_require__(256);
const PROVIDERS = [
    variations_service_1.VariationService,
    images_1.ImageService,
    categories_1.CategoryService,
    locations_1.LocationService,
    twilio_1.TwillioService,
    user_1.UserService,
    code_1.CodeService
];
const ENTITY = [
    variations_entity_1.VariationEntity,
    images_1.ImagesEntity,
    categories_1.CategoryEntity,
    locations_1.LocationEntity,
    core_1.UserEntity,
    code_1.CodeEntity
];
let VariationModule = class VariationModule {
};
VariationModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            common_1.forwardRef(() => images_1.ImagesEntity),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [variations_controller_1.VariationController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], VariationModule);
exports.VariationModule = VariationModule;


/***/ }),
/* 356 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
const images_1 = __webpack_require__(190);
const products_1 = __webpack_require__(322);
let VariationEntity = class VariationEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column('text', { nullable: true }),
    __metadata("design:type", String)
], VariationEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], VariationEntity.prototype, "sku", void 0);
__decorate([
    class_validator_1.IsNumber(),
    typeorm_2.Column(),
    __metadata("design:type", Number)
], VariationEntity.prototype, "quantity", void 0);
__decorate([
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], VariationEntity.prototype, "barcode", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", String)
], VariationEntity.prototype, "imageId", void 0);
__decorate([
    typeorm_2.ManyToOne(type => images_1.ImagesEntity, image => image.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_2.JoinTable({ name: "imageId" }),
    __metadata("design:type", typeof (_a = typeof images_1.ImagesEntity !== "undefined" && images_1.ImagesEntity) === "function" ? _a : Object)
], VariationEntity.prototype, "image", void 0);
__decorate([
    typeorm_2.Column({ nullable: true }),
    __metadata("design:type", String)
], VariationEntity.prototype, "productId", void 0);
__decorate([
    typeorm_2.ManyToOne(type => products_1.ProductEntity, product => product.id, { nullable: false, onDelete: 'SET NULL' }),
    typeorm_2.JoinTable({ name: "productId" }),
    __metadata("design:type", typeof (_b = typeof products_1.ProductEntity !== "undefined" && products_1.ProductEntity) === "function" ? _b : Object)
], VariationEntity.prototype, "product", void 0);
VariationEntity = __decorate([
    typeorm_1.Entity('variations')
], VariationEntity);
exports.VariationEntity = VariationEntity;


/***/ }),
/* 357 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const platform_express_1 = __webpack_require__(4);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(358);
const variations_service_1 = __webpack_require__(360);
const file_uploading_utils_1 = __webpack_require__(249);
const composeSecurity_guard_1 = __webpack_require__(236);
let VariationController = class VariationController extends core_1.CrudController {
    constructor(variationService) {
        super(variationService);
        this.variationService = variationService;
    }
    createVariation(productId, file, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.variationService.createVariation(productId, file, data);
        });
    }
    getProduct(productId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.variationService.getVariation(productId, id);
        });
    }
    getAllVariations(productId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.variationService.getAllVariations(productId);
        });
    }
};
__decorate([
    common_1.Post(),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', file_uploading_utils_1.fileInterceptor('server'))),
    __param(0, common_1.Param('productId')), __param(1, common_1.UploadedFile()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, typeof (_a = typeof dto_1.CreateVariationDto !== "undefined" && dto_1.CreateVariationDto) === "function" ? _a : Object]),
    __metadata("design:returntype", Promise)
], VariationController.prototype, "createVariation", null);
__decorate([
    common_1.Get(":id"),
    __param(0, common_1.Param("productId")), __param(1, common_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], VariationController.prototype, "getProduct", null);
__decorate([
    common_1.Post(":id"),
    __param(0, common_1.Param("productId")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], VariationController.prototype, "getAllVariations", null);
VariationController = __decorate([
    swagger_1.ApiTags('Variations'),
    common_1.Controller(':productId/variations'),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Variation',
        createDto: dto_1.CreateVariationDto,
        updateDto: dto_1.UpdateVariationDto,
        transformDto: dto_1.TransformVariationDto,
        searchDto: dto_1.SearchVariationDto
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof variations_service_1.VariationService !== "undefined" && variations_service_1.VariationService) === "function" ? _b : Object])
], VariationController);
exports.VariationController = VariationController;


/***/ }),
/* 358 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(359));


/***/ }),
/* 359 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const typeorm_1 = __webpack_require__(120);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const images_1 = __webpack_require__(190);
const products_1 = __webpack_require__(322);
class TransformVariationDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "id", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "name", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "sku", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TransformVariationDto.prototype, "quantity", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "barcode", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "imageId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => images_1.ImagesEntity, image => image.id),
    typeorm_1.JoinColumn({ name: "imageId" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof images_1.ImagesEntity !== "undefined" && images_1.ImagesEntity) === "function" ? _a : Object)
], TransformVariationDto.prototype, "image", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformVariationDto.prototype, "productId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => products_1.ProductEntity, product => product.id),
    typeorm_1.JoinColumn({ name: "productId" }),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof products_1.ProductEntity !== "undefined" && products_1.ProductEntity) === "function" ? _b : Object)
], TransformVariationDto.prototype, "product", void 0);
exports.TransformVariationDto = TransformVariationDto;
class CreateVariationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'SKU 345-091' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 40 }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateVariationDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "imageId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateVariationDto.prototype, "productId", void 0);
exports.CreateVariationDto = CreateVariationDto;
class UpdateVariationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'SKU 345-091' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "sku", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 40 }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateVariationDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "imageId", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "barcode", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'ABC-DEF-GHJ' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateVariationDto.prototype, "productId", void 0);
exports.UpdateVariationDto = UpdateVariationDto;
class SearchVariationDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['name', 'sku', 'quantity', 'barcode', 'productId']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformVariationDto, ['name', 'sku', 'quantity', 'barcode', 'productId'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchVariationDto.prototype, "order", void 0);
exports.SearchVariationDto = SearchVariationDto;


/***/ }),
/* 360 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const variations_entity_1 = __webpack_require__(356);
const api_error_code_1 = __webpack_require__(94);
const images_1 = __webpack_require__(190);
let VariationService = class VariationService extends core_1.CrudService {
    constructor(variationRepository, errorCodeService, imageService) {
        super(variationRepository);
        this.variationRepository = variationRepository;
        this.errorCodeService = errorCodeService;
        this.imageService = imageService;
    }
    createVariation(productId, file, variation) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (file) {
                    let createImage = yield this.imageService.upload(file);
                }
                variation.productId = productId;
                variation.sku = variation.sku ? variation.sku : variation.name.replace(/ /g, "-");
                let barcodeExist;
                if (!variation.barcode) {
                    barcodeExist = true;
                    while (barcodeExist) {
                        variation.barcode = `${process.env.BARCODE}${Math.floor(1000000000 + Math.random() * 9000000000)}`;
                        barcodeExist = (yield this.variationRepository.findOne({ barcode: variation.barcode })) === undefined ? false : true;
                    }
                }
                barcodeExist = yield this.variationRepository.findOne({ barcode: variation.barcode });
                if (barcodeExist) {
                    return this.errorCodeService.getErrorMessage(api_error_code_1.CodeEnum.BARCODE_EXISTED);
                }
                let createVariation = yield this.variationRepository.save(variation);
                if (!createVariation) {
                    return this.errorCodeService.getErrorMessage(api_error_code_1.CodeEnum.AUTH_PASSWORD_REQUIRED);
                }
                return createVariation;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getVariation(productId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getVariation = yield this.variationRepository.findOne({ id: id }, { relations: ["image", "product"] });
                if (!getVariation) {
                    return this.errorCodeService.getErrorMessage(api_error_code_1.CodeEnum.AUTH_PASSWORD_REQUIRED);
                }
                return getVariation;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getAllVariations(productId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let getAllVariations = yield this.variationRepository.find({ relations: ["image", "product"] });
                if (!getAllVariations) {
                    return this.errorCodeService.getErrorMessage(api_error_code_1.CodeEnum.AUTH_PASSWORD_REQUIRED);
                }
                return getAllVariations;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
VariationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(variations_entity_1.VariationEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof api_error_code_1.ApiErrorCodeService !== "undefined" && api_error_code_1.ApiErrorCodeService) === "function" ? _b : Object, typeof (_c = typeof images_1.ImageService !== "undefined" && images_1.ImageService) === "function" ? _c : Object])
], VariationService);
exports.VariationService = VariationService;


/***/ }),
/* 361 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var barcodes_module_1 = __webpack_require__(362);
exports.BarcodeModule = barcodes_module_1.BarcodeModule;
var barcodes_service_1 = __webpack_require__(342);
exports.BarcodeService = barcodes_service_1.BarcodeService;
var barcodes_entity_1 = __webpack_require__(345);
exports.BarcodeEntity = barcodes_entity_1.BarcodeEntity;


/***/ }),
/* 362 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const barcodes_entity_1 = __webpack_require__(345);
const barcodes_service_1 = __webpack_require__(342);
const PROVIDERS = [
    barcodes_service_1.BarcodeService
];
const ENTITY = [
    barcodes_entity_1.BarcodeEntity
];
let BarcodeModule = class BarcodeModule {
};
BarcodeModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT),
        ],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], BarcodeModule);
exports.BarcodeModule = BarcodeModule;


/***/ }),
/* 363 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const bull_1 = __webpack_require__(338);
const bull_2 = __webpack_require__(337);
const products_service_1 = __webpack_require__(336);
let ProductConsumer = class ProductConsumer {
    constructor(productService) {
        this.productService = productService;
    }
    createProduct(job) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const productData = job.data;
                yield this.productService.createProduct(productData.userId, productData.data, productData.images);
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
__decorate([
    bull_1.Process('createProduct'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof bull_2.Job !== "undefined" && bull_2.Job) === "function" ? _a : Object]),
    __metadata("design:returntype", Promise)
], ProductConsumer.prototype, "createProduct", null);
ProductConsumer = __decorate([
    bull_1.Processor('products'),
    __metadata("design:paramtypes", [typeof (_b = typeof products_service_1.ProductService !== "undefined" && products_service_1.ProductService) === "function" ? _b : Object])
], ProductConsumer);
exports.ProductConsumer = ProductConsumer;


/***/ }),
/* 364 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var suppliers_module_1 = __webpack_require__(365);
exports.SupplierModule = suppliers_module_1.SupplierModule;
var suppliers_service_1 = __webpack_require__(370);
exports.SupplierService = suppliers_service_1.SupplierService;
var suppliers_entity_1 = __webpack_require__(366);
exports.SupplierEntity = suppliers_entity_1.SupplierEntity;
var suppliers_controller_1 = __webpack_require__(367);
exports.SupplierController = suppliers_controller_1.SupplierController;


/***/ }),
/* 365 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const suppliers_entity_1 = __webpack_require__(366);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const suppliers_controller_1 = __webpack_require__(367);
const suppliers_service_1 = __webpack_require__(370);
const PROVIDERS = [
    suppliers_service_1.SupplierService
];
const ENTITY = [
    suppliers_entity_1.SupplierEntity
];
let SupplierModule = class SupplierModule {
};
SupplierModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        controllers: [suppliers_controller_1.SupplierController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], SupplierModule);
exports.SupplierModule = SupplierModule;


/***/ }),
/* 366 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
let SupplierEntity = class SupplierEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], SupplierEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], SupplierEntity.prototype, "currency", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], SupplierEntity.prototype, "country", void 0);
__decorate([
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], SupplierEntity.prototype, "phoneNumber", void 0);
__decorate([
    typeorm_2.Index({ unique: true }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    typeorm_2.Column(),
    __metadata("design:type", String)
], SupplierEntity.prototype, "email", void 0);
SupplierEntity = __decorate([
    typeorm_1.Entity('suppliers')
], SupplierEntity);
exports.SupplierEntity = SupplierEntity;


/***/ }),
/* 367 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(368);
const suppliers_service_1 = __webpack_require__(370);
const composeSecurity_guard_1 = __webpack_require__(236);
let SupplierController = class SupplierController extends core_1.CrudController {
    constructor(supplierService) {
        super(supplierService);
        this.supplierService = supplierService;
    }
};
SupplierController = __decorate([
    swagger_1.ApiTags('Suppliers'),
    common_1.Controller('suppliers'),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Supplier',
        createDto: dto_1.CreateSupplierDto,
        updateDto: dto_1.UpdateSupplierDto,
        transformDto: dto_1.TransformSupplierDto,
        searchDto: dto_1.SearchSupplierDto
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof suppliers_service_1.SupplierService !== "undefined" && suppliers_service_1.SupplierService) === "function" ? _a : Object])
], SupplierController);
exports.SupplierController = SupplierController;


/***/ }),
/* 368 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(369));


/***/ }),
/* 369 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TransformSupplierDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'John Jackie' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'USD' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'China' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+60111233492' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'jack@alibaba.com' }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformSupplierDto.prototype, "email", void 0);
exports.TransformSupplierDto = TransformSupplierDto;
class CreateSupplierDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'John Jackie' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'USD' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'China' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+60111233492' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'jack@alibaba.com' }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "email", void 0);
exports.CreateSupplierDto = CreateSupplierDto;
class UpdateSupplierDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'John Jackie' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Length(1, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'USD' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'China' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '+60111233492' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "phoneNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'jack@alibaba.com' }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.Length(6, 100),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "email", void 0);
exports.UpdateSupplierDto = UpdateSupplierDto;
class SearchSupplierDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['name', 'country', 'phoneNumber', 'email']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TransformSupplierDto, ['name', 'country', 'phoneNumber', 'email'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "order", void 0);
exports.SearchSupplierDto = SearchSupplierDto;


/***/ }),
/* 370 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const suppliers_entity_1 = __webpack_require__(366);
let SupplierService = class SupplierService extends core_1.CrudService {
    constructor(supplierRepository) {
        super(supplierRepository);
        this.supplierRepository = supplierRepository;
    }
};
SupplierService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(suppliers_entity_1.SupplierEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object])
], SupplierService);
exports.SupplierService = SupplierService;


/***/ }),
/* 371 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var stocks_module_1 = __webpack_require__(372);
exports.StockModule = stocks_module_1.StockModule;
var stocks_entity_1 = __webpack_require__(373);
exports.StockEntity = stocks_entity_1.StockEntity;
var stocks_service_1 = __webpack_require__(374);
exports.StockService = stocks_service_1.StockService;
var stocks_controller_1 = __webpack_require__(375);
exports.StockController = stocks_controller_1.StockController;


/***/ }),
/* 372 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const stocks_entity_1 = __webpack_require__(373);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const stocks_service_1 = __webpack_require__(374);
const stocks_controller_1 = __webpack_require__(375);
const PROVIDERS = [
    stocks_service_1.StockService
];
const ENTITY = [
    stocks_entity_1.StockEntity
];
let StockModule = class StockModule {
};
StockModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        controllers: [stocks_controller_1.StockController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], StockModule);
exports.StockModule = StockModule;


/***/ }),
/* 373 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = __webpack_require__(120);
const core_1 = __webpack_require__(9);
const class_validator_1 = __webpack_require__(17);
const typeorm_2 = __webpack_require__(120);
let StockEntity = class StockEntity extends core_1.AbstractEntity {
};
__decorate([
    class_validator_1.IsNotEmpty(),
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], StockEntity.prototype, "date", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], StockEntity.prototype, "status", void 0);
__decorate([
    class_validator_1.IsString(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], StockEntity.prototype, "location", void 0);
__decorate([
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", String)
], StockEntity.prototype, "category", void 0);
__decorate([
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", Number)
], StockEntity.prototype, "qtyDifference", void 0);
__decorate([
    typeorm_2.Index(),
    typeorm_2.Column(),
    __metadata("design:type", Number)
], StockEntity.prototype, "costDifference", void 0);
StockEntity = __decorate([
    typeorm_1.Entity('stocks')
], StockEntity);
exports.StockEntity = StockEntity;


/***/ }),
/* 374 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const core_1 = __webpack_require__(9);
const database_1 = __webpack_require__(130);
const stocks_entity_1 = __webpack_require__(373);
let StockService = class StockService extends core_1.CrudService {
    constructor(stockRepository) {
        super(stockRepository);
        this.stockRepository = stockRepository;
    }
};
StockService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(stocks_entity_1.StockEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object])
], StockService);
exports.StockService = StockService;


/***/ }),
/* 375 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const dto_1 = __webpack_require__(376);
const stocks_service_1 = __webpack_require__(374);
const composeSecurity_guard_1 = __webpack_require__(236);
let StockController = class StockController extends core_1.CrudController {
    constructor(stockService) {
        super(stockService);
        this.stockService = stockService;
    }
};
StockController = __decorate([
    swagger_1.ApiTags('Stocks'),
    common_1.Controller('stocks'),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Stock',
        createDto: dto_1.CreateSupplierDto,
        updateDto: dto_1.UpdateSupplierDto,
        transformDto: dto_1.TranformStockDto,
        searchDto: dto_1.SearchSupplierDto
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof stocks_service_1.StockService !== "undefined" && stocks_service_1.StockService) === "function" ? _a : Object])
], StockController);
exports.StockController = StockController;


/***/ }),
/* 376 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(377));


/***/ }),
/* 377 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
class TranformStockDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TranformStockDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '05/07/2020 22:34' }),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], TranformStockDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'In progress' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TranformStockDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Store A' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TranformStockDto.prototype, "location", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'All categories' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TranformStockDto.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '-10' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TranformStockDto.prototype, "qtyDifference", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1400000' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], TranformStockDto.prototype, "costDifference", void 0);
exports.TranformStockDto = TranformStockDto;
class CreateSupplierDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: '05/07/2020 22:34' }),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], CreateSupplierDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'In progress' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Store A' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "location", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'All categories' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateSupplierDto.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '-10' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateSupplierDto.prototype, "qtyDifference", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1400000' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], CreateSupplierDto.prototype, "costDifference", void 0);
exports.CreateSupplierDto = CreateSupplierDto;
class UpdateSupplierDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' }),
    class_validator_1.IsUUID(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '05/07/2020 22:34' }),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", typeof (_c = typeof Date !== "undefined" && Date) === "function" ? _c : Object)
], UpdateSupplierDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'In progress' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Store A' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "location", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'All categories' }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateSupplierDto.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '-10' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateSupplierDto.prototype, "qtyDifference", void 0);
__decorate([
    swagger_1.ApiProperty({ example: '1400000' }),
    class_transformer_1.Expose(),
    __metadata("design:type", Number)
], UpdateSupplierDto.prototype, "costDifference", void 0);
exports.UpdateSupplierDto = UpdateSupplierDto;
class SearchSupplierDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['date', 'status']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        type: swagger_1.PickType(TranformStockDto, ['date', 'status'])
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            date: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchSupplierDto.prototype, "order", void 0);
exports.SearchSupplierDto = SearchSupplierDto;


/***/ }),
/* 378 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const options_service_1 = __webpack_require__(379);
const options_controller_1 = __webpack_require__(383);
const options_entity_1 = __webpack_require__(202);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const products_module_1 = __webpack_require__(323);
const optionValues_module_1 = __webpack_require__(384);
const PROVIDERS = [
    options_service_1.OptionService
];
const ENTITY = [
    options_entity_1.OptionEntity
];
let OptionModule = class OptionModule {
};
OptionModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            common_1.forwardRef(() => products_module_1.ProductModule),
            common_1.forwardRef(() => optionValues_module_1.OptionValueModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        providers: [...PROVIDERS],
        controllers: [options_controller_1.OptionController],
        exports: [...PROVIDERS]
    })
], OptionModule);
exports.OptionModule = OptionModule;


/***/ }),
/* 379 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const database_1 = __webpack_require__(130);
const core_1 = __webpack_require__(9);
const options_entity_1 = __webpack_require__(202);
const options_dto_1 = __webpack_require__(380);
const shared_1 = __webpack_require__(25);
const api_error_code_1 = __webpack_require__(94);
const typeorm_2 = __webpack_require__(120);
const user_service_1 = __webpack_require__(211);
const products_service_1 = __webpack_require__(336);
const optionValues_service_1 = __webpack_require__(382);
let OptionService = class OptionService extends core_1.CrudService {
    constructor(optionRepository, userService, optionValueService, productService) {
        super(optionRepository);
        this.optionRepository = optionRepository;
        this.userService = userService;
        this.optionValueService = optionValueService;
        this.productService = productService;
    }
    createOption(userId, data) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                data.companyId = userCompanyId;
                const checkOptionName = yield this.optionRepository.createQueryBuilder('options')
                    .leftJoinAndSelect(`options.optionValues`, 'optionValues')
                    .where('options.companyId = :companyId AND LOWER(options.name) = LOWER(:name)', { companyId: data.companyId, name: data.name })
                    .getOne();
                if (checkOptionName) {
                    const values = [];
                    (_b = checkOptionName === null || checkOptionName === void 0 ? void 0 : checkOptionName.optionValues) === null || _b === void 0 ? void 0 : _b.forEach(value => values.push(value.value));
                    values.forEach(value => {
                        data.values = data.values.filter(val => value.toLowerCase().trim() !== val.value.toLowerCase().trim());
                    });
                    data.id = checkOptionName.id;
                }
                const option = yield this.optionRepository.save(data);
                data.values.forEach((optionValue) => __awaiter(this, void 0, void 0, function* () {
                    yield this.optionValueService.createOptionValue({ value: optionValue.value, optionId: option.id });
                }));
                return {
                    data: shared_1.UtilsService.plainToClass(options_dto_1.TransformOptionDto, option, {
                        excludeExtraneousValues: false,
                    }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getAllOptions(userId, filter) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                if (filter) {
                    if (filter.where) {
                        const value = filter.where.search || '';
                        const queryName = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                        filter.where =
                            {
                                name: queryName,
                                companyId: userCompanyId
                            };
                    }
                    else {
                        filter.where = { companyId: userCompanyId };
                    }
                }
                filter.relations = ['optionValues'];
                const options = yield this.optionRepository.find(filter);
                const results = yield Promise.all(options.map((option) => __awaiter(this, void 0, void 0, function* () {
                    const optionValues = option.optionValues;
                    const arrayOfValues = [];
                    optionValues.forEach(optionValue => {
                        arrayOfValues.push(optionValue.value);
                    });
                    return {
                        id: option.id,
                        name: option.name,
                        optionValues: arrayOfValues,
                        totalProducts: yield this.productService.countProductWithOptionId(userCompanyId, option.id),
                    };
                })));
                return {
                    data: shared_1.UtilsService.plainToClass(options_dto_1.TransformOptionDto, results, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getOption(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const option = yield this.findExistOption(userId, id);
                if (!option) {
                    return {
                        code: api_error_code_1.CodeEnum.OPTION_NOT_EXISTED,
                    };
                }
                return {
                    data: shared_1.UtilsService.plainToClass(options_dto_1.TransformOptionDto, option, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    getCountVariation(userId, value) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                const count = yield this.productService.getCountVariation(userCompanyId, value);
                const result = {
                    value: value,
                    variations: count
                };
                return {
                    data: result
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    countVariation(userId, option) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userExist = yield this.userService.findUserNoRelations(userId);
                const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
                const count = yield this.productService.countVariation(userCompanyId, option.name, option.value);
                const result = Object.assign(Object.assign({}, option), { variations: count });
                return {
                    data: result
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    deleteOption(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!(yield this.findExistOption(userId, id))) {
                    return {
                        code: api_error_code_1.CodeEnum.OPTION_NOT_EXISTED,
                    };
                }
                yield this.optionRepository.delete({
                    id: id
                });
                return {
                    data: shared_1.UtilsService.plainToClass(options_dto_1.TransformOptionDto, {})
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateOption(userId, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                data.id = id;
                if (!(yield this.findExistOption(userId, id))) {
                    return {
                        code: api_error_code_1.CodeEnum.OPTION_NOT_EXISTED,
                    };
                }
                data.values.forEach((optionValue) => __awaiter(this, void 0, void 0, function* () {
                    yield this.optionValueService.updateOptionValue(optionValue.id, { id: optionValue.id, value: optionValue.value, optionId: id });
                }));
                const updatedOption = yield this.optionRepository.save(data);
                return {
                    data: shared_1.UtilsService.plainToClass(options_dto_1.TransformOptionDto, updatedOption, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    findExistOption(userId, id) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const userExist = yield this.userService.findUserNoRelations(userId);
            const userCompanyId = (_a = userExist === null || userExist === void 0 ? void 0 : userExist.data) === null || _a === void 0 ? void 0 : _a.companyId;
            const option = yield this.optionRepository.findOne({
                id: id,
                companyId: userCompanyId
            }, {
                relations: ['optionValues']
            });
            return option;
        });
    }
};
OptionService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(options_entity_1.OptionEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object, typeof (_b = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" ? _b : Object, typeof (_c = typeof optionValues_service_1.OptionValueService !== "undefined" && optionValues_service_1.OptionValueService) === "function" ? _c : Object, typeof (_d = typeof products_service_1.ProductService !== "undefined" && products_service_1.ProductService) === "function" ? _d : Object])
], OptionService);
exports.OptionService = OptionService;


/***/ }),
/* 380 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = __webpack_require__(17);
const class_transformer_1 = __webpack_require__(16);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const optionValues_dto_1 = __webpack_require__(381);
class TransformOptionDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ type: optionValues_dto_1.IOptionValueDto, isArray: true }),
    class_transformer_1.Type(() => optionValues_dto_1.IOptionValueDto),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], TransformOptionDto.prototype, "values", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionDto.prototype, "companyId", void 0);
exports.TransformOptionDto = TransformOptionDto;
class CreateOptionDto extends core_1.AbstractDto {
}
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateOptionDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Color' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateOptionDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ type: optionValues_dto_1.IOptionValueDto, isArray: true }),
    class_transformer_1.Type(() => optionValues_dto_1.IOptionValueDto),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], CreateOptionDto.prototype, "values", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateOptionDto.prototype, "companyId", void 0);
exports.CreateOptionDto = CreateOptionDto;
class UpdateOptionDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' }),
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Color' }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ type: optionValues_dto_1.UpdateOptionValueDto, isArray: true }),
    class_transformer_1.Type(() => optionValues_dto_1.UpdateOptionValueDto),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], UpdateOptionDto.prototype, "values", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionDto.prototype, "companyId", void 0);
exports.UpdateOptionDto = UpdateOptionDto;
class CountVariationDto extends core_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty({ example: 'Color' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CountVariationDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Blue' }),
    class_validator_1.IsString(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CountVariationDto.prototype, "value", void 0);
exports.CountVariationDto = CountVariationDto;
class SearchOptionDto extends core_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'name']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Color" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            name: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: ['optionValues']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionDto.prototype, "relations", void 0);
exports.SearchOptionDto = SearchOptionDto;


/***/ }),
/* 381 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const class_transformer_1 = __webpack_require__(16);
const abstract_dto_1 = __webpack_require__(93);
const pagination_params_1 = __webpack_require__(15);
class TransformOptionValueDto extends abstract_dto_1.AbstractDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionValueDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionValueDto.prototype, "value", void 0);
__decorate([
    swagger_1.ApiProperty(),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], TransformOptionValueDto.prototype, "optionId", void 0);
exports.TransformOptionValueDto = TransformOptionValueDto;
class IOptionValueDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: 'Blue'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], IOptionValueDto.prototype, "value", void 0);
exports.IOptionValueDto = IOptionValueDto;
class CreateOptionValueDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: 'Blue'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateOptionValueDto.prototype, "value", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], CreateOptionValueDto.prototype, "optionId", void 0);
exports.CreateOptionValueDto = CreateOptionValueDto;
class UpdateOptionValueDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: '3c622136-3afd-4753-b2b1-17838e4ce3b6'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionValueDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: 'Blue'
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionValueDto.prototype, "value", void 0);
__decorate([
    class_transformer_1.Expose(),
    __metadata("design:type", String)
], UpdateOptionValueDto.prototype, "optionId", void 0);
exports.UpdateOptionValueDto = UpdateOptionValueDto;
class SearchOptionValueDto extends pagination_params_1.PaginationParams {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'value', 'optionId']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionValueDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Blue" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionValueDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            value: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchOptionValueDto.prototype, "order", void 0);
exports.SearchOptionValueDto = SearchOptionValueDto;


/***/ }),
/* 382 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const optionValues_entity_1 = __webpack_require__(203);
const database_constants_1 = __webpack_require__(132);
const optionValues_dto_1 = __webpack_require__(381);
const core_1 = __webpack_require__(9);
const shared_1 = __webpack_require__(25);
let OptionValueService = class OptionValueService extends core_1.CrudService {
    constructor(optionValueRepository) {
        super(optionValueRepository);
        this.optionValueRepository = optionValueRepository;
    }
    createOptionValue(value) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const optionValue = yield this.optionValueRepository.save(value);
                return {
                    data: shared_1.UtilsService.plainToClass(optionValues_dto_1.TransformOptionValueDto, optionValue, {
                        excludeExtraneousValues: false,
                    }),
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
    updateOptionValue(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                data.id = id;
                const optionValue = yield this.optionValueRepository.findOne({
                    id: id,
                    optionId: data.optionId
                });
                let updatedOptionValue;
                if (optionValue) {
                    updatedOptionValue = yield this.optionValueRepository.save(data);
                }
                return {
                    data: shared_1.UtilsService.plainToClass(optionValues_dto_1.TransformOptionValueDto, updatedOptionValue, {
                        excludeExtraneousValues: false
                    })
                };
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
OptionValueService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(optionValues_entity_1.OptionValueEntity, database_constants_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.AbstractBaseRepository !== "undefined" && core_1.AbstractBaseRepository) === "function" ? _a : Object])
], OptionValueService);
exports.OptionValueService = OptionValueService;


/***/ }),
/* 383 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const core_1 = __webpack_require__(9);
const options_dto_1 = __webpack_require__(380);
const options_service_1 = __webpack_require__(379);
const composeSecurity_guard_1 = __webpack_require__(236);
let OptionController = class OptionController extends core_1.CrudController {
    constructor(optionService) {
        super(optionService);
        this.optionService = optionService;
    }
    createOption(req, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const option = yield this.optionService.createOption(userId, data);
                this.respondWithErrorsOrSuccess(option);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getOption(req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const option = yield this.optionService.getOption(userId, id);
                this.respondWithErrorsOrSuccess(option);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getCountVariation(req, value) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const result = yield this.optionService.getCountVariation(userId, value);
                return result;
                this.respondWithErrorsOrSuccess(result);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    countVariation(req, option) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const result = yield this.optionService.countVariation(userId, option);
                return result;
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    getAllOptions(req, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const options = yield this.optionService.getAllOptions(userId, filter);
                this.respondWithErrorsOrSuccess(options);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    deleteOption(req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const option = yield this.optionService.deleteOption(userId, id);
                this.respondWithErrorsOrSuccess(option);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
    updateOption(req, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userId = req.user.id;
                const errors = yield this.validateWith(data);
                if (errors.length > 0) {
                    return this.respondWithErrors(errors);
                }
                const option = yield this.optionService.updateOption(userId, id, data);
                this.respondWithErrorsOrSuccess(option);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_a = typeof options_dto_1.CreateOptionDto !== "undefined" && options_dto_1.CreateOptionDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], OptionController.prototype, "createOption", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_c = typeof Promise !== "undefined" && Promise) === "function" ? _c : Object)
], OptionController.prototype, "getOption", null);
__decorate([
    common_1.Get('countVariation/:value'),
    __param(0, common_1.Req()), __param(1, common_1.Param('value')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], OptionController.prototype, "getCountVariation", null);
__decorate([
    common_1.Post('countVariation'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_e = typeof options_dto_1.CountVariationDto !== "undefined" && options_dto_1.CountVariationDto) === "function" ? _e : Object]),
    __metadata("design:returntype", typeof (_f = typeof Promise !== "undefined" && Promise) === "function" ? _f : Object)
], OptionController.prototype, "countVariation", null);
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_g = typeof options_dto_1.SearchOptionDto !== "undefined" && options_dto_1.SearchOptionDto) === "function" ? _g : Object]),
    __metadata("design:returntype", typeof (_h = typeof Promise !== "undefined" && Promise) === "function" ? _h : Object)
], OptionController.prototype, "getAllOptions", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", typeof (_j = typeof Promise !== "undefined" && Promise) === "function" ? _j : Object)
], OptionController.prototype, "deleteOption", null);
__decorate([
    common_1.Put(':id'),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, typeof (_k = typeof options_dto_1.UpdateOptionDto !== "undefined" && options_dto_1.UpdateOptionDto) === "function" ? _k : Object]),
    __metadata("design:returntype", typeof (_l = typeof Promise !== "undefined" && Promise) === "function" ? _l : Object)
], OptionController.prototype, "updateOption", null);
OptionController = __decorate([
    swagger_1.ApiTags('Options'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    core_1.CrudOptionsDtoDecorator({
        resourceName: 'Option',
        createDto: options_dto_1.CreateOptionDto,
        updateDto: options_dto_1.UpdateOptionDto,
        transformDto: options_dto_1.TransformOptionDto,
        searchDto: options_dto_1.SearchOptionDto
    }),
    __metadata("design:paramtypes", [typeof (_m = typeof options_service_1.OptionService !== "undefined" && options_service_1.OptionService) === "function" ? _m : Object])
], OptionController);
exports.OptionController = OptionController;


/***/ }),
/* 384 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const auth_1 = __webpack_require__(166);
const database_1 = __webpack_require__(130);
const typeorm_1 = __webpack_require__(117);
const shared_1 = __webpack_require__(25);
const optionValues_service_1 = __webpack_require__(382);
const optionValues_entity_1 = __webpack_require__(203);
const optionValues_controller_1 = __webpack_require__(385);
const PROVIDERS = [
    optionValues_service_1.OptionValueService
];
const ENTITY = [
    optionValues_entity_1.OptionValueEntity
];
let OptionValueModule = class OptionValueModule {
};
OptionValueModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_1.AuthModule),
            shared_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_1.DB_CONNECTION_DEFAULT)
        ],
        providers: [...PROVIDERS],
        controllers: [optionValues_controller_1.OptionValueController],
        exports: [...PROVIDERS]
    })
], OptionValueModule);
exports.OptionValueModule = OptionValueModule;


/***/ }),
/* 385 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const crud_controller_1 = __webpack_require__(24);
const optionValues_service_1 = __webpack_require__(382);
const composeSecurity_guard_1 = __webpack_require__(236);
const optionValues_dto_1 = __webpack_require__(381);
const crud_options_dto_decorator_1 = __webpack_require__(163);
let OptionValueController = class OptionValueController extends crud_controller_1.CrudController {
    constructor(optionValueService) {
        super(optionValueService);
        this.optionValueService = optionValueService;
    }
    createOptionValue(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const optionValue = yield this.optionValueService.createOptionValue(data);
                this.respondWithErrorsOrSuccess(optionValue);
            }
            catch (e) {
                this.respondWithErrors(e);
            }
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof optionValues_dto_1.CreateOptionValueDto !== "undefined" && optionValues_dto_1.CreateOptionValueDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], OptionValueController.prototype, "createOptionValue", null);
OptionValueController = __decorate([
    swagger_1.ApiTags('OptionValues'),
    common_1.Controller(),
    common_1.UseGuards(composeSecurity_guard_1.ComposeSecurity),
    crud_options_dto_decorator_1.CrudOptionsDtoDecorator({
        resourceName: 'OptionValue',
        createDto: optionValues_dto_1.CreateOptionValueDto,
        updateDto: optionValues_dto_1.UpdateOptionValueDto,
        transformDto: optionValues_dto_1.TransformOptionValueDto,
        searchDto: optionValues_dto_1.SearchOptionValueDto
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof optionValues_service_1.OptionValueService !== "undefined" && optionValues_service_1.OptionValueService) === "function" ? _c : Object])
], OptionValueController);
exports.OptionValueController = OptionValueController;


/***/ }),
/* 386 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const currencies_service_1 = __webpack_require__(387);
const currencies_controller_1 = __webpack_require__(388);
const currencies_entity_1 = __webpack_require__(201);
const auth_module_1 = __webpack_require__(167);
const shared_module_1 = __webpack_require__(66);
const typeorm_1 = __webpack_require__(117);
const database_constants_1 = __webpack_require__(132);
const PROVIDERS = [
    currencies_service_1.CurrencyService
];
const ENTITY = [
    currencies_entity_1.CurrencyEntity
];
let CurrencyModule = class CurrencyModule {
};
CurrencyModule = __decorate([
    common_1.Module({
        imports: [
            common_1.forwardRef(() => auth_module_1.AuthModule),
            shared_module_1.SharedModule,
            typeorm_1.TypeOrmModule.forFeature(ENTITY, database_constants_1.DB_CONNECTION_DEFAULT),
        ],
        controllers: [currencies_controller_1.CurrencyController],
        providers: [
            ...PROVIDERS
        ],
        exports: [
            ...PROVIDERS
        ]
    })
], CurrencyModule);
exports.CurrencyModule = CurrencyModule;


/***/ }),
/* 387 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const typeorm_1 = __webpack_require__(117);
const database_1 = __webpack_require__(130);
const typeorm_2 = __webpack_require__(120);
const currencies_entity_1 = __webpack_require__(201);
let CurrencyService = class CurrencyService {
    constructor(currencyRepository) {
        this.currencyRepository = currencyRepository;
    }
    getAllCurrencies(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (filter) {
                    if (filter.where) {
                        const value = filter.where.search || '';
                        if (typeof value === 'number') {
                            filter.where = { id: value };
                        }
                        else {
                            const queryName = typeorm_2.Raw(alias => `${alias} ILIKE '%${value}%'`);
                            filter.where = [
                                { currencyName: queryName },
                                { currencyCode: queryName },
                            ];
                        }
                    }
                }
                const currencies = yield this.currencyRepository.find(filter);
                const result = currencies.map(cur => {
                    return Object.assign(Object.assign({}, cur), { nameCode: `${cur.currencyName} (${cur.currencyCode})` });
                });
                return result;
            }
            catch (e) {
                console.log(e);
                throw e;
            }
        });
    }
};
CurrencyService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(currencies_entity_1.CurrencyEntity, database_1.DB_CONNECTION_DEFAULT)),
    __metadata("design:paramtypes", [typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object])
], CurrencyService);
exports.CurrencyService = CurrencyService;


/***/ }),
/* 388 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = __webpack_require__(3);
const swagger_1 = __webpack_require__(18);
const currencies_service_1 = __webpack_require__(387);
const dto_1 = __webpack_require__(389);
let CurrencyController = class CurrencyController {
    constructor(currencyService) {
        this.currencyService = currencyService;
    }
    getAllCurrencies(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const currencies = yield this.currencyService.getAllCurrencies(filter);
                return currencies;
            }
            catch (e) {
                console.log(e);
                throw (e);
            }
        });
    }
};
__decorate([
    common_1.Post('findAll'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_a = typeof dto_1.SearchCurrencyDto !== "undefined" && dto_1.SearchCurrencyDto) === "function" ? _a : Object]),
    __metadata("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CurrencyController.prototype, "getAllCurrencies", null);
CurrencyController = __decorate([
    swagger_1.ApiTags('Currencies'),
    common_1.Controller(),
    __metadata("design:paramtypes", [typeof (_c = typeof currencies_service_1.CurrencyService !== "undefined" && currencies_service_1.CurrencyService) === "function" ? _c : Object])
], CurrencyController);
exports.CurrencyController = CurrencyController;


/***/ }),
/* 389 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(390));


/***/ }),
/* 390 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const class_transformer_1 = __webpack_require__(16);
class SearchCurrencyDto {
}
__decorate([
    swagger_1.ApiProperty({
        example: ['id', 'currencyCode', 'currencyName']
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCurrencyDto.prototype, "select", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: { search: "Malaysian Ringgit" }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCurrencyDto.prototype, "where", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: {
            id: 'ASC',
            currencyName: 'DESC'
        }
    }),
    class_transformer_1.Expose(),
    __metadata("design:type", Object)
], SearchCurrencyDto.prototype, "order", void 0);
exports.SearchCurrencyDto = SearchCurrencyDto;


/***/ }),
/* 391 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(52);
const helmet = __webpack_require__(392);
const swagger_1 = __webpack_require__(393);
function bootstrap(app) {
    return __awaiter(this, void 0, void 0, function* () {
        const config = app.get(config_1.ConfigService);
        app.use(helmet());
        if (config.get('swagger.enable')) {
            swagger_1.setupSwagger(app);
        }
        app.enableShutdownHooks();
        const globalPrefix = config.get('server.globalPrefix');
        const port = config.get('server.port');
        yield app.listen(port, () => {
            console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
        });
    });
}
exports.bootstrap = bootstrap;


/***/ }),
/* 392 */
/***/ (function(module, exports) {

module.exports = require("helmet");

/***/ }),
/* 393 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = __webpack_require__(18);
const environment_1 = __webpack_require__(6);
const config_1 = __webpack_require__(52);
function setupSwagger(app) {
    const config = app.get(config_1.ConfigService);
    const options = new swagger_1.DocumentBuilder()
        .setTitle(environment_1.environment.swagger.title)
        .setDescription(environment_1.environment.swagger.description)
        .setVersion(config.getVersion())
        .addBearerAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    swagger_1.SwaggerModule.setup(environment_1.environment.swagger.path, app, document);
}
exports.setupSwagger = setupSwagger;


/***/ })
/******/ ]);