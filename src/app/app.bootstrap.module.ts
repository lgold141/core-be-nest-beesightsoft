import { environment } from '@app/environment';
import { HttpModule, Module, Global } from '@nestjs/common';
import { RouterModule } from 'nest-router';
import { CoreModule } from '@nest-core/core';
import { SharedModule } from '@nest-core/shared';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nest-core/config';
import { AuthModule } from './auth';
import { UserModule, UserEntity, ProfileEntity, ImageEntity } from './user';
import { ApiErrorCodeModule } from './api-error-code';
import { ExampleModule } from './example';
import { AppController } from './app.controller';
// import { NotificationsModule } from './notifications';
// import {ExternalModule} from './external';
// import {CacheModule} from './cache';
// import {ProjectModule} from './project';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './app-health.controller';
import { ImagesEntity, ImageModule } from './images';

const globalPrefix = environment.server.globalPrefix;
const entities = [
  ImageEntity, ProfileEntity, UserEntity, ImagesEntity
];
import { CasbinModule } from './casbin';
import * as databaseConfig from './database';

const { DB_CONNECTION_DEFAULT } = databaseConfig;
import { join } from 'path';
import { ProductModule } from './products';
import { SupplierModule } from './suppliers';
import { StockModule } from './stocks';
import { CategoryModule } from './categories';
import { LocationModule } from './locations';
import { TwilioModule } from './twilio';
import { CodeService } from './code/code.service';
import { CodeModule } from './code/code.module';
import { FlagModule } from './flag/flag.module';
import { CompanyEntity, CompanyModule } from './companies';
import { DeviceModule } from './device'
import { OptionModule } from './options/options.module';
import { CountryModule } from './countries/countries.module';
import { CurrencyModule } from './currencies/currencies.module';
import { OptionValueModule } from './optionValues/optionValues.module';

@Global()
@Module({
  imports: [
    ConfigModule,
    CoreModule,
    RouterModule.forRoutes([
      {
        path: globalPrefix,
        children: [
          { path: '/auth', module: AuthModule },
          { path: '/user', module: UserModule },
          { path: '/products', module: ProductModule },
          { path: '/suppliers', module: SupplierModule },
          { path: '/stocks', module: StockModule },
          { path: '/images', module: ImageModule },
          { path: '/categories', module: CategoryModule },
          { path: '/locations', module: LocationModule },
          { path: '/code', module: CodeModule },
          { path: '/flag', module: FlagModule },
          { path: '/company', module: CompanyModule },
          { path: '/devices', module: DeviceModule },
          { path: '/options', module: OptionModule },
          { path: '/countries', module: CountryModule },
          { path: '/currencies', module: CurrencyModule },
          { path: '/optionValues', module: OptionValueModule },
          // { path: '/account', module: AccountModule },
          // {path: '/', module: ProjectModule},
          // { path: '/', module: NotificationsModule },
        ],
      },
      // {path: '/external', module: ExternalModule},
    ]),
    ApiErrorCodeModule,
    HttpModule,
    SharedModule,
    // CacheModule,

    AuthModule,
    CompanyModule,
    DeviceModule,
    UserModule,
    // AccountModule,
    // ChatModule,
    // ExternalModule,
    // NotificationsModule,
    // ProjectModule,
    TerminusModule,
    ExampleModule,
    ProductModule,
    SupplierModule,
    StockModule,
    ImageModule,
    CategoryModule,
    LocationModule,
    TwilioModule,
    FlagModule,
    CodeModule,
    OptionModule,
    CountryModule,
    CurrencyModule,
    OptionValueModule,
  ],
  controllers: [AppController, HealthController],
})

export class AppBootstrapModule {
}