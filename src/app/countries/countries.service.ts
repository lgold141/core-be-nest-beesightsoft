import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { Raw, Repository } from 'typeorm';
import { CountryEntity } from './countries.entity';
import { SearchCountryDto } from './dto/countries.dto';

@Injectable()
export class CountryService {
  constructor(@InjectRepository(CountryEntity, DB_CONNECTION_DEFAULT) private readonly countryRepository: Repository<CountryEntity>){}

  async getAllCountries(filter?: SearchCountryDto): Promise<CountryEntity[]> {
    try {
      if (filter) {
        if (filter.where) {
          const value = filter.where.search || '';
          if(typeof value === 'number'){
            filter.where = { id: value };
          } else{
            const queryName = Raw(alias => `${alias} ILIKE '%${value}%'`);
            filter.where =
            {
              name: queryName
            }
          }
        }
      }
      const countries = await this.countryRepository.find(filter);
      return countries;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getCountry(name: string): Promise<CountryEntity> {
    try {
      const country = await this.countryRepository.findOne({where:{ nicename: name }});
      return country;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
