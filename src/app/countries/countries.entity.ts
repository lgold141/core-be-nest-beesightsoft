import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Column } from 'typeorm';
import { CompanyEntity } from '../companies/companies.entity';

@Entity('countries')
export abstract class CountryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  iso: string;

  @Column()
  name: string;

  @Column()
  nicename: string;

  @Column({ nullable: true })
  iso3: string;

  @Column({ nullable: true })
  numcode: number;

  @Column({ nullable: true })
  phonecode: string;

  @Column({ nullable: true })
  barcode: string;

  @OneToMany(type => CompanyEntity, company => company.country)
  companies: CompanyEntity[];
}
