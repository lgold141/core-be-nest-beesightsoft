import { Module, forwardRef } from '@nestjs/common';
import { CountryService } from './countries.service';
import { CountryController } from './countries.controller';
import { CountryEntity } from './countries.entity';
import { AuthModule } from '../auth';
import { SharedModule } from '@nest-core/shared';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DB_CONNECTION_DEFAULT } from '@app/database';

const PROVIDERS = [
  CountryService
];

const ENTITY = [
  CountryEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
  ],
  controllers: [CountryController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class CountryModule {}
