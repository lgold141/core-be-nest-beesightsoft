import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CountryEntity } from './countries.entity';
import { CountryService } from './countries.service';
import { SearchCountryDto } from './dto/countries.dto';

@ApiTags('Countries')
@Controller()
export class CountryController  {
  constructor(private readonly countryService: CountryService) {}

  @Post('findAll')
  async getAllCountries(@Body() filter: SearchCountryDto): Promise<CountryEntity[]> {
    try {
      const countries = await this.countryService.getAllCountries(filter);
      return countries;
    } catch (e) {
      console.log(e);
      throw(e);
    }
  }
}
