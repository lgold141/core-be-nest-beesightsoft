import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class SearchCountryDto {
    @ApiProperty({
      example: ['id', 'iso', 'nicename', 'phonecode', 'barcode']
    })
    @Expose()
    select: any;
  
    @ApiProperty({
      example: { search: "Viet Nam" }
    })
    @Expose()
    where: any;
  
    @ApiProperty({
      example: {
        id: 'ASC',
        name: 'DESC'
      }
    })
    @Expose()
    order: any;
  }