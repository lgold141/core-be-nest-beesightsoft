import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CompanyEntity } from './companies.entity';
import { CreateCompanyDto, TransformCompanyDto, UpdateCompanyDto, SearchCompanyDto } from './dto';
import { UtilsService } from '@nest-core/shared';
import { CodeEnum } from '../api-error-code/enums';
import { Raw } from 'typeorm';
import { validateEmail } from '../../utils/email.utils';
import { validatePhoneNumber, stripeZeroOut } from '../../utils/phoneNumber.utils';
import { ImageService } from '../images/images.service';
import { UserService } from '../user/user.service';
import { saveImage, getImageUrl } from '../../utils/file-uploading.utils';
import { ImagesEntity } from '../images/images.entity';
import { CountryService } from '../countries/countries.service';

@Injectable()
export class CompanyService extends CrudService<CompanyEntity> {
  constructor(@InjectRepository(CompanyEntity, DB_CONNECTION_DEFAULT) private readonly companyRepository: AbstractBaseRepository<CompanyEntity>,
    private imageService: ImageService,
    private userService: UserService,
    private countryService: CountryService) {
    super(companyRepository);
  }

  async createCompany(userId: string, company: CreateCompanyDto): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      let checkCompany = await this.checkValidCompany(company);
      if(checkCompany){
        return {
          code: CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
        };
      }
      const checkEmail = company.email ? await validateEmail(company.email) : true;
      if (!checkEmail) {
        return {
          code: CodeEnum.USER_EMAIL_INVALID,
          data: null
        }
      }

      const checkPhoneNumber = company.phoneNumber ? await validatePhoneNumber(company.phoneNumber) : true;
      if (!checkPhoneNumber) {
        return {
          code: CodeEnum.NUMBER_PHONE_WRONG,
          data: null
        }
      }

      if (company.phoneNumber) {
        company.phoneNumber = stripeZeroOut(company.phoneNumber)
      }

      const userExist = await this.userService.findUserNoRelations(userId);
      const country = await this.countryService.getCountry(userExist.data.country);
      company.countryId = country.id;
      let createCompany = await this.companyRepository.save(company);

      let updateCompanyForUser = {
        companyId: createCompany.id,
        phoneNumber: company.phoneNumber ? company.phoneNumber : undefined,
        email: company.email ? company.email : undefined,
        registerCompleted: true
      } as any;

      let { code } = await this.userService.updateUser(userId, updateCompanyForUser);
      if (code) {
        await this.companyRepository.delete({ id: createCompany.id });
        return {
          code: code
        }
      }

      return {
        data: UtilsService.plainToClass(TransformCompanyDto, createCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateUserCompany(userId: string, company: UpdateCompanyDto, images?: any): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const companyId = userExist?.data?.companyId;
      company.id = companyId;
      const checkCompanyId = await this.companyRepository.findOne({ id: companyId });
      if (!checkCompanyId) {
        return {
          code: CodeEnum.COMPANY_NOT_EXISTED
        };
      }

      let logo: ImagesEntity;
      if (images) {
        if (images.length > 1) {
          return {
            code: CodeEnum.COMPANY_LIMIT_LOGO
          }
        }
        logo = await this.uploadLogoCompany(images);
        if (checkCompanyId.logo?.id) {
          await this.imageService.delete(checkCompanyId.logo.id);
          await this.imageService.deleteImageLocal(checkCompanyId.logo.path);
        }
        company.logoId = logo.id;
      }
      const checkCompany = await this.checkValidCompany(company, companyId);
      if(checkCompany){
        return {
          code: CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
        };
      }

      const updateCompany = await this.companyRepository.save(company);
      return {
        data: UtilsService.plainToClass(TransformCompanyDto, updateCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateCompany(id: string, company: UpdateCompanyDto, images?: any): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      company.id = id;
      const checkCompanyId = await this.companyRepository.findOne({ id: id });
      if (!checkCompanyId) {
        return {
          code: CodeEnum.COMPANY_NOT_EXISTED
        };
      }
      let logo: ImagesEntity;
      if (images) {
        if (images.length > 1) {
          return {
            code: CodeEnum.COMPANY_LIMIT_LOGO
          }
        }
        logo = await this.uploadLogoCompany(images);
        if (checkCompanyId.logo?.id) {
          this.imageService.delete(checkCompanyId.logo.id);
          await this.imageService.deleteImageLocal(checkCompanyId.logo.path);
        }
        company.logoId = logo.id;
      }
      const checkCompany = await this.checkValidCompany(company, id);
      if(checkCompany){
        return {
          code: CodeEnum.COMPANY_BUSINESSNAME_EXISTED,
        };
      }

      const updateCompany = await this.companyRepository.save(company);
      return {
        data: UtilsService.plainToClass(TransformCompanyDto, updateCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findUserCompany(userId: string): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const companyId = userExist?.data?.companyId;
      const checkCompany = await this.companyRepository.findOne({ id: companyId });
      if (!checkCompany) {
        return {
          code: CodeEnum.COMPANY_NOT_EXISTED
        };
      }
      if (checkCompany.logo) {
        checkCompany.logo = await getImageUrl(checkCompany.logo, false);
      }
      return {
        data: UtilsService.plainToClass(TransformCompanyDto, checkCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findCompany(id: string): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      let checkCompany = await this.companyRepository.findOne({ id: id });
      if (!checkCompany) {
        return {
          code: CodeEnum.COMPANY_NOT_EXISTED
        };
      }
      if (checkCompany.logo) {
        checkCompany.logo = await getImageUrl(checkCompany.logo, false);
      }
      return {
        data: UtilsService.plainToClass(TransformCompanyDto, checkCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findAllCompanies(filter?: SearchCompanyDto): Promise<IApiServiceResponseInterface<TransformCompanyDto>> {
    try {
      if (filter) {
        if (filter.where) {
          let value = filter.where.search || '';
          let queryName = Raw(alias => `${alias} ILIKE '%${value}%'`);
          filter.where =
          {
            name: queryName
          }
        }
      }
      const findAllCompany = await this.companyRepository.find(filter);
      return {
        data: UtilsService.plainToClass(TransformCompanyDto, findAllCompany, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async uploadLogoCompany(images: any): Promise<any> {
    const updateImg = [];
    for (let i = 0; i < images.length; i++) {
      if (images[i].size > 0 && images[i].mimetype !== 'application/octet-stream' && images[i].mimetype !== 'application/json') {
        const img = await this.imageService.upload(saveImage(images[i]));
        updateImg.push(img.data);
      }
    }
    return updateImg[0];
  }

  async checkValidCompany(data: CreateCompanyDto | UpdateCompanyDto, id?: string): Promise<boolean> {
    const { businessName } = data;
    let countBusinessName: number; 
    if(id){
      countBusinessName = await this.companyRepository.createQueryBuilder("companies")
        .where("LOWER(companies.businessName) = LOWER(:businessName) AND id != :id", { businessName, id })
        .getCount();
    } else {
      countBusinessName = await this.companyRepository.createQueryBuilder("companies")
        .where("LOWER(companies.businessName) = LOWER(:businessName)", { businessName })
        .getCount();
    }
    if(countBusinessName > 0)
      return true;
    return false;
  }

}
