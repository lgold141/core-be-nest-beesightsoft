import { IsAscii, IsEmail, IsNotEmpty, IsString, IsUUID, Length, IsCurrency, IsNumber, MaxLength, IsOptional } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { CompanyEntity } from '../companies.entity';
import { ImagesEntity } from 'src/app/images';
import { JoinColumn, OneToOne } from 'typeorm';

export class TransformCompanyDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  address: string;

  @ApiProperty()
  @Expose()
  phoneCountry: string;

  @ApiProperty({ example: '+84868964539' })
  @Expose()
  phoneNumber: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  barcodeCountry: string;

  @ApiProperty()
  @Expose()
  barcodeCompany: string;

  @ApiProperty()
  @Expose()
  language: string;

  @ApiProperty()
  @Expose()
  timezone: string;

  @ApiProperty()
  @Expose()
  countryId: number;

  @ApiProperty()
  @Expose()
  industry: string;

  @ApiProperty()
  @Expose()
  businessName: string;

  @ApiProperty()
  @Expose()
  businessType: string;

  @Expose()
  logoId: string;

  @OneToOne(type => ImagesEntity, logo => logo.id)
  @JoinColumn({ name: "logoId" })
  @Expose()
  logo: ImagesEntity;
}

export class CreateCompanyDto extends AbstractDto {
  @ApiProperty({ example: 'Beesight Soft'})
  @Expose()
  name: string;

  @ApiProperty({ example: 'beesight.alpha.com' })
  @Expose()
  address: string;

  @ApiProperty({ example: '+84' })
  @Expose()
  phoneCountry: string;

  @ApiProperty({ example: '0868964539' })
  @Expose()
  phoneNumber: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @Expose()
  email: string;

  @ApiProperty({ example: '893' })
  @Expose()
  barcodeCountry: string;

  @ApiProperty({ example: '00000' })
  @Expose()
  barcodeCompany: string;

  @ApiProperty({ example: 'en' })
  @Expose()
  language: string;

  @ApiProperty({ example: '+7' })
  @Expose()
  timezone: string;

  @ApiProperty({ example: 232 })
  @Expose()
  countryId: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(1, 128)
  @Expose()
  businessName: string;

  @ApiProperty()
  @IsNotEmpty()
  @Expose()
  businessType: string;

  @ApiProperty({ example: '84bde5cc-3ab4-4598-bb16-9a13f1d30e9e' })
  @Expose()
  logoId: string;
}

export class UpdateCompanyDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  address: string;

  @ApiProperty()
  @Expose()
  phoneCountry: string;

  @ApiProperty()
  @Expose()
  phoneNumber: string;

  @ApiProperty()
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  barcodeCountry: string;

  @ApiProperty()
  @Expose()
  barcodeCompany: string;

  @ApiProperty()
  @Expose()
  language: string;

  @ApiProperty()
  @Expose()
  timezone: string;

  @ApiProperty()
  @Expose()
  logoId: string;

  @ApiProperty()
  @Expose()
  countryId: number;

  @ApiProperty()
  @Expose()
  businessName: string;

  @ApiProperty()
  @Expose()
  businessType: string;

  @ApiProperty()
  @MaxLength(128)
  @IsOptional()
  @Expose()
  city: string;
}

export class SearchCompanyDto extends PaginationParams<CompanyEntity> {
  @ApiProperty({
    example: ['id', 'name']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformCompanyDto, ['name'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;

  @ApiProperty({
    example: ['logo', 'options', 'country']
  })
  @Expose()
  relations: any;
}
