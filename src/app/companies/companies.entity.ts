import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber, IsEmail, IsCurrency, IsPhoneNumber, MaxLength } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn, Entity, ManyToOne } from 'typeorm';
import { ICompanyInterface } from './interfaces/companies.interface';
import { ImagesEntity } from '../images';
import { LocationEntity } from '../locations/locations.entity';
import { UserEntity } from '../user/user.entity';
import { OptionEntity } from '../options/options.entity';
import { CountryEntity } from '../countries/countries.entity';
import { ProductEntity } from '../products/products.entity';

@Entity('companies')
export abstract class CompanyEntity extends AbstractEntity implements ICompanyInterface {
  @IsString()
  @MaxLength(128)
  @Index()
  @Column({ unique: true, nullable: true})
  name: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  address: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  phoneCountry: string;

  @IsString()
  @Index()
  @Column({ unique: true, nullable: true})
  phoneNumber: string;

  @IsEmail()
  @Index()
  @Column({ unique: true, nullable: true})
  email: string;

  @IsString()
  @Index()
  @Column({ nullable: true})
  businessType: string;

  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  @Index()
  @Column({ unique: true, nullable: true })
  businessName: string;

  @IsString()
  @MaxLength(128)
  @Index()
  @Column({ nullable: true})
  city: string;

  @IsString()
  @Index()
  @Column({ nullable: true})
  countryId: number;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  barcodeCountry: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  barcodeCompany: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  language: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column({ nullable: true })
  timezone: string;

  @Column({ nullable: true })
  logoId: string;

  @OneToOne(type => ImagesEntity, logo => logo.id,
    { nullable: true, onDelete: 'SET NULL', eager: true })
  @JoinColumn({ name: "logoId" })
  logo: ImagesEntity;

  // @OneToMany(type => LocationEntity, location => location.company,{eager: true})
  @OneToMany(type => LocationEntity, location => location.company)
  locations: LocationEntity[];

  @OneToMany(type => UserEntity, user => user.company)
  users: UserEntity[];

  @OneToMany(type => OptionEntity, option => option.company)
  options: OptionEntity[];

  @ManyToOne(type => CountryEntity, country => country.id,
    { nullable: true, onDelete: 'SET NULL', eager: true })
  @JoinColumn({ name: "countryId" })
  country: CountryEntity;

  @OneToMany(type => ProductEntity, product => product.company)
  products: ProductEntity[];
}
