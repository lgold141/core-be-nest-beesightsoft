import {IEntityInterface} from '@nest-core/shared';

export interface ICompanyInterface extends IEntityInterface {
  name: string;
  address: string;
  phoneCountry: string;
  phoneNumber?: string;
  email?: string;
  barcodeCountry: string;
  barcodeCompany: string;
  language: string;
  timezone: string;
  businessName: string;
  city:string;
  businessType: string;
  logoId?: string;
}