import { Controller, UseGuards, Post, Body, Get, Param, Delete, Put, UseInterceptors, UploadedFiles, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator, IApiErrorInterface } from '@nest-core/core';
import { CompanyEntity } from './companies.entity';
import { CompanyService } from './companies.service';
import { CreateCompanyDto, SearchCompanyDto, TransformCompanyDto, UpdateCompanyDto } from './dto';
import { AnyFilesInterceptor, FileFieldsInterceptor } from '@nestjs/platform-express';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Companies')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Company',
  createDto: CreateCompanyDto,
  updateDto: UpdateCompanyDto,
  transformDto: TransformCompanyDto,
  searchDto: SearchCompanyDto
})
export class CompanyController extends CrudController<CompanyEntity> {
  constructor(private readonly companyService: CompanyService) {
    super(companyService);
  }

  @Post()
  async create(@Body() data: CreateCompanyDto, @Req() req: any): Promise<void> {
    try {
      const userId = req.user.id;
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        console.log(errors);
        return this.respondWithErrors(errors);
      }
      let company = await this.companyService.createCompany(userId, data);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put(':id')
  @UseInterceptors(AnyFilesInterceptor())
  async updateCompany(@Param('id') id: string, @Body() data: UpdateCompanyDto, @UploadedFiles() files?: any): Promise<void> {
    try {
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      let company = await this.companyService.updateCompany(id, data, files);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put()
  @UseInterceptors(AnyFilesInterceptor())
  async updateUserCompany(@Req() req: any, @Body() data: UpdateCompanyDto, @UploadedFiles() files?: any): Promise<void> {
    try {
      const userId = req.user.id;
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const company = await this.companyService.updateUserCompany(userId, data, files);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get(':id')
  async getCompany(@Param('id') id: string): Promise<void> {
    try {
      let company = await this.companyService.findCompany(id);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get()
  async getUserCompany(@Req() req: any): Promise<void> {
    try {
      const userId = req.user.id;
      const company = await this.companyService.findUserCompany(userId);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('findAll')
  async getAllCompanies(@Body() filter: SearchCompanyDto): Promise<void> {
    try {
      let company = await this.companyService.findAllCompanies(filter);
      this.respondWithErrorsOrSuccess(company);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
