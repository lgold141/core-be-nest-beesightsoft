import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CompanyEntity } from './companies.entity'
import { CompanyController } from './companies.controller';
import { CompanyService } from './companies.service';
import { ImageModule } from '../images';
import { CountryModule } from '../countries/countries.module';

const PROVIDERS = [
  CompanyService
];

const ENTITY = [
  CompanyEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => CountryModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
    ImageModule
  ],
  controllers: [CompanyController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class CompanyModule { }
