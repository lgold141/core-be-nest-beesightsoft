import { Controller, UseGuards, Get, Post, Body, Put, Param, Req, UsePipes, ValidationPipe, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { UserEntity } from '@nest-core/core';
import { CreateUserDto, SearchUserDto, TransformUserDto, UpdatePreferenceDto, UpdateUserDto } from './dto';
import { UserService } from './user.service';
import { IApiErrorInterface } from '../../../libs/core/src/interfaces';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('User')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'User',
  createDto: CreateUserDto,
  updateDto: UpdateUserDto,
  transformDto: TransformUserDto,
  searchDto: SearchUserDto
})
export class UserController extends CrudController<UserEntity> {
  constructor(private readonly userService: UserService) {
    super(userService);
  }

  // @Post()
  // async create(@Body() data: CreateUserDto): Promise<void> {
  //   try {
  //     let user = await this.userService.createUser(data);
  //     this.respondWithErrorsOrSuccess(user);
  //   } catch (e) {
  //     this.respondWithErrors(e);
  //   }
  // }

  @Put()
  async updateProfile(@Req() req, @Body() data: UpdateUserDto): Promise<void> {
    try {
      let id = req.user.id;
      let user = await this.userService.updateProfile(id, data);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put('settingPreference')
  @UsePipes(new ValidationPipe({ transform: true }))
  async updatePreference(@Req() req: any, @Body() data: UpdatePreferenceDto): Promise<void> {
    try {
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const id = req.user.id;
      const user = await this.userService.updatePreference(id, data);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }


  @Put(':id')
  @UsePipes(new ValidationPipe({ transform: true }))
  async updateUser(@Param('id') id: string, @Body() data: UpdateUserDto): Promise<void> {
    try {
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      let user = await this.userService.updateUser(id, data);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get()
  async getProfile(@Req() req): Promise<void> {
    try {
      let id = req.user.id
      let user = await this.userService.findUser(id);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  // @Delete(':id')
  // async deleteCategory(@Param('id') id: string): Promise<void> {
  //   try {
  //     let user = await this.userService.deleteUser(id);
  //     this.respondWithErrorsOrSuccess(user);
  //   } catch (e) {
  //     this.respondWithErrors(e);
  //   }
  // }

  @Post('findAll')
  async getAllUser(@Body() filter: SearchUserDto): Promise<void> {
    try {
      let user = await this.userService.findAllUser(filter);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('confirmChangeProfile')
  async confirmProfile(@Req() req, @Body() user: UpdateUserDto, @Query('code') code: number): Promise<void> {
    try {
      let confirmProfile = await this.userService.confirmChangeUser(req, user, code);
      this.respondWithErrorsOrSuccess(confirmProfile);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('resendOTPChangeProfile')
  async resendOTPChangeProfile(@Req() req, @Body() data: UpdateUserDto): Promise<void> {
    try {
      let id = req.user.id;
      let user = await this.userService.resendOTPChangeProfile(id, data);
      this.respondWithErrorsOrSuccess(user);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
