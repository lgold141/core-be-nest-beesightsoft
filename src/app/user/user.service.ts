import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { UserEntity } from './user.entity';
import { CodeEnum } from '@app/api-error-code';
import { UtilsService } from '@nest-core/shared';
import { UpdateUserDto, TransformUserDto, CreateUserDto, SearchUserDto, UpdatePreferenceDto } from './dto';
import { Raw } from 'typeorm';
import { CodeService } from '../code';
import { validatePhoneNumber, stripeZeroOut } from 'src/utils/phoneNumber.utils';
import { validateEmail } from '../../utils/email.utils';
import { TransformCodeDto } from '../code/dto';
import { checkExpiredTime } from 'src/utils/code.utils';
import { DevicesService } from '../device';
import { getIpFromRequest } from 'src/utils/request.utils';

@Injectable()
export class UserService extends CrudService<UserEntity> {
  constructor(@InjectRepository(UserEntity, DB_CONNECTION_DEFAULT) private readonly userRepository: AbstractBaseRepository<UserEntity>,
    private codeService: CodeService,
    private devicesService: DevicesService) {
    super(userRepository);
  }

  async findByEmail(email: string): Promise<UserEntity> {
    return await this.userRepository.findOne({ email });
  }

  async findByPhoneNumber(phoneNumber: string): Promise<UserEntity>  {
    return await this.userRepository.findOne({ phoneNumber });
  }

  // async createUser(user: CreateUserDto): Promise<IApiServiceResponseInterface<TransformUserDto>> {
  //   try {
  //     let userEmailExist = await this.userRepository.findOne({ email: user.email });
  //     if (userEmailExist) {
  //       return {
  //         code: CodeEnum.USER_EMAIL_UNIQUE
  //       };
  //     }
  //     let userUsernameExist = await this.userRepository.findOne({ username: user.username });
  //     if (userUsernameExist) {
  //       return {
  //         code: CodeEnum.USER_USERNAME_UNIQUE
  //       };
  //     }
  //     let createUser = await this.userRepository.save(user);
  //     return {
  //       data: UtilsService.plainToClass(TransformUserDto, createUser, { excludeExtraneousValues: false })
  //     };
  //   } catch (e) {
  //     console.log(e);
  //     throw e;
  //   }
  // }

  async updateUser(id: string, user: UpdateUserDto): Promise<IApiServiceResponseInterface<UpdateUserDto>> {
    try {
      let userExist = await this.userRepository.findOne({ id: id });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        }
      }

      if (user.phoneNumber) {
        let checkPhone = await validatePhoneNumber(user.phoneNumber);
        if (!checkPhone) {
          return {
            code: CodeEnum.NUMBER_PHONE_WRONG
          }
        }

        let checkPhoneExisted = await this.userRepository.findOne({ phoneNumber: user.phoneNumber });
        if (checkPhoneExisted && checkPhoneExisted.registerCompleted) {
          return {
            code: CodeEnum.NUMBER_PHONE_EXISTED
          }
        } else {
          if (checkPhoneExisted && checkPhoneExisted.id !== id) {
            await this.deleteUser(checkPhoneExisted.id);
          }
        }

        userExist.phoneNumber = stripeZeroOut(user.phoneNumber);
        userExist.verifiedPhoneNumber = false;

        // let result = await this.codeService.sendOTP(userExist, "verify-user");
        // if (result.code) {
        //   return {
        //     code: result.code
        //   };
        // }
      }

      if (user.email) {
        let checkMail = await validateEmail(user.email);
        if (!checkMail) {
          return {
            code: CodeEnum.EMAIL_WRONG
          }
        }

        let checkEmailExisted = await this.userRepository.findOne({ email: user.email });
        if (checkEmailExisted && checkEmailExisted.registerCompleted) {
          return {
            code: CodeEnum.EMAIL_EXISTED
          }
        } else {
          if (checkEmailExisted && checkEmailExisted.id !== id) {
            await this.deleteUser(checkEmailExisted.id);
          }
        }

        userExist.email = user.email;
        userExist.verifiedEmail = false;

        // let result = await this.codeService.sendOTPByEmail(userExist, "verify-user");
        // if (result.code) {
        //   return {
        //     code: result.code
        //   };
        // }
      }

      let updateUser = await this.userRepository.merge(userExist, user);
      await this.userRepository.save(updateUser)
      return {
        data: UtilsService.plainToClass(UpdateUserDto, user, { excludeExtraneousValues: false })
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateProfile(id: string, user: UpdateUserDto): Promise<IApiServiceResponseInterface<UpdateUserDto>> {
    try {
      let userExist = await this.userRepository.findOne({ id: id });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        }
      }

      if (user.phoneNumber) {
        let checkPhone = validatePhoneNumber(user.phoneNumber);
        if (!checkPhone) {
          return {
            code: CodeEnum.USER_PHONENUMBER_INVALID
          }
        }

        let phoneNumberExist = await this.userRepository.findOne({
          phoneNumber: stripeZeroOut(user.phoneNumber),
          registerCompleted: true
        });
        if (phoneNumberExist) {
          return {
            code: CodeEnum.USER_PHONENUMBER_UNIQUE
          }
        }

        userExist.phoneNumber = stripeZeroOut(user.phoneNumber);
        await this.codeService.sendOTP(userExist, "change-profile", false);
        userExist.verifiedPhoneNumber = false;

        return {
          code: CodeEnum.CONFIRM_CHANGE_NUMBER_PHONE,
          options: { phoneNumber: `${userExist.countryCode}${userExist.phoneNumber}` }
        }
      }

      if (user.email) {
        let checkMail = validateEmail(user.email);
        if (!checkMail) {
          return {
            code: CodeEnum.EMAIL_WRONG
          }
        }

        let emailExist = await this.userRepository.findOne({
          email: user.email,
          registerCompleted: true
        });
        if (emailExist) {
          return {
            code: CodeEnum.USER_EMAIL_UNIQUE
          }
        }

        userExist.email = user.email;
        await this.codeService.sendOTPByEmail(userExist, "change-profile", false);
        userExist.verifiedEmail = false;
        return {
          code: CodeEnum.CONFIRM_CHANGE_EMAIL,
          options: { email: `${user.email}` }
        }
      }

      let updateUser = await this.userRepository.merge(userExist, user);
      await this.userRepository.save(updateUser);
      return {
        data: UtilsService.plainToClass(UpdateUserDto, user, { excludeExtraneousValues: false })
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updatePreference(id: string, data: UpdatePreferenceDto): Promise<IApiServiceResponseInterface<UpdatePreferenceDto>> {
    try {
      const checkUser = await this.userRepository.findOne({ id: id });
      if (!checkUser) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        };
      }
      data.id = id;
      const updateUser = await this.userRepository.save(data);
      return {
        data: UtilsService.plainToClass(UpdatePreferenceDto, updateUser, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteUser(id: string): Promise<IApiServiceResponseInterface<TransformUserDto>> {
    try {
      let userExist = await this.userRepository.findOne({ id: id });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        };
      }
      this.userRepository.delete({ id: id });
      return {
        data: UtilsService.plainToClass(TransformUserDto, {})
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findUser(id: string): Promise<IApiServiceResponseInterface<TransformUserDto>> {
    try {
      let userExist = await this.userRepository.findOne({
        where: { id: id },
        relations: ['company', 'currency'],
      });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        };
      }
      return {
        data: UtilsService.plainToClass(TransformUserDto, userExist, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findAllUser(filter?: SearchUserDto): Promise<IApiServiceResponseInterface<TransformUserDto>> {
    try {
      if (filter) {
        for (var name in filter.where) {
          let value = filter.where[`${name}`];
          filter.where[`${name}`] = Raw(alias => `${alias} ILIKE '%${value}%'`)
        }
      }
      const findAllUsers = await this.userRepository.find(filter);
      return {
        data: UtilsService.plainToClass(TransformUserDto, findAllUsers, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async confirmChangeUser(req, user: UpdateUserDto, otpCode: number): Promise<IApiServiceResponseInterface<TransformUserDto>> {
    try {
      user.phoneNumber = user.phoneNumber ? stripeZeroOut(user.phoneNumber) : undefined;

      const validateCode = {
        user: req.user.id,
        code: otpCode,
        type: "change-profile"
      }

      let checkCode: IApiServiceResponseInterface<TransformCodeDto> = await this.codeService.getCode(validateCode);
      const { code, options, data } = checkCode;

      if (code) {
        return {
          code: code,
          options,
          data: null
        }
      }

      // Check expired
      if (checkExpiredTime(data.expiredAt)) {
        // Expired code, return error
        return {
          code: CodeEnum.EXPIRED_CODE
        }
      }

      // Remove code
      this.codeService.deleteCode(data.id);

      user.phoneNumber = user.phoneNumber ? stripeZeroOut(user.phoneNumber) : undefined;

      //Update verified user
      user.id = req.user.id;
      if (user.phoneNumber) {
        await this.userRepository.delete({ phoneNumber: user.phoneNumber, registerCompleted: false });
      }
      if (user.email) {
        await this.userRepository.delete({ email: user.email, registerCompleted: false });
      }

      const updateProfile = await this.userRepository.save(user);

      let id = req.user.id;
      let ip = getIpFromRequest(req);
      let userAgent = req.headers['user-agent'];
      await this.devicesService.logoutOtherDevices(ip, userAgent, id);

      return {
        data: UtilsService.plainToClass(TransformUserDto, updateProfile, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async resendOTPChangeProfile(id: string, user: UpdateUserDto): Promise<IApiServiceResponseInterface<boolean>> {
    try {
      let userExist = await this.userRepository.findOne({ id: id });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        }
      }
      if (user.phoneNumber) {
        let checkPhone = validatePhoneNumber(user.phoneNumber);
        if (!checkPhone) {
          return {
            code: CodeEnum.NUMBER_PHONE_WRONG
          }
        }
        let phoneNumberExist = await this.userRepository.findOne({
          phoneNumber: stripeZeroOut(user.phoneNumber),
          registerCompleted: true
        });
        if (phoneNumberExist) {
          return {
            code: CodeEnum.USER_PHONENUMBER_UNIQUE
          }
        }
        userExist.phoneNumber = stripeZeroOut(user.phoneNumber);
        let result = await this.codeService.sendOTP(userExist, "change-profile", true);
        if (result.code) {
          return {
            code: result.code
          };
        }
        return {
          data: true
        }
      } else if (user.email) {
        let checkMail = validateEmail(user.email);
        if (!checkMail) {
          return {
            code: CodeEnum.EMAIL_WRONG
          }
        }
        let emailExist = await this.userRepository.findOne({
          email: user.email,
          registerCompleted: true
        });
        if (emailExist) {
          return {
            code: CodeEnum.USER_EMAIL_UNIQUE
          }
        }
        userExist.email = user.email;
        let result = await this.codeService.sendOTPByEmail(userExist, "change-profile", true);
        if (result.code) {
          return {
            code: result.code
          };
        }
        return {
          data: true
        }
      }
      return {
        code: CodeEnum.CHANGE_PROFILE_USER_MUST_PHONE_OR_EMAIL
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findUserNoRelations(id: string): Promise<IApiServiceResponseInterface<TransformUserDto>> {
    try {
      const userExist = await this.userRepository.findOne({ id: id });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        };
      }
      return {
        data: UtilsService.plainToClass(TransformUserDto, userExist, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

}
