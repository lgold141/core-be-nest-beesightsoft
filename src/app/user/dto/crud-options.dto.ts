import { IsAscii, IsEmail, IsInt, IsNotEmpty, IsOptional, IsString, IsUUID, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, IsEmailUnique, IsUsernameUnique, PaginationParams, } from '@nest-core/core';
import { SchemaCodeEnum } from '@app/api-error-code';
import { UserEntity } from '../user.entity';
import { ManyToOne, JoinColumn } from 'typeorm';
import { CompanyEntity } from '../../companies';

export class TransformUserDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  lastName: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @IsEmail()
  @IsEmailUnique()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;

  @ApiProperty({ example: 'nasterblue' })
  @IsAscii()
  @IsUsernameUnique()
  @Length(6, 100)
  @IsString()
  @IsNotEmpty()
  @Expose()
  username: string;

  @ApiProperty({ example: 'Vietnamese' })
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @Expose()
  language: string;

  @ApiProperty({ example: 'Viet Nam' })
  @IsString()
  @Expose()
  country: string;

  @ApiProperty({ example: 'DD/MM/YYYY' })
  @IsString()
  @IsNotEmpty()
  @Expose()
  date: string;

  @ApiProperty({ example: '1' })
  @Expose()
  currencyId: number;

  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @Expose()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "companyId" })
  company: CompanyEntity;
  // @Exclude()
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.user;
}

export class CreateUserDto extends AbstractDto {
  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  lastName: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @IsEmail()
  @IsEmailUnique()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  verifiedEmail: boolean;

  @ApiProperty({ example: 'nasterblue' })
  @IsAscii()
  @IsUsernameUnique()
  @Length(6, 100)
  @IsString()
  @IsNotEmpty()
  @Expose()
  username: string;

  // @ApiProperty({ example: '+84868964539' })
  // @IsNotEmpty()
  // @Length(6, 15)
  // @Expose()
  // phoneNumber: string;

  // @ApiProperty({ example: 'beesight.alpha.com' })
  // @IsNotEmpty()
  // @IsString()
  // @Length(6, 15)
  // @Expose()
  // address: string;

  // @Exclude()
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.user;

  @ApiProperty({ example: 'Vietnamese' })
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @Expose()
  language: string;

  @ApiProperty({ example: 'DD/MM/YYYY' })
  @IsString()
  @IsNotEmpty()
  @Expose()
  date: string;

  @ApiProperty({ example: '1' })
  @Expose()
  currencyId: number;

}

export class UpdateUserDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  lastName: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @IsNotEmpty()
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  verifiedEmail: boolean;

  @ApiProperty({ example: '+84868964539' })
  @Expose()
  phoneNumber: string;

  @ApiProperty()
  @Expose()
  verifiedPhoneNumber: boolean;

  // @Exclude()
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.user;

  @ApiProperty({ example: 'Dark' })
  @IsString()
  @Expose()
  theme: string;

  @ApiProperty({ example: 'Vietnamese' })
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @Expose()
  language: string;

  @ApiProperty({ example: 'DD/MM/YYYY' })
  @IsString()
  @IsNotEmpty()
  @Expose()
  date: string;

  @ApiProperty({ example: '1' })
  @Expose()
  currencyId: number;

}

export class UpdatePreferenceDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @IsOptional()
  @Expose()
  id: string;

  @ApiProperty({ example: 'Dark' })
  @IsString()
  @Expose()
  theme: string;

  @ApiProperty({ example: 'en' })
  @IsString()
  @Expose()
  language: string;

  @ApiProperty({ example: 'DD/MM/YYYY' })
  @IsString()
  @Expose()
  date: string;

  @ApiProperty({ example: 1 })
  @IsInt()
  @Expose()
  currencyId: number;
}

export class SearchUserDto extends PaginationParams<UserEntity> {
  @ApiProperty({
    example: ['id', 'email', 'firstName', 'username']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformUserDto, ['id', 'email', 'firstName', 'username'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      username: 'DESC'
    }
  })
  @Expose()
  order: any;

  @ApiProperty({
    example: ['company', 'company.locations', 'currency']
  })
  @Expose()
  relations: any;
}
