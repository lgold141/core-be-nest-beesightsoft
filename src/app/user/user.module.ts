import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
// import {EmailController} from './email/email.controller';
// import {ProfileController} from './profile/profile.controller';
// import {ProfileService} from './profile/profile.service';
import { ProfileEntity } from './profile/profile.entity';
import { ImageEntity } from './profile/image.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';

const PROVIDERS = [
  UserService,
];
const ENTITIES = [
  UserEntity,
];

@Module({
  imports: [
    // JwtStrategy, needed for CurrentUser, depends on UserService,
    // which creates a circular dependency that we bypass here
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(
      ENTITIES,
      DB_CONNECTION_DEFAULT),
  ],
  controllers: [UserController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS,
  ],
})

export class UserModule {
}
