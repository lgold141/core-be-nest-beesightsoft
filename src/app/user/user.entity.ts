import { Entity, Column, ManyToOne, JoinColumn, OneToOne, RelationId } from 'typeorm';
import { AbstractUserEntity, UserEntity as UserCore } from '@nest-core/core';
import { CompanyEntity } from '../companies';
import { CurrencyEntity } from '../currencies/currencies.entity';

@Entity('core_user')
export class UserEntity extends UserCore {
    @ManyToOne(type => CompanyEntity, company => company.id,
        { nullable: false, onDelete: 'SET NULL' })
    @JoinColumn({ name: "companyId" })
    company: CompanyEntity;

    @ManyToOne(type => CurrencyEntity, currency => currency.id,
        { nullable: true, onDelete: 'SET NULL' })
    @JoinColumn({ name: "currencyId" })
    currency: CurrencyEntity;
}
