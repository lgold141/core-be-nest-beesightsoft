import {BadRequestException, Injectable, Inject} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {ImageType, IUserInterface} from '@nest-core/shared';
import * as crypto from 'crypto';
import * as sharp from 'sharp';
import {AbstractBaseRepository, CrudService} from '@nest-core/core';
import {CreateProfileDto} from './dto';
import {UserEntity} from '../user.entity';
import {ImageEntity} from './image.entity';
import {ProfileEntity} from './profile.entity';
import {DB_CONNECTION_DEFAULT} from '@app/database';

@Injectable()
export class ProfileService extends CrudService<ProfileEntity> {
  constructor(@InjectRepository(ProfileEntity, DB_CONNECTION_DEFAULT) private readonly profileRepository: AbstractBaseRepository<ProfileEntity>,
              @InjectRepository(UserEntity, DB_CONNECTION_DEFAULT) private readonly userRepository: AbstractBaseRepository<UserEntity>,
              @InjectRepository(ImageEntity, DB_CONNECTION_DEFAULT) private readonly imageRepository: AbstractBaseRepository<ImageEntity>) {
    super(profileRepository);
  }

  //
  // async create(entity: CreateProfileDto, file, user: IUserInterface): Promise<ProfileEntity> {
  //   // TODO: do in transaction: https://github.com/odavid/typeorm-transactional-cls-hooked
  //   let profile: ProfileEntity;
  //   if (file) {
  //     const avatar = {
  //       title: file.originalname,
  //       type: ImageType.Profile,
  //       user,
  //       data: await sharp(file.buffer)
  //       .resize(100)
  //       .toBuffer(),
  //       checksum: crypto
  //       .createHash('sha1')
  //       .update(file.buffer)
  //       .digest('hex'),
  //       mimeType: file.mimetype,
  //       size: file.size
  //     };
  //     profile = await super.create({avatar, ...entity});
  //   } else {
  //     profile = await super.create(entity);
  //   }
  //   await this.userRepository.update(user.id, {profile});
  //   return profile;
  // }

  // async update(id: string, entity: CreateProfileDto, file, user: IUserInterface): Promise<UpdateResult | ProfileEntity> {
  //   if (!file) {
  //     return super.update(id, entity);
  //   }
  //   const profile = await super.findOne(id);
  //   if (!profile) {
  //     throw new BadRequestException('profile not found. something wrong');
  //   }
  //   const avatar = profile.avatar
  //     ? {
  //       id: profile.avatar.id,
  //       title: file.originalname,
  //       data: await sharp(file.buffer)
  //       .resize(100)
  //       .toBuffer(),
  //       checksum: crypto
  //       .createHash('sha1')
  //       .update(file.buffer)
  //       .digest('hex'),
  //       mimeType: file.mimetype,
  //       size: file.size
  //     }
  //     : {
  //       title: file.originalname,
  //       type: ImageType.Profile,
  //       user,
  //       data: await sharp(file.buffer)
  //       .resize(100)
  //       .toBuffer(),
  //       checksum: crypto
  //       .createHash('sha1')
  //       .update(file.buffer)
  //       .digest('hex'),
  //       mimeType: file.mimetype,
  //       size: file.size
  //     };
  //
  //   return this.profileRepository.save({id, ...entity, avatar});
  // }
  //
  // async delete(id: number, user: UserEntity): Promise<any> {
  //   // TODO: no cascade OneToOne delete yet. so manually delete image first.
  //   // https://github.com/typeorm/typeorm/issues/3218
  //   await this.imageRepository.delete({user: {id: user.id}, type: ImageType.Profile});
  //   return super.delete(id);
  // }
}
