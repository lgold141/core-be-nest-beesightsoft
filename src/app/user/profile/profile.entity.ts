import {AbstractProfileEntity} from '@nest-core/core';
import {Entity} from 'typeorm';

@Entity('core_profile')
export class ProfileEntity extends AbstractProfileEntity {}
