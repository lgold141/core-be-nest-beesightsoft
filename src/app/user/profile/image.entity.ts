import {Entity} from 'typeorm';
import {AbstractImageEntity} from '@nest-core/core';

@Entity('core_image')
export class ImageEntity extends AbstractImageEntity{}
