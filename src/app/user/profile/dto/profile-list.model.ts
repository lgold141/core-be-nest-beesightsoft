import {IPaginationInterface} from '@nest-core/core';
import {ProfileEntity} from '../profile.entity';

export class ProfileList implements IPaginationInterface<ProfileEntity> {
  readonly items: ProfileEntity[];
  readonly total: number;
}
