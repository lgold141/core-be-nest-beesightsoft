import {Gender} from '@nest-core/shared';
import {IsEnum, IsOptional, IsString, Length} from 'class-validator';
import {AbstractDto, ICrudOptionsDtoInterface} from '@nest-core/core';
import {SchemaCodeEnum} from '@app/api-error-code';

export class CreateProfileDto extends AbstractDto {
  @IsOptional()
  @IsEnum(Gender)
  gender?: Gender = Gender.UNKNOW;

  @IsOptional()
  @IsString()
  @Length(10, 20)
  mobilePhone?: string;

  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.user;
}