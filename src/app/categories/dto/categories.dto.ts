import { IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { CategoryEntity } from '../categories.entity';
import { ManyToOne, JoinColumn } from 'typeorm';
import { CompanyEntity } from 'src/app/companies';

export class TransformCategoryDto extends AbstractDto {
  @Expose()
  id: string;

  @Expose()
  name: string;

  @Expose()
  key: string;

  @Expose()
  numberOfProduct: number;

  @Expose()
  children: CategoryEntity[];

  @Expose()
  parent: CategoryEntity;

  @ApiProperty()
  @Expose()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.id)
  @JoinColumn({ name: "companyId" })
  company: CompanyEntity;
}

export class CreateCategoryDto extends AbstractDto {
  @ApiProperty({ example: 'Men' })
  @IsString()
  @Length(1, 128)
  @Expose()
  name: string;

  @Expose()
  key: string;

  @Expose()
  children: CategoryEntity[];

  @ApiProperty({ example: 'd8cf855b-a2a0-4ca6-9c16-36da1f1b4040' })
  @Expose()
  parent: CategoryEntity;

  @Expose()
  companyId: string;
}

export class UpdateCategoryDto extends AbstractDto {
  @Expose()
  id: string;

  @ApiProperty({ example: 'Men' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  key: string;

  @Expose()
  children: CategoryEntity[];

  @Expose()
  parent: CategoryEntity;
}

export class SearchCategoryDto extends PaginationParams<CategoryEntity> {
  @ApiProperty({
    example: ['id', 'name']
  })
  @Expose()
  select: any;

  @ApiProperty({
    example: { search: "Men" }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;

  @Expose()
  relations: any;
}
