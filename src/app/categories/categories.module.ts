import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { CategoryEntity } from './categories.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CategoryController } from './categories.controller';
import { CategoryService } from './categories.service';
import { TwillioService } from '../twilio';
import { CodeService, CodeEntity } from '../code';
import { UserService, UserEntity } from '../user';
import { ProductModule } from '../products';

const PROVIDERS = [
  CategoryService,
  TwillioService,
  UserService,
  CodeService
];

const ENTITY = [
  CategoryEntity,
  UserEntity,
  CodeEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => ProductModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
    TwillioService
  ],
  controllers: [CategoryController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class CategoryModule { }
