import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { CodeEnum } from '@app/api-error-code';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CategoryEntity } from './categories.entity';
import { CreateCategoryDto, TransformCategoryDto, SearchCategoryDto, UpdateCategoryDto } from './dto';
import { UtilsService } from '@nest-core/shared';
import { Raw } from 'typeorm';
import { ProductService } from '../products';
import { UserService } from '../user';
import { formatSelect, formatOrder } from 'src/utils/filter.utils';

@Injectable()
export class CategoryService extends CrudService<CategoryEntity> {
  constructor(@InjectRepository(CategoryEntity, DB_CONNECTION_DEFAULT)
  private readonly categoryRepository: AbstractBaseRepository<CategoryEntity>,
    @Inject(forwardRef(() => ProductService))
    private productService: ProductService,
    private userService: UserService) {
    super(categoryRepository);
  }

  async createCategory(category: CreateCategoryDto, userId: string): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      if (category.parent && category.parent.toString() === '') {
        delete category.parent;
      }

      const user = await this.userService.findOne({ id: userId });
      category.companyId = user.companyId;

      if (!category.parent) {
        let checkNameCategoryParent = await this.categoryRepository.findOne({
          name: Raw(alias => `${alias} ILIKE '${category.name}'`),
          parent: null,
          companyId: category.companyId
        });
        if (checkNameCategoryParent) {
          return {
            code: CodeEnum.CATEGORY_NAME_EXISTED
          };
        }
        category.key = `${category.name}`;
      } else {
        let parent = await this.categoryRepository.findOne({
          id: category.parent.toString(),
          companyId: category.companyId
        });
        if (!parent) {
          return {
            code: CodeEnum.CATEGORY_NOT_EXISTED
          };
        }
        let checkNameCategoryChildren = await this.categoryRepository.findOne({
          name: Raw(alias => `${alias} ILIKE '${category.name}'`),
          parent: parent,
          companyId: category.companyId
        });
        if (checkNameCategoryChildren) {
          return {
            code: CodeEnum.CATEGORY_EXISTED
          };
        }
        category.key = `${parent.key}.${category.name}`;
      }

      let createCategory = await this.categoryRepository.save(category);
      return {
        data: UtilsService.plainToClass(TransformCategoryDto, createCategory, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateCategory(id: string, category: UpdateCategoryDto, userId: string): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      category.id = id;
      const user = await this.userService.findOne({ id: userId });
      const companyId = user.companyId;
      let checkCategoryId = await this.categoryRepository.findOne(
        {
          id: id,
          companyId: companyId
        },
        { relations: ["parent", "children"] }
      );

      if (!checkCategoryId) {
        return {
          code: CodeEnum.CATEGORY_NOT_EXISTED
        };
      }

      if (checkCategoryId.parent) {
        let parent = await this.categoryRepository.findOne({
          id: checkCategoryId.parent.id,
          companyId: companyId
        });
        if (!parent) {
          return {
            code: CodeEnum.CATEGORY_NOT_EXISTED
          };
        }
        let categoryExist = await this.categoryRepository.findOne({
          name: Raw(alias => `${alias} ILIKE '${category.name}'`),
          parent: parent,
          companyId: companyId
        });
        if (categoryExist) {
          return {
            code: CodeEnum.CATEGORY_EXISTED
          };
        }
        category.key = `${parent.key}.${category.name}`;
      } else {
        let categoryExist = await this.categoryRepository.findOne({
          name: Raw(alias => `${alias} ILIKE '${category.name}'`),
          parent: null,
          companyId: companyId
        });
        if (categoryExist) {
          return {
            code: CodeEnum.CATEGORY_EXISTED
          };
        }
        category.key = category.name;
        await this.changeKeyChildrens(category.name, checkCategoryId.children);
      }
      let updateCategory = await this.categoryRepository.save(category);
      return {
        data: UtilsService.plainToClass(TransformCategoryDto, updateCategory, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findCategory(id: string, userId: string): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      const user = await this.userService.findOne({ id: userId });
      const companyId = user.companyId;
      let checkCategoryId = await this.categoryRepository.findOne({
        id: id,
        companyId
      });
      if (!checkCategoryId) {
        return {
          code: CodeEnum.CATEGORY_NOT_EXISTED
        };
      }
      let findCategory = await this.categoryRepository.findOne(
        {
          id: id,
          companyId
        },
        { relations: ["children", "parent"] }
      );
      return {
        data: UtilsService.plainToClass(TransformCategoryDto, findCategory, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findAllCategories(userId?: string, filter?: SearchCategoryDto): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      const userExist = await this.userService.findOne({ id: userId });
      const companyId = userExist?.companyId;
      let findAllCategory: CategoryEntity[];
      const table = 'category';
      if (filter) {
        let select = formatSelect(filter.select, table);
        let order = formatOrder(filter.order, table);
        let value = filter.where?.search ?? '';
        findAllCategory = await this.categoryRepository.createQueryBuilder(table)
          .select(select)
          .leftJoinAndSelect(`${table}.children`, 'children')
          .leftJoinAndSelect('children.children', 'children_children')
          .where(`${table}.parent IS NULL`)
          .andWhere(`${table}.companyId = '${companyId}'`)
          .andWhere(`${table}.name ILIKE '%${value}%'`)
          .take(filter.take)
          .skip(filter.skip)
          .orderBy(order)
          .getMany();
      } else {
        findAllCategory = await this.categoryRepository.createQueryBuilder(table)
          .leftJoinAndSelect(`${table}.children`, 'children')
          .leftJoinAndSelect('children.children', 'children_children')
          .where(`${table}.parent IS NULL`)
          .andWhere(`${table}.companyId = '${companyId}'`)
          .getMany();
      }
      await this.productService.addTotalProductToCategories(findAllCategory);
      return {
        data: UtilsService.plainToClass(TransformCategoryDto, findAllCategory, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteCategory(id: string, userId: string): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      const userExist = await this.userService.findOne({ id: userId });
      const companyId = userExist?.companyId;
      let categoryExist = await this.categoryRepository.findOne(
        {
          id: id,
          companyId
        },
        { relations: ['children'] });
      if (!categoryExist) {
        return {
          code: CodeEnum.CATEGORY_NOT_EXISTED
        };
      }

      const unCategory = await this.getUncategory(userId);
      if (unCategory.id === id) {
        return {
          data: UtilsService.plainToClass(TransformCategoryDto, {})
        };
      }

      let categoriesList = [categoryExist.id];
      if (categoryExist.children.length > 0) {
        for (let i = 0; i < categoryExist.children.length; i++) {
          categoriesList.push(categoryExist.children[i].id);
        }
      }
      for (let i = 0; i < categoriesList.length; i++) {
        let productCondition = {
          where: {
            categoryId: categoriesList[i],
            companyId
          }
        }
        let productOfCategory = await this.productService.findAll(productCondition);
        for (let j = 0; j < productOfCategory.items.length; j++) {
          productOfCategory.items[j].categoryId = unCategory.id;
          await this.productService.create(productOfCategory.items[j]);
        }
      }

      // let parentCategories = await this.categoryRepository.find({ relations: ["parent"] });
      // parentCategories = parentCategories.filter(category => category.parent !== null);

      // for (let i = 0; i < parentCategories.length; i++) {
      //   if (parentCategories[i].parent.id === id) {
      //     parentCategories[i].parent = await this.getUncategory();
      //     await this.categoryRepository.save(parentCategories[i]);
      //   }  
      // }

      await this.categoryRepository.delete({ parent: { id } });
      await this.categoryRepository.delete({ id: id });
      return {
        data: UtilsService.plainToClass(TransformCategoryDto, {})
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  //****************** START HELPERS ******************//
  //***************************************************//
  async findOneCategory(conditions: any, options?: any): Promise<IApiServiceResponseInterface<TransformCategoryDto>> {
    try {
      let category = await this.categoryRepository.findOne(conditions, options);
      if (!category) {
        return {
          code: CodeEnum.CATEGORY_NOT_EXISTED
        };
      }

      return {
        data: UtilsService.plainToClass(TransformCategoryDto, category, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getUncategory(userId: string): Promise<any> {
    try {
      const userExist = await this.userService.findOne({ id: userId });
      const companyId = userExist?.companyId;
      let unCategory = await this.categoryRepository.findOne({ name: 'Uncategorized', companyId });
      if (!unCategory) {
        return (await this.createCategory({ name: 'Uncategorized' } as CreateCategoryDto, userId)).data;
      }
      return unCategory;

    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async mergeCategory(id: string, category: CreateCategoryDto): Promise<any> {
    try {
      let updateCategory = await this.categoryRepository.findOne({ id: id });
      return await this.categoryRepository.merge(updateCategory, category);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async changeKeyChildrens(parentName: string, childrens: CategoryEntity[]): Promise<void> {
    try {
      childrens.forEach(element => {
        element.key = `${parentName}.${element.name}`;
      });
      this.categoryRepository.save(childrens);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
  //****************** END HELPERS ******************//
  //***************************************************//
}
