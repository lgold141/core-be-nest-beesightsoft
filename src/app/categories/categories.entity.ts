import {
  Entity, Index, Tree, Column, PrimaryGeneratedColumn, TreeChildren, TreeParent, TreeLevelColumn, ManyToOne, JoinColumn
} from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString } from 'class-validator';
import { ICategoryInterface } from './interfaces/categories.interface';
import { CompanyEntity } from '../companies';

@Entity('categories')
@Tree("nested-set")
export abstract class CategoryEntity extends AbstractEntity implements ICategoryInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  name: string;

  @Column({ nullable: true })
  key: string;

  @Column({ nullable: true })
  numberOfProduct: number;

  @TreeChildren()
  children: CategoryEntity[];

  @TreeParent()
  parent: CategoryEntity;

  @Column()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.products, { eager: true })
  @JoinColumn({ name: "companyId" })
  company: CompanyEntity;
}
