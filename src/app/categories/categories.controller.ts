import { Controller, UseGuards, Get, Post, Body, Put, Param, Delete, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { CategoryEntity } from './categories.entity';
import { CreateCategoryDto, SearchCategoryDto, TransformCategoryDto, UpdateCategoryDto } from './dto';
import { CategoryService } from './categories.service';
import { IApiErrorInterface } from '@nest-core/core';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Categories')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Category',
  createDto: CreateCategoryDto,
  updateDto: UpdateCategoryDto,
  transformDto: TransformCategoryDto,
  searchDto: SearchCategoryDto
})
export class CategoryController extends CrudController<CategoryEntity> {
  constructor(private readonly categoryService: CategoryService) {
    super(categoryService);
  }

  @Post()
  async create(@Body() data: CreateCategoryDto, @Req() req): Promise<void> {
    try {
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      let category = await this.categoryService.createCategory(data, req.user.id);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put(':id')
  async updateCategory(@Param('id') id: string, @Body() data: UpdateCategoryDto, @Req() req): Promise<void> {
    try {
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      let category = await this.categoryService.updateCategory(id, data, req.user.id);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Delete(':id')
  async deleteCategory(@Param('id') id: string, @Req() req): Promise<void> {
    try {
      let category = await this.categoryService.deleteCategory(id, req.user.id);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get(':id')
  async getCategory(@Param('id') id: string, @Req() req): Promise<void> {
    try {
      let category = await this.categoryService.findCategory(id, req.user.id);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('findAll')
  async getAllCategories(@Req() req, @Body() filter: SearchCategoryDto): Promise<void> {
    try {
      let category = await this.categoryService.findAllCategories(req.user.id, filter);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
