import {IEntityInterface} from '@nest-core/shared';

export interface ICategoryInterface extends IEntityInterface {
  id: string;
  name: string;
  key: string;
}