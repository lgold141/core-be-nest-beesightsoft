import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CreateBarcodeDto, TransformBarcodeDto, SearchBarcodeDto, UpdateBarcodeDto, FindBarcodeDto } from './dto';
import { BarcodeEntity } from './barcodes.entity';
import { UtilsService } from '@nest-core/shared';
import { CodeEnum } from '@app/api-error-code';

@Injectable()
export class BarcodeService extends CrudService<BarcodeEntity> {
  constructor(@InjectRepository(BarcodeEntity, DB_CONNECTION_DEFAULT) private readonly barcodeRepository: AbstractBaseRepository<BarcodeEntity>) {
    super(barcodeRepository);
  }

  async createBarcode(data: CreateBarcodeDto): Promise<IApiServiceResponseInterface<TransformBarcodeDto>> {
    try {
      let barcode = await this.barcodeRepository.save(data);
      return {
        data: UtilsService.plainToClass(TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getBarcode(id: string): Promise<IApiServiceResponseInterface<TransformBarcodeDto>> {
    try {
      let barcode = await this.barcodeRepository.findOne({ id: id });
      if (!barcode) {
        return {
          code: CodeEnum.BARCODE_NOT_EXISTED
        }
      }
      return {
        data: UtilsService.plainToClass(TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findBarcode(filter: FindBarcodeDto): Promise<IApiServiceResponseInterface<TransformBarcodeDto>> {
    try {
      let barcode = await this.barcodeRepository.findOne(filter);
      if (!barcode) {
        return {
          code: CodeEnum.BARCODE_NOT_EXISTED
        }
      }
      return {
        data: UtilsService.plainToClass(TransformBarcodeDto, barcode, { excludeExtraneousValues: false }),
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
  async updateBarcode(data: UpdateBarcodeDto): Promise<IApiServiceResponseInterface<TransformBarcodeDto>> {
    try {
      let barcodeExisted = await this.barcodeRepository.findOne({ id: data.id });
      if (!barcodeExisted) {
        return {
          code: CodeEnum.BARCODE_NOT_EXISTED
        }
      }

      let updateBarcode = await this.barcodeRepository.update({ id: data.id }, data);
      return {
        data: UtilsService.plainToClass(TransformBarcodeDto, updateBarcode, { excludeExtraneousValues: false }),
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

}
