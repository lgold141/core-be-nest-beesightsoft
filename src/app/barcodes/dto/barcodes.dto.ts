import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { AbstractDto, PaginationParams } from '@nest-core/core';
import { BarcodeEntity } from '../barcodes.entity';

export class TransformBarcodeDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  countryCode: string;

  @ApiProperty()
  @Expose()
  lastedBarcode: string;
}

export class CreateBarcodeDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  countryCode: string;

  @ApiProperty()
  @Expose()
  lastedBarcode: string;
}

export class UpdateBarcodeDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  countryCode: string;

  @ApiProperty()
  @Expose()
  lastedBarcode: string;
}

export class FindBarcodeDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id?: string;

  @ApiProperty()
  @Expose()
  countryCode?: string;

  @ApiProperty()
  @Expose()
  lastedBarcode?: string;
}

export class SearchBarcodeDto extends PaginationParams<BarcodeEntity> {
  @ApiProperty({
    example: ['countryCode', 'lastedBarcode']
  })
  @Expose()
  select: any;

  @ApiProperty({
    example: { search: "Cong Hoa" }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
