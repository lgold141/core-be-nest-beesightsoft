import { Entity, Column } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IBarcodeInterface } from './interfaces/barcodes.interface';

@Entity('barcode')
export abstract class BarcodeEntity extends AbstractEntity implements IBarcodeInterface {
  @Column()
  countryCode: string;

  @Column()
  lastedBarcode: string;
}
