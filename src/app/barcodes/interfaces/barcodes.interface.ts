import {IEntityInterface} from '@nest-core/shared';

export interface IBarcodeInterface extends IEntityInterface {
  countryCode: string;
  lastedBarcode: string;
}