import { DynamicModule, Global, Module } from '@nestjs/common';
import { EmailService } from './email.service';

import { EmailModuleOptions } from './interfaces/email-options.interface';

const defaultConfig = {
  transport: {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SECURE,
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD
    }
  },
  defaults: {
    forceEmbeddedImages: false,
    from: undefined,
  },
  templateDir: process.env.MAIL_TEMPLATE_DIR,
};

@Global()
@Module({})
export class EmailCoreModule {
  static forRoot(userConfig: EmailModuleOptions): DynamicModule {
    const EmailConfig = EmailCoreModule.getEmailConfig(userConfig);
    return {
      module: EmailCoreModule,
      providers: [EmailService, EmailConfig],
      exports: [EmailService],
    };
  }
  static getEmailConfig(userConfig?: EmailModuleOptions) {
    const config: EmailModuleOptions = { ...defaultConfig, ...userConfig };
    return {
      name: 'EMAIL_CONFIG',
      provide: 'EMAIL_CONFIG',
      useValue: {
        transport: config.transport,
        defaults: config.defaults,
        templateDir: config.templateDir,
      } as EmailModuleOptions,
    };
  }
}
