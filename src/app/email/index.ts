export { EmailModule } from './email.module';
export { EmailModuleOptions } from './interfaces/email-options.interface';
export { EmailService } from './email.service';
export { EmailCoreModule } from './email-core.module'