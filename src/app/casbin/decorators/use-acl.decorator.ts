import {SetMetadata} from '@nestjs/common';
import {IAclResourceInterface} from '../role';
import {CASBIN_ACL_RESOURCE_TOKEN} from '../casbin.constants';

export const UseACL = (acl: IAclResourceInterface) => SetMetadata(CASBIN_ACL_RESOURCE_TOKEN, acl);
