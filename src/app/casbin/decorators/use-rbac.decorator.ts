import {SetMetadata} from '@nestjs/common';
import {IRbacResourceInterface} from '../role';
import {CASBIN_RBAC_RESOURCE_TOKEN} from '../casbin.constants';

export const UseRBAC = (...rbac: IRbacResourceInterface[]) => SetMetadata(CASBIN_RBAC_RESOURCE_TOKEN, rbac);
