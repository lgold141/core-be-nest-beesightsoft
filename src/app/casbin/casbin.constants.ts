export const CASBIN_ENFORCER = 'CASBIN_ENFORCER';
export const CASBIN_ACL_RESOURCE_TOKEN = 'CASBIN_ACL_RESOURCE_TOKEN';
export const CASBIN_RBAC_RESOURCE_TOKEN = 'CASBIN_RBAC_RESOURCE_TOKEN';

export enum AccessActionEnum {
  create = 'create',
  read = 'read',
  update = 'update',
  delete = 'delete'
}
