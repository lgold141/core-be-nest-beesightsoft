import {CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {PinoAppLoggerService} from '@nest-core/shared';
import {IRbacResourceInterface} from '../interface';
import {CASBIN_RBAC_RESOURCE_TOKEN} from '../../casbin.constants';
import {CasbinService} from '../../casbin.service';
import {flatten} from 'lodash';

@Injectable()
export class RbacResourceGuard<User extends any = any> implements CanActivate {
  constructor(private readonly reflector: Reflector,
              private readonly casbinService: CasbinService,
              @Inject(PinoAppLoggerService) private logger: PinoAppLoggerService) {
    this.logger.setContext(RbacResourceGuard.name);
  }

  protected async getUser(context: ExecutionContext): Promise<User> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) throw new UnauthorizedException();
    return user;
  }

  private async isGranted(sub: string, rbacResources: IRbacResourceInterface[]): Promise<boolean> {
    const permissions = flatten(rbacResources.map((rbacResource: IRbacResourceInterface) => {
      return rbacResource.actions.map((action: string) => {
        return [
          sub,
          rbacResource.resource,
          action
        ]
      });
    }));

    for (let permission of permissions) {
      const hasPermission = await this.casbinService.checkPermission(...permission);
      if (!hasPermission) {
        return false
      }
    }
    return true;
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const methodRbacResources = this.reflector.get<IRbacResourceInterface[]>(CASBIN_RBAC_RESOURCE_TOKEN, context.getHandler()) || [];
    const classRbacResources = this.reflector.get<IRbacResourceInterface[]>(CASBIN_RBAC_RESOURCE_TOKEN, context.getClass()) || [];
    const rbacResources: IRbacResourceInterface[] = [...methodRbacResources, ...classRbacResources] as IRbacResourceInterface[];
    const user = await this.getUser(context);
    const sub = user.username || '';
    return await this.isGranted(sub, rbacResources);
  }
}
