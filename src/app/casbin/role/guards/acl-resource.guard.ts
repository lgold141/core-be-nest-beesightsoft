import {CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {PinoAppLoggerService} from '@nest-core/shared';
import {IAclResourceInterface} from '../interface';
import {CASBIN_ACL_RESOURCE_TOKEN} from '../../casbin.constants';
import {CasbinService} from '../../casbin.service';

@Injectable()
export class AclResourceGuard<User extends any = any> implements CanActivate {
  constructor(private readonly reflector: Reflector,
              private readonly casbinService: CasbinService,
              @Inject(PinoAppLoggerService) private logger: PinoAppLoggerService) {
    this.logger.setContext(AclResourceGuard.name);
  }

  protected async getUser(context: ExecutionContext): Promise<User> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) throw new UnauthorizedException();
    return user;
  }

  private async isGranted(sub: string, aclResource: IAclResourceInterface): Promise<boolean> {
    if (!aclResource || (aclResource.roles && aclResource.roles.length === 0)) {
      return true;
    }

    const roles = aclResource.roles;
    const domain = aclResource.domain || null;
    const userRoles = await this.casbinService.getRolesForUser(sub, domain);
    // console.log(['AclResourceGuard.canActivate', {sub, roles, domain, userRoles}]);
    if (userRoles.length === 0) {
      return false;
    } else {
      const hasRole = () =>
        roles.every(role => !!userRoles.find(item => item === role));
      return hasRole();
    }
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const methodAclResource = this.reflector.get<IAclResourceInterface>(CASBIN_ACL_RESOURCE_TOKEN, context.getHandler()) || {
      roles: [],
      domain: null
    };
    const classAclResource = this.reflector.get<IAclResourceInterface>(CASBIN_ACL_RESOURCE_TOKEN, context.getClass()) || {
      roles: [],
      domain: null
    };
    const aclResource: IAclResourceInterface = {...methodAclResource, ...classAclResource} as IAclResourceInterface;
    const user = await this.getUser(context);
    const sub = user.username || '';
    return await this.isGranted(sub, aclResource);
  }
}
