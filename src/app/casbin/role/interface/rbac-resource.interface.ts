import {AccessActionEnum} from '../../casbin.constants';

export interface IRbacResourceInterface {
  resource: string,
  actions: AccessActionEnum[]
}
