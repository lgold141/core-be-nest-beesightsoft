export interface IAclResourceInterface {
  roles: string[],
  domain?: string
}
