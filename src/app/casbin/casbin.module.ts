import {DynamicModule, Module, Provider, Global} from '@nestjs/common';
import {Adapter, newEnforcer} from 'casbin';
import {ConnectionOptions} from 'typeorm';
import TypeORMAdapter from 'typeorm-adapter';
import {CASBIN_ENFORCER} from './casbin.constants';
import {CasbinService} from './casbin.service';
import {RoleService, AclResourceGuard, RbacResourceGuard} from './role';

@Global()
@Module({})
export class CasbinModule {
  public static forRootAsync(casbinModelPath: string,
                             dbConnectionOptions: ConnectionOptions): DynamicModule {
    const casbinEnforcerProvider: Provider = {
      provide: CASBIN_ENFORCER,
      useFactory: async () => {
        const adapter = await TypeORMAdapter.newAdapter(dbConnectionOptions);
        const enforcer = await  newEnforcer(
          casbinModelPath,
          (adapter as any) as Adapter
        );
        await enforcer.loadPolicy();
        return enforcer;
      }
    };
    return {
      module: CasbinModule,
      providers: [
        casbinEnforcerProvider,
        CasbinService,
        RoleService,
        AclResourceGuard,
        RbacResourceGuard
      ],
      exports: [
        casbinEnforcerProvider,
        CasbinService,
        RoleService,
        AclResourceGuard,
        RbacResourceGuard
      ],
    };
  }
}
