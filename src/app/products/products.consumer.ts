import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';
import { ProductService } from './products.service';
import { CreateProductDto } from './dto';

export interface IJob {
  userId: string;
  data: CreateProductDto[];
}
@Processor('products')
export class ProductConsumer {
  constructor(private productService: ProductService,) { }

  @Process('createProduct')
  async createProduct(job: Job<IJob>) {
    try {
      const productData = job.data as any;
      await this.productService.createProduct(productData.userId, productData.data, productData.images);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

}