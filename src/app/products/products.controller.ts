import { Controller, UseGuards, Get, Post, Put, Delete, Body, UseInterceptors, UploadedFile, Param, Query, UploadedFiles, Req, Res } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor, FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { ProductEntity } from './products.entity';
import { CreateProductDto, SearchProductDto, TransformProductDto, UpdateProductDto, PrintLabelDto, IProductIds } from './dto';
import { ProductService } from './products.service';
import { fileInterceptor, excelMulterOptions } from '../../utils/file-uploading.utils';
import { ImageService } from '../images/images.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Products')
@Controller()
@CrudOptionsDtoDecorator({
  resourceName: 'Product',
  createDto: CreateProductDto,
  updateDto: UpdateProductDto,
  transformDto: TransformProductDto,
  searchDto: SearchProductDto
})
export class ProductController extends CrudController<ProductEntity>  {
  constructor(private readonly productService: ProductService,
    private readonly imageService: ImageService) {
    super(productService);
  }

  @Get('checkBarcode')
  async checkBarcode(@Query("barcode") barcode: string) {
    try {
      return await this.productService.checkBarcode(barcode);
    } catch (e) {
      return e;
    }
  }

  @Get('checkSKU')
  async checkSKU(@Req() req, @Query("sku") sku: string) {
    try {
      return await this.productService.checkSKU(req.user.id, sku);
    } catch (e) {
      return e;
    }
  }

  @Get('downloadTemplate')
  async downloadTemplate(@Res() res): Promise<void> {
    try {
      const downloadPath = await this.productService.downloadTemplate();
      return res.download(downloadPath);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Get('generateRFID')
  async generateRFID(@Query("barcode") barcode: string) {
    try {
      return await this.productService.generateRFID(barcode);
    } catch (e) {
      return e;
    }
  }

  @UseGuards(ComposeSecurity)
  @Get('generateBarcode')
  async generateBarcode(@Req() req, @Query("count") count?: number) {
    try {
      return await this.productService.generateBarcode(req.user.id, count);
    } catch (e) {
      return e;
    }
  }

  @UseGuards(ComposeSecurity)
  @Get('generateSKU')
  async generateSKU(@Query('productName') productName: string, @Query('category') category: string, @Req() req) {
    try {
      return await this.productService.generateSKU(productName, category, req.user.id);
    } catch (e) {
      return e;
    }
  }

  @UseGuards(ComposeSecurity)
  @Post('importFile')
  @UseInterceptors(FileInterceptor('file', excelMulterOptions))
  async importFile(@Req() req, @UploadedFile() file): Promise<any> {
    try {
      let importFile = await this.productService.importFile(req, file.path);
      this.respondWithErrorsOrSuccess(importFile);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post('exportFile')
  async exportFile(@Res() res, @Req() req, @Body() productIds: IProductIds): Promise<any> {
    try {
      let exportProduct = await this.productService.exportFile(req, productIds);

      if (exportProduct.code) {
        this.respondWithErrorsOrSuccess(exportProduct);
      } else {
        return res.download(exportProduct)
      }
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post()
  @UseInterceptors(AnyFilesInterceptor())
  async createProduct(@Req() req, @Body() data: CreateProductDto, @UploadedFiles() files): Promise<void> {
    try {
      let product = await this.productService.createProduct(req.user.id, data, files);
      this.respondWithErrorsOrSuccess(product);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Get(":id")
  async getProduct(@Req() req, @Param("id") id: string): Promise<void> {
    try {
      const userId = req.user.id;
      let product = await this.productService.getProduct(userId, id);
      this.respondWithErrorsOrSuccess(product);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post("findAll")
  async getAllProduct(@Req() req, @Body() filter: SearchProductDto): Promise<void> {
    try {
      const userId = req.user.id;
      let products = await this.productService.getAllProduct(userId, filter);
      this.respondWithErrorsOrSuccess(products);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Put(":id")
  @UseInterceptors(AnyFilesInterceptor())
  async updateProduct(@Req() req, @Param("id") id: string, @Body() product: UpdateProductDto, @UploadedFiles() files): Promise<void> {
    try {
      product.id = id;
      let update = await this.productService.updateProduct(req, product, files);
      this.respondWithErrorsOrSuccess(update);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Delete(":id")
  async deleteProduct(@Param("id") ids: string[]): Promise<void> {
    try {
      let product = await this.productService.deleteProduct(ids);
      this.respondWithErrorsOrSuccess(product);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post('printLabel')
  async printZpl(@Body() data: PrintLabelDto): Promise<void> {
    try {
      const zpl = await this.productService.printLabel(data);
      this.respondWithErrorsOrSuccess(zpl);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
