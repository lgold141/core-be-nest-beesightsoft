import { Entity, Tree, Column, TreeChildren, TreeParent } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, IsNumber, IsBoolean, Length } from 'class-validator';
import { Index, JoinColumn, ManyToMany, JoinTable, ManyToOne } from 'typeorm';
import { IProductInterface } from './interfaces/products.interface';
import { ImagesEntity } from '../images';
import { CategoryEntity } from '../categories/categories.entity';
import { CompanyEntity } from '../companies/companies.entity';

@Entity('products')
@Tree("nested-set")
export abstract class ProductEntity extends AbstractEntity implements IProductInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Length(1, 128)
  @Column('text', { nullable: true })
  name: string;

  @Column({ nullable: true })
  categoryId: string;

  @ManyToOne(type => CategoryEntity, category => category.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "categoryId" })
  category: CategoryEntity;

  @IsString()
  @Index()
  @Column({ nullable: true })
  sku: string;

  @Column({ nullable: true })
  barcode: string;

  @Column({ nullable: true })
  rfid: string;

  @Column({ nullable: true })
  unit: string

  @IsNumber()
  @Column({ nullable: true, type: 'numeric' })
  quantity: number;

  @IsNumber()
  @Column({ nullable: true, type: 'numeric' })
  price: number;

  @Column({ nullable: true })
  option: string;

  @IsBoolean()
  @Column({ nullable: true, default: false })
  isVariation: boolean;

  @Column({ nullable: true })
  variationType: string;

  @Column({ nullable: true })
  variationName: string;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  images: ImagesEntity[];

  @Column()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.products, { eager: true })
  @JoinColumn({ name: "companyId" })
  company: CompanyEntity;

  @TreeChildren()
  children: ProductEntity[];

  @TreeParent()
  parent: ProductEntity;

}
