import {IEntityInterface} from '@nest-core/shared';
import {ImagesEntity} from '../../images';

export interface IProductInterface extends IEntityInterface {
  name: string;
  categoryId: string;
  sku: string;
  barcode: string;
  rfid: string;
  unit: string;
  quantity: number;
  price: number;
  variationType: string;
  variationName: string;
  images: ImagesEntity[];
}