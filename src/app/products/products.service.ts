import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { ProductEntity } from './products.entity';
import { CreateProductDto, CreateVariationDto, UpdateProductDto, TransformProductDto, SearchProductDto, toZplDto, PrintLabelDto, UpdateVariationDto, TransformVariationDto, IProductIds } from './dto';
import { CodeEnum, ApiErrorCodeService } from '@app/api-error-code';
import { ImageService, ImagesEntity } from '../images';
import { UtilsService } from '@nest-core/shared';
import { getImageUrl, genratePath, fileInterceptor, saveImage } from '../../utils/file-uploading.utils';
import { toSgtin96TagUri, sgtin96TagUriToHex } from '../../utils/barcode-epc';
import { printZpl } from '../../utils/zpl-printer';
import { readFileExcell } from '../../utils/readFile.utils';
import { findDifferentArray, findMatchArray } from '../../utils/compareArray.utils';
import { CategoryService, CategoryEntity } from '../categories';
import { IsNull, Not, Brackets, In } from 'typeorm';
import { formatOrder, formatSelect, Quantity } from '../../utils/filter.utils';
import { BarcodeService } from '../barcodes/barcodes.service';
import { FindBarcodeDto, TransformBarcodeDto, CreateBarcodeDto, UpdateBarcodeDto } from '../barcodes/dto';
import { exportProductsToExcel } from '../../utils/exportExcel.utils';

import { UserService } from '../user';
import environment from "@app/environment";
const path = require("path");
const fs = require("fs");

@Injectable()
export class ProductService extends CrudService<ProductEntity>   {
  constructor(@InjectRepository(ProductEntity, DB_CONNECTION_DEFAULT) private readonly productRepository: AbstractBaseRepository<ProductEntity>,
    @InjectQueue('products') private productQueue: Queue,
    private categoryService: CategoryService,
    private imageService: ImageService,
    private barcodeService: BarcodeService,
    private userService: UserService) {
    super(productRepository);
  }

  async createNewProduct(userId: string, product: CreateProductDto, images?: any): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    this.productQueue.add('createProduct', { userId: userId, data: product, images: images });
    return {
      data: UtilsService.plainToClass(TransformProductDto, product, { excludeExtraneousValues: false })
    }
  }

  async createProduct(userId: string, product: CreateProductDto, images?: any): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      await this.validateProduct(userId, product);
      let { code } = await this.validateProduct(userId, product);
      if (code) {
        return {
          code: code
        }
      }

      if (!product.sku) {
        product.sku = await this.generateSKU(product.name);
      }

      const user = await this.userService.findUser(userId);
      product.companyId = user.data.company.id;

      let createProduct;
      if (!product.variations) {
        if (images) {
          if (images.length > 4) {
            return {
              code: CodeEnum.PRODUCT_LIMIT_IMAGES
            }
          }

          product.images = await this.createImageProduct(images);
        }

        if (product.barcode) {
          if (product.barcode.length != 8) {
            return {
              code: CodeEnum.BARCODE_LENGTH
            }
          }
          if (await this.checkBarcode(product.barcode)) {
            return {
              code: CodeEnum.BARCODE_EXISTED
            }
          }
        } else {
          product.barcode = await this.generateBarcode(userId);
        }

        product.rfid = await this.generateRFID(product.barcode);

        const getBarcodeCountry = await this.getBarcodeCountry(userId);
        const getLastestBarcode = await this.getLastedBarcode(getBarcodeCountry) as any;

        if (getLastestBarcode && !getLastestBarcode.code) {
          await this.barcodeService.update(getLastestBarcode.data.id, { lastedBarcode: product.barcode })
        } else {
          let barcode = {
            countryCode: getBarcodeCountry,
            lastedBarcode: product.barcode
          } as CreateBarcodeDto
          await this.barcodeService.createBarcode(barcode)
        }

        createProduct = await this.productRepository.save(product);
      } else {
        delete product.barcode;

        product.quantity = 0;
        JSON.parse(product.variations).forEach(variation => product.quantity += parseInt(variation.quantity));
        createProduct = await this.productRepository.save(product);
        await this.createVariation(userId, createProduct, JSON.parse(JSON.stringify(product.variations)), images)
      }

      return {
        data: UtilsService.plainToClass(TransformProductDto, createProduct, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getProduct(userId: string, id: string): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist.data.companyId;
      let getProduct = await this.findData({
        where: { id: id, companyId: userCompanyId },
        relations: ["images", "children", "category"]
      });
      if (!getProduct) {
        return {
          code: CodeEnum.PRODUCT_NOT_EXISTED
        }
      }

      getProduct.images = getProduct.images.length > 0 ? await getImageUrl(getProduct.images, true) : [];
      getProduct.option = getProduct.option ? JSON.parse(getProduct.option) : null;
      if (getProduct.children) {
        for (let i = 0; i < getProduct.children.length; i++) {
          getProduct.children[i].option = getProduct.children[i].option ? JSON.parse(getProduct.children[i].option) : null;
          getProduct.children[i].images = await getImageUrl(getProduct.children[i].images, true);
        }
      }
      return {
        data: UtilsService.plainToClass(TransformProductDto, getProduct, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getAllProduct(userId?: string, filter?: SearchProductDto): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      let getAllProduct: ProductEntity[];
      const table = 'product';
      let total = 0;
      if (filter) {
        let searchProduct = filter.where?.search ?? '';
        let categoryName = filter.where?.categoryName ?? '';
        let categoryId = filter.where?.categoryId;
        let quantity = filter.where?.quantity;
        let select = formatSelect(filter.select, table);
        let order = formatOrder(filter.order, table);
        [getAllProduct, total] = await this.productRepository.createQueryBuilder(table)
          .select(select)
          .leftJoinAndSelect(`${table}.children`, 'children')
          .leftJoinAndSelect('children.images', 'children_images')
          .leftJoinAndSelect(`${table}.category`, 'category')
          .leftJoinAndSelect(`${table}.images`, 'images')
          .leftJoinAndSelect(`${table}.company`, 'company')
          .where(new Brackets(qb => {
            qb.where(`${table}.isVariation = false`);
            if (searchProduct) {
              qb.andWhere(new Brackets(qbSearchProduct => {
                qbSearchProduct.where(`${table}.name ILIKE '%${searchProduct}%'`)
                  .orWhere(`${table}.sku ILIKE '%${searchProduct}%'`)
                  .orWhere(`${table}.barcode ILIKE '%${searchProduct}%'`);
              }))
            }
            if (Array.isArray(categoryId) && categoryId.length) {
              qb.andWhere(`${table}.categoryId IN(:...categoryId)`, { categoryId });
            }
            if (userCompanyId) {
              qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
            }
            if (categoryName) {
              qb.andWhere(`category.name ILIKE '%${categoryName}%'`);
            }
            if (quantity) {
              switch (quantity) {
                case Quantity.LOW_OF_STOCK:
                  qb.andWhere(`${table}.quantity > 0`);
                  break;
                case Quantity.OUT_OF_STOCK:
                  qb.andWhere(`${table}.quantity = 0`);
                  break;
              }
            }
          }))
          .take(filter.take)
          .skip(filter.skip)
          .orderBy(order)
          .getManyAndCount();
      } else {
        [getAllProduct, total] = await this.productRepository.createQueryBuilder(table)
          .leftJoinAndSelect(`${table}.children`, 'children')
          .leftJoinAndSelect('children.images', 'children_images')
          .leftJoinAndSelect(`${table}.category`, 'category')
          .leftJoinAndSelect(`${table}.images`, 'images')
          .leftJoinAndSelect(`${table}.company`, 'company')
          .where(new Brackets(qb => {
            qb.where(`${table}.isVariation = false`);
            if (userCompanyId) {
              qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
            }
          }))
          .getManyAndCount();
      }
      for (let i = 0; i < getAllProduct.length; i++) {
        getAllProduct[i].images = getAllProduct[i].images ? await getImageUrl(getAllProduct[i].images, true) : [];
        getAllProduct[i].option = getAllProduct[i].option ? JSON.parse(getAllProduct[i].option) : null;
        if (getAllProduct[i].children) {
          for (let j = 0; j < getAllProduct[i].children.length; j++) {
            getAllProduct[i].children[j].option = getAllProduct[i].children[j].option ? JSON.parse(getAllProduct[i].children[j].option) : null;
            getAllProduct[i].children[j].images = getAllProduct[i].children[j].images ? await getImageUrl(getAllProduct[i].children[j].images, true) : [];
          }
        }
      }
      return {
        data: UtilsService.plainToClass(TransformProductDto, getAllProduct, { excludeExtraneousValues: false }),
        options: { total: total }
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateProduct(req, product: UpdateProductDto, images?: any): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      if (product.sku) {
        let user = await this.userService.findUser(req.user.id);
        let checkSKU = await this.productRepository.findOne({ sku: product.sku, companyId: user.data?.companyId });
        if (checkSKU && checkSKU.id !== product.id) {
          return {
            code: CodeEnum.SKU_EXISTED
          }
        }
      }

      let productExisted = await this.findData({ id: product.id }, { relations: ['images', 'children', 'children.images'] });
      if (!productExisted) {
        return {
          code: CodeEnum.PRODUCT_NOT_EXISTED
        }
      }

      if (!product.variations) {
        if (productExisted.children.length > 0) {
          for (let i = 0; i < productExisted.children.length; i++) {
            await this.productRepository.delete({ id: productExisted.children[i].id });
          }
        } else {
          if (images) {
            if (images.length > 4) {
              return {
                code: CodeEnum.PRODUCT_LIMIT_IMAGES
              }
            }

            product.images = await this.uploadImageProduct(productExisted, images)
          } else {
            for (let i = 0; i < productExisted.images.length; i++) {
              await this.imageService.delete(productExisted.images[i].id)
            }
          }
        }
      } else {
        product.quantity = 0;
        const updateVariations = product.variations ? JSON.parse(product.variations) : [];
        updateVariations.forEach((variation, idx) => {
          product.quantity += parseInt(variation.quantity);
          variation.index = idx
        });
        await this.updateVariation(req, productExisted, updateVariations, images)
      }

      let updateProduct = await this.productRepository.save(product);
      return {
        data: UtilsService.plainToClass(TransformProductDto, updateProduct, { excludeExtraneousValues: false })
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteProduct(ids: string[]): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      ids = ids.toString().split(',');
      await this.productRepository.delete({ parent: In(ids) });
      await this.productRepository.delete({ id: In(ids) });
      return {
        data: UtilsService.plainToClass(TransformProductDto, {}, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async checkBarcode(barcode: string): Promise<boolean> {
    try {
      let barcodeExist = await this.findData({ barcode: barcode });
      if (barcodeExist) {
        return true;
      }
      return false;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async checkSKU(userId: string, sku: string): Promise<boolean> {
    try {
      let user = await this.userService.findUser(userId);
      let skuExist = await this.findData({ sku: sku, companyId: user.data?.companyId });
      if (skuExist) {
        return true;
      }
      return false;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async generateRFID(barcode: string): Promise<string> {
    try {
      let itemRef;
      const serial = Math.floor(10000 + Math.random() * 999999);
      switch (barcode.length) {
        case 8: itemRef = barcode.slice(2, 7);
      }
      // let generateRFID = `${barcode}${itemRef}${serial}`;

      const sgtin96TagUri = await toSgtin96TagUri(
        barcode.toString(),
        itemRef.toString(),
        serial.toString()
      );
      const generateRFID = await sgtin96TagUriToHex(sgtin96TagUri);

      return generateRFID;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async generateBarcode(userId?, count?: number): Promise<any> {
    try {
      let barcodeOfUser = await this.getBarcodeCountry(userId);
      barcodeOfUser = barcodeOfUser.split("–")[0];
      let barcodeExist = true;
      let barcode;
      let generateBarcode;
      let generateCode = [];

      const lastedBarcode = await this.getLastedBarcode(barcodeOfUser) as any;

      // let lastestProduct = await this.productRepository.findOne({
      //  where: {
      //    barcode: Not(IsNull()),
      //   }, order: { barcode: 'DESC' }
      // })

      if (lastedBarcode && !lastedBarcode.code) {
        barcode = lastedBarcode.data.lastedBarcode.slice(0, 7);
        barcode = parseInt(barcode) + 1;
      } else {
        for (let i = 0; i < (7 - barcodeOfUser.length); i++) {
          generateCode.push(0)
        }
        barcode = `${barcodeOfUser}${generateCode.join('')}`;
      }

      if (!count) {
        while (barcodeExist) {
          generateBarcode = await this.eanCheckDigit(barcode.toString());
          barcodeExist = await this.findData({ barcode: generateBarcode }) === undefined ? false : true;
          barcode = parseInt(barcode) + 1;
        }
      } else {
        if (count < 1) {
          return {
            code: CodeEnum.COUNT_MUST_GREATER_1
          }
        }
        generateBarcode = [];

        for (let i = 0; i < count; i++) {
          barcodeExist = true;
          let genBarcode = await this.eanCheckDigit(barcode.toString());
          while (barcodeExist) {
            barcodeExist = await this.findData({ barcode: generateBarcode }) === undefined ? false : true;
            if (barcodeExist) {
              barcode = parseInt(barcode) + 1;
            }
          }

          generateBarcode.push(genBarcode);
          barcode = parseInt(barcode) + 1;
        }
      }
      return generateBarcode;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async generateSKU(productName: string, category?: string, userId?: string): Promise<string> {
    try {
      let user = await this.userService.findOne({ id: userId });
      if (user.language === 'ar') {
        productName = productName.slice(-2);
        category = category.slice(-2);
      }
      else {
        productName = productName.slice(0, 2);
        category = category.slice(0, 2);
      }
      if (productName.length === 1) {
        productName = `${productName}${productName}`;
      }
      if (category.length === 1) {
        category = `${category}${category}`;
      }
      let skuExist = true;
      let generateSKU;
      let uniqueCode = 90000;
      while (skuExist) {
        let rawString = `${category}${productName}`;
        generateSKU = `${rawString.slice(0, 4)}${uniqueCode.toString().slice(1, 5)}`;
        generateSKU = generateSKU.toUpperCase();
        skuExist = await this.findData({ sku: generateSKU }) === undefined ? false : true;
        if (skuExist) {
          uniqueCode = uniqueCode + 1;
        }
      }
      return generateSKU;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async printLabel(data: PrintLabelDto): Promise<any> {
    try {
      let zplTemplates = [];
      let zplData = data.data;
      for (let i = 0; i < zplData.length; i++) {
        for (let j = 0; j < zplData[i].quantity; j++) {
          if (!zplData[i].variationName) {
            delete zplData[i].variationName
          }
          let template = await printZpl(zplData[i])
          zplTemplates.push(template)
        }
      }

      return {
        data: zplTemplates
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async importFile(req, path): Promise<any> {
    try {
      let products = await readFileExcell(path);
      let errorCodes = [];
      const userExist = await this.userService.findUserNoRelations(req.user.id);
      const userCompanyId = userExist.data.companyId;
      const categoryConditions = {
        where: { companyId: userCompanyId }
      }
      const allCategories = await this.categoryService.findAll(categoryConditions);
      const getUncategorized = await this.categoryService.getUncategory(req.user.id);
      let createProductWithoutVariant = [];
      let createProductWithVariant = [];

      for (let i = 0; i < products.length; i++) {
        let { code } = await this.validateProduct(req.user.id, products[i]);
        if (code) {
          errorCodes.push({ line: i + 1, errorCode: code });
          continue;
        }

        let getCategory = allCategories.items.filter(category => category.key === products[i].category);
        if (getCategory.length > 0) {
          products[i].categoryId = getCategory[0].id;
        } else {
          products[i].categoryId = getUncategorized.id;
        }

        if (products[i].variations) {
          products[i].variations = JSON.stringify(products[i].variations);
          createProductWithVariant.push(products[i])
        } else {
          createProductWithoutVariant.push(products[i])
        }
      }

      if (errorCodes.length > 0) {
        return {
          code: { error: errorCodes }
        }
      }

      for (let i = 0; i < createProductWithoutVariant.length; i++) {
        this.productQueue.add('createProduct', { userId: req.user.id, data: createProductWithoutVariant[i] });
      }

      for (let i = 0; i < createProductWithVariant.length; i++) {
        this.productQueue.add('createProduct', { userId: req.user.id, data: createProductWithVariant[i] });
      }

      // Remove file
      fs.unlink(path, (e) => {
        if (e) {
          console.log(e);
          throw e;
        }
      });
      return {
        data: products
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async downloadTemplate(): Promise<any> {
    try {
      const filePath = `${__dirname.replace("dist", "")}${process.env.PRODUCT_TEMPLATES_PATH}`;
      return filePath;
    } catch (e) {
      throw e;
    }
  }

  async exportFile(req, ids: IProductIds): Promise<any> {
    try {
      let productIds = ids.productIds;
      const productPath = path.join(__dirname.replace("dist", ""), `${environment.productExport.path}/product-list-${new Date().getTime()}.xlsx`)
      var exportProduct;
      let allProduct = await this.getAllProduct(req.user.id) as any;
      if (productIds?.length) {
        let products = await this.filterProduct(productIds, allProduct.data);
        exportProduct = await exportProductsToExcel(products);
      } else {
        exportProduct = await exportProductsToExcel(allProduct.data);
      }

      await fs.writeFileSync(productPath, exportProduct)
      return await productPath;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async countProductWithOptionId(userCompanyId: string, optionId: string): Promise<number> {
    const table = 'product';
    const products = await this.productRepository.createQueryBuilder(table)
      .leftJoinAndSelect(`${table}.children`, 'children')
      .where(new Brackets(qb => {
        qb.where(`${table}.isVariation = false`);
        if (userCompanyId) {
          qb.andWhere(`${table}.companyId = '${userCompanyId}'`);
        }
      }))
      .getMany();
    let total = 0;
    products.forEach(product => {
      const productOptions = JSON.parse(product.option);
      productOptions?.forEach(option => {
        if(option.id === optionId){
          total += 1;
        }
      })
    })
    return total;
  }

  async getCountVariation(userCompanyId: string, value: string): Promise<number> {
    const table = 'product';
    const products = await this.productRepository.createQueryBuilder(table)
      .leftJoinAndSelect(`${table}.parent`, 'parent')
      .where(`parent.companyId = '${userCompanyId}'`)
      .getMany();
    let total = 0;
    products.forEach(product => {
      const arrVariationNames = product.variationName.split(',');
      arrVariationNames.forEach(name => {
        if (name.toLowerCase() === value.toLowerCase()) {
          total += 1;
        }
      })
    })
    return total;
  }

  async countVariation(userCompanyId: string, optionName: string, value: string): Promise<number> {
    const table = 'product';
    const products = await this.productRepository.createQueryBuilder(table)
      .leftJoinAndSelect(`${table}.parent`, 'parent')
      .where(`parent.companyId = '${userCompanyId}'`)
      .getMany();
    let total = 0;
    products.forEach(product => {
      const productOptions = JSON.parse(product.parent.option);
      if (productOptions?.some(option => option.name.toLowerCase() === optionName.toLowerCase())) {
        const arrVariationNames = product.variationName.split(',');
        arrVariationNames.forEach(name => {
          if(name.toLowerCase() === value.toLowerCase()) {
            total += 1;
          }
        })
      }
    })
    return total;
  }

  //****************** START HELPERS ******************//
  //***************************************************//
  async createVariation(userId, product: ProductEntity, variationsString: string, variationImages: any): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      let variations = JSON.parse((variationsString));
      let bulk = [];

      for (let i = 0; i < variations.length; i++) {
        let variation = new CreateVariationDto();
        variation.isVariation = true;

        if (variationImages && variationImages[i]) {
          variation.images = await this.createImageVariant(variationImages[i]);
        }

        if (variations[i].sku) {
          delete variations[i].sku;
        }

        if (variations[i].barcode) {
          if (variations[i].barcode.length != 8) {
            return {
              code: CodeEnum.BARCODE_LENGTH
            }
          }
          if (await this.checkBarcode(variations[i].barcode)) {
            return {
              code: CodeEnum.BARCODE_EXISTED
            }
          }
          variation.barcode = variations[i].barcode;

        } else {
          variation.barcode = await this.generateBarcode(userId);
        }

        variation.rfid = await this.generateRFID(variation.barcode);
        variation.variationName = variations[i].variationName;
        variation.quantity = variations[i].quantity;
        variation.option = JSON.stringify(variations[i].option);
        variation.parent = product;

        bulk.push(variation)

        // Update lasted barcode
        await this.updateLastedBarcode(userId, variation.barcode);
      }
      await this.productRepository.save(bulk);

      return {
        data: UtilsService.plainToClass(TransformProductDto, null, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateVariation(req, product: ProductEntity, variations: TransformVariationDto[], variationImages?: any): Promise<IApiServiceResponseInterface<TransformProductDto>> {
    try {
      var i;
      if (product.children.length === 0) {
        //Remove image
        for (let i = 0; i < product.images.length; i++) {
          await this.imageService.delete({ id: product.images[i].id });
        }

        await this.createVariation(req.user.id, product, JSON.stringify(variations), variationImages);
      } else {
        let deletedVariation = product.children.filter(findDifferentArray(variations));
        let createNewVariation = variations.filter(findDifferentArray(product.children));
        var updateVariration = variations.filter(findMatchArray(product.children));

        for (i = 0; i < deletedVariation.length; i++) {
          await this.productRepository.delete({ barcode: deletedVariation[i].barcode });
          await this.deleteProductOptions(deletedVariation, product);
        }

        if (createNewVariation.length > 0) {
          let createImages = [];
          createNewVariation.forEach(variation => {
            createImages.push(variationImages[variation.index]);
          })

          await this.createVariation(req.user.id, product, JSON.stringify(createNewVariation), createImages);
        }

        let updateImages = [];
        if (updateVariration.length > 0) {
          updateVariration.forEach(variation => {
            updateImages.push(variationImages[variation.index]);
          })
        }
        for (let i = 0; i < updateVariration.length; i++) {
          let variation = new UpdateVariationDto();
          if (updateImages && updateImages[i]) {
            variation.images = await this.uploadImageVariant(updateVariration[i], updateImages[i]);
          }

          let getVariationId = await this.productRepository.findOne({ barcode: updateVariration[i].barcode })
          if (!getVariationId) {
            return {
              code: CodeEnum.PRODUCT_NOT_EXISTED
            }
          }
          variation.id = getVariationId.id;
          variation.variationName = updateVariration[i].variationName;
          variation.quantity = updateVariration[i].quantity;
          variation.option = JSON.stringify(updateVariration[i].option);

          await this.productRepository.save(variation);
        }
      }

      return {
        data: UtilsService.plainToClass(TransformProductDto, null, { excludeExtraneousValues: false })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findData(conditions: any, options?: any): Promise<any> {
    return await this.productRepository.findOne(conditions, options);
  }

  eanCheckDigit(s) {
    let result = 0;
    let i = 1;
    for (let counter = s.length - 1; counter >= 0; counter--) {
      result = result + parseInt(s.charAt(counter)) * (1 + (2 * (i % 2)));
      i++;
    }
    return `${s}${(10 - (result % 10)) % 10}`;
  }

  async getBarcodeCountry(userId): Promise<string> {
    let user = await this.userService.findUser(userId);
    const barcodeOfUser = user.data.company.barcodeCountry ? user.data.company.barcodeCountry : process.env.BARCODE;
    return barcodeOfUser;
  }

  async getLastedBarcode(barcodeCountry: string): Promise<TransformBarcodeDto> {
    let getCode = new FindBarcodeDto();
    getCode.countryCode = barcodeCountry;
    let lastedBarcode = await this.barcodeService.findBarcode(getCode);

    return lastedBarcode as TransformBarcodeDto;
  }

  async updateLastedBarcode(userId: string, barcode: string): Promise<TransformBarcodeDto> {
    const getBarcodeCountry = await this.getBarcodeCountry(userId);
    const getLastestBarcode = await this.getLastedBarcode(getBarcodeCountry) as any;
    let updateLastedBarcode;
    if (getLastestBarcode && !getLastestBarcode.code) {
      updateLastedBarcode = await this.barcodeService.update(getLastestBarcode.data.id, { lastedBarcode: barcode })
    } else {
      let updateBarcode = {
        countryCode: getBarcodeCountry,
        lastedBarcode: barcode
      } as UpdateBarcodeDto;
      updateLastedBarcode = await this.barcodeService.createBarcode(updateBarcode)
    }

    return updateLastedBarcode;
  }

  async createImageProduct(images: any): Promise<any> {
    let updateImg = [];
    for (let i = 0; i < images.length; i++) {
      if (images[i].size > 0 && images[i].mimetype !== 'application/octet-stream' && images[i].mimetype !== 'application/json') {
        let img = await this.imageService.upload(saveImage(images[i]));
        updateImg.push(img.data);
      }
    }
    return updateImg;
  }

  async createImageVariant(image: any): Promise<any> {
    let updateImg = [];
    if (image.size > 0 && image.mimetype !== 'application/octet-stream' && image.mimetype !== 'application/json') {
      let img = await this.imageService.upload(saveImage(image));
      updateImg.push(img.data);
    }
    return updateImg;
  }

  async uploadImageProduct(product: ProductEntity, images: any): Promise<any> {
    let updateImg = [];
    for (let i = 0; i < images.length; i++) {
      if (images[i].size === 0 || images[i].mimetype === 'application/octet-stream') {
        // Delete image
        for (let i = 0; i < product.images.length; i++) {
          this.imageService.delete(product.images[i].id)
        }
      } else if (images[i].mimetype === 'application/json') {
        // Non change image
        updateImg.push(product.images[i]);
      } else {
        // New/Update image
        let img = await this.imageService.upload(saveImage(images[i]));
        updateImg.push(img.data);
      }
    }

    return updateImg;
  }

  async uploadImageVariant(variation: TransformVariationDto, image: any): Promise<any> {
    let updateImg = [];
    if (image.size === 0 || image.mimetype === 'application/octet-stream') {
      // Delete image
      for (let i = 0; i < variation.images.length; i++) {
        this.imageService.delete(variation.images[i].id)
      }
    } else if (image.mimetype === 'application/json') {
      // Non change image
      let variationImage = await this.productRepository.findOne({ barcode: variation.barcode }, { relations: ["images"] })
      updateImg = variationImage.images
    } else {
      // New/Update image
      let img = await this.imageService.upload(saveImage(image));
      updateImg.push(img.data);
    }

    return updateImg;
  }

  async validateProduct(userId: string, product: CreateProductDto): Promise<any> {
    if (product.quantity < 0) {
      return {
        code: CodeEnum.QUANTITY_GREATER_ZERO
      }
    }

    if (product.price < 0) {
      return {
        code: CodeEnum.PRICE_GREATER_ZERO
      }
    }

    if (product.sku) {
      if (await this.checkSKU(userId, product.sku)) {
        return {
          code: CodeEnum.SKU_EXISTED
        }
      }
    }

    return false;
  }

  async filterProduct(productIds: string[], products: TransformProductDto[]): Promise<TransformProductDto[]> {
    let productFilter: TransformProductDto[] = [];
    for (let i = 0; i < products.length; i++) {
      productIds.forEach(async productId => {
        if (productId === products[i].id) {
          productFilter = await this.checkProductDuplicate(productFilter, products[i]);
        } else {

          products[i].children.forEach(async variant => {
            if (variant.id === productId) {
              productFilter = await this.checkVariantDuplicate(productFilter, products[i], variant)
            }
          });
        }
      })
    }
    return productFilter;
  }

  async checkProductDuplicate(productFilter: TransformProductDto[], product: TransformProductDto): Promise<TransformProductDto[]> {
    let duplicateProduct = -1;
    productFilter.forEach((filter, idx) => {
      if (product.id === filter.id) {
        duplicateProduct = idx;
      }
    })

    if (duplicateProduct == -1) {
      productFilter.push(product);
    } else {
      productFilter[duplicateProduct] = product;
    }

    return productFilter;
  }


  async checkVariantDuplicate(productFilter: TransformProductDto[], product: TransformProductDto, variation: ProductEntity): Promise<TransformProductDto[]> {
    let duplicateProduct = -1;
    productFilter.forEach((product, idx) => {
      if (product.id === product.id) {
        duplicateProduct = idx;
      }
    })

    if (duplicateProduct == -1) {
      let data = { ...product };
      data.children = [variation];
      productFilter.push(data as TransformProductDto);
    } else {
      productFilter[duplicateProduct].children.forEach(variant => {
        if (variation.id !== variant.id) {
          productFilter[duplicateProduct].children.push(variation)
        }
      })
    }

    return productFilter;
  }

  async deleteProductOptions(variations: ProductEntity[], product: ProductEntity): Promise<TransformProductDto[]> {
    // Delete product options
    // let productOptions = product.option ? JSON.parse(product.option) : [];
    // for (let i = 0; i < productOptions.length; i++) {
    //   productOptions[i].value.forEach(value => {

    //   })
    // }
    return null;
  }

  async addTotalProductToCategories(categories: CategoryEntity[]): Promise<void> {
    if (!categories.length) {
      return;
    }
    let idsCategory = [];
    categories.forEach(category => {
      idsCategory.push(category.id);
    });
    const table = 'product';
    let getAllProduct = await this.productRepository.createQueryBuilder(table)
      .select(`${table}.categoryId as category_id`)
      // .groupBy(`${table}.categoryId`)
      .where(new Brackets(qb => {
        if (Array.isArray(idsCategory) && idsCategory.length) {
          qb.andWhere(`${table}.categoryId IN(:...idsCategory)`, { idsCategory });
        }
      }))
      .getRawMany();
    categories.forEach(category => {
      getAllProduct.forEach(product => {
        if (product.category_id === category.id) {
          category.numberOfProduct++;
        }
      });
    });
  }

  //****************** END HELPERS ******************//
  //***************************************************//
}
