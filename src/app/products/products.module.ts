import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { ProductEntity } from './products.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { ProductController } from './products.controller';
import { ProductService } from './products.service';
import { ImageService, ImagesEntity } from '../images';
import { LocationService, LocationEntity } from '../locations';
import { VariationService, VariationEntity } from '../variations';
import { TwillioService } from '../twilio';
import { CodeService, CodeEntity } from '../code';
import { UserService, UserEntity } from '../user';
import { EmailService, EmailCoreModule } from '../email';
import { CategoryService, CategoryEntity } from '../categories';
import { BarcodeService, BarcodeEntity } from '../barcodes';
import { DeviceModule } from '../device';
import { BullModule } from '@nestjs/bull';
import environment from '@app/environment';
import { ProductConsumer } from './products.consumer';

const PROVIDERS = [
  ProductService,
  ImageService,
  LocationService,
  VariationService,
  TwillioService,
  UserService,
  CodeService,
  EmailService,
  CategoryService,
  BarcodeService,
  ProductConsumer
];

const ENTITY = [
  ProductEntity,
  ImagesEntity,
  LocationEntity,
  VariationEntity,
  UserEntity,
  CodeEntity,
  CategoryEntity,
  BarcodeEntity
]

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'products',
      redis: {
        host: environment.redis.host,
        port: parseInt(environment.redis.port),
      },
    }),
    forwardRef(() => AuthModule),
    forwardRef(() => ImagesEntity),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
    DeviceModule,
    BullModule
  ],
  controllers: [ProductController],
  providers: [
    ...PROVIDERS,
    EmailCoreModule.getEmailConfig()
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class ProductModule { }
