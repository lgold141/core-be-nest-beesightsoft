import { IsNotEmpty, IsString, IsUUID, Length, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { ManyToMany, ManyToOne, JoinColumn, JoinTable } from 'typeorm';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { ProductEntity } from '../products.entity';
import { ImagesEntity } from '../../images';
import { CategoryEntity } from '../../categories';
import { Json } from 'aws-sdk/clients/marketplacecatalog';
import { CompanyEntity } from '../../companies/companies.entity';

export class variationModel {
  @Expose()
  type: string;

  @Expose()
  variationName: string;

  @Expose()
  barcode: string;

  @Expose()
  rfid: string;

  @Expose()
  sku: string;

  @Expose()
  @IsNumber()
  quantity: number;

  @Expose()
  image: string;
}

export class optionModel {
  @Expose()
  type: string;

  @Expose()
  value: string;
}

export class TransformProductDto extends AbstractDto {
  @ApiProperty()
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @Expose()
  categoryId: string;

  @ManyToOne(type => CategoryEntity, category => category.id)
  @JoinColumn({ name: "categoryId" })
  @Expose()
  category: CategoryEntity;

  @ApiProperty()
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty()
  @Expose()
  barcode: string;

  @ApiProperty()
  @Expose()
  rfid: string;

  @ApiProperty()
  @Expose()
  unit: string;

  @ApiProperty()
  @Expose()
  quantity: number;

  @ApiProperty()
  @Expose()
  price: number;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @Expose()
  children: ProductEntity[];

  @Expose()
  parent: ProductEntity;

  @Expose()
  isVariation: boolean;

  @ApiProperty()
  @Expose()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.id)
  @JoinColumn({ name: "companyId" })
  company: CompanyEntity;

  @Expose()
  total: number;

  @Expose()
  option: string;
}

export class CreateProductDto extends AbstractDto {
  @ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' })
  @IsString()
  @Length(1, 128)
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  categoryId: string;

  @ApiProperty({ example: 'SKU 345-091' })
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Length(1, 999999999999)
  @Expose()
  barcode: string;

  @Expose()
  rfid: string;

  @ApiProperty({ example: 'ABC' })
  @Expose()
  unit: string;

  @ApiProperty({ example: 40 })
  @IsNumber()
  @Expose()
  quantity: number;

  @ApiProperty({ example: 593 })
  @IsNumber()
  @Expose()
  price: number;

  @ApiProperty()
  @Expose()
  option: string;

  @ApiProperty()
  @Expose()
  variations: string;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @ApiProperty()
  @Expose()
  children: ProductEntity[];

  @ApiProperty()
  @Expose()
  parent: ProductEntity;

  @Expose()
  companyId: string;
}

export class UpdateProductDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  categoryId: string;

  @ApiProperty({ example: 'SKU 345-091' })
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty({ example: 'ABC' })
  @Expose()
  unit: string;

  @ApiProperty({ example: 40 })
  @IsNumber()
  @Expose()
  quantity: number;

  @ApiProperty({ example: 593 })
  @Expose()
  price: number;

  @ApiProperty()
  @Expose()
  option: string;

  @ApiProperty()
  @Expose()
  variations?: string;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @ApiProperty()
  @Expose()
  children: ProductEntity[];

  @ApiProperty()
  @Expose()
  parent: ProductEntity;
}

export class SearchProductDto extends PaginationParams<ProductEntity> {
  @ApiProperty({
    example: ['id', 'name', 'sku', 'barcode', 'price', 'quantity', 'unit', "isVariation", "option"]
  })
  @Expose()
  select: any;

  @ApiProperty({
    example: {
      search: "UND-PRO-NGH--NON-1594723735718",
      categoryName: "Uncategorized",
      categoryId: [
        "ff9c4678-82ad-4d83-be79-df037ff93313",
        "77cbc722-a005-4a36-953d-0a458d81dfd1"
      ],
      quantity: 1
    }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;

  @Expose()
  relations: any;
}

export class IProductIds extends AbstractDto {
  @ApiProperty({
    example: [
      "ff9c4678-82ad-4d83-be79-df037ff93313",
      "77cbc722-a005-4a36-953d-0a458d81dfd1"
    ]
  })
  @Expose()
  productIds: string[];

}