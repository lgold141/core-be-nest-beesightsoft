'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';

export class toSgtin96TagUriDto {
  @ApiProperty({
    example: '0614141'
  })
  @Expose()
  CompanyPrefix: string;

  @ApiProperty({
    example: '812345'
  })
  @Expose()
  ItemReference: string;
  @ApiProperty({
    example: '6789'
  })
  @Expose()
  SerialNumber: string;
}


export class hexToSgtin96TagUriDto {
  @ApiProperty({
    example: '3074257BF7194E4000001A85'
  })
  @Expose()
  epcHexadecimal: string;
}

export class toZplDto {
  @ApiProperty({
    example: '$35.00'
  })
  @Expose()
  price: string;

  @ApiProperty({
    example: 'Flower Dress'
  })
  @Expose()
  name: string;

  @ApiProperty({
    example: 'Small'
  })
  @Expose()
  variationName: string;

  @ApiProperty({
    example: '1234567'
  })
  @Expose()
  barcode: string;

  @ApiProperty({
    example: '3074257BF7194E4000001A85'
  })
  @Expose()
  rfid: string;

  @ApiProperty({
    example: 10
  })
  @Expose()
  quantity: number;
}

export class PrintLabelDto {
  @ApiProperty({ type: toZplDto, isArray: true })
  @Type(() => toZplDto)
  @Expose()
  data: any;
}