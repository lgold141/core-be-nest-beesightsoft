import { IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { Expose } from 'class-transformer';
import { ManyToMany, JoinTable } from 'typeorm';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { ProductEntity } from '../products.entity';
import { ImagesEntity } from '../../images';

export class TransformVariationDto extends AbstractDto {
  @ApiProperty()
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty()
  @Expose()
  barcode: string;

  @ApiProperty()
  @Expose()
  rfid: string;

  @ApiProperty()
  @Expose()
  variationType: string;

  @ApiProperty()
  @Expose()
  variationName: string;

  @ApiProperty()
  @Expose()
  quantity: number;

  @ApiProperty()
  @Expose()
  option: string;

  @Expose()
  variation: true;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @Expose()
  children: ProductEntity[];

  @Expose()
  parent: ProductEntity;

  @Expose()
  index?: number;
}

export class CreateVariationDto extends AbstractDto {
  @Expose()
  isVariation: boolean;

  @ApiProperty({ example: 'SKU 345-091' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Length(1, 999999999999)
  @Expose()
  barcode: string;

  @ApiProperty()
  @Expose()
  rfid: string;

  @Expose()
  categoryId: string;

  @ApiProperty({ example: 'size' })
  @Expose()
  variationType: string;

  @ApiProperty({ example: 'S' })
  @Expose()
  variationName: string;

  @ApiProperty()
  @Expose()
  quantity: number;

  @ApiProperty()
  @Expose()
  option: string;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @ApiProperty()
  @Expose()
  children: ProductEntity[];

  @ApiProperty()
  @Expose()
  parent: ProductEntity;
}

export class UpdateVariationDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @Expose()
  categoryId: string;

  @ApiProperty({ example: 'size' })
  @Expose()
  variationType: string;

  @ApiProperty({ example: 'S' })
  @Expose()
  variationName: string;

  @ApiProperty()
  @Expose()
  quantity: number;

  @ApiProperty()
  @Expose()
  option: string;

  @ManyToMany(type => ImagesEntity)
  @JoinTable()
  @Expose()
  images: ImagesEntity[];

  @ApiProperty()
  @Expose()
  children: ProductEntity[];

  @ApiProperty()
  @Expose()
  parent: ProductEntity;
}

export class SearchVariationDto extends PaginationParams<ProductEntity> {
  @ApiProperty({
    example: ['sku', 'barcode']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformVariationDto, ['sku', 'barcode'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;

  @ApiProperty({
    example: ['images']
  })
  @Expose()
  relations: any;
}
