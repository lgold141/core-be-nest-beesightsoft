import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { LocationEntity } from './locations.entity';
import { CreateLocationDto, TransformLocationDto, SearchLocationDto, UpdateLocationDto } from './dto';
import { UtilsService } from '@nest-core/shared';
import { CodeEnum } from '@app/api-error-code';
import { Raw } from 'typeorm';
import { UserService } from '../user/user.service';

@Injectable()
export class LocationService extends CrudService<LocationEntity> {
  constructor(@InjectRepository(LocationEntity, DB_CONNECTION_DEFAULT) private readonly locationRepository: AbstractBaseRepository<LocationEntity>,
  private userService: UserService) {
    super(locationRepository);
  }

  async createLocation(userId: string, data: CreateLocationDto): Promise<IApiServiceResponseInterface<TransformLocationDto>> {
    try {
      if (data.backroom && data.backroom < 0) {
        return {
          code: CodeEnum.BACKROOM_GREATER_ZERO,
        };
      }
      data = this.removeMultipleSpace(data);
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      const checkLocation = await this.checkValidLocation(data, userCompanyId );
      if(checkLocation === 'invalid'){
        return {
          code: CodeEnum.LOCATION_INVALID_NAME,
        };
      }
      if(checkLocation === 'name'){
        return {
          code: CodeEnum.LOCATION_NAME_EXISTED,
        };
      }
      if(checkLocation === 'address'){
        return {
          code: CodeEnum.LOCATION_ADDRESS_EXISTED,
        };
      }
      data.companyId = userCompanyId;
      const location = await this.locationRepository.save(data);
      return {
        data: UtilsService.plainToClass(TransformLocationDto, location, {
          excludeExtraneousValues: false,
        }),
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getLocation(userId: string, id: string): Promise<IApiServiceResponseInterface<TransformLocationDto>> {
    try {
      const location = await this.findExistLocation(userId, id);
      if (!location) {
        return {
          code: CodeEnum.LOCATION_NOT_EXISTED
        };
      }
      return {
        data: UtilsService.plainToClass(TransformLocationDto, location, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
  
  async getAllLocations(userId: string, filter?: SearchLocationDto): Promise<IApiServiceResponseInterface<TransformLocationDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      if (filter) {
        if (filter.where) {
          const value = filter.where.search || '';
          if(typeof value === "number") {
            filter.where = { backroom: value, companyId: userCompanyId};
          } else {
              const queryName = Raw(alias => `${alias} ILIKE '%${value}%'`);
              filter.where = [
                { name: queryName, companyId: userCompanyId },
                { type: queryName, companyId: userCompanyId },
                { address: queryName, companyId: userCompanyId },
              ];
          }
        } else{
          filter.where = { companyId: userCompanyId };
        }
      }
      const locations = await this.locationRepository.find(filter);
      return {
        data: UtilsService.plainToClass(TransformLocationDto, locations, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteLocation(userId: string, id: string): Promise<IApiServiceResponseInterface<TransformLocationDto>> {
    try {
      if (!(await this.findExistLocation(userId, id))) {
        return {
          code: CodeEnum.LOCATION_NOT_EXISTED
        };
      }
      await this.locationRepository.delete({
        id: id
      });
      return {
        data: UtilsService.plainToClass(TransformLocationDto, {})
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateLocation(userId: string, id: string, data: UpdateLocationDto): Promise<IApiServiceResponseInterface<TransformLocationDto>> {
    try {
      if (!(await this.findExistLocation(userId, id))) {
        return {
          code: CodeEnum.LOCATION_NOT_EXISTED
        };
      }
      if (data.backroom && data.backroom < 0) {
        return {
          code: CodeEnum.BACKROOM_GREATER_ZERO,
        };
      }
      data = this.removeMultipleSpace(data);
      const checkLocation = await this.checkValidLocation(data, id);
      if(checkLocation === 'invalid'){
        return {
          code: CodeEnum.LOCATION_INVALID_NAME,
        };
      }
      if(checkLocation === 'name'){
        return {
          code: CodeEnum.LOCATION_NAME_EXISTED,
        };
      }
      if(checkLocation === 'address'){
        return {
          code: CodeEnum.LOCATION_ADDRESS_EXISTED,
        };
      }
      data.id = id;
      const updatedLocation = await this.locationRepository.save(data);
      return {
        data: UtilsService.plainToClass(TransformLocationDto, updatedLocation, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findExistLocation(userId: string, id: string): Promise<LocationEntity> {
    const userExist = await this.userService.findUserNoRelations(userId);
    const userCompanyId = userExist?.data?.companyId;
    const location = await this.locationRepository.findOne({
      where: {
        id: id, 
        companyId: userCompanyId
      }
    });
    return location;
  }

  async checkValidLocation(data: CreateLocationDto | UpdateLocationDto, companyId: string, id?: string ): Promise<string> {
    const { name, address } = data;
    if(!name.replace(/\s/g, '').length){
      return 'invalid';
    }
    let locationsWithName: number, locationsWithAddress: number;
    if(id){
      locationsWithName = await this.locationRepository.createQueryBuilder("locations")
      .where("LOWER(locations.name) = LOWER(:name) AND id != :id", { name, id })
      .andWhere("locations.companyId = :companyId", { companyId })
      .getCount();
      locationsWithAddress = await this.locationRepository.createQueryBuilder("locations")
      .where("LOWER(locations.address) = LOWER(:address) AND id != :id", { address, id })
      .andWhere("locations.companyId = :companyId", { companyId })
      .getCount();
    } else {
      locationsWithName = await this.locationRepository.createQueryBuilder("locations")
        .where("LOWER(locations.name) = LOWER(:name) AND locations.companyId = :companyId", { name, companyId })
        .getCount();
      locationsWithAddress = await this.locationRepository.createQueryBuilder("locations")
        .where("LOWER(locations.address) = LOWER(:address) AND locations.companyId = :companyId", { address, companyId })
        .getCount();
    }
    if(locationsWithName > 0)
      return 'name';
    if(address === '')
      return 'valid';
    if(locationsWithAddress > 0)
      return 'address';
    return 'valid';
  }

  removeMultipleSpace(data: any): any{
    if(data.address){
      if(!data.address.replace(/\s/g, '').length){
        data.address = data.address.replace(/\s/g, '');
      } else{
        data.address = data.address.trim().replace(/\s\s+/g, ' ');
      }
    }
    if(data.name){
      data.name = data.name.trim().replace(/\s\s+/g, ' ');
    }
    return data;
  }

}
