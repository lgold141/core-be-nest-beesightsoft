import { IsNotEmpty, IsString, IsNumber, MaxLength, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { AbstractDto, PaginationParams } from '@nest-core/core';
import { LocationEntity } from '../locations.entity';

export class TransformLocationDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  type: string;

  @ApiProperty()
  @Expose()
  address: string;

  @ApiProperty()
  @Expose()
  backroom: number;

  @ApiProperty()
  @Expose()
  companyId: string;
}

export class CreateLocationDto extends AbstractDto {
  @ApiProperty({ example: 'Beesight Cong Hoa' })
  @IsNotEmpty()
  @IsString()
  @MaxLength(128)
  @Expose()
  name: string;

  @ApiProperty({ example: 'Store' })
  @Expose()
  type: string;

  @ApiProperty({ example: '18E Cong Hoa' })
  @IsString()
  @MaxLength(128)
  @IsOptional()
  @Expose()
  address?: string;

  @ApiProperty({ example: 1 })
  @IsNumber()
  @IsOptional()
  @Expose()
  backroom?: number;

  @ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' })
  @Expose()
  companyId: string;
}

export class UpdateLocationDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty({ example: 'Beesight Cong Hoa' })
  @IsString()
  @MaxLength(128)
  @IsOptional()
  @Expose()
  name?: string;

  @ApiProperty({ example: 'Store' })
  @Expose()
  type?: string;

  @ApiProperty({ example: '18E Cong Hoa' })
  @IsString()
  @MaxLength(128)
  @IsOptional()
  @Expose()
  address?: string;

  @ApiProperty({ example: 1 })
  @IsNumber()
  @IsOptional()
  @Expose()
  backroom?: number;

  @ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' })
  @Expose()
  companyId: string;
}

export class SearchLocationDto extends PaginationParams<LocationEntity> {
  @ApiProperty({
    example: ['name', 'type', 'address', 'backroom']
  })
  @Expose()
  select: any;

  @ApiProperty({
    // type: PickType(TransformLocationDto, ['name', 'type', 'address', 'backroom'])
    example: { search: "Cong Hoa" }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
