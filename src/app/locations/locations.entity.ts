import { Entity, ManyToOne } from 'typeorm';
import { AbstractEntity, IsEmailUnique } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber, IsEmail, IsCurrency, IsPhoneNumber } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn } from 'typeorm';
import { ILocationInterface } from './interfaces/locations.interface';
import { CompanyEntity } from '../companies/companies.entity';

@Entity('locations')
export abstract class LocationEntity extends AbstractEntity implements ILocationInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column({ unique: true })
  name: string;

  @IsString()
  @IsNotEmpty()
  @Column()
  type: string;

  @Column({ unique: true })
  address: string;

  @Column()
  @IsNumber()
  backroom?: number;

  @Column()
  @IsNotEmpty()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.locations)
  @JoinColumn({name: 'companyId', referencedColumnName: 'id'})
  company: CompanyEntity;
}
