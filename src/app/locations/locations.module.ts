import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { LocationEntity } from './locations.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { LocationController } from './locations.controller';
import { LocationService } from './locations.service';

const PROVIDERS = [
  LocationService
];

const ENTITY = [
  LocationEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT)
  ],
  controllers: [LocationController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class LocationModule { }
