import {IEntityInterface} from '@nest-core/shared';

export interface ILocationInterface extends IEntityInterface {
  name: string;
  type: string;
  address: string;
  backroom?: number;
  companyId: string;
}