import { Controller, UseGuards, Post, Body, Get, Param, Delete, Put, Req, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { LocationEntity } from './locations.entity';
import { CreateLocationDto, SearchLocationDto, TransformLocationDto, UpdateLocationDto } from './dto';
import { LocationService } from './locations.service';
import { IApiErrorInterface } from '@nest-core/core';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Locations')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Location',
  createDto: CreateLocationDto,
  updateDto: UpdateLocationDto,
  transformDto: TransformLocationDto,
  searchDto: SearchLocationDto
})
export class LocationController extends CrudController<LocationEntity> {
  constructor(private readonly locationService: LocationService) {
    super(locationService);
  }

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async createLocation(@Req() req:any, @Body() data: CreateLocationDto): Promise<void> {
    try {
      const userId = req.user.id;
      const location = await this.locationService.createLocation(userId, data);
      this.respondWithErrorsOrSuccess(location);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get(':id')
  async getLocation(@Req() req:any, @Param('id') id: string): Promise<void> {
    try {
      const userId = req.user.id;
      const location = await this.locationService.getLocation(userId, id);
      this.respondWithErrorsOrSuccess(location);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('findAll')
  async getAllLocations(@Req() req: any, @Body() filter: SearchLocationDto): Promise<void> {
    try {
      const userId = req.user.id;
      const locations = await this.locationService.getAllLocations(userId, filter);
      this.respondWithErrorsOrSuccess(locations);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Delete(':id')
  async deleteLocation(@Req() req:any, @Param('id') id: string): Promise<void> {
    try {
      const userId = req.user.id;
      const location = await this.locationService.deleteLocation(userId, id);
      this.respondWithErrorsOrSuccess(location);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put(':id')
  @UsePipes(new ValidationPipe({ transform: true }))
  async updateLocation(@Req() req:any, @Param('id') id: string, @Body() data: UpdateLocationDto): Promise<void> {
    try {
      const userId = req.user.id;
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const location = await this.locationService.updateLocation(userId, id, data);
      this.respondWithErrorsOrSuccess(location);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
