import {Injectable, Logger} from '@nestjs/common';
import {find, isArray} from 'lodash';
import * as format from 'string-format';
import {IApiErrorInterface} from '@nest-core/core';
import {CodeEnum, LangEnum} from './enums';
import * as resources from './lang';

const {en} = resources; // set en lang as default nhe

type LangEnumStrings = keyof typeof LangEnum;
type CodesTranslateMultiType = [CodeEnum, any][];
type CodesTranslateExtendCode = { code: CodeEnum, error: any[] };

@Injectable()
export class ApiErrorCodeService {
  private static instance: ApiErrorCodeService;
  private readonly logger = new Logger(ApiErrorCodeService.name);
  private static lang: LangEnumStrings = 'en';
  private static source = en;

  constructor() {
    this.logger.log('ApiErrorCodeService');
    // this.test();
  }

  public static getInstance(): ApiErrorCodeService {
    if (!ApiErrorCodeService.instance) {
      ApiErrorCodeService.instance = new ApiErrorCodeService();
    }

    return ApiErrorCodeService.instance;
  }

  static setSource(lang: LangEnumStrings): void {
    ApiErrorCodeService.lang = lang;
    ApiErrorCodeService.source = ApiErrorCodeService.getSource();
  }

  static getSource(): any {
    return resources[ApiErrorCodeService.lang];
  }

  static translate(data: CodeEnum, options?: any): IApiErrorInterface
  static translate(data: CodesTranslateMultiType): IApiErrorInterface[]
  static translate(data: CodeEnum | CodesTranslateMultiType | CodesTranslateExtendCode, options?: any): IApiErrorInterface | IApiErrorInterface[] {
    if (isArray(data)) {
      return ApiErrorCodeService.translateMulti(data as any);
    } else if (typeof data === 'object') {
      return ApiErrorCodeService.translateObject(data as any)
    } else {
      return ApiErrorCodeService.translateOne(data as any, options);
    }
  }

  static translateOne(code: CodeEnum, options?: any): IApiErrorInterface {
    const message = options ? format(ApiErrorCodeService.source.get(code), options) : ApiErrorCodeService.source.get(code);
    return {
      code,
      message,
      options: options ? options : null
    } as IApiErrorInterface;
  }

  static translateMulti(codes: CodesTranslateMultiType): IApiErrorInterface[] {
    return codes.map(codeTranslate => {
      const [code, options] = codeTranslate;
      return ApiErrorCodeService.translateOne(code, options);
    });
  }

  static translateObject(codes: CodesTranslateExtendCode): any[] {
    let errors = [];
    const errorLine = ApiErrorCodeService.source.get(CodeEnum.ERROR_LINE);
    codes.error.forEach(err => {
      const message = {
        code: err.errorCode,
        message: `${errorLine} ${err.line}: ${ApiErrorCodeService.source.get(err.errorCode)}`
      }
      errors.push(message)
    })
    return errors;
  }

  test(): void {
    ApiErrorCodeService.setSource('en');
    this.logger.log([
      ApiErrorCodeService.translate(CodeEnum.USER_EMAIL_INVALID),
      ApiErrorCodeService.translate(CodeEnum.USER_EMAIL_REQUIRED),
      ApiErrorCodeService.translate(CodeEnum.AUTH_PASSWORD_REQUIRED),
      ApiErrorCodeService.translate(CodeEnum.X_RATELIMIT_LIMIT, { limit: '10 request/s' }),
    ]
    );

    ApiErrorCodeService.setSource('vi');
    this.logger.log([
      ApiErrorCodeService.translate(CodeEnum.USER_EMAIL_INVALID),
      ApiErrorCodeService.translate(CodeEnum.USER_EMAIL_REQUIRED),
      ApiErrorCodeService.translate(CodeEnum.AUTH_PASSWORD_REQUIRED),
      ApiErrorCodeService.translate(CodeEnum.X_RATELIMIT_LIMIT, { limit: '10 truy cập /s' }),
    ]
    );
  }

  getErrorMessage(code: CodeEnum, options?: any): IApiErrorInterface {
    ApiErrorCodeService.setSource(ApiErrorCodeService.lang);
    const message = options ? format(ApiErrorCodeService.source.get(code), options) : ApiErrorCodeService.source.get(code);
    return {
      code,
      message
    } as IApiErrorInterface;
  }
}
