import {CodeEnum, SchemaCodeEnum} from '../enums';

export type SchemaCodeType = {
  [schema in keyof typeof SchemaCodeEnum]: {
  [property: string]: {
    [rule: string]: [CodeEnum, any]
  }
}};

export const ruleToCodeValidation: SchemaCodeType = {
  [SchemaCodeEnum.common]: {
    id: {
      isUuid: [CodeEnum.IS_UUID, null]
    },
    email: {
      isNotEmpty: [CodeEnum.USER_EMAIL_REQUIRED, null],
      isEmail: [CodeEnum.USER_EMAIL_INVALID, null],
      length: [CodeEnum.USER_EMAIL_LENGTH, {min: 6, max: 100}],
      isEmailUnique: [CodeEnum.USER_EMAIL_UNIQUE, null]
    },
    password: {
      isNotEmpty: [CodeEnum.AUTH_PASSWORD_REQUIRED, null],
      length: [CodeEnum.SIGNUP_PASSWORD_INVALID, null],
      isString: [CodeEnum.AUTH_PASSWORD_ISSTRING, null]
    },
    newPassword: {
      isNotEmpty: [CodeEnum.AUTH_NEWPASSWORD_REQUIRED, null],
      length: [CodeEnum.SIGNUP_PASSWORD_INVALID, null],
      isString: [CodeEnum.AUTH_NEWPASSWORD_ISSTRING, null]
    },
    confirmPassword: {
      isNotEmpty: [CodeEnum.AUTH_CONFIRMPASSWORD_REQUIRED, null],
      length: [CodeEnum.SIGNUP_PASSWORD_INVALID, null],
      isString: [CodeEnum.AUTH_CONFIRMPASSWORD_ISSTRING, null]
    },
    firstName: {
      isNotEmpty: [CodeEnum.USER_FIRSTNAME_REQUIRED, null],
      length: [CodeEnum.USER_FIRSTNAME_LENGTH, {min: 1, max: 128}]
    },
    lastName: {
      isNotEmpty: [CodeEnum.USER_LASTNAME_REQUIRED, null],
      length: [CodeEnum.USER_LASTNAME_LENGTH, {min: 6, max: 100}]
    },
    username: {
      isAscii: [CodeEnum.USER_USERNAME_ASCII, null],
      isNotEmpty: [CodeEnum.USER_USERNAME_REQUIRED, null],
      length: [CodeEnum.USER_USERNAME_LENGTH, {min: 6, max: 100}],
      isUsernameUnique: [CodeEnum.USER_USERNAME_UNIQUE, null]
    },
    name: {
      isNotEmpty: [CodeEnum.NAME_REQUIRED, null],
      length: [CodeEnum.NAME_INVALID, null]
    },
    businessName: {
      isNotEmpty: [CodeEnum.BUSSINESS_NAME_REQUIRED, null],
      length: [CodeEnum.BUSSINESS_NAME_LENGTH, {min: 1, max: 128}],
      isString: [CodeEnum.BUSSINESS_NAME_ISSTRING, null] 
    },
    city: {
      maxLength: [CodeEnum.COMPANY_CITY_LENGTH, null]
    },
  }
};