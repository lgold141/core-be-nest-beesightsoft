import {Global, Module} from '@nestjs/common';

import {ApiErrorCodeService} from './api-error-code.service';

@Global()
@Module({
  providers: [ApiErrorCodeService],
  exports: [ApiErrorCodeService],
})
export class ApiErrorCodeModule {
}
