import { MigrationInterface, QueryRunner } from "typeorm";
import { database } from './list.migrate';


export class MigrateDatabase implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        console.log(parseQuery(database))
        // await queryRunner.query(`ALTER TABLE "subscription" DROP COLUMN "startDate"`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        // await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "description"`);
        // await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "description"`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "displayName"`);
        // await queryRunner.query(`ALTER TABLE "project" ADD "description" character varying`);
        // await queryRunner.query(`ALTER TABLE "team" ADD "description" character varying`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "username" character varying`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "displayName" character varying`);
        // await queryRunner.query(`ALTER TABLE "subscription" ADD "startDate" date NOT NULL DEFAULT now()`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "name" character varying`);
        // await queryRunner.query(`ALTER TABLE "subscription" ALTER COLUMN "usersCapacity" DROP DEFAULT`);
        // await queryRunner.query(`ALTER TABLE "subscription" ALTER COLUMN "endDate" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // await queryRunner.query(`ALTER TABLE "subscription" ALTER COLUMN "endDate" DROP NOT NULL`);
        // await queryRunner.query(`ALTER TABLE "subscription" ALTER COLUMN "usersCapacity" SET DEFAULT 1`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        // await queryRunner.query(`ALTER TABLE "subscription" DROP COLUMN "startDate"`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "displayName"`);
        // await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        // await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "description"`);
        // await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "description"`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "displayName" character varying`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "username" character varying`);
        // await queryRunner.query(`ALTER TABLE "team" ADD "description" character varying`);
        // await queryRunner.query(`ALTER TABLE "project" ADD "description" character varying`);
        // await queryRunner.query(`ALTER TABLE "user" ADD "name" character varying`);
        // await queryRunner.query(`ALTER TABLE "subscription" ADD "startDate" date NOT NULL DEFAULT now()`);
    }

}

export const parseQuery = (database) => {
    console.log(database);
    return database;
}