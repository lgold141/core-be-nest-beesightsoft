export const database = [
  {
    table: 'products',
    column: [
      {
        name: 'id',
        conditions: ['uuid GENERATED DEFAULT uuid_generate_v4 ()', 'PRIMARY KEY', 'NOT NULL']
      },
      {
        name: 'createdAt',
        conditions: ['DATE', 'NOT NULL', 'NOT NULL DEFAULT CURRENT_DATE']
      },
      {
        name: 'updatedAt',
        conditions: ['DATE', 'NOT NULL', 'NOT NULL DEFAULT CURRENT_DATE']
      },
      {
        name: 'name',
        conditions: ['VARCHAR']
      },
      {
        name: 'categoryId',
        conditions: ['VARCHAR']
      },
      {
        name: 'sku',
        conditions: ['VARCHAR']
      },
      {
        name: 'barcode',
        conditions: ['VARCHAR', 'UNIQUE']
      },
      {
        name: 'rfid',
        conditions: ['VARCHAR']
      },
      {
        name: 'unit',
        conditions: ['VARCHAR']
      },
      {
        name: 'quantity',
        conditions: ['NUMERIC']
      },
      {
        name: 'price',
        conditions: ['NUMERIC']
      },
      {
        name: 'variationType',
        conditions: ['VARCHAR']
      },
      {
        name: 'variationName',
        conditions: ['VARCHAR']
      },
      {
        name: 'FOREIGN KEY',
        conditions: ['CONSTRAINT images FOREIGN KEY(images) REFERENCES customers(id) ON DELETE CASCADE']
      },
    ]
  }
]