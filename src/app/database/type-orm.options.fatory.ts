import {Injectable} from '@nestjs/common';
import {TypeOrmOptionsFactory as TypeOrmOptionsFactoryX, TypeOrmModuleOptions} from '@nestjs/typeorm';
import * as databaseConfig from './database.constants';

@Injectable()
export class TypeOrmOptionsFatory implements TypeOrmOptionsFactoryX {
  createTypeOrmOptions(connectionName?: string): TypeOrmModuleOptions {
    return databaseConfig[connectionName] as any;
  }
}