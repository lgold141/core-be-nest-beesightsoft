import {Global, Module, DynamicModule} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {IDatabaseOptionsInterface} from './database.options.interface';
import * as databaseConfig from './database.constants';
import {
  DB_CONNECTION_DEFAULT,
  // DB_CONNECTION_1,
  // DB_CONNECTION_2
} from '../database/database.constants';
import { MigrateDatabase } from './migrate/database.migrate';

@Global()
@Module({})
export class DatabaseModule {
  static forRootAsync(options: IDatabaseOptionsInterface): DynamicModule {
    const connectionName = options.connectionName ? options.connectionName : DB_CONNECTION_DEFAULT;
    return {
      module: DatabaseModule,
      imports: [
        MigrateDatabase,
        TypeOrmModule.forRootAsync({
          name: connectionName,
          useFactory: () => {
            const config = {
              ...databaseConfig[connectionName],
              entities: options.entities || []
            };
            // console.log({config, databaseConfig});
            return config as any;
          },
        }),
      ]
    }
  }
}
