import {ConfigService} from '@nest-core/config';

const databases = ConfigService.get('databases');
export const DB_CONFIG_DEFAULT = databases[0];
export const DB_CONFIG_1 = {
  // "entities": [
  //   __dirname + '/../**/**.entity{.ts,.js}',
  //   // __dirname + '/../../**/**.entity{.ts,.js}'
  // ],
};
export const DB_CONFIG_2 = {};

export const DB_CONNECTION_DEFAULT = 'DB_CONFIG_DEFAULT';
export const DB_CONNECTION_1 = 'DB_CONFIG_1';
export const DB_CONNECTION_2 = 'DB_CONFIG_2';
