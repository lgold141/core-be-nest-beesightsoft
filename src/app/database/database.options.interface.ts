export interface IDatabaseOptionsInterface {
  connectionName? : string,
  entities?: any[];
}