import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { AbstractDto } from '../../../../libs/core/src/dto/abstract.dto';
import { PaginationParams } from '../../../../libs/core/src/crud/pagination-params';
import { OptionValueEntity } from '../optionValues.entity';

export class TransformOptionValueDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  value: string;

  @ApiProperty()
  @Expose()
  optionId: string;
}

export class IOptionValueDto {
  @ApiProperty({
    example: 'Blue'
  })
  @Expose()
  value: string;
}

export class CreateOptionValueDto {
  @ApiProperty({
    example: 'Blue'
  })
  @Expose()
  value: string;

  @Expose()
  optionId: string;
}

export class UpdateOptionValueDto {
  @ApiProperty({
    example: '3c622136-3afd-4753-b2b1-17838e4ce3b6'
  })
  @Expose()
  id: string;

  @ApiProperty({
    example: 'Blue'
  })
  @Expose()
  value: string;

  @Expose()
  optionId: string;
}

export class SearchOptionValueDto extends PaginationParams<OptionValueEntity> {
  @ApiProperty({
    example: ['id', 'value', 'optionId']
  })
  @Expose()
  select: any;

  @ApiProperty({
    example: { search: "Blue" }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      value: 'DESC'
    }
  })
  @Expose()
  order: any;
}
