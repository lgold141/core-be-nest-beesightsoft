import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OptionValueEntity } from './optionValues.entity';
import { DB_CONNECTION_DEFAULT } from '../database/database.constants';
import { CreateOptionValueDto, TransformOptionValueDto, UpdateOptionValueDto } from './dto/optionValues.dto';
import { IApiServiceResponseInterface, AbstractBaseRepository, CrudService } from '@nest-core/core';
import { UtilsService } from '@nest-core/shared';

@Injectable()
export class OptionValueService extends CrudService<OptionValueEntity> {
  constructor(@InjectRepository(OptionValueEntity, DB_CONNECTION_DEFAULT) private readonly optionValueRepository: AbstractBaseRepository<OptionValueEntity>,) {
    super(optionValueRepository);
  }
  async createOptionValue(value: CreateOptionValueDto): Promise<IApiServiceResponseInterface<TransformOptionValueDto>> {
    try {
      const optionValue = await this.optionValueRepository.save(value);
      return {
        data: UtilsService.plainToClass(TransformOptionValueDto, optionValue, {
          excludeExtraneousValues: false,
        }),
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
  
  async updateOptionValue(id: string, data: UpdateOptionValueDto): Promise<IApiServiceResponseInterface<TransformOptionValueDto>> {
    try {
      data.id = id;
      const optionValue = await this.optionValueRepository.findOne({
        id: id, 
        optionId: data.optionId 
      });
      let updatedOptionValue: UpdateOptionValueDto;
      if(optionValue)
      {
        updatedOptionValue = await this.optionValueRepository.save(data);
      }
      return {
        data: UtilsService.plainToClass(TransformOptionValueDto, updatedOptionValue, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}