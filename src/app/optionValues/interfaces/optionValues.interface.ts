import { IEntityInterface } from '../../../../libs/shared/src/interfaces/entity.interface';
export interface IOptionValueInterface extends IEntityInterface{
  value : string,
  optionId : string,
}