import { Module, forwardRef } from '@nestjs/common';

import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { OptionValueService } from './optionValues.service';
import { OptionValueEntity } from './optionValues.entity';
import { OptionValueController } from './optionValues.controller';

const PROVIDERS = [
  OptionValueService
];

const ENTITY = [
  OptionValueEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT)
  ],
  providers: [...PROVIDERS],
  controllers: [OptionValueController],
  exports: [...PROVIDERS]
})
export class OptionValueModule {}
