import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController } from '../../../libs/core/src/crud/crud.controller';
import { OptionValueEntity } from './optionValues.entity';
import { OptionValueService } from './optionValues.service';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';
import { CreateOptionValueDto, UpdateOptionValueDto, TransformOptionValueDto, SearchOptionValueDto } from './dto/optionValues.dto';
import { CrudOptionsDtoDecorator } from '../../../libs/core/src/decorators/crud-options-dto.decorator';

@ApiTags('OptionValues')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'OptionValue',
  createDto: CreateOptionValueDto,
  updateDto: UpdateOptionValueDto,
  transformDto: TransformOptionValueDto,
  searchDto: SearchOptionValueDto
})
export class OptionValueController extends CrudController<OptionValueEntity>{
  constructor(private readonly optionValueService: OptionValueService) {
    super(optionValueService);
  }

  @Post()
  async createOptionValue(@Body() data: CreateOptionValueDto): Promise<void> {
    try {
      const optionValue = await this.optionValueService.createOptionValue(data);
      this.respondWithErrorsOrSuccess(optionValue);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}