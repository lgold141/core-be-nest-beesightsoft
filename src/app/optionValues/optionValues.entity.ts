import { Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString} from 'class-validator';
import { Column, Index} from 'typeorm';
import { IOptionValueInterface } from './interfaces/optionValues.interface';
import { OptionEntity } from '../options/options.entity';

@Entity('optionValues')
export abstract class OptionValueEntity extends AbstractEntity implements IOptionValueInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  value: string;

  @Column()
  optionId: string;

  @ManyToOne(type => OptionEntity, option => option.optionValues, { onDelete: 'CASCADE' })
  @JoinColumn({name: 'optionId', referencedColumnName: 'id'})
  option: OptionEntity;
}
