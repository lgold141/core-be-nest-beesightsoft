import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { OptionEntity } from './options.entity';
import { CreateOptionDto, TransformOptionDto, SearchOptionDto, UpdateOptionDto, CountVariationDto } from './dto/options.dto';
import { UtilsService } from '@nest-core/shared';
import { CodeEnum } from '@app/api-error-code';
import { Raw } from 'typeorm';
import { UserService } from '../user/user.service';
import { ProductService } from '../products/products.service';
import { OptionValueService } from '../optionValues/optionValues.service';

@Injectable()
export class OptionService extends CrudService<OptionEntity> {
  constructor(@InjectRepository(OptionEntity, DB_CONNECTION_DEFAULT) private readonly optionRepository: AbstractBaseRepository<OptionEntity>,
  private userService: UserService,
  private optionValueService: OptionValueService,
  private productService: ProductService,) {
    super(optionRepository);
  }

  async createOption(userId: string, data: CreateOptionDto): Promise<IApiServiceResponseInterface<TransformOptionDto>> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      data.companyId = userCompanyId;
      const checkOptionName = await this.optionRepository.createQueryBuilder('options')
        .leftJoinAndSelect(`options.optionValues`, 'optionValues')
        .where('options.companyId = :companyId AND LOWER(options.name) = LOWER(:name)',{companyId: data.companyId, name: data.name})
        .getOne();
      if(checkOptionName){
        const values: string[] = [];
        checkOptionName?.optionValues?.forEach(value => values.push(value.value))
        values.forEach(value => {
          data.values = data.values.filter(val => value.toLowerCase().trim() !== val.value.toLowerCase().trim())
        })
        data.id = checkOptionName.id;
      }
      const option = await this.optionRepository.save(data);
      data.values.forEach(async (optionValue) => {
        await this.optionValueService.createOptionValue({value: optionValue.value, optionId: option.id});
      })
      return {
        data: UtilsService.plainToClass(TransformOptionDto, option, {
          excludeExtraneousValues: false,
        }),
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getAllOptions(userId: string, filter?: SearchOptionDto): Promise<IApiServiceResponseInterface<TransformOptionDto>> {
  try {
    const userExist = await this.userService.findUserNoRelations(userId);
    const userCompanyId = userExist?.data?.companyId;
    if (filter) {
      if (filter.where) {
        const value = filter.where.search || '';
        const queryName = Raw(alias => `${alias} ILIKE '%${value}%'`);
        filter.where =
        {
          name: queryName,
          companyId: userCompanyId
        }
      } else {
        filter.where = { companyId: userCompanyId };
      }
    }
    filter.relations = ['optionValues'];
    const options = await this.optionRepository.find(filter);
    const results = await Promise.all(options.map( async (option) => {
      const optionValues = option.optionValues;
      const arrayOfValues: string[] = [];
      optionValues.forEach(optionValue => {
        arrayOfValues.push(optionValue.value);
      })
      return {
        id: option.id,
        name: option.name,
        optionValues: arrayOfValues,
        totalProducts: await this.productService.countProductWithOptionId(userCompanyId, option.id),
      }
    }))
    return {
      data: UtilsService.plainToClass(TransformOptionDto, results, {
        excludeExtraneousValues: false
      })
    };
  } catch (e) {
    console.log(e);
    throw e;
  }
}

  async getOption(userId: string,id: string): Promise<IApiServiceResponseInterface<TransformOptionDto>> {
    try {
      const option = await this.findExistOption(userId, id);
      if (!option) {
        return {
          code: CodeEnum.OPTION_NOT_EXISTED,
        };
      }
      return {
        data: UtilsService.plainToClass(TransformOptionDto, option, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getCountVariation(userId: string, value: string): Promise<any> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      const count = await this.productService.getCountVariation(userCompanyId, value);
      const result = {
        value: value,
        variations: count
      }
      return {
        data:result
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async countVariation(userId: string, option: CountVariationDto): Promise<any> {
    try {
      const userExist = await this.userService.findUserNoRelations(userId);
      const userCompanyId = userExist?.data?.companyId;
      const count = await this.productService.countVariation(userCompanyId, option.name, option.value);
      const result = {
        ...option,
        variations: count
      }
      return {
        data:result
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteOption(userId: string,id: string): Promise<IApiServiceResponseInterface<TransformOptionDto>> {
    try {
      if (!(await this.findExistOption(userId, id))) {
        return {
          code: CodeEnum.OPTION_NOT_EXISTED,
        };
      }
      await this.optionRepository.delete({
        id: id
      });
      return {
        data: UtilsService.plainToClass(TransformOptionDto, {})
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateOption(userId: string, id: string, data: UpdateOptionDto): Promise<IApiServiceResponseInterface<TransformOptionDto>> {
    try {
      data.id = id;
      if (!(await this.findExistOption(userId, id))) {
        return {
          code: CodeEnum.OPTION_NOT_EXISTED,
        };
      }
      data.values.forEach(async (optionValue) => {
        await this.optionValueService.updateOptionValue(optionValue.id , {id: optionValue.id, value: optionValue.value, optionId: id});
      })
      const updatedOption = await this.optionRepository.save(data);
      return {
        data: UtilsService.plainToClass(TransformOptionDto, updatedOption, {
          excludeExtraneousValues: false
        })
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async findExistOption(userId: string, id: string): Promise<OptionEntity> {
    const userExist = await this.userService.findUserNoRelations(userId);
    const userCompanyId = userExist?.data?.companyId;
    const option = await this.optionRepository.findOne({
      id: id, 
      companyId: userCompanyId 
    }, 
    {
      relations: ['optionValues']
    });
    return option;
  }

}
