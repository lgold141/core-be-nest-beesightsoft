import { IsNotEmpty, IsOptional, IsString, Length } from 'class-validator';
import { Expose, Type } from 'class-transformer';
import { ApiProperty, } from '@nestjs/swagger';
import { AbstractDto, PaginationParams } from '@nest-core/core';
import { OptionEntity } from '../options.entity';
import { IOptionValueDto, UpdateOptionValueDto } from '../../optionValues/dto/optionValues.dto';

export class TransformOptionDto extends AbstractDto {
  @ApiProperty()
  @Expose()
  id: string;

  @ApiProperty()
  @Expose()
  name: string;

  @ApiProperty({ type: IOptionValueDto, isArray: true })
  @Type(() => IOptionValueDto)
  @Expose()
  values: any;

  @ApiProperty()
  @Expose()
  companyId: string;
}

export class CreateOptionDto extends AbstractDto {
  @Expose()
  id: string;
  
  @ApiProperty({ example: 'Color' })
  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty({ type: IOptionValueDto, isArray: true })
  @Type(() => IOptionValueDto)
  @Expose()
  values: any;

  @IsString()
  @IsNotEmpty()
  @Expose()
  companyId: string;
}

export class UpdateOptionDto extends AbstractDto {
  @ApiProperty({ example: '72bea1e5-bf30-43e4-937d-da0b25afc453' })
  @IsOptional()
  @Expose()
  id: string;

  @ApiProperty({ example: 'Color' })
  @IsOptional()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty({ type: UpdateOptionValueDto, isArray: true })
  @Type(() => UpdateOptionValueDto)
  @Expose()
  values: any;

  @IsOptional()
  @Expose()
  companyId: string;
}

export class CountVariationDto extends AbstractDto {
  @ApiProperty({ example: 'Color' })
  @IsString()
  @Expose()
  name: string;

  @ApiProperty({ example: 'Blue' })
  @IsString()
  @Expose()
  value: string;
}

export class SearchOptionDto extends PaginationParams<OptionEntity> {
  @ApiProperty({
    example: ['id', 'name']
  })
  @Expose()
  select: any;

  @ApiProperty({
    example: { search: "Color" }
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;

  @ApiProperty({
    example: ['optionValues']
  })
  @Expose()
  relations: any;
}
