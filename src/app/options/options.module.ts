import { Module, forwardRef } from '@nestjs/common';
import { OptionService } from './options.service';
import { OptionController } from './options.controller';
import { OptionEntity } from './options.entity';
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { ProductModule } from '../products/products.module';
import { OptionValueModule } from '../optionValues/optionValues.module';

const PROVIDERS = [
  OptionService
];

const ENTITY = [
  OptionEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => ProductModule),
    forwardRef(() => OptionValueModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT)
  ],
  providers: [...PROVIDERS],
  controllers: [OptionController],
  exports: [...PROVIDERS]
})
export class OptionModule {}
