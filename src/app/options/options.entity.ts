import { Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString} from 'class-validator';
import { Column, Index} from 'typeorm';
import { CompanyEntity } from '../companies/companies.entity';
import { IOptionInterface } from './interfaces/options.interface';
import { OptionValueEntity } from '../optionValues/optionValues.entity';

@Entity('options')
export abstract class OptionEntity extends AbstractEntity implements IOptionInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  name: string;

  @Column()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.options)
  @JoinColumn({name: 'companyId', referencedColumnName: 'id'})
  company: CompanyEntity;

  @OneToMany(type => OptionValueEntity, optionValue => optionValue.option)
  optionValues: OptionValueEntity[];
}
