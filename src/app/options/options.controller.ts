import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { CreateOptionDto, UpdateOptionDto, TransformOptionDto, SearchOptionDto, CountVariationDto } from './dto/options.dto';
import { OptionEntity } from './options.entity';
import { OptionService } from './options.service';
import { IApiErrorInterface } from '../../../libs/core/src/interfaces/controller/api-error.interface';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Options')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Option',
  createDto: CreateOptionDto,
  updateDto: UpdateOptionDto,
  transformDto: TransformOptionDto,
  searchDto: SearchOptionDto
})
export class OptionController extends CrudController<OptionEntity>{
  constructor(private readonly optionService: OptionService) {
    super(optionService);
  }

  @Post()
  async createOption(@Req() req: any, @Body() data: CreateOptionDto): Promise<void> {
    try {
      const userId = req.user.id;
      const option = await this.optionService.createOption(userId, data);
      this.respondWithErrorsOrSuccess(option);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get(':id')
  async getOption(@Req() req: any, @Param('id') id: string): Promise<void> {
    try {
      const userId = req.user.id;
      const option = await this.optionService.getOption(userId, id);
      this.respondWithErrorsOrSuccess(option);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Get('countVariation/:value')
  async getCountVariation(@Req() req: any, @Param('value') value: string): Promise<void> {
    try {
      const userId = req.user.id;
      const result = await this.optionService.getCountVariation(userId, value);
      return result;
      this.respondWithErrorsOrSuccess(result);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('countVariation')
  async countVariation(@Req() req: any, @Body() option: CountVariationDto): Promise<void> {
    try {
      const userId = req.user.id;
      const result = await this.optionService.countVariation(userId, option);
      return result
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('findAll')
  async getAllOptions(@Req() req: any, @Body() filter: SearchOptionDto): Promise<void> {
    try {
      const userId = req.user.id;
      const options = await this.optionService.getAllOptions(userId, filter);
      this.respondWithErrorsOrSuccess(options);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Delete(':id')
  async deleteOption(@Req() req: any, @Param('id') id: string): Promise<void> {
    try {
      const userId = req.user.id;
      const option = await this.optionService.deleteOption(userId, id);
      this.respondWithErrorsOrSuccess(option);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Put(':id')
  @UsePipes(new ValidationPipe({ transform: true }))
  async updateOption(@Req() req: any, @Param('id') id: string, @Body() data: UpdateOptionDto): Promise<void> {
    try {
      const userId = req.user.id;
      const errors: IApiErrorInterface[] = await this.validateWith(data);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const option = await this.optionService.updateOption(userId, id, data);
      this.respondWithErrorsOrSuccess(option);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
