import {IEntityInterface} from '@nest-core/shared';

export interface IOptionInterface extends IEntityInterface {
  name: string;
  companyId: string;
}
