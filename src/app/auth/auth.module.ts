import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nest-core/config';
import { AuthUserService, CoreModule, ImageEntity, ProfileEntity, UserEntity } from '@nest-core/core';
import { SharedModule, UtilsService } from '@nest-core/shared';
import { AllowGuard, JwtAuthGuard, WsJwtAuthGuard } from './guards';
import { JwtStrategy, WsJwtStrategy } from './strategies';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './auth.service';
import { CommandHandlers } from './commands/handlers';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CodeService, CodeEntity } from '../code';
import { TwillioService } from '../twilio';
import { UserService } from '../user';
import { FlagEntity, FlagService } from '../flag';
import { DevicesService, DevicesEntity } from '../device';
import { EmailService, EmailCoreModule } from '../email';
import { LogoutDeviceGuard } from '../device/guard/logout-device.guard';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    CoreModule,
    TypeOrmModule.forFeature(
      [ImageEntity, ProfileEntity, UserEntity, CodeEntity, FlagEntity, DevicesEntity],
      DB_CONNECTION_DEFAULT),
    SharedModule,
    ConfigModule,
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get('jwt.secretOrKey'),
          signOptions: configService.get('jwt.signOptions'),
        }
      },
      inject: [ConfigService]
    }),
    CqrsModule,
    HttpModule
  ],
  controllers: [AuthController],
  providers: [
    ConfigService,
    UtilsService,
    JwtStrategy,
    WsJwtStrategy,
    AllowGuard,
    JwtAuthGuard,
    LogoutDeviceGuard,
    DevicesService,
    WsJwtAuthGuard,
    AuthUserService,
    AuthService,
    TwillioService,
    CodeService,
    UserService,
    FlagService,
    EmailService,
    ...CommandHandlers,
    EmailCoreModule.getEmailConfig()
    // {
    //   provide: APP_GUARD,
    //   useClass: AllowGuard,
    // },
    // {
    //   provide: APP_GUARD,
    //   useClass: JwtAuthGuard,
    // },
    // {
    //   provide: APP_GUARD,
    //   useClass: RoleGuard,
    // },
    // {
    //   provide: APP_GUARD,
    //   useClass: ComposeGuard,
    // },
  ],
  exports: [
    AllowGuard,
    JwtAuthGuard,
    LogoutDeviceGuard,
    DevicesService,
    WsJwtAuthGuard,
    AuthUserService,
    AuthService,
    CodeService,
    UserService,
    FlagService
  ],
})
export class AuthModule {
}
