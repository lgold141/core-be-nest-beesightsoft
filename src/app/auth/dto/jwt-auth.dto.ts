'use strict';

import {ApiProperty} from '@nestjs/swagger';
import {UserTransformDto} from './crud-options.dto';
import {Expose} from 'class-transformer';

export class JWTAuthPayloadDto {
  @ApiProperty({
    example: '1d'
  })
  @Expose()
  expiresIn: number;

  @ApiProperty({
    example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im5hc3RlcmJsdWUiLCJpZCI6IjYwNTk0MWVkLTZmMGUtNDcxZi04Y2NjLTUyM2Q0YzBjOWI0ZiIsImlhdCI6MTU4ODQ0MTQ1MSwiZXhwIjoxNTg4NTI3ODUxfQ.cTO9BMwWzkCD3htjTOoDlhud0sJYgdB4MBdAkauLRso'
  })
  @Expose()
  accessToken: string;
}

export class JWTAuthDto {
  @ApiProperty({type: UserTransformDto})
  @Expose()
  user: UserTransformDto;

  @ApiProperty({type: JWTAuthPayloadDto})
  @Expose()
  jwt: JWTAuthPayloadDto;
}
