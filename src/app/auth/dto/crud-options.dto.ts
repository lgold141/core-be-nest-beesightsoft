import { IsAscii, IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { AbstractDto, IsEmailUnique, IsUsernameUnique } from '@nest-core/core';
import { ManyToOne, JoinColumn } from 'typeorm';
import { CompanyEntity } from 'src/app/companies';

export class UserTransformDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @Expose()
  id: string;

  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  @Expose()
  lastName: string;

  @ApiProperty()
  @Expose()
  country: string;

  @ApiProperty()
  @Expose()
  countryCode: string;

  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @IsEmail()
  @IsEmailUnique()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  verifiedEmail: boolean;

  @ApiProperty({ example: '+84868964539' })
  @IsNotEmpty()
  @Length(8, 15)
  @Expose()
  phoneNumber: string;

  @ApiProperty()
  @Expose()
  verifiedPhoneNumber: boolean;

  @ApiProperty({ example: 'beesight.alpha.com' })
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  address: string;

  @ApiProperty({ example: 'nasterblue' })
  @IsAscii()
  @IsUsernameUnique()
  @Length(6, 100)
  @IsString()
  @IsNotEmpty()
  @Expose()
  username: string;

  @ApiProperty({ example: 'Vietnamese' })
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @Expose()
  language: string;

  @ApiProperty({ example: 'DD/MM/YYYY' })
  @IsString()
  @IsNotEmpty()
  @Expose()
  date: string;

  @ApiProperty({ example: '1' })
  @Expose()
  currencyId: number;

  @ApiProperty()
  @Expose()
  theme: string;

  @ApiProperty()
  @Expose()
  companyId: string;

  @ManyToOne(type => CompanyEntity, company => company.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: 'companyId' })
  company: CompanyEntity;
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;

  @Expose()
  updatePasswordAt: Date;
}

@Exclude()
export class UserAuthLoginDto extends AbstractDto {
  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @Expose()
  email?: string;

  @ApiProperty({ example: '0868964539' })
  @Expose()
  phoneNumber?: string;

  @ApiProperty({ example: '123456' })
  @IsNotEmpty()
  @IsString()
  @Length(6, 30)
  @Expose()
  password: string;

  @Expose()
  registerCompleted: boolean;
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;
}

export class UserAuthRegisterDto extends AbstractDto {
  @ApiProperty({ example: 'Nghia Vo' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 128)
  @Expose()
  firstName: string;

  @Expose()
  lastName: string;

  @ApiProperty({ example: 'dev@beesightsoft.com || 868964539' })
  @Expose()
  account: string;

  @ApiProperty({ example: 'Vietnam' })
  @Expose()
  country: string;

  @ApiProperty({ example: '+84' })
  @Expose()
  countryCode: string;

  @Expose()
  email: string;

  @Expose()
  verifiedEmail: boolean;

  @Expose()
  phoneNumber: string;

  @Expose()
  verifiedPhoneNumber: boolean;

  @Expose()
  address: string;

  @Expose()
  username: string;

  @Expose()
  language: string;

  @Expose()
  date: string;

  @Expose()
  currencyId?: number;

  @Expose()
  theme: string;

  @Expose()
  companyId?: string;
  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;

  @ApiProperty({ example: '123456' })
  @IsNotEmpty()
  @IsString()
  @Expose()
  password: string;

  @Expose()
  registerCompleted: boolean;
}

export class UserAuthRequestPasswordDto extends AbstractDto {
  @ApiProperty({ example: 'dev@beesightsoft.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;

  // @IsString()
  // @MinLength(6)
  // @ApiProperty({example: 'nasterblue'})
  //  username: string;

  @ApiProperty({ example: 'beesightsoft://reqset-password' })
  @IsString()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  appSchema: string;

  @ApiProperty({ example: 'http://beesightsoft.com/reset-password' })
  @IsString()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  webSchema: string;

  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;
}

@Exclude()
export class UserAuthResetPasswordDto extends AbstractDto {
  @IsString()
  @Length(6, 100)
  @ApiProperty({ example: '123456' })
  @Expose()
  password: string;

  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;
}

@Exclude()
export class UserChangePasswordDto extends AbstractDto {
  @IsString()
  @IsNotEmpty()
  @Length(6, 30)
  @ApiProperty({ example: '123456' })
  @Expose()
  password: string;

  @IsString()
  @IsNotEmpty()
  @Length(6, 30)
  @ApiProperty({ example: '123456' })
  @Expose()
  newPassword: string;

  @IsString()
  @IsNotEmpty()
  @Length(6, 30)
  @ApiProperty({ example: '123456' })
  @Expose()
  confirmPassword: string;

  // schemaCode: SchemaCodeEnum = SchemaCodeEnum.auth;
}

@Exclude()
export class UserConfirmChangePasswordDto extends AbstractDto {
  @IsString()
  @IsNotEmpty()
  @Length(6, 100)
  @ApiProperty({ example: '123456' })
  @Expose()
  password: string;
}

export class UserAuthForgotPasswordDto extends AbstractDto {
  @ApiProperty({ example: 'dev@beesightsoft.com || 868964539' })
  @Expose()
  account: string;

  @Expose()
  email: string;

  @Expose()
  phoneNumber: string;
}

@Exclude()
export class UserChangeForgotPasswordDto extends AbstractDto {
  @IsString()
  @IsNotEmpty()
  @Length(6, 30)
  @ApiProperty({ example: '123456' })
  @Expose()
  newPassword: string;

  @IsString()
  @IsNotEmpty()
  @Length(6, 30)
  @ApiProperty({ example: '123456' })
  @Expose()
  confirmPassword: string;
}