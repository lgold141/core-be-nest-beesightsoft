import {createParamDecorator, ExecutionContext} from '@nestjs/common';
import {ExtractJwt} from 'passport-jwt';

const extractor = ExtractJwt.fromAuthHeaderAsBearerToken();

export const RawToken = createParamDecorator((data: any, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  return extractor(request);
});
