import {SetMetadata} from '@nestjs/common';
import {AllowEnum} from '../enums';

export const Allow = (...roles: AllowEnum[]) => SetMetadata('allow', roles);

