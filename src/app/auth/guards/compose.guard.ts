import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {AllowGuard} from './allow.guard';
import {RoleGuard} from './role.guard';
import {JwtAuthGuard} from './jwt-auth.guard';

// import {WsJwtAuthGuard} from './ws-jwt-auth.guard';

@Injectable()
export class ComposeGuard implements CanActivate {
  constructor(private allowGuard: AllowGuard,
              private jwtAuthGuard: JwtAuthGuard,
              // private wsJwtAuthGuard: WsJwtAuthGuard,
              private roleGuard: RoleGuard) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    return (
      (await this.allowGuard.canActivate(context)) ||
      (
        (await this.jwtAuthGuard.canActivate(context)) &&
        // (await this.wsJwtAuthGuard.canActivate(context)) &&
        (await this.roleGuard.canActivate(context))
      )
    );
  }
}
