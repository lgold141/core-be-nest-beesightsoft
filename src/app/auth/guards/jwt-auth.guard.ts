import {ExecutionContext, Injectable, UnauthorizedException,} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {Observable} from 'rxjs';
import {JwtPayloadInterface} from '@nest-core/shared';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    return super.canActivate(context);
  }

  handleRequest<T extends JwtPayloadInterface >(err, jwtPayload: T, info, context, status): T {
    // console.log(['JwtAuthGuard', 'handleRequest', err, jwtPayload, info, context, status]);
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !jwtPayload) {
      // console.error('JwtAuthGuard.UnauthorizedException');
      throw new UnauthorizedException();
    }
    return jwtPayload;
  }
}
