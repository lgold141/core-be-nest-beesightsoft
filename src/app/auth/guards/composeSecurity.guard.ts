import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LogoutDeviceGuard } from '../../device/guard/logout-device.guard';

@Injectable()
export class ComposeSecurity implements CanActivate {
  constructor(private jwtAuthGuard: JwtAuthGuard,
    private logoutDeviceGuard: LogoutDeviceGuard) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    return (
      (await this.jwtAuthGuard.canActivate(context)) &&
      (await this.logoutDeviceGuard.canActivate(context))
    );
  }
}
