import { Body, Controller, Get, Post, Req, UseGuards, Query } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CommandBus } from '@nestjs/cqrs';
import { AbstractApiController, IApiServiceResponseInterface, SerializeHelper, RequestContext } from '@nest-core/core';
import { PinoAppLoggerService, JwtPayloadInterface } from '@nest-core/shared';
import { CurrentUser } from '../decorators';
import { AuthService } from '../auth.service';
import { AuthRegisterCommand } from '../commands';
import {
  JWTAuthDto,
  UserAuthLoginDto,
  UserAuthRegisterDto,
  UserAuthRequestPasswordDto,
  UserAuthResetPasswordDto,
  UserTransformDto,
  UserChangePasswordDto
} from '../dto';
import { ComposeSecurity } from '../guards/composeSecurity.guard';
import { stripeZeroOut } from 'src/utils/phoneNumber.utils';
import { UserAuthForgotPasswordDto, UserChangeForgotPasswordDto } from '../dto/crud-options.dto';

@ApiTags('Auth')
@Controller()
export class AuthController extends AbstractApiController {
  constructor(private readonly authService: AuthService,
    private readonly commandBus: CommandBus,
    private readonly logger: PinoAppLoggerService) {
    super();
    this.logger.setContext(AuthController.name);
  }

  @Post('register')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(UserAuthRegisterDto, null)
  })
  async register(@Body() userRegisterDto: UserAuthRegisterDto): Promise<void> {
    const errors = await this.validateWith(userRegisterDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: IApiServiceResponseInterface<UserTransformDto> = await this.commandBus.execute(new AuthRegisterCommand(userRegisterDto));
      this.logger.debug(`[register] User ${userRegisterDto.email} register`);
      this.respondWithErrorsOrSuccess<UserTransformDto>(data);
    }
  }

  @Post('resendOTPRegister')
  async resendOTPReister(@Body() resendOTPDto: UserAuthRegisterDto): Promise<void> {
    const errors = await this.validateWith(resendOTPDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      resendOTPDto.phoneNumber = resendOTPDto.phoneNumber ? stripeZeroOut(resendOTPDto.phoneNumber) : undefined;
      let data = await this.authService.resendOTPRegister(resendOTPDto);
      this.respondWithErrorsOrSuccess(data);
    }
  }

  @Post('verifiedAccount')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(JWTAuthDto, null),
    description: 'User info with access token',
  })
  async verifiedAccount(@Body() userAuthRegister: UserAuthRegisterDto, @Query('code') code: number, @Query('type') type: string): Promise<void> {
    const errors = await this.validateWith(userAuthRegister);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: IApiServiceResponseInterface<JWTAuthDto> = await this.authService.verifiedAccount(userAuthRegister, code, type);
      this.respondWithErrorsOrSuccess<JWTAuthDto>(data);
    }
  }

  @Post('login')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(JWTAuthDto, null),
    description: 'User info with access token',
  })
  async login(@Body() userLoginDto: UserAuthLoginDto): Promise<void> {
    const errors = await this.validateWith(userLoginDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: IApiServiceResponseInterface<JWTAuthDto> = await this.authService.login(userLoginDto, RequestContext.currentRequest());
      this.logger.debug(`[login] User ${userLoginDto.email} logging`);
      this.respondWithErrorsOrSuccess<JWTAuthDto>(data);
    }
  }

  @Post('comfirmOTP')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(JWTAuthDto, null),
    description: 'User info with access token',
  })
  async confirmOTP(@Body() userLoginDto: UserAuthLoginDto, @Query('code') code: number): Promise<void> {
    const errors = await this.validateWith(userLoginDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: IApiServiceResponseInterface<JWTAuthDto> = await this.authService.confirmOTP(userLoginDto, code, RequestContext.currentRequest());
      this.logger.debug(`[login] User ${userLoginDto.email} logging`);
      this.respondWithErrorsOrSuccess<JWTAuthDto>(data);
    }
  }

  @Post('request-password')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(Boolean, 'RequestPassword'),
    description: 'User info with access token',
  })
  async requestPass(@Body() userAuthRequestPasswordDto: UserAuthRequestPasswordDto): Promise<void> {
    const data: IApiServiceResponseInterface<boolean> = await this.authService.requestPassword(userAuthRequestPasswordDto);
    this.respondWithErrorsOrSuccess<boolean>(data);
  }

  @UseGuards(ComposeSecurity)
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(UserTransformDto, null),
    description: 'User info with access token',
  })
  @Post('reset-password')
  async resetPassword(@Body() userAuthResetPasswordDto: UserAuthResetPasswordDto, @CurrentUser() user: JwtPayloadInterface): Promise<void> {
    const data: IApiServiceResponseInterface<UserTransformDto> = await this.authService.resetPassword(userAuthResetPasswordDto, user);
    this.respondWithErrorsOrSuccess<UserTransformDto>(data);
  }

  @UseGuards(ComposeSecurity)
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(UserTransformDto, null),
    description: 'Me',
  })
  @Get('me')
  async getProfile(@Req() req): Promise<void> {
    const data: IApiServiceResponseInterface<UserTransformDto> = await this.authService.getAuthenticatedUser(req.user.id);
    this.respondWithErrorsOrSuccess<UserTransformDto>(data);
  }

  @UseGuards(ComposeSecurity)
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(UserTransformDto, null),
    description: 'Me2',
  })
  @Get('me2')
  async getProfile2(@CurrentUser() user: JwtPayloadInterface): Promise<void> {
    const data: IApiServiceResponseInterface<UserTransformDto> = await this.authService.getAuthenticatedUser(user.id);
    this.respondWithErrorsOrSuccess<UserTransformDto>(data);
  }

  @Get('checkCompanyAddress')
  async checkCompanyAddress(@Query("companyAddress") companyAddress: string) {
    return await this.authService.checkCompanyAddress(companyAddress);
  }

  @Post('resendOTP')
  async resendOTP(@Body() resendOTPDto: UserAuthLoginDto): Promise<void> {
    const errors = await this.validateWith(resendOTPDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      resendOTPDto.phoneNumber = resendOTPDto.phoneNumber ? stripeZeroOut(resendOTPDto.phoneNumber) : undefined;
      let data = await this.authService.resendOTP(resendOTPDto);
      this.respondWithErrorsOrSuccess(data);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post('changePassword')
  async changePassword(@Req() req, @Body() userChangePasswordDto: UserChangePasswordDto): Promise<any> {
    try {
      const errors = await this.validateWith(userChangePasswordDto);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      let changePassword = await this.authService.changePassword(req, userChangePasswordDto);
      this.respondWithErrorsOrSuccess(changePassword);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('forgotPassword')
  async forgotPassword(@Body() userAuthForgotPasswordDto: UserAuthForgotPasswordDto): Promise<any> {
    try {
      const errors = await this.validateWith(userAuthForgotPasswordDto);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const forgotPassword = await this.authService.forgotPassword(userAuthForgotPasswordDto);
      this.respondWithErrorsOrSuccess(forgotPassword);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('confirmForgotPasswordOTP')
  @ApiOkResponse({
    type: SerializeHelper.createGetOneDto(JWTAuthDto, null),
    description: 'User info with access token',
  })
  async confirmForgotPasswordOTP(@Body() userAuthForgotPasswordDto: UserAuthForgotPasswordDto, @Query('code') code: number): Promise<void> {
    const errors = await this.validateWith(userAuthForgotPasswordDto);
    if (errors.length > 0) {
      this.respondWithErrors(errors);
    } else {
      const data: IApiServiceResponseInterface<JWTAuthDto> = await this.authService.confirmForgotPasswordOTP(userAuthForgotPasswordDto, code, RequestContext.currentRequest());
      this.respondWithErrorsOrSuccess<JWTAuthDto>(data);
    }
  }

  @UseGuards(ComposeSecurity)
  @Post('changeForgotPassword')
  async changeForgotPassword(@Req() req, @Body() userChangePasswordDto: UserChangeForgotPasswordDto): Promise<any> {
    try {
      const errors = await this.validateWith(userChangePasswordDto);
      if (errors.length > 0) {
        return this.respondWithErrors(errors);
      }
      const changePassword = await this.authService.changeForgotPassword(req, userChangePasswordDto);
      this.respondWithErrorsOrSuccess(changePassword);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}