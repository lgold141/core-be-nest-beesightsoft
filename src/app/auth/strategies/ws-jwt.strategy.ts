import {Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {WsException} from '@nestjs/websockets';
import {ConfigService} from '@nest-core/config';
import {JwtPayloadInterface} from '@nest-core/shared';
import {AuthService} from '../auth.service';


const extractJwtFromWsQuery = req => {
  let token = null;
  if (req.handshake.query && req.handshake.query.token) {
    {
      token = req.handshake.query.token;
    }
    return token;
  }
};

@Injectable()
export class WsJwtStrategy extends PassportStrategy(Strategy, 'ws-jwt') {
  constructor(public readonly configService: ConfigService,
              private readonly authService: AuthService) {
    super({
      jwtFromRequest: extractJwtFromWsQuery, // ExtractJwt.fromUrlQueryParameter('token'),
      secretOrKey: configService.get('jwt.secretOrKey'),
      algorithm: [configService.get('jwt.signOptions.algorithm')],
    });
  }

  async validate(payload: JwtPayloadInterface): Promise<JwtPayloadInterface> {
    console.log(['WsJwtStrategy', {payload}]);
    const {iat, exp, id: userId} = payload;
    const timeDiff = exp - iat;
    if (timeDiff <= 0) {
      throw new WsException('UnauthorizedException');
    }
    // const user = await this.authService.getAuthenticatedUser(userId);
    // it's stateful token again
    // https://dev.to/nasterblue/json-web-tokens-jwt-la-gi-16f1
    // if (!user) {
    //   throw new WsException('UnauthorizedException');
    // }
    return payload;
  }
}
