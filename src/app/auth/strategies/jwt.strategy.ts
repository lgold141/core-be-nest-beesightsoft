import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {ConfigService} from '@nest-core/config';
import {JwtPayloadInterface} from '@nest-core/shared';
import {AuthService} from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(public readonly configService: ConfigService,
              private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('jwt.secretOrKey'),
      algorithm: [configService.get('jwt.signOptions.algorithm')],
    });
  }

  async validate(payload: JwtPayloadInterface): Promise<JwtPayloadInterface> {
    console.log(['JwtStrategy', {payload}]);
    const {iat, exp, id: userId} = payload;
    const timeDiff = exp - iat;
    if (timeDiff <= 0) {
      throw new UnauthorizedException();
    }
    // const user = await this.authService.getAuthenticatedUser(userId);
    // // it's stateful token again
    // // https://dev.to/nasterblue/json-web-tokens-jwt-la-gi-16f1
    // if (!user) {
    //   throw new UnauthorizedException();
    // }
    return payload;
  }
}
