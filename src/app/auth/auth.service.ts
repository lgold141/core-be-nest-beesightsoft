import { Injectable, Inject } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { FindOneOptions, } from 'typeorm';
import { IApiServiceResponseInterface, UserEntity, AuthUserService } from '@nest-core/core';
import { JwtPayloadInterface, UtilsService } from '@nest-core/shared';
import { ConfigService } from '@nest-core/config';
import { validateEmail } from '../../utils/email.utils';
import { validatePhoneNumber, stripeZeroOut } from '../../utils/phoneNumber.utils';

// import {EmailService} from '../email/email.service';
import {
  JWTAuthDto,
  JWTAuthPayloadDto,
  UserAuthLoginDto,
  UserAuthRegisterDto,
  UserAuthRequestPasswordDto,
  UserAuthResetPasswordDto,
  UserTransformDto,
  UserChangePasswordDto,
} from './dto';
import { CodeEnum } from '@app/api-error-code';
import { CodeService } from '../code';
import { TwillioService } from '../twilio';
import { TransformCodeDto } from '../code/dto';
import { checkExpiredTime } from '../../utils/code.utils';
import { DevicesService } from '../device';
import { getIpFromRequest } from '../../utils/request.utils';
import { UpdateUserDto } from '../user/dto';
import { UserService } from '../user';
import { UserAuthForgotPasswordDto, UserChangeForgotPasswordDto } from './dto/crud-options.dto';

export enum Provider {
  GOOGLE = 'google',
  FACEBOOK = 'facebook'
}

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService,
    private configService: ConfigService,
    private twilioService: TwillioService,
    private codeService: CodeService,
    @Inject(AuthUserService) private readonly authUserService: AuthUserService,
    private devicesService: DevicesService,
    private userService: UserService) {
  }

  /**
   * Register & login begin
   * */

  async findByEmail(email: string): Promise<IApiServiceResponseInterface<UserTransformDto>> {
    try {
      const user = await this.authUserService.findOne({ email });
      if (!user) {
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        };
      } else {
        return {
          data: UtilsService.plainToClass(UserTransformDto, user)
        }
      }
    } catch (ex) {
      console.error(['ex', ex]);
      return {
        code: CodeEnum.AUTH_USER_NOTFOUND
      };
    }
  }

  async getAuthenticatedUser(id: string): Promise<IApiServiceResponseInterface<UserTransformDto>> {
    try {
      const user = await this.authUserService.findOne(id);
      if (!user) {
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        };
      } else {
        return {
          data: UtilsService.plainToClass(UserTransformDto, user)
        }
      }
    } catch (ex) {
      console.error(['ex', ex]);
      return {
        code: CodeEnum.AUTH_USER_NOTFOUND
      };
    }
  }

  hasNumber(myString: string): boolean {
    return /\d/.test(myString);
  }

  async register(userAuthRegisterDto: UserAuthRegisterDto): Promise<IApiServiceResponseInterface<UserTransformDto>> {
    const account = userAuthRegisterDto.account;
    if (await validateEmail(account)) {
      userAuthRegisterDto.email = account;
      userAuthRegisterDto.verifiedEmail = false;
    } else if (await validatePhoneNumber(account)) {
      userAuthRegisterDto.phoneNumber = stripeZeroOut(account);
      userAuthRegisterDto.verifiedPhoneNumber = false;
    } else {
      return {
        code: CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
      }
    }

    delete userAuthRegisterDto.account;

    // Check duplicate account
    var checkAccount;
    if (userAuthRegisterDto.email) {
      checkAccount = await this.userService.findOne({ email: userAuthRegisterDto.email });
      if (checkAccount && checkAccount.registerCompleted) {
        return {
          code: CodeEnum.EMAIL_EXISTED
        }
      }
    } else if (userAuthRegisterDto.phoneNumber) {
      checkAccount = await this.userService.findOne({ phoneNumber: userAuthRegisterDto.phoneNumber });
      if (checkAccount && checkAccount.registerCompleted) {
        return {
          code: CodeEnum.NUMBER_PHONE_EXISTED
        }
      }
    }
    const checkPassword = userAuthRegisterDto.password;
    if(checkPassword.length < 6 || checkPassword.length > 30 || !this.hasNumber(checkPassword)){
      return {
        code: CodeEnum.SIGNUP_PASSWORD_INVALID
      }
    }

    var user;
    if (!checkAccount) {
      userAuthRegisterDto.registerCompleted = false;
      user = await this.authUserService.create({
        ...userAuthRegisterDto,
        ... {
          password: UtilsService.generateHashPassword(userAuthRegisterDto.password)
        }
      } as any);
    } else {
      userAuthRegisterDto.registerCompleted = false;
      userAuthRegisterDto.password = UtilsService.generateHashPassword(userAuthRegisterDto.password);
      await this.authUserService.update({ id: checkAccount.id }, userAuthRegisterDto);
      user = checkAccount;
    }

    if (userAuthRegisterDto.phoneNumber) {
      await this.codeService.sendOTP(user, "register", false);
    } else {
      await this.codeService.sendOTPByEmail(user, "register", false);
    }

    // Return error code - confirm
    return {
      code: CodeEnum.VERIFIED_ACCOUNT
    };
  }

  async resendOTPRegister(userAuthRegisterDto: UserAuthRegisterDto): Promise<IApiServiceResponseInterface<boolean>> {
    const account = userAuthRegisterDto.account;
    if (await validateEmail(account)) {
      userAuthRegisterDto.email = account;
      userAuthRegisterDto.verifiedEmail = false;
    } else if (await validatePhoneNumber(account)) {
      userAuthRegisterDto.phoneNumber = stripeZeroOut(account);
      userAuthRegisterDto.verifiedPhoneNumber = false;
    } else {
      return {
        code: CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
      }
    }

    delete userAuthRegisterDto.account;

    // Check duplicate account
    var checkAccount;
    if (userAuthRegisterDto.email) {
      checkAccount = await this.userService.findOne({ email: userAuthRegisterDto.email });
      if (checkAccount && checkAccount.registerCompleted) {
        return {
          code: CodeEnum.EMAIL_EXISTED
        }
      }
    } else if (userAuthRegisterDto.phoneNumber) {
      checkAccount = await this.userService.findOne({ phoneNumber: userAuthRegisterDto.phoneNumber });
      if (checkAccount && checkAccount.registerCompleted) {
        return {
          code: CodeEnum.NUMBER_PHONE_EXISTED
        }
      }
    }
    const checkPassword = userAuthRegisterDto.password;
    if(checkPassword.length < 6 || checkPassword.length > 30 || !this.hasNumber(checkPassword)){
      return {
        code: CodeEnum.SIGNUP_PASSWORD_INVALID
      }
    }

    var user;
    if (!checkAccount) {
      userAuthRegisterDto.registerCompleted = false;
      user = await this.authUserService.create({
        ...userAuthRegisterDto,
        ... {
          password: UtilsService.generateHashPassword(userAuthRegisterDto.password)
        }
      } as any);
    } else {
      userAuthRegisterDto.registerCompleted = false;
      userAuthRegisterDto.password = UtilsService.generateHashPassword(userAuthRegisterDto.password);
      await this.authUserService.update({ id: checkAccount.id }, userAuthRegisterDto);
      user = checkAccount;
    }

    if (userAuthRegisterDto.phoneNumber) {
      let result = await this.codeService.sendOTP(user, "register", true);
      if (result.code) {
        return {
          code: result.code
        };
      }
    } else {
      let result = await this.codeService.sendOTPByEmail(user, "register", true);
      if (result.code) {
        return {
          code: result.code
        };
      }
    }

    // Return error code - confirm
    return {
      data: true
    };
  }

  async verifiedAccount(userAuthRegisterDto: UserAuthRegisterDto, otpCode: number, type: string): Promise<IApiServiceResponseInterface<JWTAuthDto>> {
    userAuthRegisterDto.phoneNumber = userAuthRegisterDto.phoneNumber ? stripeZeroOut(userAuthRegisterDto.phoneNumber) : undefined;
    const validator: IApiServiceResponseInterface<UserEntity> = await this.validateUser(userAuthRegisterDto);
    const { data: userData } = validator;

    const validateCode = {
      user: userData.id,
      code: otpCode,
      type: type
    }

    // // Hard code OTP to login
    // if (otpCode === 0) {
    //   let updateUser = await this.authUserService.update({id: userData.id}, userData);
    //   return {
    //     data: UtilsService.plainToClass(UserTransformDto, updateUser)
    //   };
    // }
    // // End hardcode

    let checkCode: IApiServiceResponseInterface<TransformCodeDto> = await this.codeService.getCode(validateCode);
    const { code, options, data } = checkCode;

    if (code) {
      return {
        code: code,
        options,
        data: null
      }
    }

    // Check expired
    if (checkExpiredTime(data.expiredAt)) {
      // Expired code, return error
      return {
        code: CodeEnum.EXPIRED_CODE
      }
    }

    // Remove code
    this.codeService.deleteCode(data.id);

    //Update verified user
    userData.verifiedPhoneNumber = userData.verifiedPhoneNumber === false ? true : null;
    userData.verifiedEmail = userData.verifiedEmail === false ? true : null;
    const updateUser = await this.userService.updateUser(userData.id, UtilsService.plainToClass(UpdateUserDto, userData, { excludeExtraneousValues: false }));
    const permission = await this.loginPermission(UtilsService.plainToClass(UserEntity, updateUser.data, { excludeExtraneousValues: false }));
    return {
      data: UtilsService.plainToClass(JWTAuthDto, permission.data)
    };
  }

  async validateUser(input: UserAuthLoginDto, options?: FindOneOptions<UserEntity>): Promise<IApiServiceResponseInterface<UserEntity>> {
    // relations: ['role', 'employee']
    const { password, ...findData } = input;
    Object.keys(findData).forEach(key => findData[key] === undefined && delete findData[key])

    if (!findData.email && !findData.phoneNumber) {
      return {
        code: CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL
      };
    }
    const user = await this.authUserService.findOne(findData, options);

    if (!user) {
      if (input.phoneNumber) {
        return {
          code: CodeEnum.NUMBER_PHONE_NOT_EXISTED
        };
      }
      if (input.email) {
        return {
          code: CodeEnum.EMAIL_NOT_EXISTED
        };
      }
      return {
        code: CodeEnum.LOGIN_USER_MUST_PHONE_OR_EMAIL
      };
    } else {
      if (!(await UtilsService.validateHashPassword(password, user.password))) {
        return {
          code: CodeEnum.AUTH_WRONG_PASSWORD
        };
      } else {
        return {
          data: user
        };
      }
    }
  }

  async login(input: UserAuthLoginDto, req): Promise<IApiServiceResponseInterface<JWTAuthDto>> {
    let ip = getIpFromRequest(req);
    let userAgent = req.headers['user-agent'];
    let checkEmail = input.email ? validateEmail(input.email) : true;
    if (!checkEmail) {
      return {
        code: CodeEnum.USER_EMAIL_INVALID,
        data: null
      }
    }

    let checkPhoneNumber = input.phoneNumber ? validatePhoneNumber(input.phoneNumber) : true;
    if (!checkPhoneNumber) {
      return {
        code: CodeEnum.USER_PHONENUMBER_INVALID,
        data: null
      }
    }

    input.phoneNumber = input.phoneNumber ? stripeZeroOut(input.phoneNumber) : undefined;
    input.registerCompleted = true;
    const validator: IApiServiceResponseInterface<UserEntity> = await this.validateUser(input);
    const { code, options, data } = validator;

    if (code) {
      return {
        code,
        options,
        data: null
      };
    }

    if (input.email && !data.verifiedEmail) {
      await this.codeService.sendOTPByEmail(data, "verify-user", false);
      return {
        code: CodeEnum.VERIFIED_EMAIL,
        options: {
          // phoneNumber: data.phoneNumber ? `${data.countryCode}${data.phoneNumber}` : undefined,
          email: data.email ? data.email : undefined
        }
      };
    } else if (input.phoneNumber && !data.verifiedPhoneNumber) {
      await this.codeService.sendOTP(data, "verify-user", false);
      return {
        code: CodeEnum.VERIFIED_NUMBER_PHONE,
        options: {
          phoneNumber: data.phoneNumber ? `${data.countryCode}${data.phoneNumber}` : undefined,
          // email: data.email ? data.email : undefined
        }
      };
    }

    let device = await this.devicesService.getCurrentDevice(ip, userAgent, data.id);
    if (!device) {
      if (input.email) {
        await this.codeService.sendOTPByEmail(data, "login", false);
        return {
          code: CodeEnum.CONFIRM_FIRST_LOGIN,
          options: {
            email: input.email
          }
        };
      } else if (input.phoneNumber) {
        await this.codeService.sendOTP(data, "login", false);
        return {
          code: CodeEnum.CONFIRM_FIRST_LOGIN,
        };
      }
    }

    await this.devicesService.createDevice(ip, userAgent, data.id);

    //Sign : Public claims
    //nasterblue:  https://www.iana.org/assignments/jwt/jwt.xhtml
    const permission = await this.loginPermission(data);
    return {
      data: permission as JWTAuthDto
    };
  }

  /**
   * Register & login end
   * */

  async generateJWTByPayload(jwtPayload: JwtPayloadInterface): Promise<JWTAuthPayloadDto> {
    const accessToken = this.jwtService.sign(
      jwtPayload,
      this.configService.get('jwt.signOptions')
    );
    return {
      expiresIn: this.configService.get('jwt.signOptions.expiresIn'),
      accessToken: accessToken
    };
  }

  /**
   * Request & Reset password begin
   * */
  async requestPassword(userAuthRequestPasswordDto: UserAuthRequestPasswordDto): Promise<IApiServiceResponseInterface<boolean>> {
    const { email, appSchema, webSchema } = userAuthRequestPasswordDto;
    const user = await this.authUserService.findOne({ email }, {
      // relations: ['role']
    });
    let token: string;

    if (user && user.id) {
      const payload: JwtPayloadInterface = { username: user.username, id: user.id };
      const jwt: JWTAuthPayloadDto = await this.generateJWTByPayload(payload);
      token = jwt.accessToken;

      if (token) {
        const app = `${appSchema}?token=${token}`;
        const web = `${webSchema}?token=${token}`;
        console.log({ app, web });
        //TODO :  Send these schema via email
        return {
          data: true
        };
      }
    } else {
      return {
        code: CodeEnum.AUTH_USER_NOTFOUND
      };
    }
  }

  async resetPassword(userAuthResetPasswordDto: UserAuthResetPasswordDto, jwtPayload: JwtPayloadInterface): Promise<IApiServiceResponseInterface<UserTransformDto>> {
    const user = await this.authUserService.changePassword(jwtPayload.id, UtilsService.generateHashPassword(userAuthResetPasswordDto.password));
    return {
      data: UtilsService.plainToClass(UserTransformDto, user)
    };
  }

  async confirmOTP(input: UserAuthLoginDto, otpCode: number, req): Promise<IApiServiceResponseInterface<JWTAuthDto>> {
    let ip = getIpFromRequest(req);
    let userAgent = req.headers['user-agent'];
    input.phoneNumber = input.phoneNumber ? stripeZeroOut(input.phoneNumber) : undefined;
    const validator: IApiServiceResponseInterface<UserEntity> = await this.validateUser(input);
    const { data: userData } = validator;

    const validateCode = {
      user: userData.id,
      code: otpCode,
      type: "login"
    }

    // Hard code OTP to login
    if (otpCode === 0) {
      await this.devicesService.createDevice(ip, userAgent, userData.id);
      return {
        data: await this.loginPermission(userData) as JWTAuthDto
      };
    }
    // End hardcode

    let checkCode: IApiServiceResponseInterface<TransformCodeDto> = await this.codeService.getCode(validateCode);
    const { code, options, data } = checkCode;

    if (code) {
      return {
        code: code,
        options,
        data: null
      }
    }

    // Check expired
    if (checkExpiredTime(data.expiredAt)) {
      // Expired code, return error
      return {
        code: CodeEnum.EXPIRED_CODE
      }
    }

    const permission = await this.loginPermission(userData);

    await this.devicesService.createDevice(ip, userAgent, userData.id);

    // Remove code
    this.codeService.deleteCode(data.id);

    return {
      data: permission as JWTAuthDto
    };
  }

  async loginPermission(data: UserEntity): Promise<IApiServiceResponseInterface<JWTAuthDto>> {
    const { password, ...currentUser } = data;

    const payload: JwtPayloadInterface = { username: null, id: data.id, registerCompleted: data.registerCompleted };
    const jwt: JWTAuthPayloadDto = await this.generateJWTByPayload(payload);
    const userDto: UserTransformDto = UtilsService.plainToClass(UserTransformDto, currentUser);
    return {
      data: {
        user: userDto,
        jwt: jwt
      }
    };
  }

  async checkCompanyAddress(address: string): Promise<IApiServiceResponseInterface<boolean>> {
    let companyAddress = await this.authUserService.findOne({ address: address });
    if (!companyAddress) {
      return {
        code: CodeEnum.COMPANY_ADDRESS_NOT_EXISTED
      }
    }
    return {
      data: true
    };
  }

  async resendOTP(input: UserAuthLoginDto): Promise<IApiServiceResponseInterface<boolean>> {
    try {
      const validator: IApiServiceResponseInterface<UserEntity> = await this.validateUser(input);
      const { code, options, data } = validator;

      if (code) {
        return {
          code,
          options,
          data: false
        };
      }

      if (input.phoneNumber) {
        data.phoneNumber = stripeZeroOut(input.phoneNumber);
        let result = await this.codeService.sendOTP(data, "login", true);
        if (result.code) {
          return {
            code: result.code
          };
        }
        return {
          data: true
        }
      } else if (input.email) {
        let result = await this.codeService.sendOTPByEmail(data, "login", true);
        if (result.code) {
          return {
            code: result.code
          };
        }
        return {
          data: true
        }
      }
      return {
        code: CodeEnum.AUTH_RESENDOTP_MUST_PHONE_OR_EMAIL
      }
    }
    catch (e) {
      throw e;
    }
  }

  async changePassword(req, userChangePasswordDto: UserChangePasswordDto): Promise<IApiServiceResponseInterface<any>> {
    try {
      if(!/\d/.test(userChangePasswordDto.newPassword)) {
        return {
          code: CodeEnum.SIGNUP_PASSWORD_INVALID
        };
      }

      if (userChangePasswordDto.password === userChangePasswordDto.newPassword) {
        return {
          code: CodeEnum.AUTH_NEWPASSWORD_SAME_CURRENTPASSWORD
        };
      }

      if (userChangePasswordDto.newPassword !== userChangePasswordDto.confirmPassword) {
        return {
          code: CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT
        };
      }
      let id = req.user.id;

      let userExist = await this.authUserService.findOne(id);
      if (!userExist) {
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND

        };
      }
 
      if (!(await UtilsService.validateHashPassword(userChangePasswordDto.password, userExist.password))) {
        return {
          code: CodeEnum.AUTH_CURRENT_PASSWORD_INCORRECT
        };
      }

      const changePassword = await this.authUserService.changePassword(id, UtilsService.generateHashPassword(userChangePasswordDto.newPassword));
      let ip = getIpFromRequest(req);
      let userAgent = req.headers['user-agent'];
      await this.devicesService.logoutOtherDevices(ip, userAgent, id);
      return {
        data: changePassword
      }
    }
    catch (e) {
      throw e;
    }
  }
  /**
   * Request & Reset password end
   * */

  async forgotPassword(userAuthForgotPasswordDto: UserAuthForgotPasswordDto): Promise<IApiServiceResponseInterface<any>> {
    const account = userAuthForgotPasswordDto.account;
    if (await validateEmail(account)) {
      userAuthForgotPasswordDto.email = account;
    } else if (await validatePhoneNumber(account)) {
      userAuthForgotPasswordDto.phoneNumber = stripeZeroOut(account);
    } else {
      return {
        code: CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
      }
    }

    delete userAuthForgotPasswordDto.account;
    let user: UserEntity; 
    if (userAuthForgotPasswordDto.phoneNumber) {
      user = await this.userService.findByPhoneNumber(userAuthForgotPasswordDto.phoneNumber);
      if(!user){
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        }
      }
      await this.codeService.sendOTP(user, "forgot-password",false);
    } else {
      user = await this.userService.findByEmail(userAuthForgotPasswordDto.email);
      if(!user){
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        }
      }
      await this.codeService.sendOTPByEmail(user, "forgot-password",false);
    }
    return {
      code: CodeEnum.AUTH_CONFIRM_FORGOT_PASSWORD_OTP,
    };
  }

  async confirmForgotPasswordOTP(input: UserAuthForgotPasswordDto, otpCode: number, req): Promise<IApiServiceResponseInterface<JWTAuthDto>> {
    const ip = getIpFromRequest(req);
    const userAgent = req.headers['user-agent'];
    const account = input.account;
    let userForgot: UserEntity;
    if (await validateEmail(account)) {
      input.email = account;
      userForgot = await this.userService.findByEmail(account);
      if(!userForgot){
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        }
      }
    } else if (await validatePhoneNumber(account)) {
      input.phoneNumber = stripeZeroOut(account);
      userForgot = await this.userService.findByPhoneNumber(account);
      if(!userForgot){
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        }
      }
    } else {
      return {
        code: CodeEnum.ACCOUNT_MUST_PHONE_OR_EMAIL
      }
    }
    
    const validateCode = {
      user: userForgot.id,
      code: otpCode,
      type: "forgot-password"
    }

    const checkCode: IApiServiceResponseInterface<TransformCodeDto> = await this.codeService.getCode(validateCode);
    const { code, options, data } = checkCode;

    if (code) {
      return {
        code: code,
        options,
        data: null
      }
    }

    // Check expired
    if (checkExpiredTime(data.expiredAt)) {
      // Expired code, return error
      return {
        code: CodeEnum.EXPIRED_CODE
      }
    }

    const permission = await this.loginPermission(userForgot);

    await this.devicesService.createDevice(ip, userAgent, userForgot.id);

    // Remove code
    this.codeService.deleteCode(data.id);

    return {
      data: permission as JWTAuthDto
    };
  }
  
  async changeForgotPassword(req, userChangePasswordDto: UserChangeForgotPasswordDto): Promise<IApiServiceResponseInterface<any>> {
    try {
      const id = req.user.id;
      const userExist = await this.authUserService.findOne(id);
      if (!userExist) {
        return {
          code: CodeEnum.AUTH_USER_NOTFOUND
        };
      }
      const updatePasswordAt = Date.parse(userExist.updatePasswordAt.toLocaleString());
      let token = req.headers['x-access-token'] || req.headers['authorization'];
      if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
      }
      if(token){
        const decoded = await this.jwtService.verifyAsync(token);
        const iat = decoded.iat * 1000
        if(updatePasswordAt > iat){
          return {
            code: CodeEnum.AUTH_TOKEN_INVALID
          };
        }
      }
      if(!/\d/.test(userChangePasswordDto.newPassword)) {
        return {
          code: CodeEnum.SIGNUP_PASSWORD_INVALID
        };
      }

      if (userChangePasswordDto.newPassword !== userChangePasswordDto.confirmPassword) {
        return {
          code: CodeEnum.AUTH_CONFIRMPASSWORD_INCORRECT
        };
      }
      const changePassword = await this.authUserService.changePassword(id, UtilsService.generateHashPassword(userChangePasswordDto.newPassword));
      const ip = getIpFromRequest(req);
      const userAgent = req.headers['user-agent'];
      await this.devicesService.logoutOtherDevices(ip, userAgent, id);
      return {
        data: changePassword
      }
    }
    catch (e) {
      throw e;
    }
  }
}
