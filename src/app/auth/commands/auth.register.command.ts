import {ICommand} from '@nestjs/cqrs';
import {UserAuthRegisterDto} from '../dto';

export class AuthRegisterCommand implements ICommand {
  static readonly type = '[Auth] Register';

  constructor(public readonly input: UserAuthRegisterDto) {
  }
}
