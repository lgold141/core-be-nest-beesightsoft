import {CommandHandler, ICommandHandler, EventPublisher} from '@nestjs/cqrs';
import {IApiServiceResponseInterface} from '@nest-core/core';
import {AuthRegisterCommand} from '../auth.register.command';
import {AuthService} from '../../auth.service';
import {UserAuthRegisterDto, UserTransformDto} from '../../dto';

@CommandHandler(AuthRegisterCommand)
export class AuthRegisterHandler implements ICommandHandler<AuthRegisterCommand> {
  constructor(private readonly authService: AuthService,
              private publisher: EventPublisher) {
  }

  public async execute(command: AuthRegisterCommand): Promise<IApiServiceResponseInterface<UserTransformDto>> {
    const {input} = command;
    return await this.authService.register(input as UserAuthRegisterDto);
  }
}
