import { Module, forwardRef } from '@nestjs/common';
import { SharedModule } from '@nest-core/shared';
import { AuthModule } from '../auth';
import { TwillioService } from '../twilio';

const PROVIDERS = [
  TwillioService
];


@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
  ],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class TwilioModule { }
