import {IEntityInterface} from '@nest-core/shared';

export interface ITwilioInterface {
  to: string;
  message: string | number;
}