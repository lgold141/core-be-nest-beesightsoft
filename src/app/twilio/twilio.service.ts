import { environment } from '@app/environment';

import { TwilioEntity } from './twilio.entity';

const twilio = require('twilio');

export class TwillioService {
  private twilioClient: any;
  constructor() {
    this.twilioClient = twilio(environment.twilio.sid, environment.twilio.auth);
  }

  async sendSMS(data: TwilioEntity): Promise<any> {
    try {
      const vObject = {
        body: `${environment.twilio.content}${data.message}`,
        from: environment.twilio.number,
        to: data.to,
      };

      return this.twilioClient.messages.create(vObject);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}