import { Entity, Index, Tree, Column, ManyToOne, CreateDateColumn, JoinColumn } from 'typeorm';
import { AbstractEntity, UserEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { ITwilioInterface } from './interfaces/twilio.interface';

@Entity('twilio')
export abstract class TwilioEntity implements ITwilioInterface {
  @Column()
  to: string;

  @Column()
  message: string;
}
