import { Entity, Tree, Column, TreeChildren, TreeParent } from 'typeorm';
import { AbstractEntity, UserEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, IsNumber, IsBoolean, Length } from 'class-validator';
import { Index, JoinColumn, ManyToMany, JoinTable, ManyToOne } from 'typeorm';
import { IFlagInterface } from './interfaces/flag.interface';


@Entity('flag')
export abstract class FlagEntity extends AbstractEntity implements IFlagInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  user: string;

  @ManyToOne(type => UserEntity, user => user.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "user" })
  userDate: UserEntity;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  address: string;
}
