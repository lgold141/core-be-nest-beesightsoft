import { IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { Expose } from 'class-transformer';
import { ManyToMany, ManyToOne, JoinColumn, JoinTable } from 'typeorm';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, UserEntity, } from '@nest-core/core';
import { FlagEntity } from '../flag.entity';

export class TransformFlagDto extends AbstractDto {
  @ApiProperty()
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  user: string;

  @ManyToOne(type => UserEntity, user => user.id)
  @JoinColumn({ name: "user" })
  @Expose()
  userData: UserEntity;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  address: string;
}

export class CreateFlagDto extends AbstractDto {
  @ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  user: string;

  @ApiProperty({ example: 'AAAA-BBBB-CCCC-DDDD' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  address: string;
}

export class UpdateFlagDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  user: string;

  @ApiProperty({ example: 'AAAA-BBBB-CCCC-DDDD' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  address: string;
}

export class SearchFlagDto extends PaginationParams<FlagEntity> {
  @ApiProperty({
    example: ['user', 'address']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformFlagDto, ['user', 'address'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
