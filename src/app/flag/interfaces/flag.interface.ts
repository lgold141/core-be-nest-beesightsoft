import {IEntityInterface} from '@nest-core/shared';

export interface IFlagInterface extends IEntityInterface {
  user: string;
  address: string;
}