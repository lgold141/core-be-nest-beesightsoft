import { Controller, UseGuards, Get, Post, Put, Delete, Body, UseInterceptors, UploadedFile, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { FlagEntity } from './flag.entity';
import { CreateFlagDto, SearchFlagDto, TransformFlagDto, UpdateFlagDto } from './dto';
import { FlagService } from './flag.service';

@ApiTags('Flags')
@Controller()
@CrudOptionsDtoDecorator({
  resourceName: 'Flag',
  createDto: CreateFlagDto,
  updateDto: UpdateFlagDto,
  transformDto: TransformFlagDto,
  searchDto: SearchFlagDto
})
export class FlagController extends CrudController<FlagEntity>  {
  constructor(private readonly flagService: FlagService,) {
    super(flagService);
  }
}
