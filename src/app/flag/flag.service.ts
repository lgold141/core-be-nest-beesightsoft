import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { FlagEntity } from './flag.entity';
import { CreateFlagDto, TransformFlagDto } from './dto';
import { UserService } from '../user';
import { CodeEnum } from '@app/api-error-code';
import { UtilsService } from '@nest-core/shared';

@Injectable()
export class FlagService extends CrudService<FlagEntity> {
  constructor(@InjectRepository(FlagEntity, DB_CONNECTION_DEFAULT) private readonly flagRepository: AbstractBaseRepository<FlagEntity>,
  private userService: UserService) {
    super(flagRepository);
  }

  async createFlag(flag: CreateFlagDto): Promise<IApiServiceResponseInterface<TransformFlagDto>> {
    try {
      let userExist = await this.userService.findOne({ id: flag.user });
      if (!userExist) {
        return {
          code: CodeEnum.USER_NOT_EXISTED
        };
      }

      let createFlag = await this.flagRepository.save(flag);
      return {
        data: UtilsService.plainToClass(TransformFlagDto, createFlag)
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getFlag(flag: CreateFlagDto): Promise<IApiServiceResponseInterface<TransformFlagDto>> {
    try {
      let getFlag = await this.flagRepository.findOne(flag);
      if (!getFlag) {
        return {
          code: CodeEnum.FLAG_NOT_EXISTED
        };
      }
      return {
        data: UtilsService.plainToClass(TransformFlagDto, getFlag)
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteFlag(id: string): Promise<IApiServiceResponseInterface<TransformFlagDto>> {
    try {
      let getFlag = await this.flagRepository.findOne(id);
      if (!getFlag) {
        return {
          code: CodeEnum.FLAG_NOT_EXISTED
        };
      }

      let deleteFLag = await this.flagRepository.delete(id);
      return {
        data: UtilsService.plainToClass(TransformFlagDto, deleteFLag)
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
