import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { FlagService } from './flag.service';
import { FlagEntity } from './flag.entity';
import { FlagController } from './flag.controller';

const PROVIDERS = [
  FlagService
];

const ENTITY = [
  FlagEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
  ],
  controllers: [FlagController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})

export class FlagModule {}
