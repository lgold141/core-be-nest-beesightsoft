import { IsNotEmpty, IsString, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { OneToMany, ManyToMany, ManyToOne, JoinColumn, JoinTable } from 'typeorm';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { VariationEntity } from '../variations.entity';
import { ImagesEntity } from '../../images';
import { ProductEntity } from 'src/app/products';

export class TransformVariationDto extends AbstractDto {
  @Expose()
  id: string;

  @Expose()
  name: string;

  @Expose()
  sku: string;

  @Expose()
  quantity: number;

  @Expose()
  barcode: string;

  @Expose()
  imageId: string;

  @ManyToOne(type => ImagesEntity, image => image.id)
  @JoinColumn({ name: "imageId" })
  @Expose()
  image: ImagesEntity;

  @Expose()
  productId: string;

  @ManyToOne(type => ProductEntity, product => product.id)
  @JoinColumn({ name: "productId" })
  @Expose()
  product: ProductEntity;
}

export class CreateVariationDto extends AbstractDto {
  @ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: 'SKU 345-091' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty({ example: 40 })
  @Expose()
  quantity: number;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  imageId: string;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  barcode: string;
  
  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  productId: string;
}

export class UpdateVariationDto extends AbstractDto {
  @ApiProperty({ example: 'Night Creams Will Help Your Skin To Relax Bec…' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: 'SKU 345-091' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  sku: string;

  @ApiProperty({ example: 40 })
  @Expose()
  quantity: number;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  imageId: string;

  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  barcode: string;
  
  @ApiProperty({ example: 'ABC-DEF-GHJ' })
  @Expose()
  productId: string;
}

export class SearchVariationDto extends PaginationParams<VariationEntity> {
  @ApiProperty({
    example: ['name', 'sku', 'quantity', 'barcode', 'productId']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformVariationDto, ['name', 'sku', 'quantity', 'barcode', 'productId'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
