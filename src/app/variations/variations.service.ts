import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { VariationEntity } from './variations.entity';
import { CreateVariationDto } from './dto';
import { CodeEnum, ApiErrorCodeService } from '@app/api-error-code';
import { ImageService } from '../images';

@Injectable()
export class VariationService extends CrudService<VariationEntity>   {
  constructor(@InjectRepository(VariationEntity, DB_CONNECTION_DEFAULT) private readonly variationRepository: AbstractBaseRepository<VariationEntity>,
    private errorCodeService: ApiErrorCodeService,
    private imageService: ImageService) {
    super(variationRepository);
  }

  async createVariation(productId: string, file: Object, variation: CreateVariationDto): Promise<any> {
    try {
      if (file) {
        let createImage = await this.imageService.upload(file);
        // variation.imageId = createImage.id;
      }

      variation.productId = productId;
      variation.sku = variation.sku ? variation.sku : variation.name.replace(/ /g, "-");

      let barcodeExist;
      if (!variation.barcode) {
        barcodeExist = true;
        while (barcodeExist) {
          variation.barcode = `${process.env.BARCODE}${Math.floor(1000000000 + Math.random() * 9000000000)}`;
          barcodeExist = await this.variationRepository.findOne({ barcode: variation.barcode }) === undefined ? false : true;
        }
      }

      barcodeExist = await this.variationRepository.findOne({ barcode: variation.barcode });
      if (barcodeExist) {
        return this.errorCodeService.getErrorMessage(CodeEnum.BARCODE_EXISTED);
      }

      let createVariation = await this.variationRepository.save(variation);
      if (!createVariation) {
        return this.errorCodeService.getErrorMessage(CodeEnum.AUTH_PASSWORD_REQUIRED);
      }
      return createVariation;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getVariation(productId: string, id: string): Promise<any> {
    try {
      let getVariation = await this.variationRepository.findOne({ id: id }, { relations: ["image", "product"] });
      if (!getVariation) {
        return this.errorCodeService.getErrorMessage(CodeEnum.AUTH_PASSWORD_REQUIRED);
      }
      return getVariation;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getAllVariations(productId: string): Promise<any> {
    try {
      let getAllVariations = await this.variationRepository.find({ relations: ["image", "product"] });
      if (!getAllVariations) {
        return this.errorCodeService.getErrorMessage(CodeEnum.AUTH_PASSWORD_REQUIRED);
      }
      return getAllVariations;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
