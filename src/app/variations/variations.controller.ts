import { Controller, UseGuards, Get, Post, Body, UseInterceptors, UploadedFile, Param } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { VariationEntity } from './variations.entity';
import { CreateVariationDto, SearchVariationDto, TransformVariationDto, UpdateVariationDto } from './dto';
import { VariationService } from './variations.service';
import { fileInterceptor } from '../../utils/file-uploading.utils';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Variations')
@Controller(':productId/variations')
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Variation',
  createDto: CreateVariationDto,
  updateDto: UpdateVariationDto,
  transformDto: TransformVariationDto,
  searchDto: SearchVariationDto
})
export class VariationController extends CrudController<VariationEntity>  {
  constructor(private readonly variationService: VariationService) {
    super(variationService);
  }

  @Post()
  @UseInterceptors(FileInterceptor('file', fileInterceptor('server')))
  async createVariation(@Param('productId') productId: string, @UploadedFile() file, @Body() data: CreateVariationDto) {
    return await this.variationService.createVariation(productId, file, data);
  }

  @Get(":id")
  async getProduct(@Param("productId") productId: string, @Param("id") id: string) {
    return await this.variationService.getVariation(productId, id);
  }

  @Post(":id")
  async getAllVariations(@Param("productId") productId: string) {
    return await this.variationService.getAllVariations(productId);
  }
}
