import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { VariationEntity } from './variations.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { VariationController } from './variations.controller';
import { VariationService } from './variations.service';
import { ImageService, ImagesEntity } from '../images';
import { CategoryService, CategoryEntity } from '../categories';
import { LocationService, LocationEntity } from '../locations';
import { TwillioService } from '../twilio';
import { CodeService, CodeEntity } from '../code';
import { UserEntity } from '@nest-core/core';
import { UserService } from '../user';

const PROVIDERS = [
  VariationService,
  ImageService,
  CategoryService,
  LocationService,
  TwillioService,
  UserService,
  CodeService
];

const ENTITY = [
  VariationEntity,
  ImagesEntity,
  CategoryEntity,
  LocationEntity,
  UserEntity,
  CodeEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => ImagesEntity),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
  ],
  controllers: [VariationController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class VariationModule { }
