import { Test, TestingModule } from '@nestjs/testing';
import { VariationController } from './variations.controller';

describe('Variations Controller', () => {
  let controller: VariationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VariationController],
    }).compile();

    controller = module.get<VariationController>(VariationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
