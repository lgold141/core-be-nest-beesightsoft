import { Entity } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn, ManyToMany, JoinTable, PrimaryColumn, ManyToOne } from 'typeorm';
import { IVariationsInterface } from './interfaces/variations.interface';
import { ImagesEntity } from '../images';
import { ProductEntity } from '../products';

@Entity('variations')
export abstract class VariationEntity extends AbstractEntity implements IVariationsInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column('text', { nullable: true })
  name: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  sku: string;

  @IsNumber()
  @Column()
  quantity: number;

  @Index()
  @Column()
  barcode: string;

  @Column({ nullable: true })
  imageId: string;

  @ManyToOne(type => ImagesEntity,
    image => image.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinTable({ name: "imageId" })
  image: ImagesEntity;

  @Column({ nullable: true })
  productId: string;

  @ManyToOne(type => ProductEntity,
    product => product.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinTable({ name: "productId" })
  product: ProductEntity;
}
