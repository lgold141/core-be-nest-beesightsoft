import {IEntityInterface} from '@nest-core/shared';
import {ImagesEntity} from '../../images';

export interface IVariationsInterface extends IEntityInterface {
  id: string;
  name: string;
  sku: string;
  quantity: number;
  barcode: string;
  imageId: string;
  productId: string;
}