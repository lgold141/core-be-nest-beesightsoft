import {Controller, Get} from '@nestjs/common';
import {
  DiskHealthIndicator,
  DNSHealthIndicator,
  HealthCheck,
  HealthCheckService,
  MemoryHealthIndicator,
  TypeOrmHealthIndicator,
} from '@nestjs/terminus';

@Controller('health')
export class HealthController {
  constructor(private health: HealthCheckService,
              private dns: DNSHealthIndicator,
              private disk: DiskHealthIndicator,
              private memory: MemoryHealthIndicator,
              private readonly db: TypeOrmHealthIndicator) {
  }

  @Get('ready')
  @HealthCheck()
  ready() {
    return this.health.check([
      async () => this.dns.pingCheck('google', 'https://google.com'),
      async () => this.memory.checkHeap('memory_heap', 200 * 1024 * 1024),
      async () => this.memory.checkRSS('memory_rss', 3000 * 1024 * 1024),
      async () => this.disk.checkStorage('storage', {thresholdPercent: 0.8, path: '/'})
    ]);
  }

  @Get('live')
  @HealthCheck()
  live() {
    return this.health.check([
      async () => this.db.pingCheck('database', {timeout: 300})
    ]);
  }
}

