import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { ImagesEntity } from './images.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { ImageController } from './images.controller';
import { ImageService } from './images.service';
import { MulterModule } from '@nestjs/platform-express';

const PROVIDERS = [
  ImageService
];

const ENTITY = [
  ImagesEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
  ],
  controllers: [ImageController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class ImageModule { }
