import { Controller, UseGuards, Get, Post, Body, UploadedFile, UseInterceptors, Put, Param, Res } from '@nestjs/common';
import { ApiConsumes, ApiProduces, ApiBody } from "@nestjs/swagger"
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { JwtAuthGuard } from '@app/auth';
import { ImagesEntity } from './images.entity';
import { CreateImageDto, SearchImageDto, TransformImageDto, UpdateImageDto } from './dto';
import { ImageService } from './images.service';
import { fileInterceptor } from '../../utils/file-uploading.utils';

@ApiTags('Images')
@Controller()
@CrudOptionsDtoDecorator({
  resourceName: 'Image',
  createDto: CreateImageDto,
  updateDto: UpdateImageDto,
  transformDto: TransformImageDto,
  searchDto: SearchImageDto
})
export class ImageController extends CrudController<ImagesEntity> {
  constructor(private readonly imageService: ImageService) {
    super(imageService);
  }

  @Get(':imgpath')
  async getImage(@Param('imgpath') image, @Res() res) {
    try {
      return await this.imageService.getImage(image, res);
    } catch (e) {
      return e;
    }
  }
}
