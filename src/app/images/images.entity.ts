import { Entity, ManyToOne } from 'typeorm';
import { AbstractEntity } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn } from 'typeorm';
import { IImageInterface } from './interfaces/images.interface';

@Entity('images')
export abstract class ImagesEntity extends AbstractEntity implements IImageInterface {
  // @ManyToOne(
  //   type => ImagesEntity,
  //   {onDelete: 'CASCADE', nullable: false},
  // )
  // id?: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  name: string;

  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  path: string;
}
