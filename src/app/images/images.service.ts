import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { ImagesEntity } from './images.entity';
import { CodeEnum, ApiErrorCodeService } from '@app/api-error-code';
import { CreateImageDto, TransformImageDto } from './dto';
import AWS = require('aws-sdk');
var fs = require('fs');

@Injectable()
export class ImageService extends CrudService<ImagesEntity> {
  constructor(@InjectRepository(ImagesEntity, DB_CONNECTION_DEFAULT) private readonly imageRepository: AbstractBaseRepository<ImagesEntity>) {
    super(imageRepository);
  }

  async upload(file): Promise<IApiServiceResponseInterface<ImagesEntity>> {
    try {
      let image = {
        name: file.filename,
        path: file.path
      } as CreateImageDto;

      let createImage = await this.imageRepository.save(image);
      return {
        data: createImage
      };
    }
    catch (e) {
      console.log(e);
      throw e;
    }
  }

  async uploadArrayImages(files): Promise<IApiServiceResponseInterface<ImagesEntity[]>> {
    try {
      let response = [];
      files.forEach(file => {
        const fileReponse = {
          originalname: file.originalname,
          filename: file.filename,
          path: file.path
        };
        response.push(fileReponse);
      });

      let images = [];
      for (let i = 0; i < response.length; i++) {
        let img = await this.upload(response[i]);
        images.push(img.data);
      }
      return {
        data: images
      };
    }
    catch (e) {
      console.log(e);
      throw e;
    }
  }

  async delte(file): Promise<IApiServiceResponseInterface<TransformImageDto>> {
    try {
      return fs.unlinkSync(file.path);
    }
    catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteImageLocal(path): Promise<void> {
    fs.unlink(path, (err) => {
      if (err) {
        console.error(err)
        return
      }
    })
  }

  async deleteImageS3(file, res): Promise<IApiServiceResponseInterface<TransformImageDto>> {
    try {
      const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: file.path
      };

      await this.s3.deleteObject(params);
      return {
        data: null
      }
    }
    catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getImage(path: string, res) {
    try {
      return res.sendFile(path, { root: './' });
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  //****************** START HELPERS ******************//
  //***************************************************//
  s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  });
  //****************** END HELPERS ******************//
  //***************************************************//
}