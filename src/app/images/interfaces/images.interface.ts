import { IEntityInterface } from '@nest-core/shared';

export interface IImageInterface extends IEntityInterface {
  name: string;
  path: string;
}