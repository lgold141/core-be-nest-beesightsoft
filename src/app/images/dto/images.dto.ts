import { IsAscii, IsEmail, IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { ImagesEntity } from '../images.entity';

export class TransformImageDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'nghia vo' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  path: string;
}

export class CreateImageDto extends AbstractDto {
  @ApiProperty({ example: 'nghia vo' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  path: string;
}

export class UpdateImageDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'nghia vo' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: '/category/2020/06/05/nghia-vo.svg' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  path: string;
}

export class SearchImageDto extends PaginationParams<ImagesEntity> {
  @ApiProperty({
    example: ['name', 'createdAt', 'path']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformImageDto, ['name', 'path'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
