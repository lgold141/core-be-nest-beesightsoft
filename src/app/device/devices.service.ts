import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { DevicesEntity } from './devices.entity';

import {
  CreateDeviceDto
} from './dto'
import { parseString } from 'xml2js';
import { formatUserAgent } from '../../utils/devices.utils';
import { Not } from 'typeorm';
import { ILocation } from '../../utils/devices.utils';

@Injectable()
export class DevicesService extends CrudService<DevicesEntity>   {
  constructor(@InjectRepository(DevicesEntity, DB_CONNECTION_DEFAULT)
  private readonly devicesRepository: AbstractBaseRepository<DevicesEntity>,
    private httpService: HttpService) {
    super(devicesRepository);
  }

  async createDevice(ip: string, userAgent: string, userId: string): Promise<IApiServiceResponseInterface<DevicesEntity>> {
    let deviceExist = await this.getCurrentDevice(ip, userAgent, userId);
    let newDevice: DevicesEntity;
    if (deviceExist) {
      deviceExist.loginAt = new Date();
      newDevice = await this.devicesRepository.save(deviceExist);
    } else {
      newDevice = await this.devicesRepository.save(await this.getNewDevice(ip, userAgent, userId));
    }
    return {
      data: newDevice
    };
  }

  async getLocationByIP(ip: string): Promise<ILocation> {
    let ipExist = await this.devicesRepository.findOne({ deviceAddress: ip });
    if (ipExist) {
      return {
        country: ipExist.country,
        region: ipExist.region,
        city: ipExist.city
      }
    }
    let get = await this.httpService.get('http://www.geoplugin.net/xml.gp?ip=' + ip).toPromise();
    let country: string | null;
    let region: string | null;
    let city: string | null;
    parseString(get.data, (err, result) => {
      if (err) {
        throw err;
      }
      const json = JSON.parse(JSON.stringify(result, null, 4));
      country = json.geoPlugin.geoplugin_countryName[0] || null;
      region = json.geoPlugin.geoplugin_region[0] || null;
      city = json.geoPlugin.geoplugin_city[0] || null;
    });
    return {
      country,
      region,
      city
    };
  }

  async getNewDevice(ip: string, userAgent: string, userId: string): Promise<CreateDeviceDto> {
    let location = await this.getLocationByIP(ip);
    let _userAgent = formatUserAgent(userAgent);
    let newDevice = new CreateDeviceDto();
    newDevice.userId = userId;
    newDevice.name = _userAgent.name;
    newDevice.country = location.country;
    newDevice.region = location.region;
    newDevice.city = location.city;
    newDevice.deviceAddress = ip;
    newDevice.loginAt = new Date();
    newDevice.browserName = _userAgent.browserName;
    newDevice.browserVersion = _userAgent.browserVersion;
    newDevice.osName = _userAgent.osName;
    newDevice.osVersion = _userAgent.osVersion;
    newDevice.deviceVendor = _userAgent.deviceVendor;
    newDevice.deviceModel = _userAgent.deviceModel;
    return newDevice;
  }

  async getCurrentDevice(ip: string, userAgent: string, userId: string): Promise<DevicesEntity> {
    let device = await this.getNewDevice(ip, userAgent, userId);
    let deviceExist = await this.devicesRepository.findOne({
      userId: device.userId,
      name: device.name,
      country: device.country,
      region: device.region,
      city: device.city,
      deviceAddress: device.deviceAddress,
      browserName: device.browserName,
      browserVersion: device.browserVersion,
      osName: device.osName,
      osVersion: device.osVersion,
      deviceVendor: device.deviceVendor,
      deviceModel: device.deviceModel
    });
    return deviceExist;
  }

  async findAllDevices(userId: string): Promise<IApiServiceResponseInterface<DevicesEntity[]>> {
    let findAllDevices = await this.devicesRepository.find({ where: { userId }, order: { "loginAt": "DESC" } });
    return {
      data: findAllDevices
    }
  }

  async logoutOtherDevices(ip: string, userAgent: string, userId: string): Promise<void> {
    let device = await this.getCurrentDevice(ip, userAgent, userId);
    if (device) {
      let deviceId = device.id;
      await this.devicesRepository.delete({ id: Not(deviceId), userId });
    }
  }
}

