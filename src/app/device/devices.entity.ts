import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';
import { AbstractEntity, UserEntity } from '@nest-core/core';
import { IsNotEmpty, IsString } from 'class-validator';
import { IDevicesInterface } from './interfaces';

@Entity('devices')
export abstract class DevicesEntity extends AbstractEntity implements IDevicesInterface {
  @IsString()
  @IsNotEmpty()
  @Column()
  userId: string;

  @ManyToOne(type => UserEntity,
    user => user.id)
  @JoinColumn({ name: "userId" })
  user: UserEntity;

  @Column()
  name: string;

  @Column({ nullable: true })
  country: string;

  @Column({ nullable: true })
  region: string;

  @Column({ nullable: true })
  city: string;

  @Column()
  deviceAddress: string;

  @Column({
    type: 'timestamp without time zone'
  })
  loginAt: Date;

  @Column({ nullable: true })
  browserName: string;

  @Column({ nullable: true })
  browserVersion: string;

  @Column({ nullable: true })
  osName: string;

  @Column({ nullable: true })
  osVersion: string;

  @Column({ nullable: true })
  deviceVendor: string;

  @Column({ nullable: true })
  deviceModel: string;
}