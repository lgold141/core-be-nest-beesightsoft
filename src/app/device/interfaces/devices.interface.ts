import { IEntityInterface } from '@nest-core/shared';

export interface IDevicesInterface extends IEntityInterface {
  id: string;
  userId: string;
  name: string;
  country: string;
  region: string;
  city: string;
  deviceAddress: string;
  loginAt: Date;
  browserName: string;
  browserVersion: string;
  osName: string;
  osVersion: string;
  deviceVendor: string;
  deviceModel: string;
}