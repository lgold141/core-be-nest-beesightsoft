import { Controller, UseGuards, Post, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { DevicesEntity } from './devices.entity';
import { DevicesService } from './devices.service';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Devices')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Device',
  createDto: null,
  updateDto: null,
  transformDto: null,
  searchDto: null
})
export class DeviceController extends CrudController<DevicesEntity>  {
  constructor(private readonly devicesService: DevicesService) {
    super(devicesService);
  }

  @Post('findAll')
  async findAllDevices(@Req() req): Promise<void> {
    try {
      let category = await this.devicesService.findAllDevices(req.user.id);
      this.respondWithErrorsOrSuccess(category);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}