import { IsNotEmpty, IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { AbstractDto, } from '@nest-core/core';

export class TransformDeviceDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    id: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    userId: string;

    @Expose()
    name: string;

    @Expose()
    country: string;

    @Expose()
    region: string;

    @Expose()
    city: string;

    @Expose()
    deviceAddress: string;

    @Expose()
    loginAt : Date;

    @Expose()
    browserName: string | null;
  
    @Expose()
    browserVersion: string | null;
  
    @Expose()
    osName: string | null;

    @Expose()
    osVersion: string | null;
  
    @Expose()
    deviceVendor: string | null;
  
    @Expose()
    deviceModel: string | null;
}

export class CreateDeviceDto extends AbstractDto {
    id: string;

    userId: string;

    name: string;

    country: string;

    region: string;

    city: string;

    deviceAddress: string;

    loginAt : Date;

    browserName: string | null;
  
    browserVersion: string | null;
    
    osName: string | null;

    osVersion: string | null;
  
    deviceVendor: string | null;
  
    deviceModel: string | null;
}
