import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { DevicesService } from 'src/app/device/devices.service';
import { getIpFromRequest } from 'src/utils/request.utils';

@Injectable()
export class LogoutDeviceGuard implements CanActivate {
  constructor(private readonly devicesService: DevicesService) { }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest();
    let ip = getIpFromRequest(request);
    let userAgent = request.headers['user-agent'];
    let userId = request.user?.id;
    if (request.user?.registerCompleted) {
      if (!userId) {
        return true;
      }
      if (await this.devicesService.getCurrentDevice(ip, userAgent, userId)) {
        return true;
      } else {
        throw new UnauthorizedException();
      }
    } else {
      return true;
    }
  }
}