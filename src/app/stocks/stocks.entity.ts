import { Entity } from 'typeorm';
import { AbstractEntity, IsEmailUnique } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber, IsEmail, IsCurrency, IsPhoneNumber } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn } from 'typeorm';
import { IStockInterface } from './interfaces/stock.interface';

@Entity('stocks')
export abstract class StockEntity extends AbstractEntity implements IStockInterface {
  @IsNotEmpty()
  @Index()
  @Column()
  date: Date;

  @IsString()
  @Column()
  status: string;

  @IsString()
  @Column()
  location: string;

  @Index()
  @Column()
  category: string;

  @Index()
  @Column()
  qtyDifference: number;

  @Index()
  @Column()
  costDifference: number;
}