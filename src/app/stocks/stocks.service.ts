import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { StockEntity } from './stocks.entity';

@Injectable()
export class StockService extends CrudService<StockEntity> {
  constructor(@InjectRepository(StockEntity, DB_CONNECTION_DEFAULT) private readonly stockRepository: AbstractBaseRepository<StockEntity>) {
    super(stockRepository);
  }
}
