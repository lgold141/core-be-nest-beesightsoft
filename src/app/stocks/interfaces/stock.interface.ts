import {IEntityInterface} from '@nest-core/shared';

export interface IStockInterface extends IEntityInterface {
  id: string;
  date: Date;
  status: string;
  location: string;
  category: string;
  qtyDifference: number;
  costDifference: number;
}