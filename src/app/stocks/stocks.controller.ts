import { Controller, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { StockEntity } from './stocks.entity';
import { CreateSupplierDto, SearchSupplierDto, TranformStockDto, UpdateSupplierDto } from './dto';
import { StockService } from './stocks.service';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';


@ApiTags('Stocks')
@Controller('stocks')
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Stock',
  createDto: CreateSupplierDto,
  updateDto: UpdateSupplierDto,
  transformDto: TranformStockDto,
  searchDto: SearchSupplierDto
})
export class StockController extends CrudController<StockEntity> {
  constructor(private readonly stockService: StockService) {
    super(stockService);
  }
}
