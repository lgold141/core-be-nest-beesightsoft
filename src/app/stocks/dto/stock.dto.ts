import { IsAscii, IsEmail, IsNotEmpty, IsString, IsUUID, Length, IsCurrency, IsNumber } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { StockEntity } from '../stocks.entity';

export class TranformStockDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: '05/07/2020 22:34' })
  @IsNotEmpty()
  @Expose()
  date: Date;

  @ApiProperty({ example: 'In progress' })
  @Expose()
  status: string;

  @ApiProperty({ example: 'Store A' })
  @IsString()
  @Expose()
  location: string;

  @ApiProperty({ example: 'All categories' })
  @Expose()
  category: string;

  @ApiProperty({ example: '-10' })
  @Expose()
  qtyDifference: number;

  @ApiProperty({ example: '1400000' })
  @Expose()
  costDifference: number;
}

export class CreateSupplierDto extends AbstractDto {
  @ApiProperty({ example: '05/07/2020 22:34' })
  @IsNotEmpty()
  @Expose()
  date: Date;

  @ApiProperty({ example: 'In progress' })
  @Expose()
  status: string;

  @ApiProperty({ example: 'Store A' })
  @IsString()
  @Expose()
  location: string;

  @ApiProperty({ example: 'All categories' })
  @Expose()
  category: string;

  @ApiProperty({ example: '-10' })
  @Expose()
  qtyDifference: number;

  @ApiProperty({ example: '1400000' })
  @Expose()
  costDifference: number;
}

export class UpdateSupplierDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: '05/07/2020 22:34' })
  @IsNotEmpty()
  @Expose()
  date: Date;

  @ApiProperty({ example: 'In progress' })
  @Expose()
  status: string;

  @ApiProperty({ example: 'Store A' })
  @IsString()
  @Expose()
  location: string;

  @ApiProperty({ example: 'All categories' })
  @Expose()
  category: string;

  @ApiProperty({ example: '-10' })
  @Expose()
  qtyDifference: number;

  @ApiProperty({ example: '1400000' })
  @Expose()
  costDifference: number;
}

export class SearchSupplierDto extends PaginationParams<StockEntity> {
  @ApiProperty({
    example: ['date', 'status']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TranformStockDto, ['date', 'status'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      date: 'DESC'
    }
  })
  @Expose()
  order: any;
}
