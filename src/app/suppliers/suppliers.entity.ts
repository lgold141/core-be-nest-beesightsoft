import { Entity } from 'typeorm';
import { AbstractEntity, IsEmailUnique } from '@nest-core/core';
import { IsNotEmpty, IsString, Length, IsNumber, IsEmail, IsCurrency, IsPhoneNumber } from 'class-validator';
import { Column, Index, JoinColumn, OneToMany, OneToOne, RelationId, VersionColumn } from 'typeorm';
import { ISupplierInterface } from './interfaces/suppliers.interface';

@Entity('suppliers')
export abstract class SupplierEntity extends AbstractEntity implements ISupplierInterface {
  @IsString()
  @IsNotEmpty()
  @Index()
  @Column()
  name: string;

  @IsString()
  // @IsCurrency()
  @Column()
  currency: string;

  @IsString()
  @Column()
  country: string;

  @Index()
  @Column()
  phoneNumber: string;

  @Index({unique: true})
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Column()
  email: string;
}
