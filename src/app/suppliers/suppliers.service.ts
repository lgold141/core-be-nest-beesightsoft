import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AbstractBaseRepository, CrudService } from '@nest-core/core';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { SupplierEntity } from './suppliers.entity';

@Injectable()
export class SupplierService extends CrudService<SupplierEntity> {
  constructor(@InjectRepository(SupplierEntity, DB_CONNECTION_DEFAULT) private readonly supplierRepository: AbstractBaseRepository<SupplierEntity>) {
    super(supplierRepository);
  }
}
