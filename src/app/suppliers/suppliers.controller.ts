import { Controller, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator } from '@nest-core/core';
import { SupplierEntity } from './suppliers.entity';
import { CreateSupplierDto, SearchSupplierDto, TransformSupplierDto, UpdateSupplierDto } from './dto';
import { SupplierService } from './suppliers.service';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Suppliers')
@Controller('suppliers')
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Supplier',
  createDto: CreateSupplierDto,
  updateDto: UpdateSupplierDto,
  transformDto: TransformSupplierDto,
  searchDto: SearchSupplierDto
})
export class SupplierController extends CrudController<SupplierEntity> {
  constructor(private readonly supplierService: SupplierService) {
    super(supplierService);
  }
}
