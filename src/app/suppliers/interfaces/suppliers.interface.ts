import {IEntityInterface} from '@nest-core/shared';

export interface ISupplierInterface extends IEntityInterface {
  id: string;
  name: string;
  currency: string;
  country: string;
  phoneNumber: string;
  email: string;
}