import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { SupplierEntity } from './suppliers.entity'
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { SupplierController } from './suppliers.controller';
import { SupplierService } from './suppliers.service';

const PROVIDERS = [
  SupplierService
];

const ENTITY = [
  SupplierEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT)
  ],
  controllers: [SupplierController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class SupplierModule { }
