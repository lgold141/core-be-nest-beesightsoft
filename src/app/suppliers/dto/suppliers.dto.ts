import { IsAscii, IsEmail, IsNotEmpty, IsString, IsUUID, Length, IsCurrency } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, } from '@nest-core/core';
import { SupplierEntity } from '../suppliers.entity';

export class TransformSupplierDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'John Jackie' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: 'USD' })
  // @IsCurrency()
  @Expose()
  currency: string;

  @ApiProperty({ example: 'China' })
  @IsString()
  @Expose()
  country: string;

  @ApiProperty({ example: '+60111233492' })
  @Expose()
  phoneNumber: string;

  @ApiProperty({ example: 'jack@alibaba.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;
}

export class CreateSupplierDto extends AbstractDto {
  @ApiProperty({ example: 'John Jackie' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: 'USD' })
  // @IsCurrency()
  @Expose()
  currency: string;

  @ApiProperty({ example: 'China' })
  @IsString()
  @Expose()
  country: string;

  @ApiProperty({ example: '+60111233492' })
  @Expose()
  phoneNumber: string;

  @ApiProperty({ example: 'jack@alibaba.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;
}

export class UpdateSupplierDto extends AbstractDto {
  @ApiProperty({ example: 'f3609c42-3a53-4458-8ce9-7332439210ef' })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({ example: 'John Jackie' })
  @IsNotEmpty()
  @IsString()
  @Length(1, 100)
  @Expose()
  name: string;

  @ApiProperty({ example: 'USD' })
  // @IsCurrency()
  @Expose()
  currency: string;

  @ApiProperty({ example: 'China' })
  @IsString()
  @Expose()
  country: string;

  @ApiProperty({ example: '+60111233492' })
  @Expose()
  phoneNumber: string;

  @ApiProperty({ example: 'jack@alibaba.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(6, 100)
  @Expose()
  email: string;
}

export class SearchSupplierDto extends PaginationParams<SupplierEntity> {
  @ApiProperty({
    example: ['name', 'country', 'phoneNumber', 'email']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformSupplierDto, ['name', 'country', 'phoneNumber', 'email'])
  })

  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC',
      name: 'DESC'
    }
  })
  @Expose()
  order: any;
}
