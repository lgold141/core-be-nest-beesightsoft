import {Module} from '@nestjs/common';
import {AccessControlModule} from './access-control';

@Module({
  imports: [
    AccessControlModule
  ],
})
export class ExampleModule {
}
