import {Module} from '@nestjs/common';
import {AbacController, RbacController, AclController, PrinterController} from './controllers';
import {AccessControlService} from './access-control.service';
import {SharedModule, PinoAppLoggerService} from '@nest-core/shared';
import * as databaseConfig from '../../database';
import {CasbinModule} from '../../casbin';

const {DB_CONNECTION_DEFAULT} = databaseConfig;
import {join} from 'path';

@Module({
  imports: [
    SharedModule,
    CasbinModule.forRootAsync(
      join(__dirname, '..', 'src/app/example/access-control/rbac_model.conf'),
      databaseConfig[DB_CONNECTION_DEFAULT]
    )
  ],
  controllers: [
    // AclController,
    // AbacController,
    // RbacController,
    PrinterController
  ],
  providers: [AccessControlService]
})
export class AccessControlModule {
}
