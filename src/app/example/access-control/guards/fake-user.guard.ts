import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';


@Injectable()
export class FakeUserGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const fakeUser = {
      id: 123,
      username: 'nasterblue'
    };
    req.user = fakeUser;
    console.log({fakeUser});
    return true;
  }
}
