import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { toSgtin96TagUri, hexToSgtin96TagUri, sgtin96TagUriToHex } from '../../../../utils/barcode-epc';
import { toZpl, printZpl } from '../../../../utils/zpl-printer';
import { toSgtin96TagUriDto, hexToSgtin96TagUriDto, toZplDto } from '../dto/index.dto';


@ApiTags('Printer')
@Controller('printer')
export class PrinterController {
  constructor() {

  }

  @Post('toSgtin96TagUri')
  async toSgtin96TagUri(@Body() data: toSgtin96TagUriDto): Promise<string> {
    const sgtin96TagUri = await toSgtin96TagUri(
      data.CompanyPrefix,
      data.ItemReference,
      data.SerialNumber,
    );
    return sgtin96TagUri;
  }


  @Post('sgtin96TagUriToHex')
  async sgtin96TagUriToHex(@Body() data: toSgtin96TagUriDto): Promise<string> {
    const sgtin96TagUri = await toSgtin96TagUri(
      data.CompanyPrefix,
      data.ItemReference,
      data.SerialNumber,
    );
    return await sgtin96TagUriToHex(sgtin96TagUri);
  }

  @Post('hexToSgtin96TagUri')
  async hexToSgtin96TagUri(@Body() data: hexToSgtin96TagUriDto): Promise<string> {
    const epcHexadecimal = data.epcHexadecimal;
    console.log({ epcHexadecimal });
    return await hexToSgtin96TagUri(epcHexadecimal);
  }


  @Post('toZpl')
  async toZpl(@Body() data: toZplDto): Promise<string> {
    return await toZpl(data);
  }

  @Post('printZpl')
  async printZpl(@Body() data: toZplDto): Promise<any> {
    const zpl = await printZpl(data);
    return zpl;
  }
}
