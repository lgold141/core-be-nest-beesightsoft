import {Controller, Get, UseGuards} from '@nestjs/common';
import {ApiTags} from '@nestjs/swagger';
import {RbacResourceGuard, CasbinService, IRbacResourceInterface, UseRBAC, AccessActionEnum} from '../../../casbin';
import {FakeUserGuard} from '../guards/fake-user.guard';


import {
  RESOURCE_USER,
  ROLE_ADMIN,
  ROLE_USER,
  USER_DOMAIN,
  USER_PRIMARY,
  USER_SECONDARY
} from '../access-control.contstant';

const rbac: IRbacResourceInterface[] = [
  {
    resource: RESOURCE_USER,
    actions: [
      AccessActionEnum.create
    ]
  }
];

@ApiTags('RBAC (Role-Based Access Control)')
@UseRBAC(...rbac)
@UseGuards(FakeUserGuard, RbacResourceGuard)
@Controller('rbac')
export class RbacController {
  constructor(private readonly casbinService: CasbinService) {

  }

  @Get('reloadPolicy')
  async reloadPolicy(): Promise<any> {
    return await this.casbinService.reloadPolicy();
  }

  @Get('getPolicy')
  async getPolicy(): Promise<any> {
    return await this.casbinService.getPolicy();
  }

  @Get('getAllRoles')
  async getAllRoles(): Promise<any> {
    return await  this.casbinService.getAllRoles();
  }

  @Get('getAllObjects')
  async getAllObjects(): Promise<any> {
    return await  this.casbinService.getAllObjects();
  }

  @Get('getAllSubjects')
  async getAllSubjects(): Promise<any> {
    return await  this.casbinService.getAllSubjects();
  }

  @Get('getAllActions')
  async getAllActions(): Promise<any> {
    return await  this.casbinService.getAllActions();
  }

  @Get('addRoleForUser')
  async addRoleForUser(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.addRoleForUser(USER_PRIMARY, ROLE_ADMIN, USER_DOMAIN)],
      [USER_SECONDARY, await this.casbinService.addRoleForUser(USER_SECONDARY, ROLE_USER, USER_DOMAIN)],
    ]
  }

  @Get('getRolesForUser')
  async getRolesForUser(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.getRolesForUser(USER_PRIMARY, USER_DOMAIN)],
      [USER_SECONDARY, await this.casbinService.getRolesForUser(USER_SECONDARY, USER_DOMAIN)]
    ];
  }

  @Get('getImplicitRolesForUser')
  async getImplicitRolesForUser(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.getImplicitRolesForUser(USER_PRIMARY, USER_DOMAIN)],
      [USER_SECONDARY, await this.casbinService.getImplicitRolesForUser(USER_SECONDARY, USER_DOMAIN)]
    ];
  }

  @Get('getPermissionsForUser')
  async getPermissionsForUser(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.getPermissionsForUser(USER_PRIMARY)],
      [USER_SECONDARY, await this.casbinService.getPermissionsForUser(USER_SECONDARY)]
    ];
  }

  @Get('hasRoleForUser')
  async hasRoleForUser(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.hasRoleForUser(USER_PRIMARY, ROLE_ADMIN, USER_DOMAIN)],
      [USER_SECONDARY, await this.casbinService.hasRoleForUser(USER_SECONDARY, ROLE_USER, USER_DOMAIN)],
    ];
  }

  @Get('addPolicy')
  async addPolicy(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.addPolicy(...[USER_PRIMARY, 'user', 'create'])],
      [USER_PRIMARY, await this.casbinService.addPolicy(...[USER_PRIMARY, 'user', 'update'])],
      [USER_PRIMARY, await this.casbinService.addPolicy(...[USER_PRIMARY, 'user', 'delete'])],
      [USER_SECONDARY, await this.casbinService.addPolicy(...[USER_SECONDARY, 'user', 'create'])],
    ]
  }

  @Get('checkPermission')
  async checkPermission(): Promise<any> {
    return [
      [USER_PRIMARY, await this.casbinService.checkPermission(USER_PRIMARY, 'user', 'create')],
      [USER_PRIMARY, await this.casbinService.checkPermission(USER_PRIMARY, 'user', 'update')],
      [USER_PRIMARY, await this.casbinService.checkPermission(USER_PRIMARY, 'user', 'delete')],
      [USER_SECONDARY, await this.casbinService.checkPermission(USER_SECONDARY, 'user', 'create')],
      [USER_SECONDARY, await this.casbinService.checkPermission(USER_SECONDARY, 'user', 'update')],
      [USER_SECONDARY, await this.casbinService.checkPermission(USER_SECONDARY, 'user', 'delete')],
    ];
  }

}
