export const USER_PRIMARY = 'nasterblue';
export const USER_SECONDARY = 'lucifer';
export const USER_DOMAIN = null;
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_USER = 'ROLE_USER';
export const RESOURCE_USER = 'user';