import { Entity, Index, Tree, Column, ManyToOne, CreateDateColumn, JoinColumn } from 'typeorm';
import { AbstractEntity, UserEntity } from '@nest-core/core';
import { IsNotEmpty, Length, IsNumber } from 'class-validator';
import { ICodeInterface } from './interfaces/code.interface';

@Entity('code')
export abstract class CodeEntity extends AbstractEntity implements ICodeInterface {
  @IsNotEmpty()
  @Length(1, 4)
  @IsNumber()
  @Index()
  @Column()
  code: number;

  @Column()
  expiredAt: Date;

  @Column()
  user: string;

  @ManyToOne(type => UserEntity,
    user => user.id,
    { onDelete: 'CASCADE' })
  @JoinColumn({ name: "user" })
  userData: UserEntity;

  @Column()
  type: string;
}
