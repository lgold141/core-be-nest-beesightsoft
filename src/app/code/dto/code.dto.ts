import { IsNotEmpty, IsString, IsUUID, Length, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { AbstractDto, PaginationParams, UserEntity, } from '@nest-core/core';
import { CodeEntity } from '../code.entity';
import { CreateDateColumn, ManyToOne, JoinColumn } from 'typeorm';

export class TransformCodeDto extends AbstractDto {
  @Expose()
  id: string;

  @Expose()
  code: number;

  @Expose()
  expiredAt: Date;

  @Expose()
  user: string;

  @ManyToOne(type => UserEntity,
    user => user.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "user" })
  @Expose()
  userData: UserEntity;

  @Expose()
  type: string;
}

export class CreateCodeDto extends AbstractDto {
  @ApiProperty({ example: '1111' })
  @Length(1, 4)
  @IsNumber()
  @Expose()
  code: number;

  @ApiProperty({ example: '2020-07-24 17:14:50.477395' })
  @Expose()
  expiredAt: Date;

  @ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' })
  @Expose()
  user: string;

  @ManyToOne(type => UserEntity,
    user => user.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "user" })
  userData: UserEntity;

  @ApiProperty({ example: 'login' })
  @Expose()
  type: string;
}

export class UpdateCodeDto extends AbstractDto {
  @ApiProperty({ example: '1111' })
  @Length(1, 4)
  @IsNumber()
  @Expose()
  code: number;

  @ApiProperty({ example: '2020-07-24 17:14:50.477395' })
  @Expose()
  expiredAt: Date;

  @ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' })
  @Expose()
  user: string;

  @ManyToOne(type => UserEntity,
    user => user.id,
    { nullable: false, onDelete: 'SET NULL' })
  @JoinColumn({ name: "user" })
  userData: UserEntity;

  @ApiProperty({ example: 'login' })
  @Expose()
  type: string;
}

export class SearchCodeDto extends PaginationParams<CodeEntity> {
  @ApiProperty({
    example: ['code', 'user']
  })
  @Expose()
  select: any;

  @ApiProperty({
    type: PickType(TransformCodeDto, ['code', 'user'])
  })
  @Expose()
  where: any;

  @ApiProperty({
    example: {
      id: 'ASC'
    }
  })
  @Expose()
  order: any;
}

export class ValiDateCodeDto {
  @ApiProperty({ example: 8888 })
  @IsNumber()
  @Expose()
  code: number;

  @ApiProperty({ example: '868502fb-689e-4e25-984c-e0e9ccb0aad5' })
  @IsString()
  @Expose()
  user: string;

  @Expose()
  type: string;
}