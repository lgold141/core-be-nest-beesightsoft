import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '@nest-core/shared';
import { AuthModule } from '../auth';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { CodeService } from './code.service';
import { CodeController } from './code.controller';
import { CodeEntity } from './code.entity';
import { TwilioModule, TwillioService } from '../twilio';
import { EmailService, EmailCoreModule } from '../email';

const PROVIDERS = [
  CodeService,
  EmailService,
  TwillioService
];

const ENTITY = [
  CodeEntity,
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT)
  ],
  controllers: [CodeController],
  providers: [
    ...PROVIDERS,
    EmailCoreModule.getEmailConfig()
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class CodeModule { }
