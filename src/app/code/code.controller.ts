import { Controller, UseGuards, Post, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController, CrudOptionsDtoDecorator, UserEntity } from '@nest-core/core';
import { CreateCodeDto, SearchCodeDto, TransformCodeDto, UpdateCodeDto } from './dto';
import { CodeService } from './code.service';
import { CodeEntity } from './code.entity';
import { ComposeSecurity } from '../auth/guards/composeSecurity.guard';

@ApiTags('Code')
@Controller()
@UseGuards(ComposeSecurity)
@CrudOptionsDtoDecorator({
  resourceName: 'Code',
  createDto: CreateCodeDto,
  updateDto: UpdateCodeDto,
  transformDto: TransformCodeDto,
  searchDto: SearchCodeDto
})
export class CodeController extends CrudController<CodeEntity> {
  constructor(private readonly codeService: CodeService) {
    super(codeService);
  }

  @Post('resendOTP')
  async reSendOTP(@Body() user: UserEntity): Promise<void> {
    try {
      let code = await this.codeService.reSendOTP(user, "login", true);
      this.respondWithErrorsOrSuccess(code);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }

  @Post('findAll')
  async getAllCode(): Promise<void> {
    try {
      let code = await this.codeService.getALlCode();
      this.respondWithErrorsOrSuccess(code);
    } catch (e) {
      this.respondWithErrors(e);
    }
  }
}
