import { IEntityInterface } from '@nest-core/shared';

export interface ICodeInterface extends IEntityInterface {
  user: string;
  code: number;
  expiredAt: Date;
  type: string;
}