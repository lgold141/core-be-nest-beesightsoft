import { Injectable } from '@nestjs/common';
import { AbstractBaseRepository, CrudService, IApiServiceResponseInterface, UserEntity } from '@nest-core/core';
import { CodeEnum } from '@app/api-error-code';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { InjectRepository } from '@nestjs/typeorm';
import { CodeEntity } from './code.entity';
import { TransformCodeDto, ValiDateCodeDto } from './dto';
import { generateVerifyCode, checkExpiredTime } from '../../utils/code.utils';
import * as moment from 'moment'
import environment from "@app/environment";
import { UtilsService } from '@nest-core/shared';
import { TwillioService } from '../twilio';
import { EmailService } from '../email';

@Injectable()
export class CodeService extends CrudService<CodeEntity> {
  constructor(@InjectRepository(CodeEntity, DB_CONNECTION_DEFAULT)
  private readonly codeRepository: AbstractBaseRepository<CodeEntity>,
    private twilioService: TwillioService,
    private emailService: EmailService) {
    super(codeRepository);
  }

  async createCode(id: string, type: string, isCheckCodeExpire: boolean): Promise<IApiServiceResponseInterface<TransformCodeDto>> {
    try {
      
      // set don't check expire code
      isCheckCodeExpire = false;

      let checkCodeExpire = await this.codeRepository.findOne({
        user: id,
        type: type
      });
      let createCode: CodeEntity;
      if (checkCodeExpire) {
        if (!checkExpiredTime(checkCodeExpire.expiredAt) && isCheckCodeExpire) {
          return {
            code: CodeEnum.CODE_STILL_VALID
          }
        }
        checkCodeExpire.code = generateVerifyCode();
        checkCodeExpire.expiredAt = moment().add(environment.code.expireTime, 'minutes').toDate();

        createCode = await this.codeRepository.save(checkCodeExpire);
      }
      else {
        const code = {
          user: id,
          code: generateVerifyCode(),
          expiredAt: moment().add(environment.code.expireTime, 'minutes').toDate(),
          type
        }
        createCode = await this.codeRepository.save(code);
      }

      return {
        data: UtilsService.plainToClass(TransformCodeDto, createCode)
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getCode(code: ValiDateCodeDto): Promise<IApiServiceResponseInterface<TransformCodeDto>> {
    try {
      let getCode = await this.codeRepository.findOne(code);
      if (!getCode) {
        return {
          code: CodeEnum.CODE_INCORRECT
        };
      }
      return {
        data: UtilsService.plainToClass(TransformCodeDto, getCode)
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getALlCode(): Promise<IApiServiceResponseInterface<TransformCodeDto>> {
    try {
      let getAllCode = await this.codeRepository.find();

      return {
        data: UtilsService.plainToClass(TransformCodeDto, getAllCode)
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async deleteCode(id: string): Promise<IApiServiceResponseInterface<TransformCodeDto>> {
    try {
      let getCode = await this.codeRepository.findOne(id);
      if (!getCode) {
        return {
          code: CodeEnum.CODE_NOT_EXISTED
        };
      }

      let deleteCode = await this.codeRepository.delete(id);
      return {
        data: UtilsService.plainToClass(TransformCodeDto, deleteCode)
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async sendOTP(user: UserEntity, type: string, isCheckCodeExpire: boolean): Promise<any> {
    try {
      let createCode = await this.createCode(user.id, type, isCheckCodeExpire);
      if (createCode.code) {
        return {
          code: createCode.code
        }
      }
      const otp = {
        to: `${user.countryCode}${user.phoneNumber}`,
        message: createCode.data.code.toString()
      }
      this.twilioService.sendSMS(otp);
      return {
        code: null
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async sendOTPByEmail(user: UserEntity, type: string, isCheckCodeExpire: boolean): Promise<any> {
    try {
      let createCode = await this.createCode(user.id, type, isCheckCodeExpire);
      if (createCode.code) {
        return {
          code: createCode.code
        }
      }
      const mailOptions = {
        from: process.env.MAIL_HOST,
        to: user.email,
        subject: 'OTP',
        template: 'sendOTP',
        context: {
          otp: createCode.data.code.toString()
        }
      };
      this.emailService.sendMail(mailOptions);
      return {
        code: null
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async reSendOTP(user: UserEntity, type: string, isCheckCodeExpire: boolean): Promise<any> {
    try {
      if (user.phoneNumber) {
        return await this.sendOTP(user, type, isCheckCodeExpire);
      } else if (user.email) {
        return await this.sendOTPByEmail(user, type, isCheckCodeExpire)
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
