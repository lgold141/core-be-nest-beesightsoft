import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { AppBootstrapModule } from './app.bootstrap.module';
import { DatabaseModule, DB_CONNECTION_DEFAULT } from './database';
import { ImageEntity, ProfileEntity, UserEntity } from '@nest-core/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from './products';
import { SupplierEntity } from './suppliers';
import { StockEntity } from './stocks';
import { ImagesEntity } from './images';
import { CategoryEntity } from './categories';
import { LocationEntity } from './locations';
import { VariationEntity } from './variations';
import { CodeEntity } from './code';
import { FlagEntity } from './flag';
import { CompanyEntity } from './companies';
import { ProfileEntity as UserProfileEntity } from './user/profile/profile.entity';
import { ImageEntity as UserImageEntity } from './user/profile/image.entity';
import { UserEntity as UserUserEntity } from './user/user.entity';
import { DevicesEntity } from './device'
import { OptionEntity } from './options/options.entity';
import { CountryEntity } from './countries/countries.entity';
import { CurrencyEntity } from './currencies/currencies.entity';
import { BarcodeEntity } from './barcodes';
import { OptionValueEntity } from './optionValues/optionValues.entity';
const entities = [
  ImageEntity,
  ProfileEntity,
  UserEntity,
  ProductEntity,
  SupplierEntity,
  StockEntity,
  ImagesEntity,
  CategoryEntity,
  LocationEntity,
  VariationEntity,
  CodeEntity,
  FlagEntity,
  CompanyEntity,
  DevicesEntity,
  UserProfileEntity,
  UserImageEntity,
  UserUserEntity,
  OptionEntity,
  CountryEntity,
  CurrencyEntity,
  BarcodeEntity,
  OptionValueEntity
];
@Module({
  imports: [
    MulterModule.register({
      dest: '/images',
    }),
    DatabaseModule.forRootAsync({
      entities: entities
    }),
    TypeOrmModule.forFeature(
      entities,
      DB_CONNECTION_DEFAULT
    ),
    AppBootstrapModule,
  ]
})
export class AppModule {
}
