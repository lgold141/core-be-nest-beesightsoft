import { Test, TestingModule } from '@nestjs/testing';
import { CurrencyService } from './currencies.controller';

describe('Currencies Controller', () => {
  let controller: CurrencyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CurrencyService],
    }).compile();

    controller = module.get<CurrencyService>(CurrencyService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
