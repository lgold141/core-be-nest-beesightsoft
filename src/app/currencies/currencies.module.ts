import { forwardRef, Module } from '@nestjs/common';
import { CurrencyService } from './currencies.service';
import { CurrencyController } from './currencies.controller';
import { CurrencyEntity } from './currencies.entity';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../../../libs/shared/src/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DB_CONNECTION_DEFAULT } from '../database/database.constants';

const PROVIDERS = [
  CurrencyService
];

const ENTITY = [
  CurrencyEntity
]

@Module({
  imports: [
    forwardRef(() => AuthModule),
    SharedModule,
    TypeOrmModule.forFeature(ENTITY, DB_CONNECTION_DEFAULT),
  ],
  controllers: [CurrencyController],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...PROVIDERS
  ]
})
export class CurrencyModule {}
