import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DB_CONNECTION_DEFAULT } from '@app/database';
import { Raw, Repository } from 'typeorm';
import { CurrencyEntity } from './currencies.entity';
import { SearchCurrencyDto } from './dto';

@Injectable()
export class CurrencyService {
  constructor(@InjectRepository(CurrencyEntity, DB_CONNECTION_DEFAULT) private readonly currencyRepository: Repository<CurrencyEntity>){}

  async getAllCurrencies(filter?: SearchCurrencyDto): Promise<CurrencyEntity[]> {
    try {
      if (filter) {
        if (filter.where) {
          const value = filter.where.search || '';
          if(typeof value === 'number'){
            filter.where = { id: value };
          } else {
            const queryName = Raw(alias => `${alias} ILIKE '%${value}%'`);
            filter.where = [
              { currencyName: queryName },
              { currencyCode: queryName },
            ];
          }
        }
      }
      const currencies = await this.currencyRepository.find(filter);
      const result = currencies.map(cur => {
        return {
          ...cur,
          nameCode: `${cur.currencyName} (${cur.currencyCode})`
        }
      });
      return result;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
