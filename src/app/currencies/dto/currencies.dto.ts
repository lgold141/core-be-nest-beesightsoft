import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class SearchCurrencyDto {
    @ApiProperty({
      example: ['id', 'currencyCode', 'currencyName']
    })
    @Expose()
    select: any;
  
    @ApiProperty({
      example: { search: "Malaysian Ringgit" }
    })
    @Expose()
    where: any;
  
    @ApiProperty({
      example: {
        id: 'ASC',
        currencyName: 'DESC'
      }
    })
    @Expose()
    order: any;
  }