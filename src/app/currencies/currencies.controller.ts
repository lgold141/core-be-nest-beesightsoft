import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CurrencyService } from './currencies.service';
import { CurrencyEntity } from './currencies.entity';
import { SearchCurrencyDto } from './dto';

@ApiTags('Currencies')
@Controller()
export class CurrencyController {
  constructor(private readonly currencyService: CurrencyService) {}

  @Post('findAll')
  async getAllCurrencies(@Body() filter: SearchCurrencyDto): Promise<CurrencyEntity[]> {
    try {
      const currencies = await this.currencyService.getAllCurrencies(filter);
      return currencies;
    } catch (e) {
      console.log(e);
      throw(e);
    }
  }
}
