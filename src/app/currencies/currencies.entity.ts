import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Column } from 'typeorm';
import { UserEntity } from '../user/user.entity';

@Entity('currencies')
export abstract class CurrencyEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  currencyCode: string;

  @Column()
  currencyName: string;

  @OneToMany(type => UserEntity, user => user.currency)
  users: UserEntity[];
}
