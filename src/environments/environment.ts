require('dotenv').config();
import { IEnvironment } from './ienvironment';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Algorithm, Secret } from 'jsonwebtoken';

function getDatabaseConfig(): TypeOrmModuleOptions {
  switch (process.env.DB_CONNECTION) {
    case 'postgres':
      const databaseConfig: TypeOrmModuleOptions = {
        type: 'postgres',
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : 5432,
        database: process.env.DB_DATABASE || 'postgres',
        username: process.env.DB_USERNAME || 'postgres',
        password: process.env.DB_PASSWORD || 'root',
        keepConnectionAlive: process.env.TYPEORM_KEEPCONNECTIONALIVE ? JSON.parse(process.env.TYPEORM_KEEPCONNECTIONALIVE) : false,
        logging: process.env.TYPEORM_LOGGING ? JSON.parse(process.env.TYPEORM_LOGGING) : false,
        synchronize: process.env.TYPEORM_SYNCHRONIZE ? JSON.parse(process.env.TYPEORM_SYNCHRONIZE) : false,
        uuidExtension: <"pgcrypto" | "uuid-ossp">process.env.TYPEORM_UUID_EXTENSION
      };
      return databaseConfig;
      break;
  }
}

const databaseConfig = getDatabaseConfig();
console.info(`DB Config: ${JSON.stringify(databaseConfig)}`);

export const environment: IEnvironment = {
  production: process.env.APP_ENV === 'production',
  debug: process.env.APP_DEBUG ? JSON.parse(process.env.APP_DEBUG) : false,
  env: process.env.APP_ENV,

  ALLOW_WHITE_LIST: ['::ffff:127.0.0.1', '::1'],

  userPasswordBcryptSaltRounds: process.env.USER_PASSWORD_BCRYPT_SALT_ROUNDS ? parseInt(process.env.USER_PASSWORD_BCRYPT_SALT_ROUNDS, 10) : 12,

  jwt: {
    secretOrKey: process.env.JWT_SECRET as Secret,
    signOptions: {
      algorithm: process.env.JWT_SIGN_OPTIONS_ALGORITHM as Algorithm,
      expiresIn: process.env.JWT_SIGN_OPTIONS_EXPIRESIN as string | number
    },
  },

  server: {
    host: process.env.APP_URL,
    domainUrl: process.env.APP_URL + ':' + process.env.APP_PORT,
    port: process.env.APP_PORT || 3000,
    globalPrefix: process.env.APP_URL_PREFIX,
  },

  databases: [
    databaseConfig
  ],

  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD
  },

  auth: {
    clientId: process.env.OIDC_CLIENT_ID || 'ngxapi',
    issuerExternalUrl: process.env.OIDC_ISSUER_EXTERNAL_URL || 'https://keycloak.traefik.k8s/auth/realms/ngx',
    issuerInternalUrl: process.env.OIDC_ISSUER_INTERNAL_URL || 'http://keycloak-headless:8080/auth/realms/ngx',
  },

  email: {
    transport: {
      host: process.env.MAIL_HOST || 'mail.google.com',
      port: process.env.MAIL_PORT ? Number(process.env.MAIL_PORT) : 25,
      secure: process.env.EMAIL_SECURE ? JSON.parse(process.env.MAIL_SECURE) : false,
      auth: {
        user: process.env.MAIL_USERNAME || 'auth_user',
        pass: process.env.MAIL_PASSWORD || 'auth_pass',
      },
    },
    defaults: {
      from: process.env.MAIL_FROM_ADDRESS ? process.env.MAIL_FROM_ADDRESS : '"sumo demo" <sumo@demo.com>',
    },
    templateDir: process.env.MAIL_TEMPLATE_DIR || `${__dirname}/assets/email-templates`,
  },


  weather: {
    baseUrl: 'https://samples.openweathermap.org/data/2.5',
    apiKey: 'b6907d289e10d714a6e88b30761fae22',
  },

  // Key generation: https://web-push-codelab.glitch.me
  webPush: {
    subject: process.env.VAPID_SUBJECT || 'mailto: sumo@demo.com',
    publicKey:
      process.env.VAPID_PUBLIC_KEY ||
      'BAJq-yHlSNjUqKW9iMY0hG96X9WdVwetUFDa5rQIGRPqOHKAL_fkKUe_gUTAKnn9IPAltqmlNO2OkJrjdQ_MXNg',
    privateKey: process.env.VAPID_PRIVATE_KEY || 'cwh2CYK5h_B_Gobnv8Ym9x61B3qFE2nTeb9BeiZbtMI',
  },

  swagger: {
    enable: process.env.SWAGGER_ENABLED ? JSON.parse(process.env.SWAGGER_ENABLED) : false,
    title: 'Headless CMS API Docs',
    description: 'Headless CMS API Docs',
    welcomeText: 'Welcome to Headless API',
    path: process.env.SWAGGER_PATH
  },

  images: {
    path: process.env.IMAGE_PATH,
    api: process.env.IMAGE_API
  },

  barcode: process.env.BARCODE,

  twilio: {
    number: process.env.TWILIO_NUMBER,
    sid: process.env.TWILIO_SID,
    auth: process.env.TWILIO_AUTH,
    content: 'Your confirm number is '
  },

  productTemplate: {
    path: process.env.PRODUCT_TEMPLATES_PATH
  },

  productExport: {
    path: process.env.UPLOAD_FILE_PATH
  },

  code: {
    expireTime: eval(process.env.EXPIRE_TIME)
  }
};

export default environment;
