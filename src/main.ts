import {NestFactory} from '@nestjs/core';
import {AppModule} from './app/app.module';
import {bootstrap} from './boostrap';

async function start() {
  const app = await NestFactory.create(AppModule,
    {
      cors: true,
      logger: ['log', 'error', 'warn']
    });
  await bootstrap(app);
}

start();
