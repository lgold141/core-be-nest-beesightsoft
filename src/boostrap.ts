import {ConfigService} from '@nest-core/config';
// import {PinoAppLoggerService} from '@nest-core/shared';
// import {useContainer} from 'class-validator';
import * as helmet from 'helmet';
import {setupSwagger} from './swagger';

export async function bootstrap(app) {
  const config: ConfigService = app.get(ConfigService);
  app.use(helmet());
  // app.useLogger(app.get(PinoAppLoggerService));

  // Link DI container to class-validator
  // useContainer(app.select(AppModule), {fallbackOnErrors: true});

  if (config.get('swagger.enable')) {
    setupSwagger(app);
  }

  // Starts listening to shutdown hooks
  app.enableShutdownHooks();

  const globalPrefix = config.get('server.globalPrefix');
  const port = config.get('server.port');
  await app.listen(port, () => {
    console.log(
      'Listening at http://localhost:' + port + '/' + globalPrefix
    );
  });
}
