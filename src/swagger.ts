import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {INestApplication} from '@nestjs/common';
import {environment} from '@app/environment';
import {ConfigService} from '@nest-core/config';

export function setupSwagger(app: INestApplication) {
  const config: ConfigService = app.get(ConfigService);
  const options = new DocumentBuilder()
    .setTitle(environment.swagger.title)
    .setDescription(environment.swagger.description)
    .setVersion(config.getVersion())
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(environment.swagger.path, app, document);
}
